#include "drv_can.h"

SCan sCan[CAN_MAX_ID];
CanRxMsg RxMessage0;
CanTxMsg TxMessage;


void Can_Init()
{
  	GPIO_InitTypeDef 		GPIO_InitStructure;
  	NVIC_InitTypeDef  		NVIC_InitStructure;
  	CAN_InitTypeDef        	CAN_InitStructure;

  	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  	GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  	/* Configure CAN pin: TX */
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  	GPIO_Init(GPIOA, &GPIO_InitStructure); 

  	CAN_DeInit(CAN1);
  	CAN_StructInit(&CAN_InitStructure);

  	/* CAN cell init */
  	CAN_InitStructure.CAN_TTCM = DISABLE;
  	CAN_InitStructure.CAN_ABOM = DISABLE;
  	CAN_InitStructure.CAN_AWUM = DISABLE;
  	CAN_InitStructure.CAN_NART = DISABLE;
  	CAN_InitStructure.CAN_RFLM = DISABLE;
  	CAN_InitStructure.CAN_TXFP = ENABLE;
  	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	
  	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
  	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;
  	CAN_InitStructure.CAN_BS2 = CAN_BS2_3tq;
  	CAN_InitStructure.CAN_Prescaler = 45;
	
  	CAN_Init(CAN1, &CAN_InitStructure);

  	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  	NVIC_Init(&NVIC_InitStructure);
  
  	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
}

/*
filterMode = CAN_FilterMode_IdMask, CAN_FilterMode_IdList
FiltNum = 0~13
*/
void Can_FilterConfig(uint8_t filterMode, uint8_t FiltNum, uint16_t IDHigh, uint16_t IDLow, uint16_t MaskHigh, uint16_t MaskLow)
{
	CAN_FilterInitTypeDef  	CAN_FilterInitStructure;
  	CAN_FilterInitStructure.CAN_FilterNumber = FiltNum;
  	CAN_FilterInitStructure.CAN_FilterMode = filterMode;
  	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
  	CAN_FilterInitStructure.CAN_FilterIdHigh = IDHigh << 5;
  	CAN_FilterInitStructure.CAN_FilterIdLow = IDLow<<5;
  	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = MaskHigh;
  	CAN_FilterInitStructure.CAN_FilterMaskIdLow = MaskLow;
  	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
  	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  	CAN_FilterInit(&CAN_FilterInitStructure);
}

/*
index = 0~CAN_MAX_ID 구조체 순번
id = 각 구조체의 CanID
func = 데이터 수신 시 실행될 함수
*/
void Can_SetID(uint8_t index, uint16_t id, void (*func)(void))
{
	sCan[index].CanID = id;
	sCan[index].CanFlag = 0;
	
	if ( func )
	{
		sCan[index].m_fp = func;
	}
}

void Can_TxMessage(uint16_t id, uint8_t length, uint8_t* data)
{
	uint8_t i;
	for ( i = 0 ; i < length ; i ++ )
		TxMessage.Data[i] = data[i];
	
	TxMessage.DLC = length;
	TxMessage.RTR = CAN_RTR_DATA;
  	TxMessage.IDE = CAN_ID_STD;
	TxMessage.StdId = id;

	CAN_Transmit(CAN1, &TxMessage);
}

void USB_LP_CAN1_RX0_IRQHandler(void)
{
	uint8_t id;
	
  	CAN_Receive(CAN1, CAN_FIFO0, &RxMessage0);
  	CAN_ClearITPendingBit(CAN1, CAN_IT_TME);
	
	for ( id = 0 ; id < CAN_MAX_ID ; id ++ )
	{
		if ( sCan[id].CanID == RxMessage0.StdId)
		{
			sCan[id].CanRxMessage = RxMessage0;
			sCan[id].CanFlag = 1;
			if ( sCan[id].m_fp )
			{
				sCan[id].m_fp();
			}
		}
	}
}