#ifndef WIND_SENSOR_H
#define WIND_SENSOR_H

#include "extern.h"

typedef struct UPDATE_HOT_WIRE   
{
  float Wind_Speed;
  float Wind_vout;
  float temperature;
}UPDATE_HOT_WIRE;

class WIND_SENSOR
{
  private:
    
  public:
    
    //float sensorValue[2];
    //float filterValue[2];
    WIND_SENSOR();
    virtual ~WIND_SENSOR();
    uint16_t adcRead(uint8_t adc);
    uint16_t getWindValue(void);
    //void io_action();
    //void io_print(void);
};

#endif /* WIND_SENSOR_H */
