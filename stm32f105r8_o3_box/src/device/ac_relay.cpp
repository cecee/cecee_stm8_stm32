#include "ac_relay.h"

RELAY_BIT relay[4];
LED_BIT led;

AC_RELAY::AC_RELAY() {
  printf("### Open AC_RELAY ###\r\n");
  memset(&relay,0,sizeof(relay));
  memset(&led,0xFF,sizeof(led));
}

AC_RELAY::~AC_RELAY() {
}


void AC_RELAY::test() {
      printf("AC_RELAY testing\r\n");
	//set_gain(gain);
}

void AC_RELAY::io_action(void) {
  
  uint8_t i;
  printf("#### AC_RELAY io_action\r\n");
  for(i=0;i<4;i++){
      switch(i){
      case 0:
        if(relay[i].enable)  HAL_GPIO_WritePin(pRELAY1_GPIO_Port, pRELAY1_Pin, GPIO_PIN_SET); 
        else HAL_GPIO_WritePin(pRELAY1_GPIO_Port, pRELAY1_Pin, GPIO_PIN_RESET); 
        break;
      case 1:
        if(relay[i].enable)  HAL_GPIO_WritePin(pRELAY2_GPIO_Port, pRELAY2_Pin, GPIO_PIN_SET); 
        else HAL_GPIO_WritePin(pRELAY2_GPIO_Port, pRELAY2_Pin, GPIO_PIN_RESET); 
        break;      
      case 2:
        if(relay[i].enable)  HAL_GPIO_WritePin(pRELAY3_GPIO_Port, pRELAY3_Pin, GPIO_PIN_SET); 
        else HAL_GPIO_WritePin(pRELAY3_GPIO_Port, pRELAY3_Pin, GPIO_PIN_RESET); 
        break;
      case 3:
        if(relay[i].enable)  HAL_GPIO_WritePin(pRELAY4_GPIO_Port, pRELAY4_Pin, GPIO_PIN_SET); 
        else HAL_GPIO_WritePin(pRELAY4_GPIO_Port, pRELAY4_Pin, GPIO_PIN_RESET); 
        break;       
      }
  } 
  //HAL_Delay(20);
  relay[0].detect=(HAL_GPIO_ReadPin(pAC_DET1_GPIO_Port, pAC_DET1_Pin)==0)? 0:1;
  relay[1].detect=(HAL_GPIO_ReadPin(pAC_DET2_GPIO_Port, pAC_DET2_Pin)==0)? 0:1;
  relay[2].detect=(HAL_GPIO_ReadPin(pAC_DET3_GPIO_Port, pAC_DET3_Pin)==0)? 0:1;
  relay[3].detect=(HAL_GPIO_ReadPin(pAC_DET4_GPIO_Port, pAC_DET4_Pin)==0)? 0:1; 
  relay[0].alarm=(HAL_GPIO_ReadPin(FAN_ALARM_GPIO_Port, FAN_ALARM_Pin)==0)? 0:1;

  if(led.fan) HAL_GPIO_WritePin(pLED1_GPIO_Port, pLED1_Pin, GPIO_PIN_RESET); 
  else  HAL_GPIO_WritePin(pLED1_GPIO_Port, pLED1_Pin, GPIO_PIN_SET); 
  if(led.o3) HAL_GPIO_WritePin(pLED2_GPIO_Port, pLED2_Pin, GPIO_PIN_RESET); 
  else  HAL_GPIO_WritePin(pLED2_GPIO_Port, pLED2_Pin, GPIO_PIN_SET); 
  if(led.uv) HAL_GPIO_WritePin(pLED3_GPIO_Port, pLED3_Pin, GPIO_PIN_RESET); 
  else  HAL_GPIO_WritePin(pLED3_GPIO_Port, pLED3_Pin, GPIO_PIN_SET);  
  //if(led.uv) HAL_GPIO_WritePin(pLED4_GPIO_Port, pLED4_Pin, GPIO_PIN_RESET); 
  //else  HAL_GPIO_WritePin(pLED4_GPIO_Port, pLED4_Pin, GPIO_PIN_SET);

}

void AC_RELAY::io_print(void) {
  printf("===========================\r\n");  
  printf("AC CTRL[%x][%x][%x][%x]\r\n",relay[0].enable, relay[1].enable, relay[2].enable, relay[3].enable);
  printf("AC_DET [%x][%x][%x][%x]\r\n",relay[0].detect, relay[1].detect, relay[2].detect, relay[3].detect);
  printf("LED    [%x][%x][%x]\r\n",led.fan, led.o3, led.uv);
  printf("FAN_ALARM[%x]\r\n",relay[0].alarm);

}
