#ifndef QUEUE_H
#define QUEUE_H
#include "extern.h"

#define MAX_SIZE 256 // max size of queue
typedef struct _QUEUE   
{
	int queue[MAX_SIZE+1];
	int front;
	int rear;
}QUEUE;

class QUEUE_CLASS
{
    private:
    public:
      uint8_t qUart1_package=0;
      uint8_t qUart1_buffer[256];
      uint8_t qUart1_cnt=0;
      uint8_t qUart1_sz=0xff;
      uint8_t rx_buffer[128];
      uint8_t idx=0;
      uint8_t rxSz=0;
      uint8_t package_flag=0;
      uint8_t UART_FLAG = 0;    
    
      QUEUE_CLASS();
      virtual ~QUEUE_CLASS();

      void init_qUart1(void);

      int put_qUart1(int k);
      int get_qUart1(void);
      int processSerialBuf(void);
      void recvRX(uint8_t data);
      void transmit_up(void);
      void transmit_prn(void);

};

#endif 
