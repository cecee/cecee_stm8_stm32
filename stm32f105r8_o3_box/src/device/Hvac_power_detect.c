#include "stm32f1xx_hal.h"
#include "Hvac_power_detect.h"
#include "extern.h"

#define RESPONSE_SIZE 32
#define READ_TIMEOUT 100

extern UART_HandleTypeDef huart2;

 uint8_t pmonitor_buf[32];
 uint8_t pmonitor_sbuf[32];
 uint8_t pmonitor_packet=0;
 uint8_t pmonitor_cnt=0;
 uint16_t pmonitor_packetSz=7;
 
 void GetACMonitor(uint8_t *data);
 
  
 /*
 uint16_t CRC16(const uint8_t *data, uint16_t len)
{
    uint8_t nTemp; // CRC table index
    uint16_t crc = 0xFFFF; // Default value

    while (len--)
    {
        nTemp = *data++ ^ crc;
        crc >>= 8;
        //crc ^= (uint16_t)pgm_read_word(&crcTable[nTemp]);
        crc=crc^crcTable[nTemp];
    }
    return crc;
}

 
 void  setCRC(uint8_t *buf, uint16_t len){
    if(len <= 2) // Sanity check
        return;

    uint16_t crc = CRC16(buf, len - 2); // CRC of data

    // Write high and low byte to last two positions
    buf[len - 2] = crc & 0xFF; // Low byte first
    buf[len - 1] = (crc >> 8) & 0xFF; // High byte second
}

 */
 
 void  HVAC_PutAcmData(uint8_t d)
{
     if(pmonitor_packet==0){
         if(d==0xa0 || d==0xa1 || d==0xa2 || d==0xa3 || d==0x01)
         {
         	if(d==0x01) pmonitor_packetSz = 25;
		 pmonitor_packet=1;
          pmonitor_cnt=0;
 //         memset(pmonitor_sbuf,0,sizeof(pmonitor_sbuf));
          }
     }
     if(pmonitor_packet==1){
         pmonitor_sbuf[pmonitor_cnt++]=d;
     }
     
   	if(pmonitor_packet && (pmonitor_cnt>=pmonitor_packetSz)){
         pmonitor_packet=0;
         memset(pmonitor_buf, 0, sizeof(pmonitor_buf));
        // memcpy(pmonitor_buf, pmonitor_sbuf, 8);
	    memcpy(pmonitor_buf, pmonitor_sbuf, pmonitor_packetSz);
        // for(int i=0;i<8;i++) printf("pmonitor_buf[%d][%02X]\r\n",i, pmonitor_buf[i]);
         //memset(pmonitor_sbuf, 0, 8);
	    if(pmonitor_packetSz==25) GetACMonitor2(pmonitor_buf);
         else GetACMonitor(pmonitor_buf);
	    
	}
}

void GetACMonitor(uint8_t *data)
{
    int i;
    uint16_t sum=0;
    uint16_t ivalue;
    
	for(i=0;i<6;i++) sum=sum+data[i];
	sum=sum&0xff;
	if(sum !=data[6]) return;
	switch(data[0])
	{
		case 0xA0://Get voltage
			ivalue=BUILD_UINT16(data[2], data[1]);
                        //gStatus.pwr.v = ivalue*10+data[3];
			//printf("main_V[%d]\r\n",gStatus.pwr.v);
			break;	
		case 0xA1://Get current
  			//gStatus.pwr.i=(data[2]*10)+ data[3]/10;
              // printf("main_a[%d]\r\n",gStatus.pwr.i);
			break;			
		case 0xA2://Get Power
               ivalue=BUILD_UINT16(data[2], data[1]);
			//gStatus.pwr.p=ivalue;
              // printf("main_p[%d]\r\n",gStatus.pwr.p);
			break;					
	}
	
}

void GetACMonitor2(uint8_t *data)
{
    uint16_t crc=0;
    uint32_t value;
    uint16_t freq=0;

    crc = gen_crc16(data,23);

    if((crc&0xff)!=data[23] || ((crc>>8)&0xff)!=data[24]) return;

	//gStatus.pwr.v =BUILD_UINT16(data[4], data[3]);
	value=0;
	value=(data[7]<<24) | (data[8]<<16)| (data[5]<<8)| data[6];
	//gStatus.pwr.i= value/100;
	//WATT
	//value=0;
	//value=(data[11]<<24) | (data[12]<<16)| (data[9]<<8)| data[10];
	//value= (value/1000)&0xffff;
	//gStatus.pwr.p= value;
	
	//WATT-HOUR
	value=0;
	value=(data[15]<<24) | (data[16]<<16)| (data[13]<<8)| data[14];
	value= (value/1000)&0xffff;
	//gStatus.pwr.p= value;
	//Frequency
	value=0;
	freq= (data[17]<<8)| data[18];
#if 0	
	printf("gen_crc16[%x]\r\n",crc);	
	printf("main_V[%d]\r\n",gStatus.pwr.v);
	printf("main_A[%d]\r\n",gStatus.pwr.i);
	printf("main_P[%d]WH\r\n",gStatus.pwr.p);
	printf("main_Fq[%d]\r\n",freq);
#endif	

}

void ac_monitor_rx()
{
    uint8_t rx_buf[128]={0,};
    uint16_t crc;
    uint32_t current, power, energy;
    uint16_t frequency;
    uint16_t factor;
    uint16_t alarm;
    
    unsigned long startTime = HAL_GetTick(); // Start time for Timeout
    while((HAL_GetTick() - startTime < READ_TIMEOUT))
    {
        HAL_UART_Receive(&huart2, rx_buf,  64, 100);
        if(rx_buf[0]) break;
    }
    crc = gen_crc16(rx_buf,23);
    if((crc&0xff)!=rx_buf[23] || ((crc>>8)&0xff)!=rx_buf[24]) return;
     //printf("crc-> [%x] [%x][%x]\r\n",crc, rx_buf[23], rx_buf[24]);
    updata.power.v= BUILD_UINT16(rx_buf[4], rx_buf[3]);
    current=((rx_buf[7]<<24) | (rx_buf[8]<<16)| (rx_buf[5]<<8)| rx_buf[6]);
    power=((rx_buf[11]<<24) | (rx_buf[12]<<16)| (rx_buf[9]<<8)| rx_buf[10]);
    energy=((rx_buf[15]<<24) | (rx_buf[16]<<16)| (rx_buf[13]<<8)| rx_buf[14]);
    
    frequency=BUILD_UINT16(rx_buf[18], rx_buf[17]);
    factor=BUILD_UINT16(rx_buf[20], rx_buf[19]);
    alarm=BUILD_UINT16(rx_buf[22], rx_buf[21]);
    
    updata.power.a=current/100;
    updata.power.p=power/100;
    updata.power.f=factor;
    
    //updata.power.f=((rx_buf[19]<<24) | (rx_buf[20]<<16)| (rx_buf[17]<<8)| rx_buf[18])/100;
    printf("updata.power.v[%d][%d][%d][%d][%d]\r\n",updata.power.v, updata.power.a, updata.power.p, updata.power.f, frequency);    
 }	


void ac_monitor_tx( )
{
	uint8_t Rev2_buf[8]={0x01, 0x04, 0x00, 0x00, 0x00, 0x0A, 0x70, 0x0D} ;
	uint8_t size=8;
        HAL_UART_Transmit(&huart2, Rev2_buf, size, 100);// 
        ac_monitor_rx();
}	


uint16_t gen_crc16(const uint8_t *data, uint16_t size)
{
static const uint16_t wCRCTable[] = {
0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };

uint8_t nTemp;
uint16_t wCRCWord = 0xFFFF;

   while (size--)
   {
      nTemp = *data++ ^ wCRCWord;
      wCRCWord >>= 8;
      wCRCWord ^= wCRCTable[nTemp];
   }
   return wCRCWord;

}