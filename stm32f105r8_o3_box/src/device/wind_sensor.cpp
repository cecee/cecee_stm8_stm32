#include "wind_sensor.h"
#include "adc.h"
#include "math.h"

float sensorValue[2];
float filterValue[2];
float sensitivity=0.1;
UPDATE_HOT_WIRE update_hotwire;

WIND_SENSOR::WIND_SENSOR() {
  printf("### Open WIND_SENSOR ###\r\n");
  memset(&update_hotwire,0,sizeof(update_hotwire));
  sensorValue[0]=(float)adcRead(0);
  sensorValue[1]=(float)adcRead(1);
  filterValue[0]=sensorValue[0];
  filterValue[1]=sensorValue[1];
  //memset(&sensorValue,0,sizeof(sensorValue));
  //memset(&led,0xFF,sizeof(led));
}

WIND_SENSOR::~WIND_SENSOR() {
}


uint16_t WIND_SENSOR::adcRead(uint8_t adc)
{
  uint16_t value;
  ADC_ChannelConfTypeDef sConfig;
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Regular Channel */
  if(adc==0)sConfig.Channel = ADC_CHANNEL_0;
  else sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  ////sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  /*if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }*/
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  } 
   
  HAL_ADC_Start (&hadc1); 
  HAL_ADC_PollForConversion (&hadc1, HAL_MAX_DELAY); 
  value = HAL_ADC_GetValue (&hadc1); //XL
  //printf("---adcRead [%d]---\r\n",value);
  return value;
}

uint16_t WIND_SENSOR::getWindValue()
{
  uint16_t wValue=0;
  const float zeroWindAdjustment = 0.2; // negative numbers yield smaller wind speeds and vice versa.
  int TMP_Therm_ADunits;  //temp termistor value from wind sensor
  float RV_Wind_ADunits;    //RV output from wind sensor 
  float TMP_Wind_Volts, RV_Wind_Volts;
  unsigned long lastMillis;
  int TempCtimes100;
  float zeroWind_ADunits;
  float zeroWind_volts;
  float WindSpeed_MPH;
  float Itemp, Rtemp;

  float ParamA = 0.001129148;
  float ParamB = 0.000234125;
  float ParamC = 0.0000000876741;
  
  float Temp, tempC;
  //Temp = log (10000.0 * ((1024.0 / RawADC - 1)));
  

  sensorValue[0]=(float)adcRead(0); 
  sensorValue[1]=(float)adcRead(1); 
  filterValue[0]=(filterValue[0]*(1-sensitivity))+(sensorValue[0]*sensitivity);
  filterValue[1]=(filterValue[1]*(1-sensitivity))+(sensorValue[1]*sensitivity);
  
  TMP_Therm_ADunits = filterValue[0];//analogRead(analogPinForTMP);
  RV_Wind_ADunits = filterValue[1];//analogRead(analogPinForRV);
  
  RV_Wind_Volts = (RV_Wind_ADunits*3.3)/4096;//(RV_Wind_ADunits *  0.0048828125);
  TMP_Wind_Volts= (TMP_Therm_ADunits*3.3)/4096;//(TMP_Therm_ADunits *  0.0048828125);
  
  //RV_Wind_Volts=(RV_Wind_ADunits*3.3)/4096;
  //4096->3.3V
  //TMP_Wind_Volts(실제전압 )= (TMP_Therm_ADunits*3.3)/4096
  Itemp=(5-TMP_Wind_Volts)/10000; //5Vcc 10k Resistor
  Rtemp=TMP_Wind_Volts/Itemp;
  
  //첫 번째 < Steinhart-Hart 식 >
  
  Temp = log(Rtemp);
  Temp = 1 / (ParamA + ( ParamB + (ParamC * Temp * Temp )) * Temp );
  tempC = Temp - 273.15;     
  
  //두 번째 < B 또는 β 파라미터 식 >
  float ParmB=3435.0;
  tempC =(ParmB/(log(Rtemp/10000)+(ParmB/(273.15+25)))) -273.15;
  
  // these are all derived from regressions from raw data as such they depend on a lot of experimental factors
  // such as accuracy of temp sensors, and voltage at the actual wind sensor, (wire losses) which were unaccouted for.
  //TempCtimes100 = (0.005 *((float)TMP_Therm_ADunits * (float)TMP_Therm_ADunits)) - (16.862 * (float)TMP_Therm_ADunits) + 9075.4;  
  zeroWind_ADunits = -0.0006*((float)TMP_Therm_ADunits * (float)TMP_Therm_ADunits) + 1.0727 * (float)TMP_Therm_ADunits + 47.172;  //  13.0C  553  482.39
  
  //바람이 없을때 현재 론도에 대한 전압값
 // zeroWind_volts = (zeroWind_ADunits * 0.0048828125) - zeroWindAdjustment;  
  zeroWind_volts = ((zeroWind_ADunits*3.3)/4096) - zeroWindAdjustment;  
  
  // This from a regression from data in the form of 
  // Vraw = V0 + b * WindSpeed ^ c
  // V0 is zero wind at a particular temperature
  // The constants b and c were determined by some Excel wrangling with the solver.
   //WindSpeed_MPH =  pow(((RV_Wind_Volts - zeroWind_volts) /.2300) , 2.7265));  
  WindSpeed_MPH =  pow(((RV_Wind_Volts - zeroWind_volts) /.2300) , 2.7265);   
  // printf("  TMP volts [%f]\r\n",TMP_Therm_ADunits * 0.0048828125);
  // printf(" RV volts [%f]\r\n",(float)RV_Wind_Volts);
  // printf("TempC*100 [%f]\r\n", TempCtimes100);
  // printf("ZeroWind volts [%f]", zeroWind_volts);
  
  // printf("WindSpeed MPH [%.2f] TempCtimes100[%.2f]\r\n", (float)WindSpeed_MPH/1000L, TempCtimes100 );
  //this->update_hotwire.temperature=tempC;
  update_hotwire.temperature=tempC;
  update_hotwire.Wind_Speed=WindSpeed_MPH;
  
  printf("RV/TM_Volts[%.2f/%.2f]V [%.2f]ohm [%.2f]C MPH[%.2f]\r\n",RV_Wind_Volts, TMP_Wind_Volts, Rtemp, tempC, WindSpeed_MPH);

  return wValue;
}

