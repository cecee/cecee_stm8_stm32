/*
 * cbuffer.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "queue.h"
#include "extern.h"
#include <stdio.h>
   
extern UART_HandleTypeDef huart1;
extern AC_RELAY *pACrelay;
/////////////////////////////////////////////
///Queue
////////////////////////////////////////////
UP_DATA updata;
QUEUE qUart1;

QUEUE_CLASS::QUEUE_CLASS(){
}

QUEUE_CLASS::~QUEUE_CLASS(){
}
  
void QUEUE_CLASS::init_qUart1(void){
  HAL_GPIO_WritePin(U1TXEN_GPIO_Port, U1TXEN_Pin, GPIO_PIN_RESET); 
  qUart1.front=0;
  qUart1.rear=0;
  memset(&updata,0,sizeof(updata));
  printf("QUEUE_CLASS UP_DATA[%d]\r\n",sizeof(UP_DATA));
}  

int QUEUE_CLASS::put_qUart1(int k){
    //printf("Queue put..........k[%x]\r\n",k);
    
    if ((qUart1.rear + 1) % MAX_SIZE == qUart1.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart1.rear, qUart1.front);
        return -1;
    }

    qUart1.queue[ qUart1.rear] = k;
    qUart1.rear = (qUart1.rear+1) % MAX_SIZE;
    return k;
}


int QUEUE_CLASS::get_qUart1(void)
{
    int i=0;
     if (qUart1.front == qUart1.rear){
        return -1;
    }
    i = qUart1.queue[qUart1.front];
   qUart1.front = (qUart1.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}


int QUEUE_CLASS::processSerialBuf(void)
{
    uint8_t i,pSz;
    REQUEST req;
    uint8_t sum;
    int d;    
        //printf("processSerialBuf \r\n");
	for(;;)
	{
          d =get_qUart1();
          if(d==-1 )break;
          d=d&0xff;
          if((qUart1_package==0) && (d==0xFA)) 
          {
             qUart1_package=1;
             qUart1_cnt=0;
             qUart1_sz=0xff;
             memset(qUart1_buffer,0,sizeof(qUart1_buffer));
          }
   	  if(qUart1_package){
              qUart1_buffer[qUart1_cnt++]=d;
              if(d=='@' && qUart1_cnt==qUart1_buffer[1])               
              {
                  qUart1_package=0;
                  //printf("rx 485  \r\n");
                  pSz=qUart1_cnt;qUart1_cnt=0;
                  if(pSz==8){
                      memcpy(&req, qUart1_buffer, pSz);
                      printf("req.id[%x] qUart1_cnt[%d]\r\n",req.id, qUart1_cnt);
                      sum=0;
                      for(i=0;i<pSz-2;i++)sum=sum+qUart1_buffer[i];
                      if(req.id==MYID && req.sum==sum){
                        printf("REQ id[%x] data[%x] sum[%x][%x]\r\n",req.id,  req.data, req.sum, sum); 
                        relay[0].enable= (_CheckBit(req.data, 0)!=0) ? 1: 0;
                        relay[1].enable= (_CheckBit(req.data, 1)!=0) ? 1: 0;
                        relay[2].enable= (_CheckBit(req.data, 2)!=0) ? 1: 0;
                        relay[3].enable= (_CheckBit(req.data, 3)!=0) ? 1: 0;
                        led.fan=relay[0].enable;
                        led.o3=relay[1].enable;
                        led.uv=relay[2].enable;
                        pACrelay->io_action();
                        transmit_up();
                      }
                  }
               }
               else if(qUart1_cnt>12) qUart1_package=0;
          }
    }         
    return 1;
}

void QUEUE_CLASS::transmit_up(void){
  
  uint8_t i;
  uint8_t buf[32];
  uint8_t sum=0;  
  uint8_t size=sizeof(updata);

  updata.stx=0xFA;
  updata.size=size;
  updata.id=SWAPBYTE_US(MYID);
  memcpy(&updata.relay,&relay,4);
  updata.sum=0;
  updata.etx='@';  
  
  memcpy(buf,&updata,sizeof(updata));
  sum=0;
  for(i=0;i<size-1;i++)  sum=sum+buf[i];
  buf[size-2]=sum;
  __HAL_IWDG_RELOAD_COUNTER(&hiwdg);
  HAL_GPIO_WritePin(U1TXEN_GPIO_Port, U1TXEN_Pin, GPIO_PIN_SET); 
  HAL_UART_Transmit(&huart1, buf, size, 100);
  HAL_GPIO_WritePin(U1TXEN_GPIO_Port, U1TXEN_Pin, GPIO_PIN_RESET); 
  
}

void QUEUE_CLASS::transmit_prn(void) {
  printf("===========================\r\n");
  printf("stx[%x] size[%d] id[%04x] \r\n",updata.stx, updata.size, updata.id); 
  printf("v[%04x] A[%04x] P[%04x] \r\n",updata.power.v, updata.power.a, updata.power.p);
  printf("RY1 E[%02x] D[%02x] A[%02x] \r\n",updata.relay[0].enable, updata.relay[0].detect, updata.relay[0].alarm);
  printf("RY2 E[%02x] D[%02x] A[%02x] \r\n",updata.relay[1].enable, updata.relay[1].detect, updata.relay[1].alarm);
  printf("RY3 E[%02x] D[%02x] A[%02x] \r\n",updata.relay[2].enable, updata.relay[2].detect, updata.relay[2].alarm);
  printf("RY4 E[%02x] D[%02x] A[%02x] \r\n",updata.relay[3].enable, updata.relay[3].detect, updata.relay[3].alarm);
  //printf("LED FAN[%x] O3[%x] UV[%x]\r\n",updata.led.fan, updata.led.o3, updata.led.uv);
  printf("SUM[%x] ETX[%x] \r\n",updata.sum, updata.etx);
  
}
