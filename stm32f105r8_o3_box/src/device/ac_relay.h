#ifndef AC_RELAY_H
#define AC_RELAY_H

#include "extern.h"

class AC_RELAY
{
  private:
  public:
    AC_RELAY();
    virtual ~AC_RELAY();
    void test(void);
    void io_action();
    void io_print(void);
};

#endif /* AC_RELAY_H */
