#ifndef HVAC_POWER_DETECT_H_
#define HVAC_POWER_DETECT_H_
#define CRC16 0x8005
//#define CRC16 0xA001
int  GetSerialBuf_power_detect(void);
void HVAC_PutAcmData(uint8_t d);
void ac_monitor_tx(void);
void ac_monitor_rx(void);
void GetACMonitor(uint8_t *data);
void GetACMonitor2(uint8_t *data);
uint16_t gen_crc16(const uint8_t *data, uint16_t size);
#endif
