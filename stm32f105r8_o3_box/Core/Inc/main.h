/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define pDIP0_Pin GPIO_PIN_4
#define pDIP0_GPIO_Port GPIOA
#define pDIP1_Pin GPIO_PIN_5
#define pDIP1_GPIO_Port GPIOA
#define pDIP2_Pin GPIO_PIN_6
#define pDIP2_GPIO_Port GPIOA
#define pDIP3_Pin GPIO_PIN_7
#define pDIP3_GPIO_Port GPIOA
#define pRELAY1_Pin GPIO_PIN_0
#define pRELAY1_GPIO_Port GPIOB
#define pRELAY2_Pin GPIO_PIN_1
#define pRELAY2_GPIO_Port GPIOB
#define pRELAY3_Pin GPIO_PIN_2
#define pRELAY3_GPIO_Port GPIOB
#define pRELAY4_Pin GPIO_PIN_10
#define pRELAY4_GPIO_Port GPIOB
#define pLED1_Pin GPIO_PIN_11
#define pLED1_GPIO_Port GPIOB
#define pLED2_Pin GPIO_PIN_12
#define pLED2_GPIO_Port GPIOB
#define pLED3_Pin GPIO_PIN_13
#define pLED3_GPIO_Port GPIOB
#define FAN_ALARM_Pin GPIO_PIN_9
#define FAN_ALARM_GPIO_Port GPIOC
#define U1TXEN_Pin GPIO_PIN_8
#define U1TXEN_GPIO_Port GPIOA
#define pAC_DET1_Pin GPIO_PIN_4
#define pAC_DET1_GPIO_Port GPIOB
#define pAC_DET2_Pin GPIO_PIN_5
#define pAC_DET2_GPIO_Port GPIOB
#define pAC_DET3_Pin GPIO_PIN_6
#define pAC_DET3_GPIO_Port GPIOB
#define pAC_DET4_Pin GPIO_PIN_7
#define pAC_DET4_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
