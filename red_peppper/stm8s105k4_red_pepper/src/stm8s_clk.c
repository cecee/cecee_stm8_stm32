/**
  ******************************************************************************
  * @file stm8s_clk.c
  * @brief This file contains all the functions for the CLK peripheral.
  * @author STMicroelectronics - MCD Application Team
  * @version V1.1.1
  * @date 06/05/2009
  ******************************************************************************
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  * @image html logo.bmp
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "stm8s_clk.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* Private Constants ---------------------------------------------------------*/

/**
  * @addtogroup CLK_Private_Constants
  * @{
  */

uc8 HSIDivFactor[4] = {1, 2, 4, 8}; /*!< Holds the different HSI Dividor factors */
uc8 CLKPrescTable[8] = {1, 2, 4, 8, 10, 16, 20, 40}; /*!< Holds the different CLK prescaler values */

/**
  * @}
  */

/* Public functions ----------------------------------------------------------*/
/**
  * @addtogroup CLK_Public_Functions
  * @{
  */

/**
  * @brief Deinitializes the CLK peripheral registers to their default reset
  * values.
  * @par Parameters:
  * None
  * @retval None
  * @par Warning:
  * Resetting the CCOR register: \n
  * When the CCOEN bit is set, the reset of the CCOR register require
  * two consecutive write instructions in order to reset first the CCOEN bit
  * and the second one is to reset the CCOSEL bits.
  */
void CLK_DeInit(void)
{

    CLK->ICKR = CLK_ICKR_RESET_VALUE;
    CLK->ECKR = CLK_ECKR_RESET_VALUE;
    CLK->SWR  = CLK_SWR_RESET_VALUE;
    CLK->SWCR = CLK_SWCR_RESET_VALUE;
    CLK->CKDIVR = CLK_CKDIVR_RESET_VALUE;
    CLK->PCKENR1 = CLK_PCKENR1_RESET_VALUE;
    CLK->PCKENR2 = CLK_PCKENR2_RESET_VALUE;
    CLK->CSSR = CLK_CSSR_RESET_VALUE;
    CLK->CCOR = CLK_CCOR_RESET_VALUE;
    while (CLK->CCOR & CLK_CCOR_CCOEN)
    {}
    CLK->CCOR = CLK_CCOR_RESET_VALUE;
    CLK->CANCCR = CLK_CANCCR_RESET_VALUE;
    CLK->HSITRIMR = CLK_HSITRIMR_RESET_VALUE;
    CLK->SWIMCCR = CLK_SWIMCCR_RESET_VALUE;

}


/**
  * @brief Starts or Stops manually the clock switch execution.
  * @par Full description:
  * NewState parameter set the SWEN.
  * @param[in] NewState new state of SWEN, value accepted ENABLE, DISABLE.
  * @retval None
  */
void CLK_ClockSwitchCmd(FunctionalState NewState)
{

    /* Check the parameters */
    assert_param(IS_FUNCTIONALSTATE_OK(NewState));

    if (NewState != DISABLE )
    {
        /* Enable the Clock Switch */
        CLK->SWCR |= CLK_SWCR_SWEN;
    }
    else
    {
        /* Disable the Clock Switch */
        CLK->SWCR &= (u8)(~CLK_SWCR_SWEN);
    }

}

/**
  * @brief Configures the slow active halt wake up
  * @param[in] NewState: specifies the Slow Active Halt wake up state.
  * can be set of the following values:
  * - DISABLE: Slow Active Halt mode disabled;
  * - ENABLE:  Slow Active Halt mode enabled.
  * @retval None
  */
void CLK_SlowActiveHaltWakeUpCmd(FunctionalState NewState)
{

    /* check the parameters */
    assert_param(IS_FUNCTIONALSTATE_OK(NewState));

    if (NewState != DISABLE)
    {
        /* Set S_ACTHALT bit */
        CLK->ICKR |= CLK_ICKR_SWUAH;
    }
    else
    {
        /* Reset S_ACTHALT bit */
        CLK->ICKR &= (u8)(~CLK_ICKR_SWUAH);
    }

}


/**
  * @brief Configures the HSI clock dividers.
  * @param[in] HSIPrescaler : Specifies the HSI clock divider to apply.
  * This parameter can be any of the @ref CLK_Prescaler_TypeDef enumeration.
  * @retval None
  */
void CLK_HSIPrescalerConfig(CLK_Prescaler_TypeDef HSIPrescaler)
{

    /* check the parameters */
    assert_param(IS_CLK_HSIPRESCALER_OK(HSIPrescaler));

    /* Clear High speed internal clock prescaler */
    CLK->CKDIVR &= (u8)(~CLK_CKDIVR_HSIDIV);

    /* Set High speed internal clock prescaler */
    CLK->CKDIVR |= (u8)HSIPrescaler;

}



/**
  * @brief  Enables or disables the specified CLK interrupts.
  * @param[in] CLK_IT This parameter specifies the interrupt sources.
  * It can be one of the values of @ref CLK_IT_TypeDef.
  * @param[in] NewState New state of the Interrupt.
  * Value accepted ENABLE, DISABLE.
  * @retval None
  */
void CLK_ITConfig(CLK_IT_TypeDef CLK_IT, FunctionalState NewState)
{

    /* check the parameters */
    assert_param(IS_FUNCTIONALSTATE_OK(NewState));
    assert_param(IS_CLK_IT_OK(CLK_IT));

    if (NewState != DISABLE)
    {
        switch (CLK_IT)
        {
        case CLK_IT_SWIF: /* Enable the clock switch interrupt */
            CLK->SWCR |= CLK_SWCR_SWIEN;
            break;
        case CLK_IT_CSSD: /* Enable the clock security system detection interrupt */
            CLK->CSSR |= CLK_CSSR_CSSDIE;
            break;
        default:
            break;
        }
    }
    else  /*(NewState == DISABLE)*/
    {
        switch (CLK_IT)
        {
        case CLK_IT_SWIF: /* Disable the clock switch interrupt */
            CLK->SWCR  &= (u8)(~CLK_SWCR_SWIEN);
            break;
        case CLK_IT_CSSD: /* Disable the clock security system detection interrupt */
            CLK->CSSR &= (u8)(~CLK_CSSR_CSSDIE);
            break;
        default:
            break;
        }
    }

}

/**
  * @brief Configures the HSI and CPU clock dividers.
  * @param[in] ClockPrescaler Specifies the HSI or CPU clock divider to apply.
  * @retval None
  */
void CLK_SYSCLKConfig(CLK_Prescaler_TypeDef ClockPrescaler)
{

    /* check the parameters */
    assert_param(IS_CLK_PRESCALER_OK(ClockPrescaler));

    if (((u8)ClockPrescaler & (u8)0x80) == 0x00) /* Bit7 = 0 means HSI divider */
    {
        CLK->CKDIVR &= (u8)(~CLK_CKDIVR_HSIDIV);
        CLK->CKDIVR |= (u8)((u8)ClockPrescaler & (u8)CLK_CKDIVR_HSIDIV);
    }
    else /* Bit7 = 1 means CPU divider */
    {
        CLK->CKDIVR &= (u8)(~CLK_CKDIVR_CPUDIV);
        CLK->CKDIVR |= (u8)((u8)ClockPrescaler & (u8)CLK_CKDIVR_CPUDIV);
    }

}


/**
  * @brief Enables the Clock Security System.
  * @par Full description:
  * once CSS is enabled it cannot be disabled until the next reset.
  * @par Parameters:
  * None
  * @retval None
  */
void CLK_ClockSecuritySystemEnable(void)
{
    /* Set CSSEN bit */
    CLK->CSSR |= CLK_CSSR_CSSEN;
}

/**
  * @brief Returns the clock source used as system clock.
  * @par Parameters:
  * None
  * @retval  Clock source used.
  * can be one of the values of @ref CLK_Source_TypeDef
  */
CLK_Source_TypeDef CLK_GetSYSCLKSource(void)
{
    return((CLK_Source_TypeDef)CLK->CMSR);
}


/**
  * @brief This function returns the frequencies of different on chip clocks.
  * @par Parameters:
  * None
  * @retval the master clock frequency
  */


/**
  * @brief Adjusts the Internal High Speed oscillator (HSI) calibration value.
  * @par Full description:
  * @param[in] CLK_HSICalibrationValue calibration trimming value.
  * can be one of the values of @ref CLK_HSITrimValue_TypeDef
  * @retval None
  */
void CLK_AdjustHSICalibrationValue(CLK_HSITrimValue_TypeDef CLK_HSICalibrationValue)
{

    /* check the parameters */
    assert_param(IS_CLK_HSITRIMVALUE_OK(CLK_HSICalibrationValue));

    /* Store the new value */
    CLK->HSITRIMR = (u8)((CLK->HSITRIMR & (u8)(~CLK_HSITRIMR_HSITRIM))|((u8)CLK_HSICalibrationValue));

}

/**
  * @brief Reset the SWBSY flag (SWICR Reister)
  * @par Full description:
  * This function reset SWBSY flag in order to reset clock switch operations (target
  * oscillator is broken, stabilization is longing too much, etc.).  If at the same time \n
  * software attempts to set SWEN and clear SWBSY, SWBSY action takes precedence.
  * @par Parameters:
  * None
  * @retval None
  */
void CLK_SYSCLKEmergencyClear(void)
{
    CLK->SWCR &= (u8)(~CLK_SWCR_SWBSY);
}


/**
  * @brief Checks whether the specified CLK interrupt has is enabled or not.
  * @param[in] CLK_IT specifies the CLK interrupt.
  * can be one of the values of @ref CLK_IT_TypeDef
  * @retval ITStatus, new state of CLK_IT (SET or RESET).
  */
ITStatus CLK_GetITStatus(CLK_IT_TypeDef CLK_IT)
{

    ITStatus bitstatus = RESET;

    /* check the parameters */
    assert_param(IS_CLK_IT_OK(CLK_IT));

    if (CLK_IT == CLK_IT_SWIF)
    {
        /* Check the status of the clock switch interrupt */
        if ((CLK->SWCR & (u8)CLK_IT) == (u8)0x0C)
        {
            bitstatus = SET;
        }
        else
        {
            bitstatus = RESET;
        }
    }
    else /* CLK_IT == CLK_IT_CSSDIE */
    {
        /* Check the status of the security system detection interrupt */
        if ((CLK->CSSR & (u8)CLK_IT) == (u8)0x0C)
        {
            bitstatus = SET;
        }
        else
        {
            bitstatus = RESET;
        }
    }

    /* Return the CLK_IT status */
    return bitstatus;

}

/**
  * @brief Clears the CLK�s interrupt pending bits.
  * @param[in] CLK_IT specifies the interrupt pending bits.
  * can be one of the values of @ref CLK_IT_TypeDef
  * @retval None
  */
void CLK_ClearITPendingBit(CLK_IT_TypeDef CLK_IT)
{

    /* check the parameters */
    assert_param(IS_CLK_IT_OK(CLK_IT));

    if (CLK_IT == (u8)CLK_IT_CSSD)
    {
        /* Clear the status of the security system detection interrupt */
        CLK->CSSR &= (u8)(~CLK_CSSR_CSSD);
    }
    else /* CLK_PendingBit == (u8)CLK_IT_SWIF */
    {
        /* Clear the status of the clock switch interrupt */
        CLK->SWCR &= (u8)(~CLK_SWCR_SWIF);
    }

}
/**
  * @}
  */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
