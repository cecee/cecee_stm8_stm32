#include "spi.h"
#include "include.h"

void  SPI0_Init(void)
{
  SPI_DeInit();
    
  SPI_Init(
  SPI_FIRSTBIT_MSB, 
  SPI_BAUDRATEPRESCALER_2,//SPI_BAUDRATEPRESCALER_32, 
  SPI_MODE_MASTER, 
  SPI_CLOCKPOLARITY_LOW, 
  SPI_CLOCKPHASE_1EDGE,//SPI_CLOCKPHASE_2EDGE, 
  SPI_DATADIRECTION_2LINES_FULLDUPLEX, 
  SPI_NSS_SOFT,7);//SPI_NSS_HARD, 0x00 );
  SPI_Cmd(ENABLE);
}

void SPI_WriteByte(unsigned char Data)
{
  disableInterrupts();

  while (SPI_GetFlagStatus(SPI_FLAG_TXE) == RESET);
  SPI->DR = Data;
  enableInterrupts();
}

void SPI_Write3Byte(unsigned char d0, unsigned char d1, unsigned char d2)
{
  //disableInterrupts();
  while (SPI_GetFlagStatus(SPI_FLAG_TXE) == RESET);  SPI->DR = d0;
  while (SPI_GetFlagStatus(SPI_FLAG_TXE) == RESET);  SPI->DR = d1;
  while (SPI_GetFlagStatus(SPI_FLAG_TXE) == RESET);  SPI->DR = d2;
  //enableInterrupts();
}

 unsigned char SPI_WriteReadByte(unsigned char Data)
{

	while (SPI_GetFlagStatus(SPI_FLAG_TXE) == RESET);
	SPI->DR = Data;
	while(SPI_GetFlagStatus(SPI_FLAG_RXNE) == RESET);
	return SPI->DR;

}