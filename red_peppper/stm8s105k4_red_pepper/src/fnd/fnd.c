#include "include.h"
#include "fnd.h"
#include <stdlib.h>
//    a
// f      b
//    g 
// e     c
//   d
const u8 nFND_DATA[16] = {0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xd8,0x80,0x98,0x88,0x83,0xc6,0xa1,0x86,0x8e};

void writer_fnd_addr(u8 addr){
//  if(_CheckBit(addr, 0)) GPIO_WriteHigh(GPIOA, GPIO_PIN_1); else GPIO_WriteLow(GPIOA, GPIO_PIN_1);//_SetBit(fnd_addr, 7);
//  if(_CheckBit(addr, 1)) GPIO_WriteHigh(GPIOA, GPIO_PIN_2); else GPIO_WriteLow(GPIOA, GPIO_PIN_2);//_SetBit(fnd_addr, 6);
//  if(_CheckBit(addr, 2)) GPIO_WriteHigh(GPIOB, GPIO_PIN_5); else GPIO_WriteLow(GPIOB, GPIO_PIN_5);//_SetBit(fnd_addr, 5);
//  if(_CheckBit(addr, 3)) GPIO_WriteHigh(GPIOB, GPIO_PIN_4); else GPIO_WriteLow(GPIOB, GPIO_PIN_4);//_SetBit(fnd_addr, 4);
  switch(addr){
    case 0:
        GPIO_WriteLow(GPIOA, GPIO_PIN_1);
        GPIO_WriteLow(GPIOA, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);
      break;
    case 1:
        GPIO_WriteHigh(GPIOA, GPIO_PIN_1);
        GPIO_WriteLow(GPIOA, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);      
      break; 
    case 2:
        GPIO_WriteLow(GPIOA, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOA, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);       
      break;
    case 3:
        GPIO_WriteHigh(GPIOA, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOA, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);       
      break; 
    case 4:
        GPIO_WriteLow(GPIOA, GPIO_PIN_1);
        GPIO_WriteLow(GPIOA, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);        
      break;
    case 5:
        GPIO_WriteHigh(GPIOA, GPIO_PIN_1);
        GPIO_WriteLow(GPIOA, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);     
      break;   
    case 6:
        GPIO_WriteLow(GPIOA, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOA, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);       
      break;
    case 7:
        GPIO_WriteHigh(GPIOA, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOA, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);      
      break; 
    case 8:
        GPIO_WriteLow(GPIOA, GPIO_PIN_1);
        GPIO_WriteLow(GPIOA, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);       
      break;
//    case 9:
//        GPIO_WriteHigh(GPIOA, GPIO_PIN_1);
//        GPIO_WriteLow(GPIOA, GPIO_PIN_2);
//        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
//        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);      
//      break; 
//    case 10:
//        GPIO_WriteLow(GPIOA, GPIO_PIN_1);
//        GPIO_WriteHigh(GPIOA, GPIO_PIN_2);
//        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
//        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);       
//      break;
//    case 11:
//      break;       
  }

}
void writer_fnd_data(u8 fnd_data){

  if(gCUR.flg.blink){
    fnd_data=0xFF;
  }
  u8 gpioc_odr= GPIOC->ODR & 0x07;
  u8 gpioc_data=(fnd_data<<3);
  gpioc_data=gpioc_data |gpioc_odr;
  GPIOC->ODR=gpioc_data;
  /*
  if(_CheckBit(fnd_data, 0)) GPIO_WriteHigh(GPIOC, GPIO_PIN_3); else GPIO_WriteLow(GPIOC, GPIO_PIN_3);
  if(_CheckBit(fnd_data, 1)) GPIO_WriteHigh(GPIOC, GPIO_PIN_4); else GPIO_WriteLow(GPIOC, GPIO_PIN_4);
  if(_CheckBit(fnd_data, 2)) GPIO_WriteHigh(GPIOC, GPIO_PIN_5); else GPIO_WriteLow(GPIOC, GPIO_PIN_5);
  if(_CheckBit(fnd_data, 3)) GPIO_WriteHigh(GPIOC, GPIO_PIN_6); else GPIO_WriteLow(GPIOC, GPIO_PIN_6); 
  if(_CheckBit(fnd_data, 4)) GPIO_WriteHigh(GPIOC, GPIO_PIN_7); else GPIO_WriteLow(GPIOC, GPIO_PIN_7);
  */
  if(_CheckBit(fnd_data, 5)) GPIO_WriteHigh(GPIOD, GPIO_PIN_0); else GPIO_WriteLow(GPIOD, GPIO_PIN_0);
  if(_CheckBit(fnd_data, 6)) GPIO_WriteHigh(GPIOD, GPIO_PIN_2); else GPIO_WriteLow(GPIOD, GPIO_PIN_2);
  if(_CheckBit(fnd_data, 7)) GPIO_WriteHigh(GPIOD, GPIO_PIN_5); else GPIO_WriteLow(GPIOD, GPIO_PIN_5);

}

void writer_data(u8 add, u8 data){
  u8 fnd_data;
  switch(add){
    case 7:
      fnd_data=nFND_DATA[data];
      if(sec_bling_flag) fnd_data&=0x7F;//_ClearBit(fnd_data, 7); 
      else fnd_data|=0x80;//_SetBit(fnd_data, 7);
      break;
    case 8:
      fnd_data=data;//LED
      break;
    case 9:
      fnd_data=0xff;//LED_oFF
      break;
    default:
      fnd_data=nFND_DATA[data];
      break;
  }
  writer_fnd_addr(add);  
  writer_fnd_data(fnd_data);
}

void FND_Update(u16 digit){
  u8 data;
  u8 min,sec;
  u8 curr_temp;
  
  switch(digit){
  case 0:
    curr_temp=gCUR.current_temp/10;
    data= (gCUR.flg.process_seq==PROCESS_ERR)? 0x0E : curr_temp/10;
    break;
  case 1:
    curr_temp=gCUR.current_temp/10;
    if(gCUR.flg.process_seq==PROCESS_ERR){
#ifdef DEBUG  
 // printf("gCUR.error_info.data[%x]\r\n",gCUR.error_info.data);
#endif  
      switch(gCUR.error_info.data & 0x1F){
        case 1: case 2: case 3:   data=3;    break;
        case 4: data=4; break; 
        case 8: data=1; break; 
        case 0x09: data=5;  break; 
        case 0x0A: data=6;  break;           
        case 0x10: data=2;  break;    
      }
    }
    else data= curr_temp%10;
    break; 
  case 2:
    data=gCUR.seljung_temp/10;
    break; 
  case 3:
    data=gCUR.seljung_temp%10;
    break;
  case 4://min 1x:xx
    min=gCUR.current_minute/60;
    data=min/10;
    break; 
  case 5://min x1:xx
    min=gCUR.current_minute/60;
    data=min%10;
    break;     
  case 6://sec xx:1x
    min=gCUR.current_minute/60;
    sec=(gCUR.current_minute-(min*60))/10;
    data=sec;
    break;
  case 7://sec xx:x1
    min=gCUR.current_minute/60;
    sec=(gCUR.current_minute-(min*60))%10;
    data=sec;
    break; 
  case 8://LED
    //FND_Write(~gCUR.led.data,digit,1);
    data=0xFF;
    if(gCUR.led.b.heater)_ClearBit(data, 0);
    if(gCUR.led.b.fan) _ClearBit(data, 1); 
    switch(gCUR.pgm_mode){
      case 0: _ClearBit(data, 2); break;//ilban
      case 1: _ClearBit(data, 3); break;//pepper
      case 2: _ClearBit(data, 4); break;//gokgam
    }
    if(gCUR.auto_damper) _ClearBit(data, 5); else  _SetBit(data, 5);
    if(gCUR.led.b.damper_open) { _ClearBit(data, 6); _SetBit(data, 7);}
    else{ _SetBit(data, 6);  _ClearBit(data, 7);}

    break;   
    case 9://LED
      data=0xff;
      break;
  }
  writer_data(digit, data);
}





