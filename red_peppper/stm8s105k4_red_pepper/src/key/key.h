
#ifndef _KEY_H
#define _KEY_H		

typedef enum _KEY_MAP{
  KEY_NULL=0,
  KEY_MODE,
  KEY_TEMP_UP,
  KEY_TEMP_DN,
  KEY_TIME_UP,
  KEY_TIME_DN,
  KEY_DAMPER,
}KEY_MAP;

u16 get_adc_channel(u8 mChannel);
void get_adc();
void get_key(u16 key_adc);
void key_process(KEY_MAP key);
u16 add_unit(u16 value, u8 unit, u8 up);
void put_rec_ct(int ct_value);
int max_ct();
void get_adc_key(void);
#endif  
	 
	 



