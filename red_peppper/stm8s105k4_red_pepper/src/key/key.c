#include "include.h"
#define MAX_TEMPERATURE 80
#define MIN_TEMPERATURE 20
#define REC_CT_SIZE 20

int rec_ct[REC_CT_SIZE]={0,};
u8 err_release_key_buf[4]={0,};
u8 err_release_key_cnt=0;


void put_rec_ct(int ct_value){
  for(u8 i=1;i<REC_CT_SIZE;i++){
    rec_ct[i-1]=rec_ct[i];
  }
  rec_ct[REC_CT_SIZE-1]=ct_value;
  #ifdef DEBUG  
   // printf("[%d][%d][%d][%d][%d] \r\n", rec_ct[0], rec_ct[1], rec_ct[2], rec_ct[3], rec_ct[4]);
 #endif
}
int max_ct(){
  int max = rec_ct[0];
  //int min = rec_ct[0];
  for (int i = 0; i < REC_CT_SIZE; i++) {
      if (rec_ct[i] > max) max = rec_ct[i];
      //if (rec_ct[i] < min) min = rec_ct[i];
  }
  return max;
}


u16 get_adc_channel(u8 mChannel){
  u16 ResultADC = 0;
  u16 temph = 0;
  u8 templ = 0;

  ADC1->CSR  = 0x00;
  ADC1->CR1  = 0x00;
  ADC1->CR2  = 0x00;
  ADC1->CR3  = 0x00;
  ADC1->CR1 |= 0x40;//2mhz
  ADC1->CR2 |= 0x08;//0x08;//0000_1000 ;right, No scan,

  ADC1->CSR |= (u8)mChannel;
  ADC1->CR1 |= ADC1_CR1_ADON;//ADC on
  ADC1->CR1 |= ADC1_CR1_ADON;

  while(!(ADC1->CSR & 0x80));

  templ = ADC1->DRL;
  temph = ADC1->DRH;
  temph = (u16)(templ | (u16)(temph << 8));

  ResultADC = temph;
  ADC1->CSR &= 0x7F;//EOC Clear

 return ResultADC & 0x3ff;
}

void get_adc(){
  u16 dac_adc;
  u16 adc_ct;
  gCUR.temp_adc=(u16)get_adc_filter(get_adc_channel(0), 0);
 // dac_adc =(u16)get_adc_filter(get_adc_channel(2), 1);
 // gCUR.dac_get_volt=adc_to_volt(dac_adc);
 //process CT+
  adc_ct =(u16)get_adc_channel(3);
  put_rec_ct(adc_ct);
  gCUR.ct_adc=max_ct();
  //process CT-
#ifdef DEBUG  
 // printf("ct_adc[%d]--\r\n",gCUR.ct_adc);
#endif 
}

void get_adc_key(){
  u16 adkey;
  adkey=get_adc_channel(1);//adkey
  get_key(adkey);
  key_process(gCUR.key);
}

void get_key(u16 key_adc){
  //_printf("get_key[%d]\r\n",key_adc);
  u8 key;
  if(key_adc>950) key=KEY_NULL;
  else if(key_adc>800 && key_adc<=950) key=KEY_MODE;//900
  else if(key_adc>680 && key_adc<=800)key=KEY_DAMPER; //755
  else if(key_adc>550 && key_adc<=680)key=KEY_TIME_DN; //606
  else if(key_adc>400 && key_adc<=550)key=KEY_TIME_UP; //459
  else if(key_adc>250 && key_adc<=400)key=KEY_TEMP_DN; //310
  else if(key_adc<=100)key=KEY_TEMP_UP; //0
  //_printf("get_key[%d]\r\n",key_adc);
  gCUR.key=(KEY_MAP)key;
}

u16 add_unit(u16 value, u8 unit, u8 up){
    u16 xx=value/unit;
    if(up) xx++;
    else xx--;
    return xx*unit;
}

void key_process(KEY_MAP key){
  static u8 ex_key=0;
  static u16 key_press_cnt=0;
  int lval;
  u8 beef_on=0;
  if(key ==KEY_NULL) {
    ex_key=0; key_press_cnt=0; 
    return;
  }
//release error++ 
    if(key==KEY_TEMP_DN){
      err_release_key_cnt=0;
      memset(err_release_key_buf,0,4);
    }
    err_release_key_buf[err_release_key_cnt++]= key;
    if(err_release_key_buf[0]==KEY_TEMP_DN && err_release_key_buf[1]==KEY_TEMP_UP && err_release_key_buf[2]==KEY_TIME_DN && err_release_key_buf[3]==KEY_TIME_UP){
      err_release_flag^=1;
      if(err_release_flag) gCUR.flg.process_seq=PROCESS_ING;
    }
  #ifdef DEBUG  
      printf("err_release_key_buf[%d][%d][%d][%d]\r\n", err_release_key_buf[0], err_release_key_buf[1], err_release_key_buf[2], err_release_key_buf[3]);
  #endif
      if(err_release_key_cnt>3){
        err_release_key_cnt=0;
        memset(err_release_key_buf,0,4);
      }

//release error--  
  switch(key){
    //case KEY_NULL: ex_key=0; key_press_cnt=0; return;
    case KEY_TEMP_UP:
    case KEY_TEMP_DN:
      if(gCUR.pgm_mode) {
        //sub_set_audio(VOICE_HI_TEMP);//나중에 고칠거
        //seljung_flag=5;
        set_voice(VOICE_NO_CHANGE);  //only without eeprom save
        //ex_key=0; key_press_cnt=0; 
        return;
      }
      if(ex_key==key){
        key_press_cnt++;
        //key_press_cnt=(key_press_cnt>10) ? 10 : key_press_cnt;
      }
      else key_press_cnt=0; 
      
      lval=gCUR.seljung_temp;
      if(key_press_cnt>=2){
        if(key_press_cnt%2==0){
          if(key==KEY_TEMP_UP) lval=add_unit(lval, 5, 1);//up 5
          else lval=add_unit(lval, 5, 0);//dn 5
          beef_on=1; 
        }
       }
      else{
        lval=(key==KEY_TEMP_UP)? lval+1: lval-1;
        beef_on=1; 
      }
      lval=(lval>MAX_TEMP)? MAX_TEMP: lval;
      lval=(lval<MIN_TEMP)? MIN_TEMP: lval;
      gCUR.seljung_temp=lval;
      if(gCUR.pgm_mode==0) {
        for(u8 i=0;i<5;i++){
          gMODE_TABLE.ctrl_obj[i].seljung_temperature=gCUR.seljung_temp;
        }
      }
      set_temp_pwm(gCUR.seljung_temp*10);
      set_heater_pwm();
      set_voice(VOICE_TEMP_SELJUNG); 
      temp_sample_cnt=0;
      calibration_period=0;
      break;  
      
    case KEY_TIME_UP:
    case KEY_TIME_DN:  
      if(gCUR.pgm_mode) {
        //seljung_flag=5;
        set_voice(VOICE_NO_CHANGE);  //only without eeprom save
        return;
      }
      if(ex_key==key){
        key_press_cnt++;
        //key_press_cnt=(key_press_cnt>10) ? 10 : key_press_cnt;
      }
      else key_press_cnt=0; 
      
      lval=gCUR.current_minute;
      if(key_press_cnt>=2){
         if(key_press_cnt%2==0){
            if(key==KEY_TIME_UP) lval=add_unit(lval, 30, 1);
            else lval=add_unit(lval, 30, 0);
            beef_on=1; 
         }
      }
      else{
        lval=(key==KEY_TIME_UP)? lval+1: lval-1;
        beef_on=1; 
      }
      lval=(lval>=5999) ? 5999: lval;
      lval=(lval<=0) ? 0: lval;
      if(gCUR.pgm_mode==0){
        //lval= (lval >=gMODE_TABLE.ctrl_obj[0].remain_time)? gMODE_TABLE.ctrl_obj[0].remain_time: lval;
        for(u8 i=0;i<5;i++){
          gMODE_TABLE.ctrl_obj[i].remain_time=0;
        }
        gMODE_TABLE.ctrl_obj[0].remain_time=lval;
      }
      one_sec_tick=0;//time설정시 세팅할때 0에서 시작하도록
      gCUR.current_minute=lval;
      set_heater_pwm();
      set_voice(VOICE_TIME_SELJUNG); 
      break;
      
#ifdef PGM_MODE      
    case KEY_MODE:
      beef_on=1; 
      gCUR.pgm_mode=(gCUR.pgm_mode>=2) ? 0: gCUR.pgm_mode+1;
      gMODE_TABLE=mode_table[gCUR.pgm_mode];
      gCUR.current_minute=gMODE_TABLE.ctrl_obj[0].remain_time;
      gCUR.seljung_temp=gMODE_TABLE.ctrl_obj[0].seljung_temperature; 
      if(gCUR.pgm_mode){ //ilban이 아니면
        gCUR.auto_damper=1; //auto
      }
      //gCUR.flg.eeprom_seljung_save=1;
      seljung_flag=2;
      break;  
#endif      
    case KEY_DAMPER:
      beef_on=1; 
      gCUR.auto_damper=(gCUR.auto_damper>=1) ? 0: gCUR.auto_damper+1;
      if(gCUR.auto_damper==0){ //manual
        gCUR.led.b.damper_open=1;//open
      }
      else{//auto
        //gCUR.led.b.damper_close=(gCUR.ctrl.damper_close) ? 1:0;
        //gCUR.led.b.damper_close=0;
      }
      //seljung_flag=2;
      set_voice(VOICE_NULL); //설정이 변경 되었습니다. with saving EEPROM
      break;       
  }

  one_ms_tick=0;
 if(beef_on) beep_on(); 
  ex_key=(u8)key; 
#ifdef DEBUG  
 // printf("--key_process key[%d] key_press[%d]\r\n", key, key_press);
#endif  
}