
#ifndef _SUB_ROUTINE_H
#define _SUB_ROUTINE_H	


void set_voice(u8 num);

void main_loop(void);
void process_start(void);
void process_ing(void);
void process_end(void);

void sub_one_ms(void);
void sub_one_min(void);
void sub_one_sec(void);
void heater_on(u8 on);
//void sub_saving_eeprom(void);
void sub_error_check(void);

u8 get_damper_open(void);
void damper_open(u8 open);

void sub_set_audio(u8 audio_id);
void audio_port_init();
void delay_us(u32 i);

void beep_on();
void set_heater_pwm(void);

u16 adc_to_volt(u16 adc_value);
void put_rec_temp(int temp);
int diff_rec_temp(void);
int avr_rec_temp(void);
void clear_rec_temp(void);
void heater_led_control(void);
void fan_on(u8 on);

#endif  
	 
	 



