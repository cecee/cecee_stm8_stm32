#include "include.h"

#define REC_TEMP_SIZE 5
#define THREHOLD_CT 530
#define TEMP_SAMPLEING_RATE 5 //5sec
#define CALIBRATION_PERIOD 5  //5*5 = 25sec

#define DAMPER_ON_OFFSET 1
#define DAMPER_OFF_OFFSET 5


int rec_temp[REC_TEMP_SIZE]={0,};
u16 ex_tmp_offset=0;
u16 ending_cnt=0;
int temp_sample_cnt=0; 
int calibration_period=0; 

void set_voice(u8 num){
  gVOICE_INFO.voice_num = num;
  gVOICE_INFO.delay = EEPROM_WRITER_DELAY;
  gVOICE_INFO.flag = 1;
}

void process_start(void){
  

}

void process_ing(void){
  

}
void process_end(void){
  damper_open(1);//open
  heater_on(0);
  set_temp_pwm(0);
#ifdef DEBUG  
//  printf("ending_cnt[%d] !!\r\n", ending_cnt);
#endif  
  if(ending_cnt>ENDING_COOLING){
    ending_cnt=ENDING_COOLING+1;
    fan_on(0);
  }
  else{
    fan_on(1);
  }
  delay_us(1000);//FND 찌꺼기 때문에...
}

void main_loop(void){
  u8 old_seq = gCUR.flg.process_seq;
#ifdef DEBUG 
 // printf("main_loop gCUR.error_info.data[%d] !!\r\n", gCUR.error_info.data);
#endif 
  if(gCUR.flg.process_seq!=PROCESS_ERR){
    gCUR.flg.process_seq=(gCUR.current_minute)? PROCESS_ING : PROCESS_END;//1 :ending
  }
  
  if(gCUR.flg.process_seq==PROCESS_END){
    if(old_seq==PROCESS_ING) gCUR.flg.finish_flag=1; //endig message
    process_end();
    
  }
  else gCUR.flg.finish_flag=0;
   
  //time saved for every minute
  if(gCUR.flg.eeprom_time_save){
    gCUR.flg.eeprom_time_save=0;
    save_eeprom_current_time(gCUR.current_minute);
  };
  
//voice ++  
  if(gVOICE_INFO.flag==1 && gVOICE_INFO.delay==0){
      gVOICE_INFO.flag=0;
      switch(gVOICE_INFO.voice_num){
        case VOICE_TEMP_SELJUNG:
           sub_set_audio(VOICE_TEMP_SELJUNG);
          break;   
        case VOICE_TIME_SELJUNG:
          sub_set_audio(VOICE_TIME_SELJUNG);
          break;      
        case VOICE_END:
          sub_set_audio(VOICE_END);
          break;   
        case VOICE_JANGAE:
          sub_set_audio(VOICE_JANGAE);
          break;           
        case VOICE_NO_CHANGE:
          sub_set_audio(VOICE_NO_CHANGE);
          break;
      }
      //sub_saving_eeprom();    
      save_eeprom_seljung_temp();//temp, mode, baesub all save
  }
//voice ++ 
  
  
  
}

void put_rec_temp(int temp){
  for(u8 i=1;i<REC_TEMP_SIZE;i++){
    rec_temp[i-1]=rec_temp[i];
  }
  rec_temp[REC_TEMP_SIZE-1]=temp;
#ifdef DEBUG  
//    printf("[%d][%d][%d][%d][%d] \r\n", rec_temp[0], rec_temp[1], rec_temp[2], rec_temp[3], rec_temp[4]);
#endif
}

int diff_rec_temp(){
  int max = rec_temp[0];
  int min = rec_temp[0];
  for (int i = 0; i < REC_TEMP_SIZE; i++) {
      if (rec_temp[i] > max) max = rec_temp[i];
      if (rec_temp[i] < min) min = rec_temp[i];
  }
  return max-min;
}

void clear_rec_temp(){
 // memset(rec_temp,0,REC_TEMP_SIZE);
  for (int i = 0; i < REC_TEMP_SIZE; i++) {
      rec_temp[i]=0;
  }
}

int avr_rec_temp(){
  int sum = 0;
  u8 cnt=0;
  for (int i = 0; i < REC_TEMP_SIZE; i++) {
    if(  rec_temp[i]){
      cnt++;
      sum= sum+rec_temp[i];
    }
  }
  return sum/cnt;
}


u16 calibration_set_temp(u16 set_temp){
  int tmp_offset;
  int tmp_diff, tmp_avr=0;
  u16 tmp_seljung=(gCUR.seljung_temp)*10;
  u16 lset_temp =set_temp;
  u8 diff_sign=0;// 0:plus 1:minus
  if(calibration_period > CALIBRATION_PERIOD){
  calibration_period=0;
  tmp_diff=diff_rec_temp(); //max-min
  tmp_avr=avr_rec_temp();
  
  if(tmp_diff<5 && tmp_avr>MININUM_CONTROL_TEMP) {
    diff_sign=(tmp_seljung>tmp_avr)? 0:1;
    tmp_offset=abs(tmp_seljung-tmp_avr);
    //if(tmp_offset>30)tmp_offset=30;
    if(diff_sign) tmp_offset=tmp_offset*(-1);
    lset_temp=gCUR.ex_set_temp+tmp_offset;
#ifdef DEBUG  
 printf("tmp_avr[%d] tmp_diff[%d] tmp_offset[%d] lset_temp[%d] diff_sign[%d]\r\n", tmp_avr,tmp_diff, tmp_offset, lset_temp, diff_sign);
#endif 
  }
  clear_rec_temp();
}

#ifdef DEBUG  
 printf("--lset_temp[%d] ex_set_tmp[%d] gCUR.current_temp[%d]\r\n",lset_temp, gCUR.ex_set_temp, gCUR.current_temp);
#endif
  return lset_temp;
}

//=====================
//  Every second 
//=====================
void sub_one_sec(void)
{
  u16 lset_temp;//=tmp_seljung;
  u8 ldamper_open=0;
  
  if(gCUR.flg.process_seq==PROCESS_END)ending_cnt++;
  else ending_cnt=0;
  //gCUR.flg.sec_toggle^=1;//":" mark display
  sec_bling_flag=1;
  sec_bling_Time=0;
  
  temp_sample_cnt++;
#ifdef DEBUG  
  printf("### gCUR.flg.process_seq[%d] \r\n", gCUR.flg.process_seq);
  //  printf("==sub_one_sec flag[%d] delay[%d]\r\n", gVOICE_INFO.flag, gVOICE_INFO.delay);
#endif   
  if(gVOICE_INFO.flag && gVOICE_INFO.delay>0) {
    if(gVOICE_INFO.delay>0) gVOICE_INFO.delay--;
    else gVOICE_INFO.delay=0;
  }
  gCUR.current_temp=get_temperature(gCUR.temp_adc)+TEMP_READ_OFFSET;
  //gCUR.oh_temp=get_temperature(gCUR.oh_adc);
  gCUR.current_temp= (gCUR.current_temp<0) ? 0: gCUR.current_temp;
  gCUR.current_temp= (gCUR.current_temp>990) ? 990: gCUR.current_temp;  

  
  switch(gCUR.flg.process_seq){
    case PROCESS_ING:
      if(temp_sample_cnt>TEMP_SAMPLEING_RATE){
        temp_sample_cnt=0;
        calibration_period++;
        put_rec_temp(gCUR.current_temp);   
      }      
#if 1      
      if(gCUR.ex_set_temp==0) lset_temp=(gCUR.seljung_temp)*10;
      else lset_temp=gCUR.ex_set_temp; 
      
      lset_temp=calibration_set_temp(lset_temp);
      
      if( lset_temp != gCUR.ex_set_temp){
         set_temp_pwm(lset_temp);
      }
#else
      set_temp_pwm(gCUR.seljung_temp*10);
#endif
      
      ldamper_open = get_damper_open();
      damper_open(ldamper_open);
      fan_on(1);
      heater_led_control();      
      break;
    case PROCESS_ERR: 
      damper_open(1);
      gCUR.led.b.heater=0;
      //gCUR.flg.blink^=1; 
      set_temp_pwm(0);
      beep_on();
      break;
    case PROCESS_END: 
      break;
  }
  sub_error_check();
  if(gCUR.error_info.data) gCUR.flg.process_seq=PROCESS_ERR;
}

//=====================
//  Every minute 
//=====================
void sub_one_min(void){
  u16 ex_current_minute;
  if(gCUR.flg.process_seq==PROCESS_ING){
    ex_current_minute=gCUR.current_minute;
    gCUR.current_minute =(gCUR.current_minute<1)? 0 : gCUR.current_minute-1;
    gCUR.flg.eeprom_time_save=1; //for time saving every minite
    set_heater_pwm();
    if((ex_current_minute && (gCUR.current_minute==0)) || gCUR.flg.finish_flag){
    //gCUR.flg.finish_flag=0;
      set_voice(VOICE_END); //no voice only saving EEPROM
    } 
  }
  //voice_error_message();
  else if(gCUR.flg.process_seq==PROCESS_ERR) set_voice(VOICE_JANGAE); 
}

void set_heater_pwm(void){
  u8 i;
  for(i=1;i<5;i++){
    if(gCUR.current_minute >=gMODE_TABLE.ctrl_obj[i].remain_time){
      gCUR.ctrl.mode_current_seq=i-1;
      if(gCUR.current_minute==gMODE_TABLE.ctrl_obj[i].remain_time) {
        gCUR.seljung_temp=gMODE_TABLE.ctrl_obj[i].seljung_temperature ;
      }
      break;
    }
  }
  heater_on(1);
}

void heater_led_control(void){
  u16 tmp_seljung=(gCUR.seljung_temp)*10;
  int diff_val=tmp_seljung-gCUR.current_temp;
  if(gCUR.led.b.heater && diff_val<-50 )  heater_on(0);
}

void heater_on(u8 on){
  gCUR.led.b.heater =on;
}

void sub_error_check(void){
  
  static u8 fan_err_cnt=0;
  //gCUR.error_info.data=0;
//fan error check++ 
  if(err_release_flag==0){
    if(gCUR.led.b.fan){
      if(gCUR.ct_adc<THREHOLD_CT){
        fan_err_cnt++;
        if(fan_err_cnt>5) {
          fan_err_cnt=100;
          gCUR.error_info.b.fan_error=1;
        }
      }
//      else {
//        fan_err_cnt=0;
//        gCUR.error_info.b.fan_error=0;
//      }
    }
  }
  else {
    gCUR.error_info.b.fan_error=0;
  }
//fan error check--
#ifdef DEBUG 
//  gCUR.error_info.b.fan_error=0;
#endif 
  
  if(gCUR.temp_adc>1000) gCUR.error_info.b.thermist_short=1;
  else if(gCUR.temp_adc<50) gCUR.error_info.b.thermist_open=1;
  
  if(gCUR.current_temp>900){
    gCUR.error_info.b.over_heater=1;
    GPIO_WriteHigh(GPIOD, GPIO_PIN_3); //pOH_CTRL
  }
//  if(gCUR.oh_temp>999){
//    //printf("gCUR.error_info.b.over_heater\r\n");
//    gCUR.error_info.b.over_heater=1;
//    GPIO_WriteHigh(GPIOD, GPIO_PIN_3); //pOH_CTRL
//  }
//  else GPIO_WriteLow(GPIOD, GPIO_PIN_3); //pOH_CTRL
}

void damper_open(u8 open){
  if(open){
    //open
    gCUR.led.b.damper_open=1;
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5); //pDAMP 
  }
  else{
    //close
    gCUR.led.b.damper_open=0;
    GPIO_WriteLow(GPIOE, GPIO_PIN_5); //pDAMP
  }
}


u8 get_damper_open(void){
  static u8 damper_open=1;
  static signed char damper_cnt=0;
  u16 damper_on_threshold_temp;
  u16 damper_off_threshold_temp;
#ifdef DEBUG  
//  printf("mode[%d] auto_damper[%d]\r\n",gCUR.mode, gCUR.auto_damper);
#endif
  if(gCUR.auto_damper==0) { //auto damper->manual allways open baesub
    damper_open=1;
    return damper_open;
  }
  if(gCUR.pgm_mode==0) { //일반모드
    damper_on_threshold_temp=(gCUR.seljung_temp - DAMPER_ON_OFFSET)*10; //DAMPER_OFFSET
    damper_off_threshold_temp = (gCUR.seljung_temp - DAMPER_OFF_OFFSET)*10;
    //damper_threshold_temp=(damper_threshold_temp*98)/100;//98%도달하면
    if(damper_open){
      if(damper_off_threshold_temp >= gCUR.current_temp) damper_open=0;
    }
    else {
      if(damper_on_threshold_temp <= gCUR.current_temp)damper_open=1;
    }
    
    //if(damper_on_threshold_temp >= gCUR.current_temp) damper_open=0;// gCUR.ctrl.damper_close=1;
   // else damper_open=1;//gCUR.ctrl.damper_close=0;
#ifdef DEBUG  
    printf("pgm_mode[%d] damper_threshold_temp[%d] temp[%d] damper_open[%d]\r\n",gCUR.pgm_mode, damper_threshold_temp, gCUR.current_temp, damper_open);
#endif
    return damper_open;
  }
  
  if(gCUR.ctrl.mode_current_seq==0){
    damper_open=0;//gCUR.ctrl.damper_close=1;
  }
  else if(gCUR.ctrl.mode_current_seq==gMODE_TABLE.baesub_position){
  //do something
   u16 delta=gCUR.current_minute-gMODE_TABLE.ctrl_obj[gMODE_TABLE.baesub_position+1].remain_time;
   if(delta%gMODE_TABLE.baesub_period ==0){
     damper_open=0;
     damper_cnt=gMODE_TABLE.baesub_duration;
   }
   else if(gCUR.led.b.damper_open){
    damper_cnt--;
    if(damper_cnt<=0){
      damper_cnt=0;
      damper_open=1;
    }
   }  
  }
  else{
    damper_open=1;
  }
}

void audio_port_init(){
   GPIO_WriteLow(GPIOF, GPIO_PIN_4); //pAUD2
   GPIO_WriteLow(GPIOD, GPIO_PIN_6); //pAUD1
   GPIO_WriteLow(GPIOD, GPIO_PIN_7); //pAUD0
}

void sub_set_audio(u8 audio_id){
  switch(audio_id){
    case VOICE_NULL:  
      break;   
    case VOICE_NO_CHANGE://설정변경을 할수없습니다.
      GPIO_WriteHigh(GPIOD, GPIO_PIN_7); //pAUD0
      break;
    case VOICE_HI_TEMP://고온경보 확인 바랍니다.
      GPIO_WriteHigh(GPIOD, GPIO_PIN_6); //pAUD1
      break; 
    case VOICE_JANGAE://장애가 발생하였습니다
      GPIO_WriteHigh(GPIOF, GPIO_PIN_4); //pAUD2
      break;  
    case VOICE_TEMP_SELJUNG://ondo seljung finish 
      GPIO_WriteHigh(GPIOD, GPIO_PIN_7); //pAUD0
      GPIO_WriteHigh(GPIOD, GPIO_PIN_6); //pAUD1
      break;  
    case VOICE_END://건조가 완료되었습니다
      GPIO_WriteHigh(GPIOD, GPIO_PIN_7); //pAUD0
      GPIO_WriteHigh(GPIOF, GPIO_PIN_4); //pAUD2
      break; 
    case VOICE_BEGIN://건조가 시작되었습니다
      GPIO_WriteHigh(GPIOF, GPIO_PIN_4); //pAUD2
      GPIO_WriteHigh(GPIOD, GPIO_PIN_6); //pAUD1
      break; 
    case VOICE_TIME_SELJUNG://시간설정이 완료되었습니다.
      GPIO_WriteHigh(GPIOD, GPIO_PIN_7); //pAUD0
      GPIO_WriteHigh(GPIOF, GPIO_PIN_4); //pAUD2
      GPIO_WriteHigh(GPIOD, GPIO_PIN_6); //pAUD1
      break; 
  }
  delay_us(10000);
  audio_port_init();
}

u16 adc_to_volt(u16 adc_value){
  float unit_value=ADC_FULL_SCALE /1024.0f;//* adc_value ;
  return (u16)(unit_value*adc_value);
}

void beep_on()
{
  //printf("BEEP_ON\r\n");
  GPIO_WriteHigh(GPIOD, GPIO_PIN_4); //beep
  Beep_Time=0; 
  Beep_flag=1;
}

void fan_on(u8 on){
  if(on){
    gCUR.led.b.fan=1;
    GPIO_WriteHigh(GPIOC, GPIO_PIN_2);//OFF
  }
  else{
    gCUR.led.b.fan=0;
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);//ON 
  }

}
void delay_us(u32 i)
{
  while(i--);
}
