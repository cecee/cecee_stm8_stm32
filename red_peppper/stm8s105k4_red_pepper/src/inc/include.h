/************************************************************
*	ProjectName:	   LT8522EX
*	FileName:	       lt8522ex.c
*	BuildData:	     2014-03-27
*	Version��        V1.00
* Company:	       Lontium
************************************************************/

#ifndef		_INCLUDE_H
#define		_INCLUDE_H

#include <stdio.h>
#include <stdlib.h>
#include "stm8s.h"
#include "main.h"
#include "string.h"
#include "stm8s_uart1.h"
#include "stm8s_uart2.h"
#include "stm8s_spi.h"
#include "stm8s_beep.h"
#include "stm8s_i2c.h"
#include "stm8s_flash.h"

#include "../util/util.h"
#include "../fnd/fnd.h"   
#include "../thermist/thermist.h"
//#include "../triac/triac.h"
#include "../sub_routine/sub_routine.h"
#include "../key/key.h"

#define xDEBUG
#define xPGM_MODE
#define FUN_BEEP
#define EEPROM_WRITER_DELAY 2 //x10ms  5sec
#define CURR_TIME_FLASH_ADDRESS         0x4000
#define CURR_SELJUNG_TEMP_FLASH_ADDRESS 0x4004
#define MINUTE 60 //2 //60
#define ADC_FULL_SCALE 470
#define DAC_FULL_SCALE 470 
#define PARAMETER_BETA 4016.95f
#define THERMIST_RES 50  //50k

#define ENDING_COOLING 1*MINUTE   //300 // 60*5��
#define MININUM_CONTROL_TEMP 300 // 30��
#define TEMP_READ_OFFSET 20
#define TEMP_SELJUNG_OFFSET 40
#define MAX_TEMP 70 // 30��
#define MIN_TEMP 20 // 30��

enum VOICE_MESSAGE
{
  VOICE_NULL=0, 
  VOICE_NO_CHANGE,
  VOICE_HI_TEMP,
  VOICE_JANGAE,
  VOICE_TEMP_SELJUNG,
  VOICE_END,
  VOICE_BEGIN,
  VOICE_TIME_SELJUNG,
};

enum PROCESS_SEQ
{
  PROCESS_ING=0, 
  PROCESS_END,
  PROCESS_ERR,
};

typedef struct {
  u16 delta;
  u8 sign:1;
}DIFF;

typedef struct {
//  u8 damper_close:1;
  u8 fan:1;
  u8 heater:1;
  u8 mode_current_seq:4;
}bCTRL;

typedef struct {
  u8 eeprom_time_save:1;  
  u8 eeprom_seljung_save:1;  
  u8 blink:1; 
  u8 sec_toggle:1;
  u8 finish_flag:1;
  u8 process_seq:2;
}bFLAG;

typedef struct {
  u8 Power:1;         //A
  u8 fan:1;           //B
  u8 heater:1;        //C
  u8 damper_open:1;  //E
  u8 error:1;         //G
}bLED;

typedef struct {
  u8 thermist_short:1;
  u8 thermist_open:1;
  u8 over_heater:1;
  u8 fan_error:1;  
  u8 heater_error:1;
}bERROR;

typedef union _LED_INFO
{
  bLED b;
  u8 data;
}LED_INFO;

typedef union _ERROR_INFO
{
  bERROR b;
  u8 data;
}ERROR_INFO;

typedef union _EEPROM_DATA
{
  u8  data[4];
  u32 d32;
}EEPROM_DATA;

typedef struct 
{
  u16 remain_time;
  u8 seljung_temperature;
}CTRL_OBJECT;

typedef struct 
{
//  u16 operater_time;
  u8 baesub_period;
  u8 baesub_duration;
  u8 baesub_position;
  CTRL_OBJECT ctrl_obj[5];
}MODE_TABLE;


typedef struct 
{
  u8 pgm_mode;
  u8 auto_damper;//배습
  u8 seljung_temp;//0~255
  u16 ex_set_temp; 
  int current_temp;//
  int oh_temp;//
  u16 dac_ref_volt;
  u16 dac_get_volt;
  u16 dac_pwm;
  u16 DACref;
 // u16 temp_volt;
  u16 temp_adc;
  u16 oh_adc;
  u16 ct_adc;
  u16 current_minute;  
  LED_INFO led;
  bCTRL ctrl;
  bFLAG flg;
  KEY_MAP key;
  ERROR_INFO error_info;
}CURRENT_DATA;

typedef struct {
  u8  voice_num:4;   
  u8  delay:3;
  u8  flag:1; 
}VOICE_INFO;


static const MODE_TABLE mode_table[3] = 
{
    { 0,  0,  1,  {{1680,65},{0,65}    ,{0,65}  ,{0,65} ,{0,65}}},//ilban
    { 3,  1,  1,  {{1680,65},{1380,54} ,{0,54}  ,{0,0}  ,{0,0}}},//pepper
    { 3,  1,  1,  {{30,60}  ,{28,35}   ,{5,35}  ,{0,35} ,{0,0}}} //kogam
};

extern CURRENT_DATA gCUR;
extern MODE_TABLE gMODE_TABLE;
extern VOICE_INFO gVOICE_INFO;

extern u32 one_ms_tick;
extern u32 one_sec_tick;
extern u32 ms_cnt;
extern u16 Beep_Time;
extern u8 Beep_flag;
extern u8 sec_bling_flag;
extern u16 sec_bling_Time;
//extern u8 key_press;
extern u8 seljung_flag;
//extern u16 system_init_count;
extern u8 err_release_flag;
extern u8 fnd_id;
extern int temp_sample_cnt; 
extern int calibration_period;
#endif