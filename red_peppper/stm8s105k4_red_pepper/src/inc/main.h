/************************************************************
*	ProjectName:	  HOT PEPPER
*	FileName:	  main.h
*	BuildData:	  2022-07-01
* Company:	       cecee
************************************************************/

#ifndef		_MAIN_H
#define		_MAIN_H

extern  void GPIO_Initial(void);
extern  void CLK_Configuration(void);
extern  void save_eeprom_seljung_temp();
extern  void save_eeprom_current_time(u32 time);

#define pFLD_GPIO_Port GPIOC
#define pFLD GPIO_PIN_3

#define pLDATA_GPIO_Port GPIOC
#define pHDATA_GPIO_Port GPIOD

#define pDamp_GPIO_Port GPIOB
#define pDamp GPIO_PIN_5
#define pPWM0_GPIO_Port GPIOB
#define pPWM0 GPIO_PIN_4

#define pATH_GPIO_Port GPIOD
#define pATH GPIO_PIN_2
#define pAKEY_GPIO_Port GPIOD
#define pAKEY GPIO_PIN_3


#endif