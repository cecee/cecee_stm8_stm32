#ifndef UTIL_H
#define UTIL_H

#include "stm8s.h"

#define xDEBUG
#ifndef BIT 
#define BIT(x) (1<<(x)) 
#endif 

#define u8  unsigned char
#define u16 unsigned int
#define u32 unsigned long

#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
            
//extern CURRENT_STATUS gValue;
//void _ClearBit(u16 Data, u8 loc);
//void Udelay(u32 dd);
//void Delay(int dd);
//void delay(u32 dly); 
//void delay_ms(u16 dly);
void CUartTxChar(u8 ucValue);
void _printf(u8 *pFmt, u32 wVal);
long _sprintf(char *buf, char *format, long arg);
char *itoa( char *a, int i);
#endif