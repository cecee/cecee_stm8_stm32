#include "include.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

CURRENT_DATA gCUR;
MODE_TABLE gMODE_TABLE;
VOICE_INFO gVOICE_INFO;

const u8 fnd_seq[12] = {0,1,8,2,3,8,4,5,8,6,7,8};

u32 one_ms_tick=0;
u32 one_sec_tick=0;
u32 ms_tick=0;
u32 ms_cnt=0;
u8 fnd_id=0;
//u8 key_press=0;
//u8 seljung_flag=0;

u16 Beep_Time=0;
u16 sec_cnt=0;
u8 Beep_flag=0;
u8 beep_status=0;
u8 sec_bling_flag=0;
u16 sec_bling_Time=0;
//u8 SET_BEEP=1;
//u16 system_init_count=0;
u8 err_release_flag=0;
int fputc(int ch, FILE *f)
{
  u8 temp[1]={ch};
  UART2_SendData8(temp[0]); //udelay(1);
  while(!(UART2->SR&0x40));
  return(ch);
}

void get_current_parameter(void)
{
  u8 data[8]={0x00,};
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  data[0]=FLASH_ReadByte(CURR_TIME_FLASH_ADDRESS+0);
  data[1]=FLASH_ReadByte(CURR_TIME_FLASH_ADDRESS+1);//비교하는 대상
  data[2]=FLASH_ReadByte(CURR_TIME_FLASH_ADDRESS+2);
  data[3]=FLASH_ReadByte(CURR_TIME_FLASH_ADDRESS+3);
  data[4]=FLASH_ReadByte(CURR_SELJUNG_TEMP_FLASH_ADDRESS+0);
  data[5]=FLASH_ReadByte(CURR_SELJUNG_TEMP_FLASH_ADDRESS+1);
  data[6]=FLASH_ReadByte(CURR_SELJUNG_TEMP_FLASH_ADDRESS+2);
  data[7]=FLASH_ReadByte(CURR_SELJUNG_TEMP_FLASH_ADDRESS+3);
  FLASH_Lock(FLASH_MEMTYPE_DATA);
  //gCUR.current_minute=(data[0]<<24)+(data[1]<<16)+(data[2]<<8)+data[3];
  gCUR.current_minute=(data[2]<<8)+data[3];
#ifdef PGM_MODE  
  gCUR.mode=data[5]&0xFF;
#else
  gCUR.pgm_mode=0;
#endif  
  gCUR.auto_damper=data[6]&0xFF;
  gCUR.seljung_temp=data[7]&0xFF;
  gMODE_TABLE=mode_table[gCUR.pgm_mode];
  //gCUR.led.b.DD=1;
  //test++
 // gCUR.error=5;
  //test--
 // printf("[%x][%x][%x][%x]-[%x]\r\n",data[4], data[5],data[6], data[7],(u16)(gCUR.mode));  
  //printf("(1)current_minute[%d] seljung_temp[%d]\r\n", gCUR.current_minute, gCUR.seljung_temp); 
}

void save_eeprom_current_time(u32 time)
{
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  FLASH_ProgramWord(CURR_TIME_FLASH_ADDRESS, time);//4byte
  FLASH_Lock(FLASH_MEMTYPE_DATA);
}

void save_eeprom_seljung_temp(){
  
  EEPROM_DATA ee;
  ee.data[0]=0xaa; //MSB
  ee.data[1]=gCUR.pgm_mode;  
  ee.data[2]=gCUR.auto_damper;
  ee.data[3]=gCUR.seljung_temp;
  //_printf("--save_eeprom data[%x]\r\n",ee.d32);
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  FLASH_ProgramWord(CURR_SELJUNG_TEMP_FLASH_ADDRESS, ee.d32);
  FLASH_ProgramWord(CURR_TIME_FLASH_ADDRESS, gCUR.current_minute);
  //save_eeprom_current_time(gCUR.current_minute);
  FLASH_Lock(FLASH_MEMTYPE_DATA);
}

void CLK_Configuration(void)
{
  // Fmaster = 16MHz 
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  //CLK_PeripheralClockConfig(CLK_PERIPHERAL_I2C, ENABLE);
}



void GPIO_Initial(void)
{
  //GPIO_DeInit(SWIM_GPIO_PORT);
  GPIO_DeInit(GPIOA);
  GPIO_Init(GPIOA, GPIO_PIN_1, GPIO_MODE_OUT_PP_HIGH_FAST); //FA0
  GPIO_Init(GPIOA, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_FAST); //FA1

  GPIO_DeInit(GPIOB);
  GPIO_Init(GPIOB, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2, GPIO_MODE_IN_FL_NO_IT);//pAN0 //THERMIST
  //GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_IN_FL_NO_IT);//pAN1 //KEY
  //GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_IN_FL_NO_IT);//pAN2 //pOH_IN
  //GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_OUT_PP_HIGH_FAST);//pOH_CTRL
  
  GPIO_Init(GPIOB, GPIO_PIN_4|GPIO_PIN_5, GPIO_MODE_OUT_OD_HIZ_FAST);//pFA2 pFA3
  //GPIO_Init(GPIOB, GPIO_PIN_6, GPIO_MODE_OUT_PP_HIGH_FAST);//pFA1
  //GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_FAST);//pFA2
  //GPIO_Init(GPIOB, GPIO_PIN_4, GPIO_MODE_OUT_PP_HIGH_FAST);//pFA3
  
  GPIO_DeInit(GPIOC);
  GPIO_Init(GPIOC, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7, GPIO_MODE_OUT_OD_HIZ_FAST);//pHEATER
  GPIO_Init(GPIOC, GPIO_PIN_1|GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_FAST);//pHEATER //damper
 
  GPIO_DeInit(GPIOD);
  GPIO_Init(GPIOD, GPIO_PIN_0, GPIO_MODE_OUT_OD_HIZ_FAST);//FD5
  GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_OUT_OD_HIZ_FAST);//FD6
  GPIO_Init(GPIOD, GPIO_PIN_5, GPIO_MODE_OUT_OD_HIZ_FAST);//FD7
  GPIO_Init(GPIOD, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST);//pOH_CTRL
  GPIO_Init(GPIOD, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST);//beep
  GPIO_Init(GPIOD, GPIO_PIN_6, GPIO_MODE_OUT_PP_HIGH_FAST);//pAU1
  GPIO_Init(GPIOD, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_FAST); //pAU0
  
  
  GPIO_DeInit(GPIOE);
  GPIO_Init(GPIOE, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST);//pDAMP
  
  GPIO_DeInit(GPIOF);
  GPIO_Init(GPIOF, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST); //pAU2
  
}

void Timer1_Init(void) //pwm
{

  u16 TIM1_Period=1024;//10000;//1023;
  u16 pwm_init=512;//0;
  TIM1->CR1 &= ~ BIT(0); // Close TIM1 
  TIM1->PSCRH = 0; 
  TIM1->PSCRL = 0; // 0: 16.5KHZ 7:1.9KHZ  8:1.72KHZ
  TIM1->ARRH = TIM1_Period>>8; 
  TIM1->ARRL = TIM1_Period&0x00ff;

//  TIM1->CCR1H = pwm_init>>8;
//  TIM1->CCR1L = pwm_init&0x00ff; //
//  TIM1->CCR2H = pwm_init>>8;
//  TIM1->CCR2L = pwm_init&0x00ff; // 
  //TIM1->CCR3H = pwm_init>>8;
  //TIM1->CCR3L = pwm_init&0x00ff; // 
  TIM1->CCR1H = pwm_init>>8;
  TIM1->CCR1L = pwm_init&0x00ff; //   
  
  TIM1->CCMR1 = 0x60; //  CH1 PWM mode 1
  //TIM1->CCMR2 = 0x60; //  CH2 PWM mode 1
  //TIM1->CCMR3 = 0x60; //  CH2 PWM mode 1
  //TIM1->CCMR4 = 0x60; //  CH2 PWM mode 1
// ch12
  TIM1->CCER1 |= BIT(0); //  Enable OC1(CH1)
//  TIM1->CCER1 |= BIT(4); //  Enable OC2(CH2)
//ch34  
//  TIM1->CCER2 |= BIT(0); //  Enable OC2(CH3)
 // TIM1->CCER2 |= BIT(4); // Enable OC2(CH4)
  TIM1->CR1 |= BIT(0); //  enable TIM1 
  TIM1->BKR |= BIT(7); //  prohibit brake 
}

void Timer2_Init(void)//1msec
{
  //1ms 
  TIM2->SR1 = 0;     // clear overflow flag
  TIM2->PSCR = 7;    // 8us// Prescaler to divide Fcpu by 128: 8 us clock.// Max CPU freq = 16 MHz
  TIM2->ARRH = 0x00;//0x04; // per10msec, 16bit
  TIM2->ARRL = 0x7d;//0xE2;
  TIM2->IER |= 0x1;  // Update interrupt enabled
  TIM2->CR1 |= 0x1;  // Counter enabled  
}
#if 0
void Timer4_Init(void) //100us
{
  //1/2/4/8/16/32/64/128
  TIM4->SR1 = 0;       // clear overflow flag
  TIM4->PSCR = 6;     // Prescaler to divide Fcpu by 64: 4 us clock.// Max CPU freq = 16 MHz
  TIM4->ARR = 25;    // 25*4 = 100 us. 10KHZ
  TIM4->IER = 0x01;   // Enable interrupt
  TIM4->CR1 = 0x01;   // Start timer
}
#endif

main()
{
  CLK_Configuration();
  GPIO_Initial();
  
#ifdef DEBUG  
  UART2_DeInit(); 
  UART2_Init(115200, UART2_WORDLENGTH_8D, UART2_STOPBITS_1, UART2_PARITY_NO, UART2_SYNCMODE_CLOCK_DISABLE, UART2_MODE_RX_DISABLE|UART2_MODE_TX_ENABLE);//UART2_MODE_RX_DISABLE//UART2_MODE_TXRX_ENABLE
#endif
  Timer1_Init();//pwm 
  Timer2_Init();//1msec
//  Timer4_Init();//100usec
  memset(&gCUR,0,sizeof(gCUR));
  audio_port_init();
  get_current_parameter();
  enableInterrupts();
  
  gCUR.ex_set_temp=0;
  if(gCUR.current_minute)sub_set_audio(VOICE_BEGIN);
  else sub_set_audio(VOICE_END);
  //sub_set_audio(VOICE_HI_TEMP);
#ifdef DEBUG   
  printf("start red-pepper !!\r\n");
#endif  
  fan_on(1);
  set_heater_pwm();
 // set_temp_pwm((gCUR.seljung_temp+10)*10);
  while (1)
  {
    main_loop();
  }
}

#if 0
#pragma vector=7
__interrupt void EXTI_PORTC_IRQHandler(void)
{
   //_printf("HZ60_cnt[%d]\n\r",HZ60_cnt);
  // HZ60_cnt=0;
  // HZ60_flg=0;
   //GPIO_WriteLow(GPIOC, GPIO_PIN_7);
}


#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  //u8 data;
 // data = UART1_ReceiveData8();
 // put_qUart1(data);
}

#endif


//==================================
//       TIMER4 100usec Timer
//==================================
#pragma vector = 25
__interrupt void _100us_ISR(void)
{
}


/////////////////////////
//  TIMER1 10ms Timer
/////////////////////////
#if 0
#pragma vector=0xD
__interrupt void _10ms_ISR(void)
{
  TIM1->SR1 = 0;
}

#endif


/////////////////////////
//  TIMER2 1ms Timer
/////////////////////////
#pragma vector = 0x0F
__interrupt void Timer2_1ms(void)
{
  static u8 key_tick=0;
  TIM2->SR1 = 0;
  one_ms_tick++;
  key_tick++;
  Beep_Time++;
  sec_bling_Time++;
  fnd_id++;
  //if(fnd_id >11)fnd_id=0;
  if(fnd_id >16)fnd_id=0;
//FOR LED BRIGHT UP +++
//  FND_Update(fnd_seq[fnd_id]);
    if(fnd_id%2==0) {
    FND_Update(8); 
  }
  else{
   FND_Update(fnd_id/2);
  }
//FOR LED BRIGHT UP ---
  
  if(key_tick>200){
    key_tick=0;
    get_adc_key();
    get_adc();
  }
  
  if(one_ms_tick>1000){
    one_ms_tick=0;
    one_sec_tick++;
    
    if(one_sec_tick>MINUTE){
      one_sec_tick=0;
      sub_one_min();
    }
    sub_one_sec();
  }
  //GPIO_WriteReverse(GPIOA, GPIO_PIN_1);
  if(Beep_flag && Beep_Time>50){
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    Beep_flag=0;
  }
  if(sec_bling_flag && sec_bling_Time>500){
    sec_bling_flag=0;
  }
}

