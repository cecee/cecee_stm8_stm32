//#include "stm32f1xx_hal.h"

#include "include.h"
#include "thermist.h"
#include "math.h"

float filterValue;
float oh_filterValue;

int __pow(int down, int up)
{
    int round;
    int val = 1;
    for(round = 0; round < up; round++)
        val *= down;
    return val;
}

void floatToStr(char *strMsg, float f, int underPointNum)
{
  char strBuf[32];
  int pointOver, pointUnder;
    
    pointOver = (int)f;
    pointUnder = (f-pointOver)*(__pow(10, underPointNum));
    sprintf(strBuf, "%d.%d", pointOver, pointUnder);
#ifdef DEBUG    
    printf("%s:%s\r\n",strMsg, strBuf);
#endif    
    return;
}

float get_adc_filter(u16 adc, u8 idx)
{
  float sensitivity=0.5f;
  if(idx==0){
    filterValue=((filterValue*(1-sensitivity))+(adc*sensitivity));
    return filterValue;
  }
  else {
    oh_filterValue=((oh_filterValue*(1-sensitivity))+(adc*sensitivity));
    return oh_filterValue;
  }
}

int get_temperature(u16 adc)
{
  u16 vcc=ADC_FULL_SCALE;
  float Rbase=51.0f;//51k
  float fV,fR ,tempC;
  float ParmB= PARAMETER_BETA;//3950.0f;
  long ohm;
  int tempx10;
#ifdef DBG
 // printf("\r\n----\r\nadc_temp[%d]\r\n", adc_temp);
#endif  
  //fV=(float)(adc_temp /2.50f);
  //fV=(float)(adc*0.48828f);//500(5V*100)/1024//vcc/1024
  fV=(float)(adc*(vcc/1024.0));//500(5V*100)/1024//vcc/1024
  //fR=((vcc*50)/fV)-Rbase;//kohm
  fR=((vcc*Rbase)/fV)-Rbase;//kohm
  //floatToStr((char *)"fR",fR,3);
  ohm=(long)(fR*1000);//floatx1000(fR);
 // gCUR.temp_volt=(u16)(fV);
  //printf("gCUR.temp_volt[%d]\r\n", gCUR.temp_volt);
#ifdef DBG
 //printf("adc[%d] temp_volt[%d]\r\n", adc, (int)fV);
#endif 
  //tempC =(ParmB/(log(ohm/112197.33f)+(ParmB/(273.15f+25.0f)))) -273.15f;
  tempC =(ParmB/(log(ohm/50000.0f)+(ParmB/(273.15f+25.0f)))) -273.15f;
  //tempC=25;
  //floatToStr((char *)"fV",fV,3);
  //floatToStr((char *)"fI",fI,4);
  //floatToStr((char *)"fR",fR,2);
#ifdef DBG  
  //floatToStr((char *)"tempC",tempC,2);
#endif  
  //gCUR.current_temp=(int)(tempC*10);
  tempx10=(int)(tempC*10);
  //printf("ohm[%d] tempx10[%x]\r\n", ohm, tempx10);
  return tempx10;
}

u16 temp_to_ohm(u16 temp)
{
  u16 ohm;
  float ftemp=(temp+5)/10;
  float ParmB=PARAMETER_BETA;
  float To=298.15f;
  float T=ftemp+273.15;//+0.5;//+0.5 mean temp+0.5

  u16 Ro=50000;//TM_Ref Resistor
  ohm=(u16) (Ro*exp((ParmB/T)-(ParmB/To)));
#ifdef DBG
 // printf("ohm[%x]\r\n", ohm);
#endif 
  return ohm;
}

void set_pwm(u16 pwm)
{
  pwm=(pwm>1024)?1024:pwm;
  TIM1->CCR1H = pwm>>8;
  TIM1->CCR1L = pwm&0x00ff; 
  gCUR.dac_pwm=pwm;
}

void set_temp_pwm(u16 temp)
{
  u16 lohm, lvolt, lpwm;
  u16 ltemp;
  gCUR.ex_set_temp=temp;
  if(temp<20){
   set_pwm(0);
   return;
  }
  ltemp= temp + TEMP_SELJUNG_OFFSET;
  lohm = temp_to_ohm(ltemp);
  lvolt=(u16)((51000/(51000.0f + lohm)) * ADC_FULL_SCALE);
  lpwm= (u16)((1024.0f/DAC_FULL_SCALE) * lvolt);
  gCUR.dac_ref_volt=lvolt;
  #ifdef DEBUG   
    printf("### \r\n set_temp[%d] lohm[%d] lvolt[%d] pwm[%d]\r\n",ltemp, lohm, lvolt, lpwm);
  #endif 
  set_pwm(lpwm);
}
//u16 temp_to_adc(u16 temp)
//{ 
//  u16 adc=0;
//  u16 ltemp;
//  u16 i;
//  for(i=512;i<920;i++){//25~90
//    ltemp=get_temperature(i);
//    //printf("ltemp[%d][%d][%d]\r\n", i, ltemp, temp);
//    if(ltemp>temp){
//     //  printf("ltemp[%d][%d][%d]\r\n", i, ltemp, temp);
//     adc=i;
//     break;
//    }
//  }
//  return adc;
//}
