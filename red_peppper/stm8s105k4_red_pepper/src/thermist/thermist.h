/*
dht22.h
*/
#ifndef _THERMIST_H_
#define _THERMIST_H_


int __pow(int down, int up);
void floatToStr(char *strMsg, float f, int underPointNum);
u32 floatx1000(float f);
//void get_temperature(void);
int get_temperature(u16 adc);
float get_adc_filter(u16 adc, u8 idx);
//float get_temperature_filter(u16 temp);
//float get_over_heater_filter(u16 oh_adc);
u16 temp_to_ohm(u16 temp);
u16 temp_to_adc(u16 temp);
void set_temp_pwm(u16 temp);
void set_pwm(u16 pwm);

#endif