#ifndef UTIL_H
#define UTIL_H

#include "stm8s.h"

#define DEBUG
#ifndef BIT 
#define BIT(x) (1<<(x)) 
#endif 

#define IR2104

#define u8  unsigned char
#define u16 unsigned int
#define u32 unsigned long

#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사

typedef struct 
{
  u8 sign;
  u8 ex_sign;
  u8 stepDnFlag;
  u8 relay;
  u8 dir;
  u8 stop;
  u8 changing_dir;
  u8 buffer_zero;
  u8 state; 
  u8 break_state; 
  u8 zero_cnt;
  u16 wait_msec;
  u16 delta;
  u16 ctrlValue;
  u16 ex_ctrlValue;
  u16 f_percent;
  u16 r_percent;
  u16 ex_pwm;
  u16 bank[4][6];
  int b4_break;
}CURRENT_STATUS;

extern CURRENT_STATUS gValue;
extern u8 BREAK_FLAG;
extern u8 DIRECTION_FLAG;
extern u8 DECAY_FLAG;
extern int break_cnt;

void Udelay(u32 dd);
void Delay(int dd);
void delay(u32 dly); 
void delay_ms(u16 dly);
void CUartTxChar(u8 ucValue);
//void _printf(u8 *pFmt, u32 wVal);
//long _sprintf(char *buf, char *format, long arg);
char *itoa( char *a, int i);
#endif