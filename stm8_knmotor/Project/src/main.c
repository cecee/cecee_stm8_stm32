
/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "stm8s_uart1.h"
#include "stm8s_spi.h"
#include "stm8s_exti.h"
#include "stm8s_gpio.h"
#include "stm8s_flash.h"
#include "stm8s_tim1.h"
#include "stm8s_tim4.h"
#include "stm8s_beep.h"
#include "stm8s_i2c.h"
#include "./kn_motor/kn_motor.h"
#include "./kn_pwm/kn_pwm.h"
#include "./util/util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AFTER_MSEC  300 //pwm0 이후 Stop Delay millisec  ?? 300->400ms delay
#define WAIT_MSEC   300 //power on delay


void Beep_On(void);
void CLK_Configuration(void);
void GPIO_Configuration(void);

//////////////////////

void Timer1_Init(void);
void Timer4_Init(void);
void Timer2_Init(void);
u8 i;

void delay_delay(unsigned int mul);

u16 timer4_cnt=0;
u32 timer_1ms=0;
u8  time_10ms_cnt=0;

//u16 Wait_tmp=0;
u16 Beep_Time=0;
u16  sec_cnt=0;
u8 Beep_flag=0;
u8 SET_BEEP=1;
u8 Flag_1ms=0;
u8 Flag_10ms=0;

CURRENT_STATUS gValue;

int fputc(int ch, FILE *f)
{
  u8 temp[1]={ch};
  //HAL_UART_Transmit(&huart4, temp, 1, 2);
  CUartTxChar(temp[0]); 
  return(ch);
}

void BEEP_ON()
{
  Beep_Time=0; 
  Beep_flag=1;///
  BEEP_Cmd(ENABLE); //
}

u8 get_state(){
  static u8 ex_dir=-1;
  u8 state;
 // printf("[%03d][%03d] b4[%03d]\r\n",gValue.ex_ctrlValue, gValue.ex_pwm, gValue.b4_break);
  if(gValue.buffer_zero || gValue.dir==2) {
    gValue.buffer_zero=0;
    state=0; 
  }
  else if(gValue.ex_ctrlValue>10) state=1;

  if(ex_dir != gValue.dir){
    //set_diretion_led(2); 
    ex_dir=gValue.dir;
//    if(ex_dir==2) state=0;
    state=3; 
  }
//-----------------------
  if(gValue.state==0xA0) state=gValue.state;
  return state;
}

void change_subroutine(u8 state){
  printf("#state(%d)\r\n", state);
  switch(state){
    case 0://stop
      printf("#stop(%03d)\r\n", gValue.b4_break);
      motor_stop(gValue.b4_break);
      if(gValue.b4_break>AFTER_MSEC)delay_ms(AFTER_MSEC);
      else delay_ms(gValue.b4_break);
      set_brake(1); //브레이크 ON
      gValue.break_state=2; 
      break;
     case 1: //run
      gValue.dir=get_direction();
      set_diretion_led(gValue.dir); 
      gValue.break_state=0;
      set_brake(0);//break release(해재)
      break;
     case 2: //fast dn
     // slow_decay();
       break;
    case 3://change direction
      //printf("#1 change direction\r\n");
      motor_stop(gValue.ex_pwm);
      delay_ms(AFTER_MSEC);
      set_diretion_led(gValue.dir);    
      printf("#2 change direction->%d\r\n", gValue.dir);
      break; 
  }
 // gValue.ex_pwm=0;
}

void done_subroutine(u8 state){
 // printf("done_subroutine[%x][%03d] [%03d]\r\n", state, break_cnt, gValue.wait_msec);
  switch(state){
    case 0://stop
      break;
    case 1: //run
      break;
    case 2: //fast dn
      break;
    case 3://change direction
      break;
    case 0xA0:
      if(gValue.wait_msec>WAIT_MSEC) {
        gValue.state=0xA1;
      }
      break;
  }

}


void main(void)
{
  u8 state, ex_state;
  CLK_Configuration(); /* Configures clocks */
  GPIO_Configuration();/* Configures GPIOs */
  UART1_DeInit(); // Initialize serial
  UART1_Init(115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_ITConfig(UART1_IT_RXNE,ENABLE); //test
  set_relay(0);
  
  printf("(1)Start!!\r\n"); 
  Udelay(100000);//1sec
  //printf("(2)car_init!!\r\n"); 
  //set_relay(0);
  
  Timer2_Init();  //10ms
  Timer4_Init();  //1ms
  PWM_init(); //Timer1 PWM  
  car_init();
  enableInterrupts();   
  ex_state=0xA0;
  while (1)
  {
    gValue.state=get_state();
    if(ex_state != gValue.state){
      change_subroutine(gValue.state);
    }
    done_subroutine(gValue.state);
    ex_state=gValue.state;
  }
   
}

#if 1
//===================================================
//             TIMER2 1millisec Timer
//===================================================
void Timer2_Init() {
  TIM2->SR1 = 0;     // clear overflow flag
  TIM2->PSCR = 7;    // Prescaler to divide Fcpu by 128: 8 us clock.// Max CPU freq = 16 MHz
  TIM2->ARRH = 0x04; // per10msec, 16bit
  TIM2->ARRL = 0xE2;
  TIM2->IER |= 0x1;  // Update interrupt enabled
  TIM2->CR1 |= 0x1;  // Counter enabled
}

#pragma vector = 0x0F
__interrupt void Timer2_10ms(void)
{
  TIM2->SR1 = 0;
 // GPIO_WriteReverse(GPIOC, pBK);
  sub_10ms();
  //printf(".");
}

#endif

//==================================
//       TIMER4 1mSec Timer
//==================================
void Timer4_Init(void)
{
  TIM4->SR1 = 0;       // clear overflow flag
  TIM4->PSCR = 7;     // Prescaler to divide Fcpu by 128: 4 us clock.// Max CPU freq = 16 MHz
  TIM4->ARR = 125;    // 125*4*2 = 1000 us.
  TIM4->IER = 0x01;   // Enable interrupt
  TIM4->CR1 = 0x01;   // Start timer
}

#pragma vector = 25
__interrupt void Timer4_1ms(void)
{
  static u8 ms=0;
  
  if(gValue.state>=0xA0) {
    if( gValue.ctrlValue>5) {
      ;//gValue.wait_msec=0;
    }
    else gValue.wait_msec++; 
  }
 if(gValue.break_state==2) break_cnt++;
 if(gValue.state==1) PWM_1ms();
  //GPIO_WriteReverse(GPIOC, pBK);
  TIM4->SR1 = 0;      // clear overflow flag  
  //Wait_tmp++;
  //Beep_Time++;

}

#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  //u8 data;
  //data = UART1_ReceiveData8();

}

#if 0
#pragma vector=6 //5A 6B 7D 
__interrupt void EXTI_PORTB_IRQHandler(void)
{
   // disableInterrupts();
   // Udelay(5000);//20msec
   PWR_IRQ=1;

}

#pragma vector=8 //5A 6B 7D 
__interrupt void EXTI_PORTD_IRQHandler(void)
{
  //if(GPIO_ReadInputPin(GPIOD,EMER_IN)==0) EMER_CNT++;
  //else if(GPIO_ReadInputPin(GPIOD,COIN_IN)==0) {
  //	COIN_CNT++;
  //}
}
#endif

void CLK_Configuration(void)
{
  /* Fmaster = 16MHz */
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  //CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER4, DISABLE);
  //CLK_PeripheralClockConfig(CLK_Peripheral_TypeDef CLK_Peripheral, FunctionalState NewState);
  //CLK-> CKDIVR | = 0x00;
}

void GPIO_Configuration(void)
{

  GPIO_DeInit(GPIOA); /* IR-Port */
   
  GPIO_DeInit(GPIOB);/* GPIOB reset */
  //GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_IN_PU_NO_IT);  
  //GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_IN_PU_NO_IT); 
  //GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_IN_PU_NO_IT); 
  //GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_IN_PU_NO_IT); 
  
  GPIO_Init(GPIOB, DIR_FF, GPIO_MODE_IN_PU_NO_IT);   
  GPIO_Init(GPIOB, DIR_RR, GPIO_MODE_IN_PU_NO_IT);

//LED_CTL MRES_PIN
  GPIO_DeInit(GPIOC); // C-Port 
  
//pPWM0    (GPIO_PIN_1) 
//pRTN     (GPIO_PIN_3) 
//pBK      (GPIO_PIN_4) 
//pBISANG  (GPIO_PIN_5) 
//pDIR_EN  (GPIO_PIN_6) 
//pDIR     (GPIO_PIN_7) 
  
  GPIO_Init(GPIOC, pRTN, GPIO_MODE_IN_PU_NO_IT); 
  GPIO_Init(GPIOC, pBK, GPIO_MODE_OUT_PP_LOW_FAST);   
  GPIO_Init(GPIOC, pBISANG, GPIO_MODE_OUT_PP_HIGH_FAST);   
  GPIO_Init(GPIOC, pDIR_EN, GPIO_MODE_OUT_PP_LOW_FAST);  
  GPIO_Init(GPIOC, pDIR, GPIO_MODE_OUT_PP_HIGH_FAST);
//  GPIO_Init(GPIOC, pDIS, GPIO_MODE_OUT_PP_HIGH_FAST);


 //----------------------------------------------------------------------
  GPIO_DeInit(GPIOD); 
  GPIO_Init(GPIOD, pLED_FF, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOD, pLED_RR, GPIO_MODE_OUT_PP_HIGH_FAST);
  //GPIO_Init(GPIOD, pM2_RELAY, GPIO_MODE_OUT_PP_LOW_FAST);
  
  //---------------------------------------------------------------------
 
  GPIO_DeInit(GPIOE); 
  GPIO_Init(GPIOE, pRELAY, GPIO_MODE_OUT_PP_LOW_FAST);

  GPIO_DeInit(GPIOF); 
  //GPIO_Init(GPIOF, MUTE_RELAY, GPIO_MODE_OUT_PP_HIGH_SLOW);//MUTE
#if 0
  EXTI_DeInit();    
 // EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOB, EXTI_SENSITIVITY_FALL_LOW);
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_FALL_ONLY);
  EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  
 // EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  
#endif
  
}


void delay_delay(unsigned int mul)
{
   for(; mul>0; mul--);
}









