#include "kn_pwm.h"
#include "stm8s_gpio.h"
#include "../kn_motor/kn_motor.h"
#include "../util/util.h"
#include <stdio.h>

#define RR_PERCENT 100  //후진 퍼센트
#define FET_MAX  980L //1024
#define SLOW_DECAY 1024L //

u16 TIM1_Period=1024;//10000;//1023;

void PWM_init(void)
{
          
  u16 pwm_init=0;
  
  TIM1->CR1 &= ~ BIT(0); // Close TIM1 
  TIM1->PSCRH = 0; 
  TIM1->PSCRL = 0; // 0: 16.5KHZ 7:1.9KHZ  8:1.72KHZ

  TIM1->ARRH = TIM1_Period>>8; 
  TIM1->ARRL = TIM1_Period&0x00ff;

  TIM1->CCR1H = pwm_init>>8;
  TIM1->CCR1L = pwm_init&0x00ff; //
  TIM1->CCR2H = pwm_init>>8;
  TIM1->CCR2L = pwm_init&0x00ff; // 
  
  TIM1->CCMR1 = 0x60; //  CH1 PWM mode 1
  TIM1->CCMR2 = 0x60; //  CH2 PWM mode 1
  
  TIM1->CCER1 |= BIT(0); //  Enable OC1(CH1)
  TIM1->CCER1 |= BIT(4); //  Enable OC2(CH2)

  TIM1->CR1 |= BIT(0); //  enable TIM1 
  TIM1->BKR |= BIT(7); //  prohibit brake 
  
 // ---------------------------------
}


void set_pwm2(u16 pwmA, u16 pwmB)
{
  TIM1->CCR1H = pwmA>>8;
  TIM1->CCR1L = pwmA&0x00ff; //
  TIM1->CCR2H = pwmB>>8;
  TIM1->CCR2L = pwmB&0x00ff; // 
}



void set_relay2_with_decay(u8 m1, u8 m2)
{
  printf("[%d][%d]\r\n",m1,m2);
  set_pwm2(0, 0);
  delay_ms(10);
  //set_relay2(m1,m2);
  delay_ms(20);// 20ms wait for RELAY OPERATING(7ms)
  
}

void set_pwm_frequency(u16 div)
{
    TIM1->PSCRH = div>>8;
    TIM1->PSCRL = div&0xFF; // 0: 16.5KHZ 7:1.9KHZ  8:1.72KHZ
    
}


void slow_decay()
{
   
    //ldecay=20;//14:00 test
    //ldecay =15;//약간뒤로밀림
    u16 ldecay=20;
    printf("SLOW DECAYING[%d]~~\r\n",ldecay);
    //set_relay2_with_decay(0,0);
    set_pwm2(SLOW_DECAY, SLOW_DECAY);
    delay_ms(200);// 
    set_pwm2(0, 0);
    gValue.ex_pwm=0;
  
}

void decay(u16 time){
    set_pwm2(0, 0);
    delay_ms(time);// 
}

void fast_decay(u16 time)
{
   
    //ldecay=20;//14:00 test
    //ldecay =15;//약간뒤로밀림
    u16 i;
    u16 ldecay;
    u8 step=6;
    u16 step_value, step_time_ms, ltime;
    ltime=time*2;
    if(ltime>600)  ltime=600;
    step_time_ms=ltime/step;
    step_value=1000/step;
    
    ldecay=1000;
    printf("FAST DECAYING[%d]~~\r\n",ldecay);
    //slow decay

     
#ifdef IR2104   
    set_pwm(0);
     delay_ms(300);//
#else    
    //test++
     //set_relay2_with_decay(0,0);
//    set_pwm_frequency(512);//16->948HZ  32->485hz //64->246 //128 124HZ //256 //62HZ
    set_pwm2(0, 0);
    
    for(i=0;i<6;i++){
     // if(i==3)set_pwm_frequency(250);
      //else set_pwm_frequency(128);
      set_pwm2(i*step_value, i*step_value);
      delay_ms(step_time_ms);//480ms
    }
    set_pwm2(800, 800);
     delay_ms(50);//
    set_pwm2(1024, 1024);
    delay_ms(200);//
 //   set_pwm_frequency(0);//16Khz
 //   while(1);
    //test--
#endif    
    set_pwm2(0, 0);
    gValue.ex_pwm=0;
  
}



void set_pwm(u16 req_pwm)
{
    set_pwm2(req_pwm, req_pwm);
    gValue.ex_pwm=req_pwm;
}


void PWM_1ms(void)
{

  long req_pwm;
  int mControlPWM;
  u16 percent;

  if(gValue.state!=1)    return;
  // percent=(gValue.dir==0) ? gValue.f_percent: RR_PERCENT;
  
  percent=(gValue.dir==0) ? gValue.f_percent: (gValue.r_percent*RR_PERCENT/100L);
  req_pwm= ((gValue.ctrlValue/100L) * percent) + ((gValue.ctrlValue%100L) * percent)/100L;
  
  if(req_pwm>gValue.ex_pwm){
    mControlPWM= gValue.ex_pwm+1;
    //if(req_pwm <=mControlPWM) mControlPWM=req_pwm;
    mControlPWM = (mControlPWM>=FET_MAX) ? FET_MAX :mControlPWM;
  }
  else if(req_pwm<gValue.ex_pwm) {
    mControlPWM =  gValue.ex_pwm- 1;
    if(mControlPWM<=0) mControlPWM=0;
  }
  else mControlPWM=  gValue.ex_pwm;
  set_pwm(mControlPWM);

}


