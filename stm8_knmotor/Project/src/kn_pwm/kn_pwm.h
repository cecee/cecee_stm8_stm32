#ifndef KN_PWM_H
#define KN_PWM_H
#include "../util/util.h"

void PWM_init(void);
void set_pwm(u16 pwm);
void set_pwm2(u16 pwmA, u16 pwmB);
void set_relay2_with_decay(u8 m1, u8 m2);
void decay(u16 time);
void slow_decay(void);
void fast_decay(u16 time);
void PWM_1ms(void);
void set_pwm_frequency(u16 div);
#endif


