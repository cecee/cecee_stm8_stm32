#define SCL  (GPIO_PIN_4) 
#define SDA  (GPIO_PIN_5) 

#if 1
#define CH7026A_ADDR 0xEC //AS 0
#define CH7026B_ADDR 0xEA //AS 1
#else
#define CH7026B_ADDR 0xEC //AS 0
#define CH7026A_ADDR 0xEA //AS 1
#endif
typedef unsigned char	uint8;

u8 I2CWrite(u8 dev, u8 reg, u8 val);

u8 i2c_write_byte(u8 Addr, u8 Reg, u8 Data);
u8 I2C_ReadByte(u8 dev, u8 Reg);

void udelay(u32 dd);

void I2C_CLOCK();
void I2C_START();
void I2C_RESTART();
void I2C_STOP();
void I2C_9BIT_WR(u8 dev_add);
u8 I2C_9BIT_RD();

void init_ch7026();
