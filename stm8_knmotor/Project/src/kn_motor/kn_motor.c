#include "stm8s.h"
#include "stm8s_gpio.h"
#include "kn_motor.h"
#include "../kn_pwm/kn_pwm.h"
#include <stdio.h>
#include <string.h>
 

#define A_NORRMAL 1     //normal :기울기 y=ax+b 의 a
#define A_STOPING  4    //normal :기울기 y=ax+b 의 a
#define B_OFFSET   180L //offset :y=ax+b 의 b 180L=>0
#define SECOND_POLL 60 //Stop시 기울기가 변화는 Point  value
#define TROTTLE_MAX  870L //


//static CURRENT_STATUS gValue;

//u16 bank[4][6];
u8 ex_sign=0;
u16 continueDecCnt=0;
u8 BREAK_FLAG=0;
u8 DECAY_FLAG=0;
u16 DECAY=0;
int break_cnt=0;

//u8 DIRECTION_FLAG=0;
u16 direction_delay_cnt=0;
u16 direction_delay_ref=0;

void car_init(void)
{

  gValue.stepDnFlag=0;
  gValue.stop=1;
  gValue.wait_msec=0;
  gValue.changing_dir=0;
  gValue.dir=get_direction(); 
  gValue.state=0xA0;
  set_pwm_frequency(0);
  set_pwm(0); 
  set_diretion_led(gValue.dir);
  set_relay(1);
  delay_ms(20);
  GPIO_WriteHigh(GPIOC, pDIR_EN);
  //gValue.bisang=get_bisang();
 //  set_brake(0); //브레이크 ON
  
}


void set_relay(u8 on)
{
  if(on) GPIO_WriteHigh(GPIOE, pRELAY);
  else GPIO_WriteLow(GPIOE, pRELAY);
  gValue.relay=on;
}

/*
void sub_1ms(void)
{

//  if(gValue.seqStatus<2) 
//  {
//       set_pwm(0);
//       return;
//  }
  //if(DECAY_FLAG) return;
  //if(BREAK_FLAG)  break_cnt++;
  //printf(".");
  if(gValue.state==0) break_cnt++;
  else if(gValue.state==1) break_cnt=0;
 // set_PWM();
}
*/
u8 get_direction(void)
{
  ///dir 0:전진 1:후진 2:정지
  u8  dir=2;
  u8 ff=0, rr=0, tmp_f, tmp_r;
  
  ff = GPIO_ReadInputPin(GPIOB, DIR_FF);
  rr = GPIO_ReadInputPin(GPIOB, DIR_RR);
  delay_ms(2);
  tmp_f = GPIO_ReadInputPin(GPIOB, DIR_FF);
  tmp_r = GPIO_ReadInputPin(GPIOB, DIR_RR);
  
  
  if(ff) ff=1; else ff=0;
  if(rr) rr=1; else rr=0;
  if(tmp_f) tmp_f=1; else tmp_f=0;
  if(tmp_r) tmp_r=1; else tmp_r=0;
  
  
 // printf("(1)get_direction[%d]\r\n",ff);
 // if(tmp_f !=ff || tmp_r !=rr ) return 0;//ff
  
  //if(ff ==0 && rr ==0 ) dir=0;//rr
  if(ff ==1 && rr ==1 ) dir=1;//ff
  else dir =0;
  return dir;
}

void sub_10ms(void)
{
  u8 bisang;
  u16 raw0,raw1, f_raw;//,raw2,raw3;
  u16 lpercent;//0~100

  bisang = get_bisang();
  if( gValue.relay && !bisang) {
    printf("Set BISANG.....\r\n");
     GPIO_WriteLow(GPIOC, pDIR_EN);//PWM forcely to ZERO
     set_pwm2(0, 0);
     set_relay(0);
      return;
  }
  raw0 = get_adc(0);//trottle
  raw1 = get_adc(1);//ff percent
  
#if 0
 //test++
  if(raw0 <512) raw0=(raw0*10)/15; //512->341
 else if(raw0 <768) raw0=raw0-171;//512-171=341
 else  raw0=(raw0*2)-768;//256;
  //test--
#endif 
 if(raw0>=1024)raw0=1024;
 if( raw0<B_OFFSET){
   gValue.zero_cnt++;
   if(gValue.zero_cnt>40) {
      gValue.b4_break=(int)gValue.ex_pwm;
      gValue.buffer_zero=1;
      gValue.zero_cnt=200;
     }
 }
 else {
  gValue.zero_cnt=0;
 }
  f_raw=moving_average_filter(raw0,0);
  lpercent =moving_average_filter(raw1,1);
  gValue.f_percent =quantized_percent(lpercent);
  gValue.r_percent =gValue.f_percent;
  gValue.ex_sign=gValue.sign;
  gValue.ex_ctrlValue=gValue.ctrlValue;
  gValue.ctrlValue=quantized(f_raw, B_OFFSET);  
  gValue.sign= (gValue.ctrlValue>=gValue.ex_ctrlValue) ? 0:1; //0 increase 1:decrease  
  gValue.delta =(gValue.sign) ? gValue.ex_ctrlValue-gValue.ctrlValue : gValue.ctrlValue-gValue.ex_ctrlValue;   
  
 // if( gValue.ctrlValue>5) gValue.wait_msec=0;
  
  gValue.dir=get_direction(); 
  
    
  return;
  

//---------------------------
//방향 전환 체크
//---------------------------

//--------------------------------------------
    
#ifdef DEBUG
//printf("[%03d] sub_10ms [%02d][%03d]-[%02d] cdc[%d]\r\n",gValue.msec,  gValue.dir,  gValue.ex_pwm, gValue.seqStatus,continueDecCnt);  
  printf("dir[%01d]  sign[%01d] delta[%03d] DnFlag[%01d] ctrlValue[%04d] exPWM[%04d] \r\n",gValue.dir, gValue.sign, gValue.delta, gValue.stepDnFlag, gValue.ctrlValue, gValue.ex_pwm);
 //  printf("[%d]  gValue.f_raw[%04d] f_control[%04d] [%03d] [%04d]\r\n",gValue.seqStatus, gValue.f_raw,  gValue.f_control, gValue.f_percent, gValue.ex_pwm);
#endif  
   bk_release();
   
}


void motor_stop(int value)
{
  u16 i;
  int lpwm;
  if(value<0) {
    set_pwm(0); 
    return;
  }
  lpwm=value-2;
  for(i=0;i<1024;i++){
    lpwm=lpwm-1;
    if(lpwm<1) {
       set_pwm(0); 
      break;
    }
    else set_pwm(lpwm); 
    Udelay(1000);
    //delay_ms(1);
 }
  //decay_(value);
  set_pwm(0); 
}


u16 moving_average_filter(u16 val, u8 idx)
{
  u16 mbank[6]={0,};
  u32 sum;
  u16 avr;
  mbank[0]=gValue.bank[idx][1];
  mbank[1]=gValue.bank[idx][2];
  mbank[2]=gValue.bank[idx][3];
  mbank[3]=gValue.bank[idx][4];
  mbank[4]=gValue.bank[idx][5];
  mbank[5]=val;
  sum=mbank[0]+mbank[1]+mbank[2]+mbank[3]+mbank[4]+mbank[5];
  avr=sum/6L;
  memcpy(gValue.bank[idx],mbank,sizeof(mbank));
  
  //if(idx==0) printf("b4[%03d][%03d][%03d][%03d]\r\n", mbank[2] ,mbank[3] ,mbank[4] ,mbank[5]);
  
//  if(idx==0 && (mbank[0]>B_OFFSET) && (mbank[1]>B_OFFSET) && (mbank[2]>B_OFFSET) && (mbank[3]>B_OFFSET) && (mbank[4]>B_OFFSET) && (mbank[5]<B_OFFSET)){
//    //gValue.b4_break=mbank[1]-B_OFFSET;
//    gValue.b4_break=(int)gValue.ex_pwm;
//    gValue.buffer_zero=1;
//   // printf("b4[%03d]\r\n", gValue.b4_break);
//  }
  
  if(avr>1024L)avr=1024L;
  return avr;
}

u16 quantized(u16 val, u16 offset)
{
  u16 result;
  u16 lvaluet;
  u16 delta;
  if(val<=offset) return 0;
  lvaluet=val-offset;
  delta= TROTTLE_MAX- offset;
  
  result=(lvaluet*1024L)/(delta);
  //result=(val*1024L*10)/(delta*10);
  result=(result>1020L) ? 1024L: result;
  return result;
}

u16 quantized_percent(u16 val)
{
  u16 result;
  result=(val*1000L)/10240L;
  result=(result>97L) ? 100L: result;
  return result;
}

void bk_release()
{
  if(BREAK_FLAG==0 && (gValue.ctrlValue>10 || gValue.ex_pwm>0) ) {
    set_brake(0);//break release(해재)
    return;
  }
}

void set_brake(u8 brake)
{
  //_printf("set_brake[%d]",brake);
  if(gValue.state>=0xA0){
    GPIO_WriteLow(GPIOC, pBK);
    return;
  }
  if(brake) GPIO_WriteLow(GPIOC, pBK);
  else GPIO_WriteHigh(GPIOC, pBK); 
  gValue.stop=brake;
}

u8 get_bisang()
{
  u8 value;
 value = GPIO_ReadInputPin(GPIOC, pBISANG);
 value= (value) ? 1:0;
 return value;
}

u8 get_brake(void)
{
  u8 bk;
  bk = GPIO_ReadInputPin(GPIOC, pRTN);
  bk=1;
  return bk;
}



void set_diretion_led(u8 dir)
{
    switch(dir){
    case 0://ff
      GPIO_WriteHigh(GPIOC, pDIR);
      GPIO_WriteLow(GPIOD, pLED_FF);
      GPIO_WriteHigh(GPIOD, pLED_RR);      
      break;  
    case 1://rr
      GPIO_WriteLow(GPIOC, pDIR);
      GPIO_WriteHigh(GPIOD, pLED_FF);
      GPIO_WriteLow(GPIOD, pLED_RR);
      break;
    case 2://stop
      //GPIO_WriteHigh(GPIOC, pDIR);
      GPIO_WriteHigh(GPIOD, pLED_FF);
      GPIO_WriteHigh(GPIOD, pLED_RR);
      break;   
  }
   
}


u16 get_adc(u8 mChannel)
{
  u16 ResultADC = 0;
  u16 temph = 0;
  u8 templ = 0;

  ADC1->CSR  = 0x00;
  ADC1->CR1  = 0x00;
  ADC1->CR2  = 0x00;
  ADC1->CR3  = 0x00;
  ADC1->CR1 |= 0x40;//2mhz
  ADC1->CR2 |= 0x08;//0x08;//0000_1000 ;right, No scan,

  ADC1->CSR |= (u8)mChannel;
  ADC1->CR1 |= ADC1_CR1_ADON;//ADC on
  ADC1->CR1 |= ADC1_CR1_ADON;

  while(!(ADC1->CSR & 0x80));

  templ = ADC1->DRL;
  temph = ADC1->DRH;
  temph = (u16)(templ | (u16)(temph << 8));

  ResultADC = temph;
  ADC1->CSR &= 0x7F;//EOC Clear

 return ResultADC & 0x3ff;
}
