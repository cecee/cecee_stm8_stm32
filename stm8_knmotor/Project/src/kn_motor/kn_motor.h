#ifndef KN_MOTOR_H
#define KN_MOTOR_H

#include "stm8s_adc1.h"
#include "../util/util.h"

//port C
#define pPWM0    (GPIO_PIN_1) 
#define pRTN     (GPIO_PIN_3) 
#define pBK      (GPIO_PIN_4) 
#define pBISANG  (GPIO_PIN_5) 
#define pDIR_EN  (GPIO_PIN_6) 
#define pDIR     (GPIO_PIN_7) 

//port B
#define DIR_FF  (GPIO_PIN_7) 
#define DIR_RR  (GPIO_PIN_6) 
//port D
#define pLED_FF  (GPIO_PIN_0) 
#define pLED_RR  (GPIO_PIN_2) 
#define pTEST   (GPIO_PIN_7) 
//#define pM2_RELAY   (GPIO_PIN_4) //beep port
//port E
#define pRELAY  (GPIO_PIN_5) 
//#define pM1_RELAY   (GPIO_PIN_5) 
//#define ADC_MIN_OFFSET  180

#define TROTTLE_OFFSET  180
//#define TROTTLE_MAX  900-TROTTLE_OFFSET
//#define TROTTLE_MIN  180-TROTTLE_OFFSET
//#define TROTTLE_GAP  TROTTLE_MAX-TROTTLE_MIN

//#define PWM_MAX  820 //1024 80%

#define DPDT_100  114 // (820/720)*100 

void car_init(void);
//void sub_1ms(void);
void sub_10ms(void);
//filter+
u16 moving_average_filter(u16 val, u8 idx);
u16 quantized(u16 val, u16 offset);
u16 quantized_percent(u16 val);
void bk_release();
//filter-

//void set_relay(u8 on);
u16 get_adc(u8 mChannel);
u8 get_direction(void);
u8 get_bisang(void);

//void set_direction();
void set_relay(u8 on);

void set_diretion_led(u8 dir);
u8 get_brake(void);
void set_brake(u8 brake);
void motor_stop(int value);

#endif