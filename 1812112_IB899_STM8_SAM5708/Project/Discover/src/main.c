
/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "stm8s_uart1.h"
#include "stm8s_spi.h"
#include "stm8s_exti.h"
#include "stm8s_gpio.h"
#include "stm8s_flash.h"
#include "stm8s_tim1.h"
#include "stm8s_tim4.h"
#include "stm8s_beep.h"
#include "stm8s_i2c.h"
#include "ccbox.h"
 

#include <stdlib.h>
#include <string.h>

///////////////////////
void main_init(void);
void process_Rs232(void);
void CUartTxChar(u8 ucValue);

void Power_ON(void);
void Power_OFF(void);
//void check_update_key(void);
void Wait_Delay(u16 data);
void Beep_On(void);
void CLK_Configuration(void);
void GPIO_Configuration(void);
void check_botton(void);
void check_botton_test(void);
//void prn_read_flash(u16 add);
void lisence_out(void);
void Udelay(u32 dd);
void Delay(int dd);
//////////////////////
void STM_Timer_Init(void);

//void SEND_SET_MIC_SCORE(void);
//void MIDI_MUTE(u8 data);
void NRPN_SND(u8 data1,u8 data2,u8 data3);
//void nrpn5504(u8 data1, u8 data2, u8 data3, u8 data4);
//void I2C_Init(void);
//u8 I2C_Write(u8 Addr, u8 Reg, u8 Data);
//void I2C_GnerateSTART(void);
//void _I2C_Init(void);
void Reset_RK3288(void);

u8 i;
//u8 I2C_ADD;
//u8 I2C_DAT;
u8 FF_Flag=0;
void delay_delay(unsigned int mul);

//u8 p_st=0;//First power off
u8 IO_OK=0;
u8 COIN_CNT=0;
//u16 Tick_Base;
u16 timer_bdip=0;
u16 Wait_Base=0;
u16 bootCnt=0;
u16 bootCntSec=0;

u8 Auto_boot=0;
//u16 hdmi_chk_tick=0;
//u8 hdmi_req=0;

u16 Wait_tmp=0;
u16 Beep_Time=0;
u8  RC_flag=0;
u8  boot_flag=0;
u16  sec_cnt=0;

u8 LICENSE=0;

u8 Beep_flag=0;
u8 SET_BEEP=1;
u8 MK_MIDI=0;
u8 MK_LIC=0;
u8 REP_flag=1;
u8 u_power=0;
char RX_Buf[16];


u8 last_index=0;
u8 find_s=0;
u8 UART_Rx_OK=0;
u8 BufWindex=0;

u8 bdip[4]={0,0,0,0};

u8 flip=0;
u8 ff_out=0;
u8 ex_kdata=0;
u8 nagaCnt=0;
u8 pressedKey=0;

typedef union 
{
  	u8 	blisn[4];
  	u32 wlisn;
}LISN;

union 
{
  	u8 	bir_data[4];
  	u32 wir_data;
}IR_DATA;

///
LISN UnionBdip;
LISN UnionOnOFF;
PWR_STATUS P_STATUS;
///

/////IR_Receive/////////
u16  r_dat;   // Remocon Data..
u16  ir_cnt;
//u8   ir_dat[24];
u8   ir_dat[36];
u8   dat[8];
u8   ir_n;
u8   ir_flg;
//u8 x;
u8 PWR_IRQ=0;


//IR_DATA.bir_data.wir_data
void BEEP_ON()
{
	Beep_Time=0; 
	Beep_flag=1;///
	BEEP_Cmd(ENABLE);	///
}

void Power_OFF()
{
//	u32 delay_time=0;
//	u8 toggle=0;
	P_STATUS.CurONOFF=0;
	SOUND_MUTE(1);//mute
	POWER_LED(0);
	GPIO_WriteLow(GPIOC, EX_COIN_PC2);
	GPIO_WriteLow(GPIOB, PWR_EN_SYS);///PWR_EN_SYS off
	Wait_Delay(1000);//Wait 100mS
	GPIO_WriteLow(GPIOC, RES_HDMI);//RES_HDMI GPIO_WriteLow
	GPIO_WriteLow(GPIOC, RES_SAM);//Midi Reset High
	GPIO_WriteLow(GPIOC, RES_RK);//Midi Reset High
	GPIO_WriteLow(GPIOE, DC_CTL_PE5);
	Wait_Delay(2000);//Wait 100mS
	
//	_printf("PWR (OFF)\r\n",0);//

	RC_flag=0;
	IR_DATA.wir_data=0;

}

void Power_ON()
{
	u8 i;
	RC_flag=boot_flag=0;
	P_STATUS.CurONOFF=1;
	POWER_LED(1);
	GPIO_WriteHigh(GPIOC, EX_COIN_PC2);
	SOUND_MUTE(1);//mute
	GPIO_WriteHigh(GPIOE, DC_CTL_PE5);
        BEEP_ON();
	for(i=0;i<3;i++){//Wait 300ms
		Wait_Delay(1000);
		POWER_LED(i%2);
	}
    POWER_LED(1);
//	Wait_Delay(3000);//Wait 300ms
	
	GPIO_WriteHigh(GPIOB, PWR_EN_SYS);//PWR_EN_SYS High
	GPIO_WriteLow(GPIOC, RES_RK);//Midi Reset High
	GPIO_WriteLow(GPIOC, RES_HDMI);//Midi Reset High
	//Delay(1);
	Wait_Delay(100);//10ms
	GPIO_WriteHigh(GPIOC, RES_RK);//Midi Reset High
	GPIO_WriteHigh(GPIOC, RES_HDMI);//Midi Reset High
//  GPIO_WriteLow(GPIOC, RES_SAM);//Midi Reset High
	for(i=0;i<8;i++){//org 8 Wait 4Sec
		Wait_Delay(5000);
		POWER_LED(i%2);
	}
    POWER_LED(1);
//	Wait_Delay(40000);//Wait 5sec LRCK가 나와야 Setting 가는 함

	GPIO_WriteLow(GPIOC, RES_SAM);//Midi Reset High
	//	Delay(1);
	Wait_Delay(200);//10ms
	GPIO_WriteHigh(GPIOC, RES_SAM);//Midi Reset High
	Wait_Delay(3000);//200ms
	sam_init();
	Get_eeprom();
	SOUND_MUTE(0);//normal 

}

void Wait_Delay(u16 data)
{
//data 10000->1.0 sec
	Wait_Base=0;
	while(Wait_Base<=data){}//
}

void NRPN_SND(u8 data1, u8 data2, u8 data3)
{

//u8 NRPN[9]={0xb0, 0x63, data1, 0xb0, 0x62, data2,0xb0, 0x06, data3};//KEY1, KEY2 ON else OFF
u8 NRPN[9]={0xb0, 0x63, 0x00, 0xb0, 0x62, 0x00, 0xb0, 0x06, 0x00};//KEY1, KEY2 ON else OFF

u8 i;
	NRPN[2]=data1;
	NRPN[5]=data2;
	NRPN[8]=data3;
	
	for(i=0;i<9;i++)
	{
		CUartTxChar(NRPN[i]);
		//Udelay(1000);
	}
	Delay(2);
}

void Get_eeprom(void)
{
	u8 x;
	FLASH_Unlock(FLASH_MEMTYPE_DATA);

	UnionBdip.blisn[2]=FLASH_ReadByte(0x4000+2);
	UnionBdip.blisn[3]=FLASH_ReadByte(0x4000+3);//비교하는 대상
        UnionOnOFF.blisn[2]=FLASH_ReadByte(0x4004+2);
        UnionOnOFF.blisn[3]=FLASH_ReadByte(0x4004+3);
      
	FLASH_Lock(FLASH_MEMTYPE_DATA);
        
  	if((UnionBdip.blisn[3]+UnionBdip.blisn[2]) != 0xff) 
  	{
          x=(GPIO_ReadInputData(GPIOB))&0x0f;
          x=(GPIO_ReadInputData(GPIOB))&0x0f;
          UnionBdip.blisn[2]=~x;
          UnionBdip.blisn[3]= x;
          FLASH_Unlock(FLASH_MEMTYPE_DATA);
          FLASH_ProgramWord(0x4000,UnionBdip.wlisn);
          FLASH_Lock(FLASH_MEMTYPE_DATA);            

        }
        
        if(UnionOnOFF.blisn[2] != 0xAA) 
  	{

          UnionOnOFF.blisn[2]=0xAA;
          UnionOnOFF.blisn[3]= 0x00;//DC IN POWER OFF
          FLASH_Unlock(FLASH_MEMTYPE_DATA);
          FLASH_ProgramWord(0x4004,UnionOnOFF.wlisn);
          FLASH_Lock(FLASH_MEMTYPE_DATA);  

        }
        
}

void main_init(void)
{
  CLK_Configuration(); /* Configures clocks */
  GPIO_Configuration();/* Configures GPIOs */
  SOUND_MUTE(1);//MUTE
  UART1_DeInit(); // Initialize serial
//  UART1_Init(115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_Init(31250, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
 UART1_ITConfig(UART1_IT_RXNE,ENABLE); //test
//  UART1_ITConfig(UART1_IT_RXNE_OR,ENABLE); //test
  
  STM_Timer_Init();

  BEEP_DeInit();
  BEEP_Init(BEEP_FREQUENCY_1KHZ);//1KHZ
  BEEP_Cmd(DISABLE);
  FLASH_DeInit();//0x00_4000-0x00_427f
  Get_eeprom();
  
/////No Using Reboot function+++
  P_STATUS.BootFail= 0;//LISN.blisn[1];
  P_STATUS.PWR_MODE= 0;//LISN.blisn[2];
/////No Using Reboot function---
  enableInterrupts();
}

void sam_init()
{
	NRPN_SND(0x06, 0x00, 0x65);//Music Input Level
	NRPN_SND(0x08, 0x00, 0x74);//Mic1 Input Level
	NRPN_SND(0x08, 0x01, 0x74);//Mic2 Input Level
	NRPN_SND(0x08, 0x10, 0x55);//Direct L
	NRPN_SND(0x08, 0x11, 0x55);//Direct R

	NRPN_SND(0x08, 0x12, 0x20);//Echo Send 0x20 --- Using Echo Volume
	
	NRPN_SND(0x09, 0x00, 0x46);//Echo EQ1-Low
	NRPN_SND(0x09, 0x01, 0x43);//Echo EQ2-
	//NRPN_SND(0x09, 0x02, 0x43);//Echo EQ3-
	NRPN_SND(0x09, 0x03, 0x44);//Echo EQ4-
	NRPN_SND(0x09, 0x04, 0x5A);//Echo EQ5-High

	NRPN_SND(0x02, 0x00, 0x0c);//Mono echo-Rerev
	NRPN_SND(0x02, 0x05, 0x4d);//77d Echo Time
	NRPN_SND(0x02, 0x06, 0x45);//69d feedback
	
}
  
void main(void)
{
	int i;
	main_init();
  P_STATUS.PWR_MODE=UnionOnOFF.blisn[3];
	IR_DATA.wir_data=0;
	RC_flag=boot_flag=0;        
        
  if(UnionOnOFF.blisn[3]==0) {
    P_STATUS.CurONOFF=0;
    POWER_LED(0);     
    Power_OFF();
    //sam_init();   
  }
  else{
    
      P_STATUS.CurONOFF=1;
      POWER_LED(1);

     Wait_Delay(1500);//150ms 최소 One Key입력 될때 까징 기달
    Power_ON();
    sam_init();      
  
 }

  for (;;)
  {

		check_botton();
		//check_botton_test();
		//_printf("main]\r\n",0);
		if(P_STATUS.CurONOFF)
		{
			process_Rs232();
			for(i=0;i<COIN_CNT;i++){
				GPIO_WriteLow(GPIOC, EX_COIN_PC2);
				Udelay(2000);
				GPIO_WriteHigh(GPIOC, EX_COIN_PC2);
			}
			COIN_CNT=0;
		}
 
  }

}

void process_Rs232()
{
// ^!xxxxxx&
 u8 res_add;
 u8 res_dat;
 u32 wdata;
 
 if(UART_Rx_OK)//Have RS232 
  {
      UART_Rx_OK=0;
      if (RX_Buf[0]==0xb0 && RX_Buf[1] == 0x63 && RX_Buf[3]==0xb0 && RX_Buf[4] == 0x62 && RX_Buf[6]==0xb0 && RX_Buf[7] == 0x06)
      {
    		P_STATUS.BootFail=0;
   			Auto_boot=0;
      } 
      else return;
      
	 	 if (RX_Buf[2] == 0x39)  //NRPN39xx yy -- xx(0x00~0x7f): YAMAHA registor ADD  yy(0~0xff) :DATA
		 {
			res_add=RX_Buf[5];
    		res_dat=RX_Buf[8];	
			switch (res_add) // NRPN39xx yy -- xx(0x80~0xff) : for special function
			{ 
				case 0x80: // NRPN3980 //MUTE
					SOUND_MUTE(res_dat); //0:Normal 1:Mute
					break;  
				case 0x81:// NRPN3981 -- REC SOURCE SELECTOR
                  if(res_dat==0)  GPIO_WriteLow(GPIOA,GPIO_PIN_2);
                  else GPIO_WriteHigh(GPIOA,GPIO_PIN_2);
      		break;
		      			
					  case 0x82: // NRPN3981 //RES_SAM Active
  								GPIO_WriteLow(GPIOC, RES_SAM);//Midi Reset High
 								Delay(1);
 							 	GPIO_WriteHigh(GPIOC, RES_SAM);//Midi Reset High
							  break; 	  
						case 0x83: // NRPN3983 -- ENABLE BUZZER
						   		SET_BEEP=(res_dat==0)? 0:1; //0: BUZZER_Off  1: BUZZER_On
							  	break;	
						case 0x84: // NRPN3984 -- Beep
								if(SET_BEEP)	BEEP_ON();
		      				break;		
						case 0x85: // NRPN3985 -- Light Control
                if(res_dat==0)  GPIO_WriteLow(GPIOC,LAMP_PC1);
                else GPIO_WriteHigh(GPIOC,LAMP_PC1);
                break;		
						case 0xA0: //BDIP  writing EEPROM  ;LICENSE Release
              UART1_ITConfig(UART1_IT_RXNE,DISABLE); 
              FLASH_DeInit();
              CUartTxChar(0xff);  //왜이러지 몰라도 UART에서 멈추어 있는 증상을 없애기 위해서                     
              UnionBdip.blisn[2]=~bdip[3];//뒤집어서 Save해서 메모리 깨졌을때 대비
              UnionBdip.blisn[3]= bdip[3];
              wdata=UnionBdip.wlisn;
              FLASH_Unlock(FLASH_MEMTYPE_DATA); // Udelay(150000);
              //while (FLASH_GetFlagStatus(FLASH_FLAG_DUL) == RESET); 
              FLASH_ProgramWord(0x4000,wdata); // Udelay(150000);
              //while (FLASH_GetFlagStatus(FLASH_FLAG_EOP) == RESET); 
              FLASH_Lock(FLASH_MEMTYPE_DATA); //  Udelay(150000);
              CUartTxChar(0xff);  
              LICENSE=0;
              UART1_ITConfig(UART1_IT_RXNE,ENABLE); 
						  	break; 	
                                                        
                                                        
          case 0xA1: //DCPower 들어왔을때 초기 전원 상태 Control 
              UART1_ITConfig(UART1_IT_RXNE,DISABLE); 
              FLASH_DeInit();
              CUartTxChar(0xff);  //왜이러지 몰라도 UART에서 멈추어 있는 증상을 없애기 위해서                     
              UnionOnOFF.blisn[2]=0xAA;//뒤집어서 Save해서 메모리 깨졌을때 대비
              UnionOnOFF.blisn[3]= res_dat;
              wdata=UnionOnOFF.wlisn;
              FLASH_Unlock(FLASH_MEMTYPE_DATA); // Udelay(150000);
              FLASH_ProgramWord(0x4004,wdata); // Udelay(150000);
              FLASH_Lock(FLASH_MEMTYPE_DATA); //  Udelay(150000);
              CUartTxChar(0xff);  
              UART1_ITConfig(UART1_IT_RXNE,ENABLE); 
              break; 	
            case 0xB0: // Power OFF
								Power_OFF();
                                                        break;
						case 0xB1: // Power OFF
								Reset_RK3288();
                                                        break;							  

						default: break;
							
			}	 //case
			res_add=0x00;
	  }//NRPN39xx yy 
		memset(RX_Buf,0,sizeof(RX_Buf));
		//RX_Buf[0]=0;
	}
}		


void FlashInit(void)
{
	FLASH_Unlock(FLASH_MEMTYPE_DATA);
	FLASH_ProgramWord(0x4000,0xffffffff);
	FLASH_ProgramWord(0x4004,0xffffffff);
	FLASH_Lock(FLASH_MEMTYPE_DATA);
}

void check_botton(void)
{
 

  if(IR_DATA.wir_data==0x0cf35fa0 || IR_DATA.wir_data==0x065710EF || IR_DATA.wir_data==0x20ff0ff0)//0x065710EF praise Power KEY
//  if(IR_DATA.wir_data==0x0cf35fa0 || IR_DATA.wir_data==0x065710EF || IR_DATA.wir_data==0x02bd14eb || IR_DATA.wir_data==0xb24ddc23)//0x065710EF praise Power KEY
  {
    IR_DATA.wir_data=0;
    if( P_STATUS.PWR_MODE==0)
    {
      if(P_STATUS.CurONOFF)	 Power_OFF(); 
      else 		Power_ON();
    }
  } 

	if(PWR_IRQ)
	{
            if(P_STATUS.CurONOFF)    {if(P_STATUS.PWR_MODE==0) Power_OFF();} //power ON--> change to OFF
            else Power_ON();
            PWR_IRQ=0;
	}

}


void STM_Timer_Init(void)
{

	TIM4->SR1 = 0;       // clear overflow flag
	TIM4->PSCR = 6;     // Prescaler to divide Fcpu by 64: 4 us clock.// Max CPU freq = 16 MHz
	//  TIM4->ARR = 125;    // 125*4 = 500 us.
	TIM4->ARR = 24;    // 25*4 = 100 us. 10KHZ
	TIM4->IER = 0x01;   // Enable interrupt
	TIM4->CR1 = 0x01;   // Start timer
	TIM1->PSCRH = 0x00;  // 8M  f=fck/(PSCR+1)
	TIM1->PSCRL = 0x07; // PSCR=0x1F3F，f=8M/(0x1F3F+1)=1000Hz，Period : 1ms
	//	TIM1->PSCRH = 0x03;  // 8M  f=fck/(PSCR+1)
	//	TIM1->PSCRL = 0x1f; // PSCR=0x031f，f=8M/(0x031f+1)=10000Hz，Period : 0.1ms

//	TIM1->ARRH = 0x00;  // 0.1msx200(0xc8)
//	TIM1->ARRL = 0xc8;  // 20ms(0xc8)
	TIM1->ARRH = 0x4E;  // 0x4e20=20000us=20ms
	TIM1->ARRL = 0x20;  // 
	
	TIM1->IER = 0x01;   // Enable interrupt
	TIM1->CR1 = 0x01;   // Start timer
}

/////////////////////////
//10ms Timer
/////////////////////////
#pragma vector=0xD
__interrupt void TIM1_OVR_UIF(void)
{
	TIM1->SR1 = 0;
	timer_bdip++;
	if(timer_bdip>BDIP_PERIOD)
	{
		timer_bdip=0;

		if(P_STATUS.CurONOFF)
		{
    		if(GPIO_ReadInputPin(GPIOD,GPIO_PIN_2)==0)//EMER
        {
          GPIO_WriteLow(GPIOD, EGPIO1);
          Udelay(200);//2msec
          GPIO_WriteHigh(GPIOD, EGPIO1); 		
    		}
 
    		BDipSigOut();
     		if(LICENSE){
				GPIO_WriteLow(GPIOD, BDIP_OUT);
				Udelay(200);//2msec
				GPIO_WriteHigh(GPIOD, BDIP_OUT);
			}	  		
    		
     	}

	}
	//enableInterrupts();
}



#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  u8 data;
//0xb0,0x63,0x39,0xb0,0x62,0xXX,0xb0,0x06,0xYY
 
  data = UART1_ReceiveData8();
	//_printf("Rcv [%x]\r\n",data);
	switch(find_s){
		case 0:
			if(data == 0xb0){
				last_index=BufWindex=0; 
				memset(RX_Buf, 0,10); 
				RX_Buf[BufWindex++] = data;
//				BufWindex++;
				find_s=1;			
			}
			break;
		case 1:
			if (data==0x63 ){	
				RX_Buf[BufWindex++] = data;
//		  		BufWindex++;
		  		find_s++;
	  		}
	  		else 
	  		{
		  		BufWindex=0;
		  		find_s=0;
		  	//	return;	
	  		}	  	
	  		break;
	  	default:
			if (find_s>=2)
			{
				RX_Buf[BufWindex++] = data;
//				BufWindex++;
				find_s++;
			}	
			if (BufWindex>=9) 
			{ 
				UART_Rx_OK=1;
				BufWindex=find_s=0;
			}			
			break;		
	}
}

#pragma vector=6 //5A 6B 7D 
__interrupt void EXTI_PORTB_IRQHandler(void)
{
   // disableInterrupts();
   // Udelay(5000);//20msec
   PWR_IRQ=1;
   //   pressedKey=1;
   // enableInterrupts();   
}


#pragma vector=8 //5A 6B 7D 
__interrupt void EXTI_PORTD_IRQHandler(void)
{
		COIN_CNT++;

}

#pragma vector = 25
__interrupt void STM_Timer_ISR(void)
{

 u8  i,j;
 u8  buf = 0;
 u8  MAX_IR=0;
 u8 xx;
 //static u8 Attack_key;	
 //u8 Release_key;
 
 //u8 kdata;
 
 TIM4->SR1 = 0;      // clear overflow flag
  Wait_Base++;
  Wait_tmp++;
  Beep_Time++;
  bootCnt++;

  if(Beep_flag==1 && Beep_Time >=1000)	 {BEEP_Cmd(DISABLE); Beep_flag=0;}	
 ///IR RECEV
  if(ir_cnt++ >= 1000){ir_cnt =0;ir_flg = 0;ir_dat[0]=0;}
     switch(ir_flg)
		{
		   case 0x00:
		     	if (GPIO_ReadInputPin(GPIOA,GPIO_PIN_3)==0){ir_cnt=0; ir_flg=1; ir_n= 0;}
			 		break;
		   case 0x01:
		      if (GPIO_ReadInputPin(GPIOA,GPIO_PIN_3)!=0) {ir_cnt=0;ir_flg =2;}
		    	break;
		   case 0x02:
		    if (GPIO_ReadInputPin(GPIOA,GPIO_PIN_3)==0)
			  {
			  if(ir_n<36)ir_dat[ir_n++]=ir_cnt; 
			  ir_flg=1;
				if(ir_cnt>=100){MAX_IR=ir_n;ir_n=0;}
			  }
			 break;
		   case 0x03:
		     ir_flg=4;
             xx = ir_dat[0];
//          _printf("^^%x!\r\n",xx);  //0x2b, 0x2c, 0x2d  
 		     if(!((xx >= 34) && (xx <= 47))) {ir_flg=0; ir_cnt=0;break;}//retun
//			 for(i=0;i<7;i++)
			   for(i=0;i<4;i++)
             {
                for(j=0;j<8;j++)
                {
                        xx    = ir_dat[i*8+j+1];
                        buf >>= 1;
                        if     ((xx >=  3) && (xx <=  7)) buf &=0x7f ;
						            else if((xx >= 10) && (xx <= 18)) buf |= 0x80;
                             else   {ir_flg=0; ir_cnt=0;break;}//return;//(ERR);
                }
                //dat[i] = buf;
                IR_DATA.bir_data[i]=buf;
             }
             
	     break; 
		   default:   break;	 
		}		
		if(MAX_IR>0x15)	{ir_flg = 3;}
}



void CLK_Configuration(void)
{
  /* Fmaster = 16MHz */
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
}

void GPIO_Configuration(void)
{

   GPIO_DeInit(GPIOA); /* IR-Port */
   GPIO_Init(GPIOA, GPIO_PIN_3, GPIO_MODE_IN_FL_NO_IT);//remoconGPIO_MODE_IN_FL_NO_IT
   GPIO_Init(GPIOA, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_SLOW);//remoconGPIO_MODE_IN_FL_NO_IT
  
  GPIO_DeInit(GPIOB);/* GPIOB reset */
  GPIO_Init(GPIOB, GPIO_PIN_LNIB, GPIO_MODE_IN_PU_NO_IT);   //BDIP Read port
 
  //GPIO_Init(GPIOB, PSW_PIN, GPIO_MODE_IN_PU_NO_IT);         //PSW_PIN
  GPIO_Init(GPIOB, PSW_PIN, GPIO_MODE_IN_PU_IT);         //PSW_PIN
  
  GPIO_Init(GPIOB, PWR_EN_SYS, GPIO_MODE_OUT_PP_HIGH_SLOW); //PWR_EN_SYS
 // GPIO_Init(GPIOB, LED_CTL_PB7,GPIO_MODE_OUT_PP_HIGH_SLOW);     //LED_CTL
  GPIO_Init(GPIOB, _PB4, GPIO_MODE_OUT_PP_HIGH_SLOW);     //LED_CTL
 // GPIO_Init(GPIOB, GPIO_PIN_7, GPIO_MODE_OUT_PP_HIGH_SLOW);

//LED_CTL MRES_PIN
  GPIO_DeInit(GPIOC); // C-Port 
  GPIO_Init(GPIOC, LAMP_PC1, GPIO_MODE_OUT_PP_HIGH_SLOW);
  GPIO_Init(GPIOC, LED0_PIN, GPIO_MODE_OUT_PP_HIGH_SLOW);//LED_PIN 
  GPIO_Init(GPIOC, LED1_PIN, GPIO_MODE_OUT_PP_HIGH_SLOW);//LED_PIN 
  GPIO_Init(GPIOC, RES_HDMI, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(GPIOC, RES_RK, GPIO_MODE_OUT_PP_LOW_SLOW);//AUDIO_SEL
  GPIO_Init(GPIOC, RES_SAM, GPIO_MODE_OUT_OD_LOW_SLOW);//SAM RESET Open Drain
  GPIO_Init(GPIOC, EX_COIN_PC2, GPIO_MODE_OUT_PP_HIGH_SLOW);//COIN OUT //MBOX -->GPIO_MODE_OUT_OD_LOW_SLOW

  GPIO_DeInit(GPIOD); /* RTD1283_MIDI_IRQ */
  GPIO_Init(GPIOD, COIN, GPIO_MODE_IN_PU_IT);//irq
  GPIO_Init(GPIOD, EMER, GPIO_MODE_IN_PU_NO_IT);//PD2
  GPIO_Init(GPIOD, EGPIO1, GPIO_MODE_OUT_PP_HIGH_SLOW);//PD3 EMER 1sec주기
  GPIO_Init(GPIOD, BDIP_OUT, GPIO_MODE_OUT_PP_HIGH_SLOW);//PD7 BDIP 1sec주기
  GPIO_Init(GPIOD, BZ_PD4, GPIO_MODE_OUT_PP_LOW_SLOW);//PD3 EMER 1sec주기
//  GPIO_Init(GPIOD, GPIO_PIN_6, GPIO_MODE_IN_PU_NO_IT);//PD6 rxd
 
  GPIO_DeInit(GPIOE); /* RTD1283_PWR Port */
  GPIO_Init(GPIOE, DC_CTL_PE5, GPIO_MODE_OUT_PP_LOW_SLOW);//default Power OFF

  GPIO_DeInit(GPIOF); /* GPIOF MUTE_RELAY */
  GPIO_Init(GPIOF, MUTE_RELAY, GPIO_MODE_OUT_PP_HIGH_SLOW);//MUTE

  EXTI_DeInit();    
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOB, EXTI_SENSITIVITY_FALL_ONLY);
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_FALL_ONLY);
 // EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  
  EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  

  
}


void Delay(int dd)
{
int i,j;
for(i=0;i<dd;i++){for(j=0;j<1562;j++){}}
}

void Udelay(u32 dd)
{
  u32 i,j;
  for(i=0;i<dd;i++){for(j=0;j<3;j++){}
	}
}

void CUartTxChar(u8 ucValue)
{
  UART1_SendData8(ucValue); //udelay(1);
	while(!(UART1->SR&0x40));
}

void _printf(u8 *pFmt, u32 wVal)
{

	u8 SendData;
	while (SendData=*(pFmt++))
	{
		if (SendData==(char)'%')
		{
			SendData=*(pFmt++);
			if(SendData==(char)'d' || SendData==(char)'x')
			{
				if(wVal)
				{
					u32 divider=10000;
					u8 dispValue,tmp;
					
					if (SendData==(char)'x')
						divider=0x1000;
					while(divider)
					{
						dispValue=wVal/divider;
						wVal-=(dispValue*divider);
						//if(dispValue)
						{
							if(dispValue>9)
								tmp = dispValue + ((char)'A'-10);
							else
								tmp = dispValue + (char)'0';
							CUartTxChar(tmp);
						}
						if(SendData==(char)'d')
							divider/=10;
						else
							divider/=0x10;
					}
				}
				else
					CUartTxChar('0');
				
			}
		}
		else
			CUartTxChar(SendData);
	}
}


void delay_delay(unsigned int mul)
{
   for(; mul>0; mul--);
}


void SOUND_MUTE(u8 data)
{
//data 0:Normal 1:MUTE
 if(data==0) GPIO_WriteLow(GPIOF, MUTE_RELAY);
 else GPIO_WriteHigh(GPIOF, MUTE_RELAY);
}

void POWER_LED(u8 data)
{
  if(data){
  	GPIO_WriteHigh(GPIOC, LED1_PIN);//Midi Reset High
  	GPIO_WriteLow(GPIOC, LED0_PIN);//Midi Reset High
	}
	else
	{
  	GPIO_WriteHigh(GPIOC, LED0_PIN);//Midi Reset High
  	GPIO_WriteLow(GPIOC, LED1_PIN);//Midi Reset High
	
	}

}

void Reset_RK3288(void)
{
//	GPIO_WriteLow(GPIOC, RES_RK);
// 	else GPIO_WriteHigh(GPIOC, RES_RK);
	Power_OFF();
	Wait_Delay(1000);//Wait 0.2S
	Power_ON();
	Auto_boot=0;
}


void BDipSigOut()
{
	static u8 bdip_rflag=0;
	bdip_rflag++;
	bdip_rflag=bdip_rflag%2;
	bdip[bdip_rflag]=(GPIO_ReadInputData(GPIOB))&0x0f;
	if(bdip[0]==bdip[1]) 
	{
	  bdip[3]=bdip[0];
	  if(UnionBdip.blisn[3]!=bdip[0])  	LICENSE=1;

	}
	
	
}

