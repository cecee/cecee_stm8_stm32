#ifndef KN_PWM_H
#define KN_PWM_H
#include "util.h"

void PWM_init(void);
void PWM_Enable(u8 enable);
void set_pwm(u16 pwm);
void set_pwm2(u16 pwmA, u16 pwmB);
void decay(u16 time);
void slow_decay(void);
void fast_decay(u16 time);
void PWM_1ms(void);
//void set_pwm_frequency(u16 div);
void set_pwmA(u16 pwm);
void set_pwmB(u16 pwm);
#endif


