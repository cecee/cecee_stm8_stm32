#ifndef UTIL_H
#define UTIL_H

#include "stm8s.h"

#define DEBUG
#ifndef BIT 
#define BIT(x) (1<<(x)) 
#endif 

#define IR2104

#define u8  unsigned char
#define u16 unsigned int
#define u32 unsigned long


#define HIGH 1
#define LOW  0



typedef union 
{
 struct
 {
  u8  trot_zero: 1;
  u8  Lsw: 1; 
  u8  Rsw: 1;
  u8  Fsw: 1; 
  u8  Bsw: 1;
  u8  fr: 2; 
  u8  bisang: 1; 
 } b;
 u8 value;
}INPUT_STATUS_UNION;

typedef union 
{
 struct
 {
  u8  standy_ok: 1;
  u8  bisang: 1; 
 } b;
 u8 value;
}OUTPUT_STATUS_UNION;

typedef struct
{
  u8  ff: 1;
  u8  rr: 1;
  u8  left: 1;  
  u8  right: 1;  
  u8  left_rotate: 1;	
  u8  right_rotate: 1;
}DRIVE_STATUS;

typedef struct 
{
  u8 sign;
  u8 ex_sign;
  u8 relay;
  u8 stop;
  u8 changing_dir;
  u8 state; 
  u8 break_state; 
  u8 zero_cnt;
  u16 wait_msec;
  u16 delta;
  u16 ctrlValue;
  u16 ex_ctrlValue;
  u16 f_percent;
  u16 ex_pwm;
  u16 ex_pwmA;
  u16 ex_pwmB;
  u16 bank[2][6];
  int b4_break;
  INPUT_STATUS_UNION input;
  OUTPUT_STATUS_UNION output;
  DRIVE_STATUS drv_state;
}CURRENT_STATUS;

extern CURRENT_STATUS gValue;

extern u8 BREAK_FLAG;
extern u8 DIRECTION_FLAG;
extern u8 DECAY_FLAG;
extern int break_cnt;

void Udelay(u32 dd);
void Delay(int dd);
void delay(u32 dly); 
void delay_ms(u16 dly);
void CUartTxChar(u8 ucValue);
//void _printf(u8 *pFmt, u32 wVal);
//long _sprintf(char *buf, char *format, long arg);
//char *itoa( char *a, int i);

#endif