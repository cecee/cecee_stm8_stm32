#ifndef _INCLUDE_H
#define _INCLUDE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm8s.h"
#include "stm8s_type.h"
#include "stm8s_i2c.h"
#include "stm8s_gpio.h"

#include "main.h"
#include "string.h"
#include "stm8s_uart1.h"
#include "kn_motor_6ax.h"
#include "kn_pwm_cf.h"
#include "util.h"
#include "MPU6050.h"
#include "I2C_Master.h"

#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define PI 3.1415926535897932384626433832795  



#endif