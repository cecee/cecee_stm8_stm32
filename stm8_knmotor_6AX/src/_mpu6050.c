#include "mpu6050.h"
#include "include.h"

accel_t_gyro_union accel_t_gyro;

void mpu6050_init()
{ 
  u8 i;
 u8 c[16];
 printf("InvenSense MPU-6050\r\n");
 // default at power-up:
 // Gyro at 250 degrees second
 // Acceleration at 2g
 // Clock source at internal 8MHz
 // The device is in sleep mode.

 MPU6050_ReadI2C_ByteN(MPU6050_WHO_AM_I, &c[0], 1);//0x75 read
 printf("MPU6050_WHO_AM_I : %x\r\n",c[0]);
 
 // According to the datasheet, the 'sleep' bit
 // should read a '1'.
 // That bit has to be cleared, since the sensor
 // is in sleep mode at power-up. 
 //error = MPU6050_read (MPU6050_PWR_MGMT_1, &c, 1);
 //printf("PWR_MGMT_1 : %x\r\n",c);
 // Clear the 'sleep' bit to start the sensor.
 MPU6050_WriteI2C_Byte(MPU6050_PWR_MGMT_1, 0x00);
 //MPU6050_ReadI2C_ByteN(MPU6050_PWR_MGMT_1, &c[0], 1);
 //printf("PWR_MGMT_1 : %x\r\n",c[0]);
 
 setClockSource(MPU6050_CLOCK_PLL_XGYRO);
 
 
 //MPU6050_WriteI2C_Byte(MPU6050_I2C_SLV0_ADDR, 0xAA);
//MPU6050_WriteI2C_Byte(MPU6050_I2C_SLV0_REG, 0x55);
 //MPU6050_WriteI2C_Byte(0x0e, 0x0e);
 //memset(&c[0],0, 2);
 //MPU6050_ReadI2C_ByteN(MPU6050_I2C_SLV0_ADDR, &c[0], 10);
 //printf("0d:[%x][%x] \r\n",c[0], c[1]);
 
 memset(&c[0],0, 16); 
 MPU6050_ReadI2C_ByteN(0x70, &c[0], 10);
 
 for(i=0;i<14;i++){
  printf("[%d][%d]\r\n",i, c[i]);
 }

}

void getMPU6050()
{
 u8 c[16];
 u8 i;
  int error;
 double dT;
 //printf("##getMPU6050[%d]\r\n", sizeof(accel_t_gyro) );
 // Read the raw values.
 // Read 14 bytes at once, 
 // containing acceleration, temperature and gyro.
 // With the default settings of the MPU-6050,
 // there is no filter enabled, and the values
 // are not very stable.
 
 MPU6050_ReadI2C_ByteN(MPU6050_ACCEL_XOUT_H, (u8 *)&accel_t_gyro, sizeof(accel_t_gyro));
 //MPU6050_read (MPU6050_WHO_AM_I, &c, 1);
 //printf("MPU6050_WHO_AM_I : %x\r\n",c);
 printf("x_accel[%d] x_gyro[%d] temperature[%d]\r\n",accel_t_gyro.value.x_accel, accel_t_gyro.value.x_gyro, accel_t_gyro.value.temperature);
 
 memset(&c[0],0, 16); 
 MPU6050_ReadI2C_ByteN(MPU6050_ACCEL_XOUT_H, &c[0], 14);
 
 for(i=0;i<14;i++){
  printf("[%d][%d]\r\n",i, c[i]);
 }
// Serial.println(error,DEC);

#if 0
 // Swap all high and low bytes.
 // After this, the registers values are swapped, 
 // so the structure name like x_accel_l does no 
 // longer contain the lower byte.
 u8 swap;
 #define SWAP(x,y) swap = x; x = y; y = swap

 SWAP (accel_t_gyro.reg.x_accel_h, accel_t_gyro.reg.x_accel_l);
 SWAP (accel_t_gyro.reg.y_accel_h, accel_t_gyro.reg.y_accel_l);
 SWAP (accel_t_gyro.reg.z_accel_h, accel_t_gyro.reg.z_accel_l);
 SWAP (accel_t_gyro.reg.t_h, accel_t_gyro.reg.t_l);
 SWAP (accel_t_gyro.reg.x_gyro_h, accel_t_gyro.reg.x_gyro_l);
 SWAP (accel_t_gyro.reg.y_gyro_h, accel_t_gyro.reg.y_gyro_l);
 SWAP (accel_t_gyro.reg.z_gyro_h, accel_t_gyro.reg.z_gyro_l);


 // Print the raw acceleration values

 Serial.print(F("accel x,y,z: "));
 Serial.print(accel_t_gyro.value.x_accel, DEC);
 Serial.print(F(", "));
 Serial.print(accel_t_gyro.value.y_accel, DEC);
 Serial.print(F(", "));
 Serial.print(accel_t_gyro.value.z_accel, DEC);
 Serial.println(F(""));


 // The temperature sensor is -40 to +85 degrees Celsius.
 // It is a signed integer.
 // According to the datasheet: 
 // 340 per degrees Celsius, -512 at 35 degrees.
 // At 0 degrees: -512 - (340 * 35) = -12412

 Serial.print(F("temperature: "));
 dT = ( (double) accel_t_gyro.value.temperature + 12412.0) / 340.0;
 Serial.print(dT, 3);
 Serial.print(F(" degrees Celsius"));
 Serial.println(F(""));


 // Print the raw gyro values.

 Serial.print(F("gyro x,y,z : "));
 Serial.print(accel_t_gyro.value.x_gyro, DEC);
 Serial.print(F(", "));
 Serial.print(accel_t_gyro.value.y_gyro, DEC);
 Serial.print(F(", "));
 Serial.print(accel_t_gyro.value.z_gyro, DEC);
 Serial.print(F(", "));
 Serial.println(F(""));
#endif
 delay(1000);
}



bool writeBits(u8 regAddr, u8 bitStart, u8 length, u8 data) {
    //      010 value to write
    // 76543210 bit numbers
    //    xxx   args: bitStart=4, length=3
    // 00011100 mask byte
    // 10101111 original value (sample)
    // 10100011 original & ~mask
    // 10101011 masked | value
    u8 b;
    if (MPU6050_ReadI2C_Byte(regAddr, &b) != 0) {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        data <<= (bitStart - length + 1); // shift data into correct position
        data &= mask; // zero all non-important bits in data
        b &= ~(mask); // zero all important bits in existing byte
        b |= data; // combine data with existing byte
        return MPU6050_WriteI2C_Byte(regAddr, b);
    } else {
        return false;
    }
}


void setClockSource(u8 source) {
    writeBits(MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT, MPU6050_PWR1_CLKSEL_LENGTH, source);
}


#if 0


void MPU6050_Base::initialize() {
    setClockSource(MPU6050_CLOCK_PLL_XGYRO);
    setFullScaleGyroRange(MPU6050_GYRO_FS_250);
    setFullScaleAccelRange(MPU6050_ACCEL_FS_2);
    setSleepEnabled(false); // thanks to Jack Elston for pointing this one out!
}

uint8_t MPU6050_Base::getFullScaleGyroRange() {
    I2Cdev::readBits(devAddr, MPU6050_RA_GYRO_CONFIG, MPU6050_GCONFIG_FS_SEL_BIT, MPU6050_GCONFIG_FS_SEL_LENGTH, buffer, I2Cdev::readTimeout, wireObj);
    return buffer[0];
}
/** Set full-scale gyroscope range.
 * @param range New full-scale gyroscope range value
 * @see getFullScaleRange()
 * @see MPU6050_GYRO_FS_250
 * @see MPU6050_RA_GYRO_CONFIG
 * @see MPU6050_GCONFIG_FS_SEL_BIT
 * @see MPU6050_GCONFIG_FS_SEL_LENGTH
 */
void MPU6050_Base::setFullScaleGyroRange(uint8_t range) {
    I2Cdev::writeBits(devAddr, MPU6050_RA_GYRO_CONFIG, MPU6050_GCONFIG_FS_SEL_BIT, MPU6050_GCONFIG_FS_SEL_LENGTH, range, wireObj);
}
void MPU6050_Base::setClockSource(uint8_t source) {
    I2Cdev::writeBits(devAddr, MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT, MPU6050_PWR1_CLKSEL_LENGTH, source, wireObj);
}

bool I2Cdev::writeBits(uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t data) {
    //      010 value to write
    // 76543210 bit numbers
    //    xxx   args: bitStart=4, length=3
    // 00011100 mask byte
    // 10101111 original value (sample)
    // 10100011 original & ~mask
    // 10101011 masked | value
    uint8_t b;
    if (readByte(devAddr, regAddr, &b) != 0) {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        data <<= (bitStart - length + 1); // shift data into correct position
        data &= mask; // zero all non-important bits in data
        b &= ~(mask); // zero all important bits in existing byte
        b |= data; // combine data with existing byte
        return writeByte(devAddr, regAddr, b);
    } else {
        return false;
    }
}



#endif




