#include "stm8s.h"
#include "stm8s_gpio.h"
#include "kn_motor_6ax.h"
#include "kn_pwm_cf.h"
#include "include.h"
#include <stdio.h>
#include <string.h>
 

#define A_NORRMAL 1     //normal :기울기 y=ax+b 의 a
#define A_STOPING  4    //normal :기울기 y=ax+b 의 a
#define B_OFFSET   180L //offset :y=ax+b 의 b 180L=>0
#define SECOND_POLL 60 //Stop시 기울기가 변화는 Point  value
#define TROTTLE_MAX  870L //

#define SET_MOTOR_A_DIR(DIR)   ((DIR==HIGH)?  GPIO_WriteLow(pDIR_port,pDIR): GPIO_WriteHigh(pDIR_port,pDIR))
#define SET_MOTOR_B_DIR(DIR)   ((DIR==HIGH)?  GPIO_WriteHigh(pDIR2_port,pDIR2): GPIO_WriteLow(pDIR2_port,pDIR2))//H/W(엔더슨컨넥트 내부결선 꼬였음

#define GET_RIGHT_BREAK()  GPIO_ReadInputPin(pR_SW_port, pR_SW)
#define GET_LEFT_BREAK()   GPIO_ReadInputPin(pL_SW_port, pL_SW)

CURRENT_STATUS gValue;

u8 ex_sign=0;
u16 continueDecCnt=0;
u8 BREAK_FLAG=0;
u8 DECAY_FLAG=0;
u16 DECAY=0;
int break_cnt=0;

//u8 DIRECTION_FLAG=0;
u16 direction_delay_cnt=0;
u16 direction_delay_ref=0;
BUZZER buz;

void CF_CAR_init(void)
{

  memset(&gValue,0,sizeof(gValue));
  buzzer_on(0);
  gValue.stop=1;
  get_TLP280(); 
  gValue.state=0xA0;
  //set_pwm_frequency(0);
  set_pwm(0); 
  set_diretion_led(gValue.input.b.fr);
  M24V_relay(1);
  delay_ms(20);
  PWM_Enable(1);
  //gValue.bisang=get_bisang();
 //  set_brake(0); //브레이크 ON
  
}

void buzzer_on(u8 on)
{
  if(on) GPIO_WriteHigh(pBEEP_port, pBEEP);
  else GPIO_WriteLow(pBEEP_port, pBEEP);
  buz.on=on;
}

void get_TLP280(void)
{
 // get_FBdirection(); 
  u8 dir=0;
  u8 rdir=0, ff=0, bb=0, ll=0, rr=0;
  ff = GPIO_ReadInputPin(pDIRSW_FF_port, pDIRSW_FF);
  bb = GPIO_ReadInputPin(pDIRSW_BB_port, pDIRSW_BB);
  ll = GPIO_ReadInputPin(pL_SW_port, pL_SW);
  rr = GPIO_ReadInputPin(pR_SW_port, pR_SW);
  
  gValue.input.b.Fsw= (ff) ? 0: 1;
  gValue.input.b.Bsw= (bb) ? 0: 1;
  if(!ff)_SetBit(rdir, 0);
  if(!bb)_SetBit(rdir, 1);
  switch(rdir){
    case 0:  dir=2; break;
    case 1:  dir=1; break;
    case 2:  dir=0; break;
    default:  dir=2; break;  
 } 
 gValue.input.b.fr=dir;

 gValue.input.b.Lsw= (ll) ? 0: 1;
 gValue.input.b.Rsw= (rr) ? 0: 1;  
  
}

void CF_CAR_Driver_rotation(u8 left)
{
  if(left){
    SET_MOTOR_A_DIR(1);
    SET_MOTOR_B_DIR(0);
  }
  else{
    SET_MOTOR_A_DIR(0);
    SET_MOTOR_B_DIR(1); 
  }
  
}

void CF_CAR_Driver_direction(u8 jenjin)
{
  if(jenjin>1) return;
  SET_MOTOR_A_DIR(jenjin);
  SET_MOTOR_B_DIR(jenjin);
}



void M24V_relay(u8 on)
{
  if(on) GPIO_WriteHigh(pRELAY_port, pRELAY);
  else GPIO_WriteLow(pRELAY_port, pRELAY);
  gValue.relay=on;
}



void sub_10ms(void)
{
  //u8 bisang=0;
  u16 raw0,raw1, f_raw;//,raw2,raw3;
  u16 lpercent;//0~100
  
  if(gValue.input.b.bisang==0 && gValue.relay && gValue.state<0x80){
      gValue.input.b.bisang = get_bisang();
  }
  if(gValue.input.b.bisang){
    if(gValue.output.b.bisang==0){
      //printf("Set BISANG.....[%x]\r\n", gValue.state);
      set_pwm(0);
      delay_ms(200);
      GPIO_WriteLow(pDIR_EN_port, pDIR_EN);//PWM forcely to ZERO    
      GPIO_WriteLow(pDIR_EN2_port, pDIR_EN2);//PWM forcely to ZERO    
      M24V_relay(0);
      set_brake(1); //브레이크 ON
      gValue.output.b.bisang=1;
    }
    return;
  }
  raw0 = get_adc(0);//trottle
  raw1 = get_adc(1);//ff percent
  
#if 0
 //test++
  if(raw0 <512) raw0=(raw0*10)/15; //512->341
 else if(raw0 <768) raw0=raw0-171;//512-171=341
 else  raw0=(raw0*2)-768;//256;
  //test--
#endif 
 if(raw0>=1024)raw0=1024;
 if( raw0<B_OFFSET){
   gValue.zero_cnt++;
   if(gValue.zero_cnt>40) {
      gValue.b4_break=(int)gValue.ex_pwm;
      gValue.input.b.trot_zero=1;
      gValue.zero_cnt=200;
     }
 }
 else {
  gValue.zero_cnt=0;
  gValue.input.b.trot_zero=0;
 }
 
  f_raw=moving_average_filter(raw0,0);
  lpercent =moving_average_filter(raw1,1);
  gValue.f_percent =quantized_percent(lpercent);
  //gValue.r_percent =gValue.f_percent;
  gValue.ex_sign=gValue.sign;
  gValue.ex_ctrlValue=gValue.ctrlValue;
  gValue.ctrlValue=quantized(f_raw, B_OFFSET);  
  gValue.sign= (gValue.ctrlValue>=gValue.ex_ctrlValue) ? 0:1; //0 increase 1:decrease  
  gValue.delta =(gValue.sign) ? gValue.ex_ctrlValue-gValue.ctrlValue : gValue.ctrlValue-gValue.ex_ctrlValue;   
  
  if( gValue.ctrlValue>5 || gValue.input.b.Lsw || gValue.input.b.Rsw) {
   // printf("gValue.input.value[%d]  \r\n",gValue.input.value);
    gValue.wait_msec=0;
  }
  get_TLP280();
  
  //printf("dir[%d]  f_percent[%d] delta[%d] ctrlValue[%d] exPWM[%d] \r\n",gValue.input.b.fr, gValue.f_percent, gValue.delta, gValue.ctrlValue, gValue.ex_pwm);
  //bk_release();
  return;

  // bk_release();
   
}


void motor_stop(int value)
{
  u16 i;
  int lpwm;
  if(value<0) {
    set_pwm(0); 
    return;
  }
  lpwm=value-2;
  for(i=0;i<1024;i++){
    lpwm=lpwm-1;
    if(lpwm<1) {
       set_pwm(0); 
      break;
    }
    else set_pwm(lpwm); 
    Udelay(1200);
    //delay_ms(1);
 }
  set_pwm(0); 
}


void motor_stopA(int value)
{
  u16 i;
  int lpwm;
  if(value<0) {
    set_pwmA(0); 
    return;
  }
  lpwm=value-2;
  for(i=0;i<1024;i++){
    lpwm=lpwm-1;
    if(lpwm<1) {
       set_pwmA(0); 
      break;
    }
    else set_pwmA(lpwm); 
    Udelay(1200);
 }
  set_pwmA(0); 
}

void motor_stopB(int value)
{
  u16 i;
  int lpwm;
  if(value<0) {
    set_pwmB(0); 
    return;
  }
  lpwm=value-2;
  for(i=0;i<1024;i++){
    lpwm=lpwm-1;
    if(lpwm<1) {
       set_pwmB(0); 
      break;
    }
    else set_pwmB(lpwm); 
    Udelay(1200);
 }
  set_pwmB(0); 
}



u16 moving_average_filter(u16 val, u8 idx)
{
  u16 mbank[6]={0,};
  u32 sum;
  u16 avr;
  mbank[0]=gValue.bank[idx][1];
  mbank[1]=gValue.bank[idx][2];
  mbank[2]=gValue.bank[idx][3];
  mbank[3]=gValue.bank[idx][4];
  mbank[4]=gValue.bank[idx][5];
  mbank[5]=val;
  sum=mbank[0]+mbank[1]+mbank[2]+mbank[3]+mbank[4]+mbank[5];
  avr=sum/6L;
  memcpy(gValue.bank[idx],mbank,sizeof(mbank));
 
  if(avr>1024L)avr=1024L;
  return avr;
}

u16 quantized(u16 val, u16 offset)
{
  u16 result;
  u16 lvaluet;
  u16 delta;
  if(val<=offset) return 0;
  lvaluet=val-offset;
  delta= TROTTLE_MAX- offset;
  
  result=(lvaluet*1024L)/(delta);
  //result=(val*1024L*10)/(delta*10);
  result=(result>1020L) ? 1024L: result;
  return result;
}

u16 quantized_percent(u16 val)
{
  u16 result;
  result=(val*1000L)/10240L;
  result=(result>97L) ? 100L: result;
  return result;
}

void bk_release()
{
  if(BREAK_FLAG==0 && (gValue.ctrlValue>10 || gValue.ex_pwm>0) ) {
    set_brake(0);//break release(해재)
    return;
  }
}

void set_brake(u8 brake)
{
  //_printf("set_brake[%d]",brake);
  if(gValue.state>=0xA0){
    GPIO_WriteLow(pBK_port, pBK);
    return;
  }
  if(brake) GPIO_WriteLow(pBK_port, pBK);
  else GPIO_WriteHigh(pBK_port, pBK); 
  gValue.stop=brake;
}

u8 get_bisang()
{
  u8 value;
 value = GPIO_ReadInputPin(pBISANG_port, pBISANG);
 value= (value) ? 0:1;
 return value;
}

u8 get_brake(void)
{
  u8 bk;
  bk = GPIO_ReadInputPin(pRTN_port, pRTN);
  bk=1;
  return bk;
}

void set_diretion_ctrl(u8 dir)
{
  //printf("#(2)set_diretion_led[%d]\r\n",dir);
  switch(dir){
    case 0://ff
      CF_CAR_Driver_direction(0);//전진
      break;  
    case 1://rr
      CF_CAR_Driver_direction(1);//후진
      break;
  }
   
}


void set_led(u8 f, u8 r)
{
 // printf("#(2)set_diretion_led[%d]\r\n",dir);
  u8 led=0;
  if(f)_SetBit(led, 0); else _ClearBit(led, 0);
  if(r)_SetBit(led, 1); else _ClearBit(led, 1);
  
  switch(led){
    case 0:
      GPIO_WriteHigh(pLED_FF_port, pLED_FF);
      GPIO_WriteHigh(pLED_RR_port, pLED_RR);      
      break;  
    case 1:
      GPIO_WriteLow(pLED_FF_port, pLED_FF);
      GPIO_WriteHigh(pLED_RR_port, pLED_RR);
      break;
    case 2:
      GPIO_WriteHigh(pLED_FF_port, pLED_FF);
      GPIO_WriteLow(pLED_RR_port, pLED_RR);
      break;   
    case 3:
      GPIO_WriteLow(pLED_FF_port, pLED_FF);
      GPIO_WriteLow(pLED_RR_port, pLED_RR);
      break;       
      
  }
   
}

void set_diretion_led(u8 dir)
{
  //printf("#(2)set_diretion_led[%d]\r\n",dir);
  switch(dir){
    case 0://ff
      //CF_CAR_Driver_direction(0);//전진
      GPIO_WriteLow(pLED_FF_port, pLED_FF);
      GPIO_WriteHigh(pLED_RR_port, pLED_RR);      
      break;  
    case 1://rr
      //CF_CAR_Driver_direction(1);//후진
      GPIO_WriteHigh(pLED_FF_port, pLED_FF);
      GPIO_WriteLow(pLED_RR_port, pLED_RR);
      break;
    case 2://stop
      GPIO_WriteHigh(pLED_FF_port, pLED_FF);
      GPIO_WriteHigh(pLED_RR_port, pLED_RR);
      break;   
  }
   
}


u16 get_adc(u8 mChannel)
{
  u16 ResultADC = 0;
  u16 temph = 0;
  u8 templ = 0;

  ADC1->CSR  = 0x00;
  ADC1->CR1  = 0x00;
  ADC1->CR2  = 0x00;
  ADC1->CR3  = 0x00;
  ADC1->CR1 |= 0x40;//2mhz
  ADC1->CR2 |= 0x08;//0x08;//0000_1000 ;right, No scan,

  ADC1->CSR |= (u8)mChannel;
  ADC1->CR1 |= ADC1_CR1_ADON;//ADC on
  ADC1->CR1 |= ADC1_CR1_ADON;

  while(!(ADC1->CSR & 0x80));

  templ = ADC1->DRL;
  temph = ADC1->DRH;
  temph = (u16)(templ | (u16)(temph << 8));

  ResultADC = temph;
  ADC1->CSR &= 0x7F;//EOC Clear

 return ResultADC & 0x3ff;
}
