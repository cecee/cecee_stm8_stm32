#include "kn_pwm_cf.h"
#include "stm8s_gpio.h"
#include "kn_motor_6ax.h"
#include "include.h"
#include "util.h"
#include <stdio.h>

#define RR_PERCENT 100  //후진 퍼센트
#define FET_MAX  980L //1024
#define SLOW_DECAY 1024L //

u16 TIM1_Period=1024;//10000;//1023;


void PWM_init(void)
{
          
  u16 pwm_init=0;
  
  TIM1->CR1 &= ~ BIT(0); // Close TIM1 
  TIM1->PSCRH = 0; 
  TIM1->PSCRL = 0; // 0: 16.5KHZ 7:1.9KHZ  8:1.72KHZ
  
  TIM1->ARRH = TIM1_Period>>8; 
  TIM1->ARRL = TIM1_Period&0x00ff;

  TIM1->CCR1H = pwm_init>>8;
  TIM1->CCR1L = pwm_init&0x00ff; //
  TIM1->CCR2H = pwm_init>>8;
  TIM1->CCR2L = pwm_init&0x00ff; // 
  
  TIM1->CCMR1 = 0x60; //  CH1 PWM mode 1
  TIM1->CCMR2 = 0x60; //  CH2 PWM mode 1
  
  TIM1->CCER1 |= BIT(0); //  Enable OC1(CH1)
  TIM1->CCER1 |= BIT(4); //  Enable OC2(CH2)

  TIM1->CR1 |= BIT(0); //  enable TIM1 
  TIM1->BKR |= BIT(7); //  prohibit brake 
 
 // ---------------------------------
}


void PWM_Enable(u8 enable)
{
  if(enable) {
    GPIO_WriteHigh(pDIR_EN_port, pDIR_EN);
    GPIO_WriteHigh(pDIR_EN2_port, pDIR_EN2);
  }
  else {
    GPIO_WriteLow(pDIR_EN_port, pDIR_EN);
    GPIO_WriteLow(pDIR_EN2_port, pDIR_EN2);    
  }
}


void set_pwm2(u16 pwmA, u16 pwmB)
{
  TIM1->CCR1H = pwmA>>8;
  TIM1->CCR1L = pwmA&0x00ff; //
  TIM1->CCR2H = pwmB>>8;
  TIM1->CCR2L = pwmB&0x00ff; // 
  gValue.ex_pwmA=pwmA;
  gValue.ex_pwmB=pwmB;
  if(pwmA>=pwmB) gValue.ex_pwm=pwmA;
  else gValue.ex_pwm=pwmB;
}

void set_pwmA(u16 pwm)
{
  TIM1->CCR1H = pwm>>8;
  TIM1->CCR1L = pwm&0x00ff; //
  gValue.ex_pwmA=pwm;
}

void set_pwmB(u16 pwm)
{
  TIM1->CCR2H = pwm>>8;
  TIM1->CCR2L = pwm&0x00ff; // 
  gValue.ex_pwmB=pwm;
}


//void set_pwm_frequency(u16 div)
//{
//    TIM1->PSCRH = div>>8;
//    TIM1->PSCRL = div&0xFF; // 0: 16.5KHZ 7:1.9KHZ  8:1.72KHZ
//}


void slow_decay()
{
    u16 ldecay=20;
    //printf("SLOW DECAYING[%d]~~\r\n",ldecay);
    set_pwm2(SLOW_DECAY, SLOW_DECAY);
    delay_ms(200);// 
    set_pwm2(0, 0);
    gValue.ex_pwm=0;
  
}

void decay(u16 time){
    set_pwm2(0, 0);
    delay_ms(time);// 
}

void fast_decay(u16 time)
{
    //printf("FAST DECAYING~~\r\n");
    set_pwm(0);
    delay_ms(300);//
    gValue.ex_pwm=0;
  
}

void set_pwm(u16 req_pwm)
{
    set_pwm2(req_pwm, req_pwm);
    //gValue.ex_pwm=req_pwm;
    //gValue.ex_pwmA=req_pwm;
    //gValue.ex_pwmB=req_pwm;
}


void PWM_1ms(void)
{

  u16 percent, req_pwm, ctrlValue;
  int mControlPWM, mControlPWM1, mControlPWM2;
  
  percent=gValue.f_percent;
  ctrlValue=gValue.ctrlValue;
//  if(gValue.input.b.fr==0) { percent=gValue.f_percent;}
//  else if(gValue.input.b.fr==1) { percent=RR_PERCENT;}
//  else percent=0;
  switch(gValue.state){
    case 0: //정지      
    case 2://fast DN
    case 3://change Dirction
      return;
    case 1: //운행
      break;      
    case 4://rotation
      if(gValue.input.b.fr!=2){
        if(ctrlValue<ROTATION_PWM) ctrlValue=ROTATION_PWM;
      }
      break; 
    default: return;    
  }  
  
  if(gValue.input.b.fr==2) { percent=0;}
  req_pwm= ((ctrlValue/100L) * percent) + ((ctrlValue%100L) * percent)/100L;
  
  if(req_pwm>gValue.ex_pwm){
    mControlPWM= gValue.ex_pwm+1;
    mControlPWM = (mControlPWM>=FET_MAX) ? FET_MAX :mControlPWM;
  }
  else if(req_pwm<gValue.ex_pwm) {
    mControlPWM =  gValue.ex_pwm- 1;
    if(mControlPWM<=0) mControlPWM=0;
  }
  else mControlPWM= gValue.ex_pwm;
  
  mControlPWM1=mControlPWM2=mControlPWM;
  
  //set_pwm(mControlPWM);
  if((gValue.input.b.Lsw || gValue.input.b.Rsw) && (gValue.state!=4)){
    //printf("LR[%d]\r\n",mControlPWM);
    if(gValue.input.b.Lsw) {
      mControlPWM1= gValue.ex_pwmA-2;
      if(mControlPWM1<=(mControlPWM/3)) mControlPWM1=mControlPWM/3;
    }
    else if(gValue.input.b.Rsw) {
      mControlPWM2= gValue.ex_pwmB-2;
      if(mControlPWM2<=(mControlPWM/3)) mControlPWM2=mControlPWM/3;      
    }
  }
  //printf("LR[%d][%d]\r\n",mControlPWM1,mControlPWM2);
  set_pwm2(mControlPWM1, mControlPWM2);
}


