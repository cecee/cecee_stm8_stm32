#ifndef EXTERN_H_
#define EXTERN_H_


#ifdef __cplusplus
// extern "C" {
#endif


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
//#include <string>
//#include "string.h"
//#include<string.h>
#include "stm32f1xx_hal.h"
#include "gpio.h"
#include "../device/ili9488/ILI9488_GFX.h"
#include "../device/ili9488/ili9488_STM32_Driver.h"
#include "../device/view_page/o3_main_page.h"
#include "../device/keypad/keypad_adc.h"
#include "../device/rs485/rs485.h"
#include "../device/orange_box/orange.h"
#include "../device/spi_flash/spi_flash.h"
#include "../device/ozone_control/ozone_control.h"
#include "../device/modem/modem.h"

//#include "../device/mqtt/mqtt.h"
//#include "../device/util/util.h"
//#include "iwdg.h"

//#include "../device/HX711.h"
//#include "../device/cat4109.h"
//#include "../device/tja1050.h"
//#include "../device/ac_relay.h"
//#include "../device/queue.h"
//#include "../device/Hvac_power_detect.h"

//#include "gpio.h"

#define MYID 0xAAA0 //O3 control BOX
#define LOGOUT

#define SBOX1 0
#define SBOX2 1
#define SBOX3 2
#define BBOX 3

#define FALSE		0
#define TRUE		1
#define RET_OK		(0)
#define RET_ERR	(-1)
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)
#define ERR		1
#define WARN	     2
#define MSG		3
#define PRT		4
#define GRN		5
#define ATR		6

#define HIGH 1
#define LOW  0

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사
 
typedef  uint8_t u8;
typedef  uint16_t u16;

typedef struct _LED_BIT
{
    uint8_t  fan: 1;
    uint8_t  o3: 1;
    uint8_t  uv: 1;	
    uint8_t  err: 1;	
}LED_BIT;    

typedef struct _REQUEST   
{
  uint8_t stx;
  uint8_t size;
  uint16_t id;
  uint8_t data;
  uint8_t dumy;  
  uint8_t sum;
  uint8_t etx;
}REQUEST;

typedef struct _UP_DATA   
{
  uint8_t stx; // 1
  uint8_t size;	//1
  uint16_t id; //2
  uint8_t sum; //1
  uint8_t etx; //1
}UP_DATA;

extern uint8_t JOB_SEQ;
extern uint8_t MQTT_PUP;
extern uint8_t SEQ_CNT;
extern int8_t JOB_State;
extern int8_t exJOB_State;

extern SUB_BOARD_ACCESS sub_bd_access; 


extern KEYPAD_ADC *pKEY;
extern RS485 *pRS485;
extern ORANGE_BOX *pOrange;
extern OZONE_CONTROL *pOzoneCtrl;
extern MODEM *pModem;
//extern MQTT *pMqtt;
//extern UTIL *pUtil;

extern uint8_t TIM2_Event;
extern uint8_t TIM1_KeyEvent;
extern uint16_t TICK_SEC;
extern uint16_t TICK_MIN;
extern uint16_t ELAPSED_MIN;
extern uint16_t ELAPSED_SEC;


extern UP_DATA updata;
//extern RELAY_BIT relay[4];
extern LED_BIT led;

extern uint8_t rxd1; 
extern uint8_t rxd2; 
extern uint8_t rxd4; 



#ifdef __cplusplus
//}
#endif

 
#endif
