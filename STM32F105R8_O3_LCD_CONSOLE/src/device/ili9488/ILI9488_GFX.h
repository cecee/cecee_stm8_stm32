//-----------------------------------
//	ILI9488 GFX library for STM32
//-----------------------------------
#ifndef ILI9341_GFX_H
#define ILI9341_GFX_H

#ifdef __cplusplus
extern "C" {
#endif
  
#include "stm32f1xx_hal.h"
#include "ILI9488_STM32_Driver.h"

#define HORIZONTAL_IMAGE 0
#define VERTICAL_IMAGE	1

typedef struct 
{
  uint16_t info[2];
}FONT_CHAR_INFO; 

typedef struct 
{
  uint8_t CharHeight;
  uint8_t StartChar;
  uint8_t EndChar;
  uint8_t WidthPixcel;
  const FONT_CHAR_INFO *Descriptors;
  const uint8_t *Bitmaps;
}FONT_INFO; 

void _drawFastVLine(int16_t x, int16_t y, int16_t h, color_t color);
void _drawFastHLine(int16_t x, int16_t y, int16_t w, color_t color);
void _fillRect(int16_t x, int16_t y, int16_t w, int16_t h, color_t color);
void fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, color_t color);
void TFT_fillRoundRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t r, color_t color);

void ILI9341_Draw_Hollow_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color);
void ILI9341_Draw_Filled_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color);
void ILI9341_Draw_Hollow_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color);
void ILI9341_Draw_Hollow_Rectangle_Fill(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color);

void ILI9341_Draw_Filled_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color);
void ILI9341_Draw_Char(char Character, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour);
void ILI9341_Draw_Text(const char* Text, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour);
void ILI9341_Draw_Filled_Rectangle_Size_Text(uint16_t X0, uint16_t Y0, uint16_t Size_X, uint16_t Size_Y, color_t color);
void ILI9341_Draw_LedBar(uint16_t x, uint16_t y, uint16_t value);

//USING CONVERTER: http://www.digole.com/tools/PicturetoC_Hex_converter.php
//65K colour (2Bytes / Pixel)
//void ILI9341_Draw_Image(const char* Image_Array, uint8_t Orientation);
void ILI9341_Draw_Image(const char* Image_Array, uint8_t Orientation, uint16_t posX, uint16_t posY, uint16_t imgX, uint16_t imgY);

void GUI_DrawFontTTF(uint8_t fntsz, uint16_t x, uint16_t y, color_t fc, color_t bc, uint8_t *s,uint8_t mode);

#ifdef __cplusplus
}
#endif

#endif
