//-----------------------------------
//	ILI9488 Driver library for STM32
//-----------------------------------
#ifndef ILI9341_STM32_DRIVER_H
#define ILI9341_STM32_DRIVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"

//#define ILI9341_SCREEN_HEIGHT 240 
//#define ILI9341_SCREEN_WIDTH 	320

#define ILI9488_SCREEN_HEIGHT 320 
#define ILI9488_SCREEN_WIDTH 480
//SPI INSTANCE
#define HSPI_INSTANCE	&hspi2

//CHIP SELECT PIN AND PORT, STANDARD GPIO
#define LCD_CS_PORT	SPI2_CS_GPIO_Port
#define LCD_CS_PIN	SPI2_CS_Pin

//DATA COMMAND PIN AND PORT, STANDARD GPIO
#define LCD_DC_PORT	LCD_DC_GPIO_Port
#define LCD_DC_PIN	LCD_DC_Pin

//RESET PIN AND PORT, STANDARD GPIO
#define	LCD_RST_PORT	pLCD_RESET_GPIO_Port
#define	LCD_RST_PIN	pLCD_RESET_Pin

//500
#define BURST_MAX_SIZE 	600 
#define SCREEN_VERTICAL_1		0
#define SCREEN_HORIZONTAL_1		1
#define SCREEN_VERTICAL_2		2
#define SCREEN_HORIZONTAL_2		3
//#define tft_color(color) ( (uint16_t)((color >> 8) | (color << 8)) )
#define _swap(a, b) { int16_t t = a; a = b; b = t; }

#define	LCD_CS_SET  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET)
#define	LCD_CS_CLR  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET)
#define	LCD_DC_SET  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET)
#define	LCD_DC_CLR  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET)

typedef struct {
	uint8_t r;
	uint8_t g;
	uint8_t b;
}color_t;

extern const color_t TFT_BLACK;
extern const color_t TFT_NAVY;
extern const color_t TFT_DARKGREEN;
extern const color_t TFT_DARKCYAN;
extern const color_t TFT_MAROON;
extern const color_t TFT_PURPLE ; 
extern const color_t TFT_OLIVE;
extern const color_t TFT_LIGHTGREY;
extern const color_t TFT_DARKGREY;
extern const color_t TFT_GREY;
extern const color_t TFT_BLUE;
extern const color_t TFT_GREEN;
extern const color_t TFT_CYAN;
extern const color_t TFT_RED; 
extern const color_t TFT_MAGENTA;
extern const color_t TFT_YELLOW;
extern const color_t TFT_WHITE ;
extern const color_t TFT_ORANGE ;
extern const color_t TFT_GREENYELLOW;
extern const color_t TFT_PINK;
extern uint16_t LCD_HEIGHT;
extern uint16_t LCD_WIDTH;

void ili9488_test(void);
void ILI9341_SPI_Init(void);
void ILI9341_SPI_Send(unsigned char SPI_Data);
void ILI9341_Write_Command(uint8_t Command);
void ILI9341_Write_Data(uint8_t Data);
void ILI9341_WriteData_16Bit(uint16_t Data);

void ILI9341_Set_Address(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);
void ILI9341_Reset(void);
void ILI9341_Set_Rotation(uint8_t Rotation);
void ILI9341_Enable(void);
void ILI9341_Init(void);


void ILI9341_Fill_Screen(color_t color);
void ILI9341_Draw_Colour(color_t color);
void ILI9341_Draw_Pixel(uint16_t X,uint16_t Y, color_t color);
void ILI9341_Draw_Colour_Burst(color_t color, uint32_t Size);

void ILI9341_Draw_Rectangle(uint16_t X, uint16_t Y, uint16_t Width, uint16_t Height, color_t color);
void ILI9341_Draw_Horizontal_Line(uint16_t X, uint16_t Y, uint16_t Width, color_t color);
void ILI9341_Draw_Vertical_Line(uint16_t X, uint16_t Y, uint16_t Height, color_t color);
///
void commandList(const uint8_t *addr);

#ifdef __cplusplus
}
#endif

#endif

