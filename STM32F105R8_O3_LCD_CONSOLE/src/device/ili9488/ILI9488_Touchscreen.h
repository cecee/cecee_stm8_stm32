#ifndef TOUCH_H
#define TOUCH_H

#include "stm32f1xx_hal.h"
//RETURN VALUES FOR TP_Touchpad_Pressed 
#define TOUCHPAD_NOT_PRESSED	0
#define TOUCHPAD_PRESSED		1

//RETURN VALUES FOR TP_Read_Coordinates
#define TOUCHPAD_DATA_OK					1
#define TOUCHPAD_DATA_NOISY				0

//HARDCODED CALIBRATION
#define TP_XA	600
#define TP_XB	3280
#define TP_YA	1070
#define TP_YB	2900	
#define TP_DX	TP_XB-TP_XA
#define TP_DY	TP_YB-TP_YA	
		


//#define NO_OF_POSITION_SAMPLES	 	1000
typedef struct {
	uint16_t x;
	uint16_t y;
} touch_pos_t;
		
typedef struct {
	uint16_t xA;
	uint16_t xB;
	uint16_t yA;
	uint16_t yB;
	uint16_t deltaX;
	uint16_t deltaY;  
} touch_offset_t;

extern const touch_offset_t tOffSet;
extern touch_pos_t gTpos;

void touch_init(void);
void TouchRead(void);

uint16_t TouchReadX(void);
uint16_t TouchReadY(void);

//Read coordinates of touchscreen press. Position[0] = X, Position[1] = Y
//uint8_t TP_Read_Coordinates(uint16_t Coordinates[2]);

//Check if Touchpad was pressed. Returns TOUCHPAD_PRESSED (1) or TOUCHPAD_NOT_PRESSED (0)
//uint8_t TP_Touchpad_Pressed(void);

#endif
