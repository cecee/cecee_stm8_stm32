//-----------------------------------
//	ILI9488 Driver library for STM32
//-----------------------------------
#include "ILI9488_STM32_Driver.h"
#include "spi.h"
#include "gpio.h"
#include "extern.h"

//#include "snow_tiger.h"
//#include "red.h"

#define DELAY 0x80
#define ILI9341_SWRESET 0x01
#define ILI9341_PIXFMT  0x3A
#define TFT_MADCTL	   0x36
#define MADCTL_MY  0x80
#define MADCTL_MX  0x40
#define MADCTL_MV  0x20
#define MADCTL_ML  0x10
#define MADCTL_RGB 0x00
#define MADCTL_BGR 0x08
#define MADCTL_MH  0x04
/* Global Variables ------------------------------------------------------------------*/
extern TIM_HandleTypeDef htim1;

uint16_t LCD_HEIGHT = ILI9488_SCREEN_HEIGHT;
uint16_t LCD_WIDTH  = ILI9488_SCREEN_WIDTH;

// Color definitions constants
const color_t TFT_BLACK       = {   0,   0,   0 };
const color_t TFT_NAVY        = {   0,   0, 128 };
const color_t TFT_DARKGREEN   = {   0, 128,   0 };
const color_t TFT_DARKCYAN    = {   0, 128, 128 };
const color_t TFT_MAROON      = { 128,   0,   0 };
const color_t TFT_PURPLE      = { 128,   0, 128 };
const color_t TFT_OLIVE       = { 128, 128,   0 };
const color_t TFT_LIGHTGREY   = { 192, 192, 192 };
const color_t TFT_DARKGREY    = { 128, 128, 128 };
const color_t TFT_GREY    = { 93, 88, 83 };
const color_t TFT_BLUE        = {   0,   0, 255 };
const color_t TFT_GREEN       = {   0, 255,   0 };
const color_t TFT_CYAN        = {   0, 255, 255 };
const color_t TFT_RED         = { 255,   0,   0 };
const color_t TFT_MAGENTA     = { 255,   0, 255 };
const color_t TFT_YELLOW      = { 255, 255,   0 };
const color_t TFT_WHITE       = { 255, 255, 255 };
const color_t TFT_ORANGE      = { 255, 165,   0 };
const color_t TFT_GREENYELLOW = { 173, 255,  47 };
const color_t TFT_PINK        = { 255, 192, 203 };

const uint8_t ILI9488_init[] = {
  18,                   	// 18 commands in list
  ILI9341_SWRESET, DELAY,   	// 1: Software reset, no args, w/delay
  200,				// 200 ms delay
  0xE0, 15, 0x00, 0x03, 0x09, 0x08, 0x16, 0x0A, 0x3F, 0x78, 0x4C, 0x09, 0x0A, 0x08, 0x16, 0x1A, 0x0F,
  0xE1, 15,	0x00, 0x16, 0x19, 0x03, 0x0F, 0x05, 0x32, 0x45, 0x46, 0x04, 0x0E, 0x0D, 0x35, 0x37, 0x0F,
  0xC0, 2,   //Power Control 1
	0x17,    //Vreg1out
	0x15,    //Verg2out
  0xC1, 1,   //Power Control 2
	0x41,    //VGH,VGL
  0xC5, 3,   //Power Control 3
	0x00,
	0x12,    //Vcom
	0x80,
  TFT_MADCTL, 1, // Memory Access Control (orientation)
  (MADCTL_MV | MADCTL_BGR),
  // *** INTERFACE PIXEL FORMAT: 0x66 -> 18 bit; 0x55 -> 16 bit
  ILI9341_PIXFMT, 1, 0x66,
  0xB0, 1, // Interface Mode Control
  0x00,    // 0x80: SDO NOT USE; 0x00 USE SDO
  0xB1, 1, //Frame rate
  0xA0,    //60Hz
  0xB4, 1, //Display Inversion Control
  0x02,    //2-dot
  0xB6, 2, //Display Function Control  RGB/MCU Interface Control
  0x02,    //MCU
  0x02,    //Source,Gate scan direction
  0xE9, 1, // Set Image Function
  0x00,    // Disable 24 bit data
  0x53, 1, // Write CTRL Display Value
  0x28,    // BCTRL && DD on
  0x51, 1, // Write Display Brightness Value
  0x80,    //
  0xF7, 4, // Adjust Control
  0xA9,
  0x51,
  0x2C,
  0x02,    // D7 stream, loose
  0x11, DELAY,  //Exit Sleep
  120,
  0x29, 0,      //Display on
};

void ili9488_test(void){
	printf("ili9488_test..!!\n");
	//ILI9341_Fill_Screen(TFT_GREY);
	ILI9341_Fill_Screen(TFT_YELLOW);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	//ILI9341_Draw_LedBar(20,100,50);
	//return;
#if 1		
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	//ILI9341_Set_Rotation(SCREEN_VERTICAL_2);
	ILI9341_Draw_Text("FPS TEST, 40 loop 2 screens", 10, 10, TFT_BLACK, 1, TFT_RED);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_YELLOW);

	uint32_t Timer_Counter = 0;
	for(uint32_t j = 0; j < 2; j++)
	{
		HAL_TIM_Base_Start(&htim1);
		printf("loop[%d]\r\n",j);
		for(uint16_t i = 0; i < 2; i++)
		{
			printf("TFT_WHITE\r\n");
			ILI9341_Fill_Screen(TFT_WHITE);
			printf("TFT_BLACK\r\n");
			ILI9341_Fill_Screen(TFT_BLACK);
		}
		//20.000 per second! 50ms
		HAL_TIM_Base_Stop(&htim1);		
		Timer_Counter += __HAL_TIM_GET_COUNTER(&htim1);
		__HAL_TIM_SET_COUNTER(&htim1, 0);
	}
	
	
	Timer_Counter /= 2;
	printf("Timer_Counter[%d]\r\n",Timer_Counter);
		
	char counter_buff[30];		
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	sprintf(counter_buff, "Timer counter value: %d", Timer_Counter*2);		
	ILI9341_Draw_Text(counter_buff, 10, 10, TFT_BLACK, 1, TFT_WHITE);
		
	double seconds_passed = 2*((float)Timer_Counter / 20000);
	sprintf(counter_buff, "Time: %.3f Sec", seconds_passed);		
	ILI9341_Draw_Text(counter_buff, 10, 30, TFT_BLACK, 2, TFT_WHITE);
	
	double timer_float = 20/(((float)Timer_Counter)/20000);	//Frames per sec
	
	sprintf(counter_buff, "FPS:  %.2f", timer_float);
	ILI9341_Draw_Text(counter_buff, 10, 50, TFT_BLACK, 2, TFT_WHITE);
	double MB_PS = timer_float*240*320*2/1000000;
	sprintf(counter_buff, "MB/S: %.2f", MB_PS);
	ILI9341_Draw_Text(counter_buff, 10, 70, TFT_BLACK, 2, TFT_WHITE);
	double SPI_utilized_percentage = (MB_PS/(6.25 ))*100;		//50mbits / 8 bits
	sprintf(counter_buff, "SPI Utilized: %.2f", SPI_utilized_percentage);
	ILI9341_Draw_Text(counter_buff, 10, 90, TFT_BLACK, 2, TFT_WHITE);
	HAL_Delay(1000);
#endif	
	//static uint16_t x = 0;
	//static uint16_t y = 0;
	//return;
//----------------------------------------------------------COUNTING MULTIPLE SEGMENTS		
#if 1	
	char Temp_Buffer_text[40];
	ILI9341_Fill_Screen(TFT_WHITE);
	//ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Set_Rotation(SCREEN_VERTICAL_2);
	ILI9341_Draw_Text("Counting multiple segments at once", 10, 10, TFT_RED, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);
	for(uint16_t i = 0; i <= 5; i++)
	{
		sprintf(Temp_Buffer_text, "Counting: %d", i);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 10, TFT_BLACK, 1, TFT_WHITE);		
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 30, TFT_BLUE, 1, TFT_WHITE);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 50, TFT_RED, 1, TFT_WHITE);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 70, TFT_GREEN, 1, TFT_WHITE);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 90, TFT_BLACK, 3, TFT_YELLOW);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 110, TFT_BLUE, 3, TFT_YELLOW);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 130, TFT_RED, 3, TFT_YELLOW);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 150, TFT_GREEN, 3, TFT_YELLOW);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 170, TFT_WHITE, 3, TFT_YELLOW);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 190, TFT_BLUE, 3, TFT_YELLOW);
		ILI9341_Draw_Text(Temp_Buffer_text, 10, 210, TFT_RED, 3, TFT_YELLOW);		
	}
	
	HAL_Delay(1000);
#endif	
//----------------------------------------------------------COUNTING SINGLE SEGMENT	
#if 0
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Counting single segment", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);

	for(uint16_t i = 0; i <= 100; i++)
	{
	sprintf(Temp_Buffer_text, "Counting: %d", i);
	ILI9341_Draw_Text(Temp_Buffer_text, 10, 10, TFT_BLACK, 3, TFT_WHITE);			
	}
	
	HAL_Delay(1000);
#endif
//----------------------------------------------------------ALIGNMENT TEST
#if 1
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Rectangle alignment", 10, 10, TFT_BLACK, 3, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);
	
	ILI9341_Draw_Hollow_Rectangle_Coord(50, 50, 100, 100, TFT_BLACK);
	ILI9341_Draw_Filled_Rectangle_Coord(20, 20, 50, 50, TFT_BLACK);
	ILI9341_Draw_Hollow_Rectangle_Coord(10, 10, 19, 19, TFT_BLACK);
	ILI9341_Draw_Filled_Rectangle_Coord(430, 270, 480, 320, TFT_GREEN);
	HAL_Delay(1000);
#endif	
//----------------------------------------------------------LINES EXAMPLE		
#if 1
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Randomly placed and sized", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("Horizontal and Vertical lines", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);
		
	for(uint32_t i = 0; i < 30000; i++)
	{
		//uint32_t random_num = 0;
		uint16_t xr = 0;
		uint16_t yr = 0;
		uint16_t radiusr = 0;
		color_t colourr={0,0,0};

		xr = rand();
		yr = rand();
		radiusr = rand();

		colourr.r = rand();
		colourr.g = rand();
		colourr.b = rand();
		
		xr &= 0x01FF;
		yr &= 0x01FF;
		radiusr &= 0x001F;			
		//ili9341_drawpixel(xr, yr, WHITE);
		ILI9341_Draw_Horizontal_Line(xr, yr, radiusr, colourr);			
		ILI9341_Draw_Vertical_Line(xr, yr, radiusr, colourr);
	}
	
	HAL_Delay(1000);
#endif			
//----------------------------------------------------------HOLLOW CIRCLES EXAMPLE		
#if 1
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Randomly placed and sized", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("Circles", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);		
	for(uint32_t i = 0; i < 3000; i++)
	{
		//uint32_t random_num = 0;
		uint16_t xr = 0;
		uint16_t yr = 0;
		uint16_t radiusr = 0;
		color_t colourr={0,0,0};

		xr = rand();
		yr = rand();
		radiusr = rand();

		colourr.r = rand();
		colourr.g = rand();
		colourr.b = rand();
		
		xr &= 0x01FF;
		yr &= 0x01FF;
		radiusr &= 0x001F;			
		//ili9341_drawpixel(xr, yr, WHITE);
		ILI9341_Draw_Hollow_Circle(xr, yr, radiusr*2, colourr);
	}
	HAL_Delay(1000);
#endif
//----------------------------------------------------------FILLED CIRCLES EXAMPLE		
#if 1
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Randomly placed and sized", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("Filled Circles", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);
		
	for(uint32_t i = 0; i < 1000; i++)
	{
		//uint32_t random_num = 0;
		uint16_t xr = 0;
		uint16_t yr = 0;
		uint16_t radiusr = 0;
		color_t colourr={0,0,0};

		xr = rand();
		yr = rand();

		radiusr = rand();
		colourr.r = rand();
		colourr.g = rand();
		colourr.b = rand();
		
		xr &= 0x01FF;
		yr &= 0x01FF;
		radiusr &= 0x001F;			
		//ili9341_drawpixel(xr, yr, WHITE);
		ILI9341_Draw_Filled_Circle(xr, yr, radiusr/2, colourr);
	}
	HAL_Delay(1000);
#endif
//----------------------------------------------------------HOLLOW RECTANGLES EXAMPLE		
#if 0
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Randomly placed and sized", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("Rectangles", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);
	
	for(uint32_t i = 0; i < 20000; i++)
	{
		uint32_t random_num = 0;
		uint16_t xr = 0;
		uint16_t yr = 0;
		uint16_t radiusr = 0;
		color_t colourr={0,0,0};

		xr = rand();
		yr = rand();
		radiusr = rand();
		colourr.r = rand();
		colourr.g = rand();
		colourr.b = rand();
		
		xr &= 0x01FF;
		yr &= 0x01FF;
		radiusr &= 0x001F;			
		//ili9341_drawpixel(xr, yr, WHITE);
		ILI9341_Draw_Hollow_Rectangle_Coord(xr, yr, xr+radiusr, yr+radiusr, colourr);
	}
	HAL_Delay(1000);
#endif
//----------------------------------------------------------FILLED RECTANGLES EXAMPLE		
#if 0
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Randomly placed and sized", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("Filled Rectangles", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);
	
	for(uint32_t i = 0; i < 20000; i++)
	{
		uint32_t random_num = 0;
		uint16_t xr = 0;
		uint16_t yr = 0;
		uint16_t radiusr = 0;
		color_t colourr={0,0,0};

		xr = rand();
		yr = rand();
		radiusr = rand();
		colourr.r = rand();
		colourr.g = rand();
		colourr.b = rand();
		
		xr &= 0x01FF;
		yr &= 0x01FF;
		radiusr &= 0x001F;			
		//ili9341_drawpixel(xr, yr, WHITE);
		ILI9341_Draw_Rectangle(xr, yr, radiusr, radiusr, colourr);
	}
	HAL_Delay(1000);
#endif			
//----------------------------------------------------------INDIVIDUAL PIXEL EXAMPLE		
#if 0		
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Slow draw by selecting", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("and adressing pixels", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);
	x = 0;
	y = 0;	
	while (y < 320)
	{
	while ((x < 480) && (y < 320))
	{
		
		if(x % 2)
		{
			ILI9341_Draw_Pixel(x, y, TFT_BLACK);
		}
		
		x++;
	}
	
		y++;
		x = 0;
	}
	
	x = 0;
	y = 0;	

	
	while (y < 320)
	{
	while ((x < 480) && (y < 320))
	{
		
		if(y % 2)
		{
			ILI9341_Draw_Pixel(x, y, TFT_BLACK);
		}
		
		x++;
	}
	
		y++;
		x = 0;
	}
	HAL_Delay(2000);
#endif		
//----------------------------------------------------------INDIVIDUAL PIXEL EXAMPLE		
#if 0
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Random position and colour", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("500000 pixels", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	ILI9341_Fill_Screen(TFT_WHITE);	

	for(uint32_t i = 0; i < 500000; i++)
	{
		uint32_t random_num = 0;
		uint16_t xr = 0;
		uint16_t yr = 0;
		color_t color={0,0,0};
		color.r = rand();
		color.g = rand();
		color.b = rand();
		xr = rand();
		yr = rand();
		xr &= 0x01FF;
		yr &= 0x01FF;
		ILI9341_Draw_Pixel(xr, yr, color);
	}
	HAL_Delay(2000);
#endif		
//----------------------------------------------------------565 COLOUR EXAMPLE, Grayscale		
#if 0
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("Colour gradient", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("Grayscale", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	HAL_Delay(2000);
	for(uint16_t i = 0; i <= (480); i++)
	{
		uint16_t Red = 0;
		uint16_t Green = 0;
		uint16_t Blue = 0;
		Red = i/(10);
		Red <<= 11;
		Green = i/(5);
		Green <<= 5;
		Blue = i/(10);
		color_t RGB_color;
		RGB_color.r=Red;
		RGB_color.g=Green;
		RGB_color.b=Blue;
		ILI9341_Draw_Rectangle(i, x, 1, 320, RGB_color);
		
	}
	HAL_Delay(2000);
#endif		
//----------------------------------------------------------IMAGE EXAMPLE, Snow Tiger		
#if 0
	ILI9341_Fill_Screen(TFT_WHITE);
	ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	ILI9341_Draw_Text("RGB Picture", 10, 10, TFT_BLACK, 1, TFT_WHITE);
	ILI9341_Draw_Text("TIGER", 10, 20, TFT_BLACK, 1, TFT_WHITE);
	//HAL_Delay(2000);	
	printf("IMAGE EXAMPLE\r\n");
	//ILI9341_Draw_Image((const char*)snow_tiger, SCREEN_VERTICAL_2);	
	ILI9341_Draw_Image((const char*)snow_tiger, SCREEN_HORIZONTAL_2, 240,0, 240, 320);	
	ILI9341_Set_Rotation(SCREEN_VERTICAL_1);
	HAL_Delay(5000);		
#endif
}

/* Initialize SPI */
void ILI9341_SPI_Init(void)
{
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);	//CS OFF
}

/*Send data (char) to LCD*/
void ILI9341_SPI_Send(unsigned char SPI_Data)
{
  HAL_SPI_Transmit(HSPI_INSTANCE, &SPI_Data, 1, 1);
}

/* Send command (char) to LCD */
void ILI9341_Write_Command(uint8_t Command)
{
  LCD_CS_CLR;
  LCD_DC_CLR;	
  ILI9341_SPI_Send(Command);
  LCD_CS_SET;		
}

/* Send Data (char) to LCD */
void ILI9341_Write_Data(uint8_t Data)
{
  LCD_CS_CLR;  
  LCD_DC_SET;	
  ILI9341_SPI_Send(Data);	
  LCD_CS_SET;
}

void ILI9341_WriteData_16Bit(uint16_t Data)
{	
   ILI9341_Write_Data(Data>>8);
   ILI9341_Write_Data(Data);
}

/* Set Address - Location block - to draw into */
void ILI9341_Set_Address(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2)
{
	//printf("ILI9341_Set_Address [%d][%d]-[%d][%d]\r\n",X1,Y1,X2,Y2);
     ILI9341_Write_Command(0x2A);
	ILI9341_Write_Data(X1>>8);
	ILI9341_Write_Data(X1);
	ILI9341_Write_Data(X2>>8);
	ILI9341_Write_Data(X2);

	ILI9341_Write_Command(0x2B);
	ILI9341_Write_Data(Y1>>8);
	ILI9341_Write_Data(Y1);
	ILI9341_Write_Data(Y2>>8);
	ILI9341_Write_Data(Y2);
	ILI9341_Write_Command(0x2C);
}

/*HARDWARE RESET*/
void ILI9341_Reset(void)
{
	HAL_GPIO_WritePin(LCD_RST_PORT, LCD_RST_PIN, GPIO_PIN_SET);
	HAL_Delay(20);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	HAL_Delay(20);
	HAL_GPIO_WritePin(LCD_RST_PORT, LCD_RST_PIN, GPIO_PIN_SET);	
}

/*Ser rotation of the screen - changes x0 and y0*/
void ILI9341_Set_Rotation(uint8_t Rotation) 
{
	
uint8_t screen_rotation = Rotation;

ILI9341_Write_Command(0x36);
HAL_Delay(1);
switch(screen_rotation) 
	{
		case SCREEN_VERTICAL_1:
			ILI9341_Write_Data(0x40|0x08);
			LCD_WIDTH = 320;//240;
			LCD_HEIGHT = 480;//320;
			break;
		case SCREEN_HORIZONTAL_1:
			ILI9341_Write_Data(0x20|0x08);
			LCD_WIDTH  = 480;//320;
			LCD_HEIGHT = 320;//240;
			break;
		case SCREEN_VERTICAL_2:
			ILI9341_Write_Data(0x80|0x08);
			LCD_WIDTH  = 320;//240;
			LCD_HEIGHT = 480;//320;
			break;
		case SCREEN_HORIZONTAL_2:
			ILI9341_Write_Data(0x40|0x80|0x20|0x08);
			LCD_WIDTH  = 480;//320;
			LCD_HEIGHT = 320;//240;
			break;
		default:
			//EXIT IF SCREEN ROTATION NOT VALID!
			break;
	}
}

/*Enable LCD display*/
void ILI9341_Enable(void)
{
  HAL_GPIO_WritePin(BL_ON_GPIO_Port, BL_ON_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LCD_RST_PORT, LCD_RST_PIN, GPIO_PIN_SET);
}

/*Initialize LCD display*/
void ILI9341_Init(void)
{
  printf("ILI9341_Init\r\n");
  ILI9341_Enable();
  ILI9341_SPI_Init();
  ILI9341_Reset();
  commandList(ILI9488_init);
}

void commandList(const uint8_t *addr) {
  uint8_t i, numCommands, numArgs, cmd;
  uint16_t ms;
  uint8_t *value;
  numCommands = *addr++;         // Number of commands to follow
 // printf("numCommands:[%d]\r\n",numCommands);
  while(numCommands--) {         // For each command...
    cmd = *addr++;               // save command
    numArgs  = *addr++;          //   Number of args to follow
    ms       = numArgs & DELAY;  //   If high bit set, delay follows args
    numArgs &= ~DELAY;           //   Mask out delay bit
    //printf("cmd:[%02x] numArgs:[%d]-",cmd, numArgs);
    ILI9341_Write_Command(cmd);
    for(i=0;i<numArgs;i++){
	 value=(uint8_t *)addr+i;
	 ILI9341_Write_Data(value[0]);
    	 //printf("[%02x]",value[0]%0xff);
    }
   // printf("\r\n");
	//disp_spi_transfer_cmd_data(cmd, (uint8_t *)addr, numArgs);
    addr += numArgs;
    if(ms) {
      ms = *addr++;              // Read post-command delay time (ms)
      if(ms == 255) ms = 500;    // If 255, delay for 500 ms
	 HAL_Delay(ms);
	 //printf("DELAY->[%d]ms\r\n",ms);
	 //vTaskDelay(ms / portTICK_RATE_MS);
    }
  }
}



//INTERNAL FUNCTION OF LIBRARY, USAGE NOT RECOMENDED, USE Draw_Pixel INSTEAD
/*Sends single pixel colour information to LCD*/
void ILI9341_Draw_Colour(color_t color)
{
//SENDS COLOUR
unsigned char TempBuffer[3];// = {color>>8, color};
TempBuffer[0]=color.r;
TempBuffer[1]=color.g;
TempBuffer[2]=color.b;

HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);	
HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
//HAL_SPI_Transmit(HSPI_INSTANCE, TempBuffer, 2, 1);
HAL_SPI_Transmit(HSPI_INSTANCE, TempBuffer, 3, 1);
HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);
}

//INTERNAL FUNCTION OF LIBRARY
/*Sends block colour information to LCD*/
void ILI9341_Draw_Colour_Burst(color_t color, uint32_t Size)
{

	unsigned char burst_buffer[BURST_MAX_SIZE];
	uint32_t Buffer_Size = 0;
	if((Size*3) < BURST_MAX_SIZE) Buffer_Size = Size;
	else Buffer_Size = BURST_MAX_SIZE;
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);	
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	for(uint32_t j = 0; j < Buffer_Size; j+=3)  
	{
		burst_buffer[j+0] =255-color.r;//chifted;
		burst_buffer[j+1] =255-color.g;//Colour;
		burst_buffer[j+2] =255-color.b;//Colour;
	}
	//uint32_t Sending_Size = Size*2;
	uint32_t Sending_Size = Size*3;
	uint32_t Sending_in_Block = Sending_Size/Buffer_Size;
	uint32_t Remainder_from_block = Sending_Size%Buffer_Size;
	//printf("Sending_Size[%d] Sending_in_Block[%d] Remainder_from_block[%d]\r\n",Sending_Size,Sending_in_Block, Remainder_from_block);
	if(Sending_in_Block != 0)
	{
	  for(uint32_t j = 0; j < (Sending_in_Block); j++)
          {
            HAL_SPI_Transmit(HSPI_INSTANCE, (unsigned char *)burst_buffer, Buffer_Size, 10);	
          }
	}
	//REMAINDER!
	HAL_SPI_Transmit(HSPI_INSTANCE, (unsigned char *)burst_buffer, Remainder_from_block, 10);	
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);
	//DBG(WARN,"ILI9341_Draw_Colour_Burst..!!\n");
}

//FILL THE ENTIRE SCREEN WITH SELECTED COLOUR (either #define-d ones or custom 16bit)
/*Sets address (entire screen) and Sends Height*Width ammount of colour information to LCD*/

//void ILI9341_Draw_Colour_Burst(color_t color, uint32_t Size)
void ILI9341_Fill_Screen(color_t color)
{
 // DBG(ATR,"ILI9341_Fill_Screen..!!\n");
  ILI9341_Set_Address(0,0,LCD_WIDTH,LCD_HEIGHT);	
  ILI9341_Draw_Colour_Burst(color, LCD_WIDTH*LCD_HEIGHT);	
}

//DRAW PIXEL AT XY POSITION WITH SELECTED COLOUR
//
//Location is dependant on screen orientation. x0 and y0 locations change with orientations.
//Using pixels to draw big simple structures is not recommended as it is really slow
//Try using either rectangles or lines if possible
//
void ILI9341_Draw_Pixel(uint16_t X,uint16_t Y,color_t color) 
{
  if((X >=LCD_WIDTH) || (Y >=LCD_HEIGHT)) return;	//OUT OF BOUNDS!
	
  //ADDRESS
  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET);	
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
  ILI9341_SPI_Send(0x2A);
  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);	
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);		

  //XDATA
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);	
  unsigned char Temp_Buffer[4] = {X>>8,X, (X+1)>>8, (X+1)};
  HAL_SPI_Transmit(HSPI_INSTANCE, Temp_Buffer, 4, 1);
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

  //ADDRESS
  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET);	
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);	
  ILI9341_SPI_Send(0x2B);
  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);			
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);			

  //YDATA
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
  unsigned char Temp_Buffer1[4] = {Y>>8,Y, (Y+1)>>8, (Y+1)};
  HAL_SPI_Transmit(HSPI_INSTANCE, Temp_Buffer1, 4, 1);
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

  //ADDRESS	
  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET);	
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);	
  ILI9341_SPI_Send(0x2C);
  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);			
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);			

  //COLOUR	
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
  unsigned char Temp_Buffer2[3] ;//= {Colour>>8, Colour};
  Temp_Buffer2[0]=255-color.r;
  Temp_Buffer2[1]=255-color.g;
  Temp_Buffer2[2]=255-color.b;

  HAL_SPI_Transmit(HSPI_INSTANCE, Temp_Buffer2, 3, 1);
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);
	
}

//DRAW RECTANGLE OF SET SIZE AND HEIGTH AT X and Y POSITION WITH CUSTOM COLOUR
//
//Rectangle is hollow. X and Y positions mark the upper left corner of rectangle
//As with all other draw calls x0 and y0 locations dependant on screen orientation
//

void ILI9341_Draw_Rectangle(uint16_t X, uint16_t Y, uint16_t Width, uint16_t Height, color_t color)
{
  //printf("LCD_WIDTH:%d X:%d LCD_HEIGHT:%d Y:%d\r\n",LCD_WIDTH, X, LCD_HEIGHT, Y);
  if((X >=LCD_WIDTH) || (Y >=LCD_HEIGHT)) return;
  if((X+Width-1)>=LCD_WIDTH)Width=LCD_WIDTH-X;
  if((Y+Height-1)>=LCD_HEIGHT)Height=LCD_HEIGHT-Y;
  ILI9341_Set_Address(X, Y, X+Width-1, Y+Height-1);
  ILI9341_Draw_Colour_Burst(color, Height*Width);
}

//DRAW LINE FROM X,Y LOCATION to X+Width,Y LOCATION
void ILI9341_Draw_Horizontal_Line(uint16_t X, uint16_t Y, uint16_t Width, color_t color)
{
  if((X >=LCD_WIDTH) || (Y >=LCD_HEIGHT)) return;
  if((X+Width-1)>=LCD_WIDTH)Width=LCD_WIDTH-X;
  ILI9341_Set_Address(X, Y, X+Width-1, Y);
  ILI9341_Draw_Colour_Burst(color, Width);
}

//DRAW LINE FROM X,Y LOCATION to X,Y+Height LOCATION
void ILI9341_Draw_Vertical_Line(uint16_t X, uint16_t Y, uint16_t Height, color_t color)
{
if((X >=LCD_WIDTH) || (Y >=LCD_HEIGHT)) return;
if((Y+Height-1)>=LCD_HEIGHT)
	{
		Height=LCD_HEIGHT-Y;
	}
ILI9341_Set_Address(X, Y, X, Y+Height-1);
ILI9341_Draw_Colour_Burst(color, Height);
}
