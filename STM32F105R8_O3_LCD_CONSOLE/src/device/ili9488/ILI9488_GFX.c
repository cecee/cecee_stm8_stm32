//-----------------------------------
//	ILI9488 GFX library for STM32
//-----------------------------------


#include "ILI9488_GFX.h"
#include "./font/5x5_font.h"
//#include "./font/godic_36pt.h"
#include "./font/godic_24pt.h"
#include "spi.h"
#include "extern.h"
#include "ILI9488_STM32_Driver.h"

/*Draw hollow circle at X,Y location with specified radius and colour. X and Y represent circles center */
void ILI9341_Draw_Hollow_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color)
{
	int x = Radius-1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (Radius << 1);

    while (x >= y)
    {
        ILI9341_Draw_Pixel(X + x, Y + y, color);
        ILI9341_Draw_Pixel(X + y, Y + x, color);
        ILI9341_Draw_Pixel(X - y, Y + x, color);
        ILI9341_Draw_Pixel(X - x, Y + y, color);
        ILI9341_Draw_Pixel(X - x, Y - y, color);
        ILI9341_Draw_Pixel(X - y, Y - x, color);
        ILI9341_Draw_Pixel(X + y, Y - x, color);
        ILI9341_Draw_Pixel(X + x, Y - y, color);

        if (err <= 0)
        {
            y++;
            err += dy;
            dy += 2;
        }
        if (err > 0)
        {
            x--;
            dx += 2;
            err += (-Radius << 1) + dx;
        }
    }
}

/*Draw filled circle at X,Y location with specified radius and colour. X and Y represent circles center */
void ILI9341_Draw_Filled_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color)
{
	
		int x = Radius;
    int y = 0;
    int xChange = 1 - (Radius << 1);
    int yChange = 0;
    int radiusError = 0;

    while (x >= y)
    {
        for (int i = X - x; i <= X + x; i++)
        {
            ILI9341_Draw_Pixel(i, Y + y,color);
            ILI9341_Draw_Pixel(i, Y - y,color);
        }
        for (int i = X - y; i <= X + y; i++)
        {
            ILI9341_Draw_Pixel(i, Y + x,color);
            ILI9341_Draw_Pixel(i, Y - x,color);
        }

        y++;
        radiusError += yChange;
        yChange += 2;
        if (((radiusError << 1) + xChange) > 0)
        {
            x--;
            radiusError += xChange;
            xChange += 2;
        }
    }
		//Really slow implementation, will require future overhaul
		//TODO:	https://stackoverflow.com/questions/1201200/fast-algorithm-for-drawing-filled-circles	
}

/*Draw a hollow rectangle between positions X0,Y0 and X1,Y1 with specified colour*/
void ILI9341_Draw_Hollow_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color)
{
	uint16_t 	X_length = 0;
	uint16_t 	Y_length = 0;
	uint8_t		Negative_X = 0;
	uint8_t 	Negative_Y = 0;
	float 		Calc_Negative = 0;
	
	Calc_Negative = X1 - X0;
	if(Calc_Negative < 0) Negative_X = 1;
	Calc_Negative = 0;
	
	Calc_Negative = Y1 - Y0;
	if(Calc_Negative < 0) Negative_Y = 1;
	
	
	//DRAW HORIZONTAL!
	if(!Negative_X)
	{
		X_length = X1 - X0;		
	}
	else
	{
		X_length = X0 - X1;		
	}
	ILI9341_Draw_Horizontal_Line(X0, Y0, X_length, color);
	ILI9341_Draw_Horizontal_Line(X0, Y1, X_length, color);
	
	
	
	//DRAW VERTICAL!
	if(!Negative_Y)
	{
		Y_length = Y1 - Y0;		
	}
	else
	{
		Y_length = Y0 - Y1;		
	}
	ILI9341_Draw_Vertical_Line(X0, Y0, Y_length, color);
	ILI9341_Draw_Vertical_Line(X1, Y0, Y_length, color);
	
	if((X_length > 0)||(Y_length > 0)) 
	{
		ILI9341_Draw_Pixel(X1, Y1, color);
	}
	
}


void _drawFastVLine(int16_t x, int16_t y, int16_t h, color_t color) {
	ILI9341_Draw_Vertical_Line(x, y, h, color);
}

//--------------------------------------------------------------------------
void _drawFastHLine(int16_t x, int16_t y, int16_t w, color_t color) {
	ILI9341_Draw_Horizontal_Line(x, y, w, color);
}

void _fillRect(int16_t x, int16_t y, int16_t w, int16_t h, color_t color) {
	// clipping
  ILI9341_Draw_Filled_Rectangle_Coord(x, y, x+w, y+h, color);
}

void fillCircleHelper(int16_t x0, int16_t y0, int16_t r,	uint8_t cornername, int16_t delta, color_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;
	int16_t ylm = x0 - r;

	while (x < y) {
		if (f >= 0) {
			if (cornername & 0x1) _drawFastVLine(x0 + y, y0 - x, 2 * x + 1 + delta, color);
			if (cornername & 0x2) _drawFastVLine(x0 - y, y0 - x, 2 * x + 1 + delta, color);
			ylm = x0 - y;
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		if ((x0 - x) > ylm) {
			if (cornername & 0x1) _drawFastVLine(x0 + x, y0 - y, 2 * y + 1 + delta, color);
			if (cornername & 0x2) _drawFastVLine(x0 - x, y0 - y, 2 * y + 1 + delta, color);
		}
	}
}

void TFT_fillRoundRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t r, color_t color)
{
	//x += dispWin.x1;
	//y += dispWin.y1;

	// smarter version
	_fillRect(x + r, y, w - 2 * r, h, color);

	// draw four corners
	fillCircleHelper(x + w - r - 1, y + r, r, 1, h - 2 * r - 1, color);
	fillCircleHelper(x + r, y + r, r, 2, h - 2 * r - 1, color);
}



void ILI9341_Draw_Hollow_Rectangle_Fill(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color)
{
	uint16_t 	X_length = 0;
	uint16_t 	Y_length = 0;
	uint8_t		Negative_X = 0;
	uint8_t 	Negative_Y = 0;
	float 		Calc_Negative = 0;
	
        ILI9341_Draw_Hollow_Rectangle_Coord(X0, Y0, X1, Y1, color);
        ILI9341_Draw_Rectangle(X0, Y0, X1-X0, Y1-Y0, TFT_GREENYELLOW);
#if 0
	Calc_Negative = X1 - X0;
	if(Calc_Negative < 0) Negative_X = 1;
	Calc_Negative = 0;
	
	Calc_Negative = Y1 - Y0;
	if(Calc_Negative < 0) Negative_Y = 1;
	
	//DRAW HORIZONTAL!
	if(!Negative_X)X_length = X1 - X0;
	else X_length = X0 - X1;	

	ILI9341_Draw_Horizontal_Line(X0, Y0, X_length, color);
	ILI9341_Draw_Horizontal_Line(X0, Y1, X_length, color);
	//DRAW VERTICAL!
	if(!Negative_Y)Y_length = Y1 - Y0;
	else Y_length = Y0 - Y1;
	ILI9341_Draw_Vertical_Line(X0, Y0, Y_length, color);
	ILI9341_Draw_Vertical_Line(X1, Y0, Y_length, color);
	
	if((X_length > 0)||(Y_length > 0)) ILI9341_Draw_Pixel(X1, Y1, color);
#endif
	
}

/*Draw a filled rectangle between positions X0,Y0 and X1,Y1 with specified colour*/
void ILI9341_Draw_Filled_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color)
{
	uint16_t 	X_length = 0;
	uint16_t 	Y_length = 0;
	uint8_t	Negative_X = 0;
	uint8_t 	Negative_Y = 0;
	int32_t 	Calc_Negative = 0;
	
	uint16_t X0_true = 0;
	uint16_t Y0_true = 0;
	
	Calc_Negative = X1 - X0;
	if(Calc_Negative < 0) Negative_X = 1;
	Calc_Negative = 0;
	
	Calc_Negative = Y1 - Y0;
	if(Calc_Negative < 0) Negative_Y = 1;
	
	
	//DRAW HORIZONTAL!
	if(!Negative_X)
	{
		X_length = X1 - X0;
		X0_true = X0;
	}
	else
	{
		X_length = X0 - X1;
		X0_true = X1;
	}
	
	//DRAW VERTICAL!
	if(!Negative_Y)
	{
		Y_length = Y1 - Y0;
		Y0_true = Y0;		
	}
	else
	{
		Y_length = Y0 - Y1;
		Y0_true = Y1;	
	}
	
	ILI9341_Draw_Rectangle(X0_true, Y0_true, X_length, Y_length, color);	
}

/*Draws a character (fonts imported from fonts.h) at X,Y location with specified font colour, size and Background colour*/
/*See fonts.h implementation of font on what is required for changing to a different font when switching fonts libraries*/
void ILI9341_Draw_Char(char Character, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour) 
{
	uint8_t 	function_char;
	uint8_t 	i,j;

	function_char = Character;
	//if(Size==3){
	//  printf("1-X:%d Y:%d [%c]\r\n",X,Y,function_char);
	//}	
		
	if (function_char < ' ') {
	   Character = 0;
	} 
	else {function_char -= 32;}

	char temp[CHAR_WIDTH];
	for(uint8_t k = 0; k<CHAR_WIDTH; k++)
	{
		temp[k] = font[function_char][k];
	}
    // Draw pixels
	ILI9341_Draw_Rectangle(X, Y, CHAR_WIDTH*Size, CHAR_HEIGHT*Size, Background_Colour);
    for (j=0; j<CHAR_WIDTH; j++) {
        for (i=0; i<CHAR_HEIGHT; i++) {
            if (temp[j] & (1<<i)) {			
			if(Size == 1)
			{
				ILI9341_Draw_Pixel(X+j, Y+i, color);
			}
			else
			{
			    ILI9341_Draw_Rectangle(X+(j*Size), Y+(i*Size), Size, Size, color);
			}
            }						
        }
    }
}

/*Draws an array of characters (fonts imported from fonts.h) at X,Y location with specified font colour, size and Background colour*/
/*See fonts.h implementation of font on what is required for changing to a different font when switching fonts libraries*/
void ILI9341_Draw_Text(const char* Text, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour)
{
    while (*Text) {
        ILI9341_Draw_Char(*Text++, X, Y, color, Size, Background_Colour);
        X += CHAR_WIDTH*Size;
	   //printf("ILI9341_Draw_Text X:%d\r\n",X);
    }
}

void ILI9341_Draw_LedBar(uint16_t x, uint16_t y, uint16_t value)
{
  color_t color;
  uint8_t x0, ix, w=30;
  uint8_t i;
  color.r=0xff; color.g=0x0; color.b=0x0; 
  x0=x;
  for(i=0;i<8;i++){
    ix= x0 + (w*i);
    ILI9341_Draw_Filled_Rectangle_Coord(ix, y, ix+20, y+80, color);
  }
}

void ILI9341_Draw_RGB565(const char* Image_Array, uint16_t X, uint16_t Y, uint16_t width, uint16_t line)
{
  uint16_t oneLineDataSz, oneLinePushSz;
  //uint16_t block, push_sz;
  uint16_t i, ix, iy, rgb565;
  uint32_t k;
  color_t color;
  uint8_t tmp[4];
  //uint8_t* push_buffer;//[(width*3)+10];
  //uint8_t* buf;//[(width*3)+10];
  uint8_t push_buffer[(240*3)+10];
  
  //uint8_t push_bufferx[480];
  //memset(push_buffer,0,sizeof(push_buffer));
  ILI9341_Set_Address(X,Y,X+width-1,Y+line);	////
  oneLineDataSz = width*2;//byte/pixel 16bit 565RGB
  oneLinePushSz = width*3;//byte/pixel 24bit 888RGB
  
  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);	
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
  k=0;
  for( i = 0; i < line; i++)
  {			
          k= oneLineDataSz*i;
          //push_buffer = malloc(oneLinePushSz+1);
          //push_buffer = malloc(1000);
          //printf("buf[%x]\r\n",push_buffer);
          //memset(buf,0,sizeof(buf));
       //printf("Draw oneline[%d] k[%d]\r\n",i, k);
          for(ix=0,iy=0 ;ix < oneLineDataSz; ix+=2)
          {
                  
               tmp[0] = Image_Array[k+ix];
                  tmp[1] = Image_Array[k+ix+1];
                  rgb565=BUILD_UINT16(tmp[1], tmp[0]);
                  //rgb565=0xf800;
                  color.r=(rgb565>>11)<<3;
                  color.g=((rgb565>>5)&0x3f)<<2;
                  color.b=(rgb565&0x1f)<<1;
                  push_buffer[iy+0]=color.r;
                  push_buffer[iy+1]=color.g;
                  push_buffer[iy+2]=color.b;
                  iy+=3;
                  //if(i==0 && ix==0) printf("Draw oneline[%x] push_buffer[%x] \r\n",rgb565, push_buffer[0]);
                  //printf("Draw oneline[%d]\r\n",k);
          }
          //printf("Draw oneline oneLinePushSz[%d] iy[%d]\r\n",oneLinePushSz, iy);
          HAL_SPI_Transmit(&hspi2, push_buffer, oneLinePushSz, 20);
          //free(push_buffer);
  }
  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);
}

/*Draws a full screen picture from flash. Image converted from RGB .jpeg/other to C array using online converter*/
//USING CONVERTER: http://www.digole.com/tools/PicturetoC_Hex_converter.php
//65K colour (2Bytes / Pixel)
void ILI9341_Draw_Image(const char* Image_Array, uint8_t Orientation, uint16_t posX, uint16_t posY, uint16_t imgX, uint16_t imgY)
{
	printf("ILI9341_Draw_Image Orientation[%d]\r\n", Orientation);	
#if 1 
        if(Orientation == SCREEN_HORIZONTAL_1)
	{
	  ILI9341_Set_Rotation(SCREEN_HORIZONTAL_1);
	  ILI9341_Draw_RGB565(Image_Array, posX, posY,imgX,imgY);
	}
	else if(Orientation == SCREEN_HORIZONTAL_2)
	{
	  ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	  ILI9341_Draw_RGB565(Image_Array, posX, posY,imgX,imgY);

	}
	else if(Orientation == SCREEN_VERTICAL_2)
	{
	  ILI9341_Set_Rotation(SCREEN_VERTICAL_2);
	  ILI9341_Draw_RGB565(Image_Array, posX, posY,imgX,imgY);
	}
	else if(Orientation == SCREEN_VERTICAL_1)
	{
	  ILI9341_Set_Rotation(SCREEN_VERTICAL_1);
	  ILI9341_Draw_RGB565(Image_Array, posX, posY,imgX,imgY);
	}
#endif
}

/*****************************************************************************
 * @name       :void GUI_DrawFontTTF(u8 fntsz, u16 x, u16 y, u16 fc, u16 bc, u8 *s,u8 mode)
 * @function   :Display  TTF Bitmap Fnt 24pt/36pt
 * @parameters :
               fntsz 0:24pt 1:36pt
               x:the bebinning x coordinate of the Chinese character
               y:the bebinning y coordinate of the Chinese character
               fc:the color value of Chinese character
               bc:the background color of Chinese character
               s:the start address of the Chinese character
               mode:0-no overlying,1-overlying
 * @retvalue   :None
******************************************************************************/ 
void GUI_DrawFontTTF(uint8_t fntsz, uint16_t x, uint16_t y, color_t fc, color_t bc, uint8_t *s,uint8_t mode)
{
  uint16_t i,ix,iy,ixx,j;
  uint16_t k;
  uint16_t HZnum;
  uint16_t x0=x;
  uint16_t xA, xpos, ypos;
  
  uint8_t charHeightByte,  charSpaceByte;
  uint16_t font_pixcel_height;  
  uint8_t drw_ot, drw_in;
  
  int font_index;
  uint16_t font_pixcel_width;
  uint16_t font_pixcel_offset;
  uint16_t font_byte_width;
  uint16_t font_byte_height;
  uint16_t bitmap_offset;
  
  uint8_t byte_data;
  uint8_t StartChar;
  
  if(fntsz==0){
    charHeightByte = godic_24ptFontInfo.CharHeight;
    charSpaceByte = godic_24ptFontInfo.WidthPixcel;
    StartChar= godic_24ptFontInfo.StartChar;
    drw_ot=40;
    drw_in=38;  
  }
  else if(fntsz==1){
    charHeightByte = godic_24ptFontInfo.CharHeight;
    charSpaceByte = godic_24ptFontInfo.WidthPixcel;
    StartChar= godic_24ptFontInfo.StartChar;
    drw_ot=56;
    drw_in=54;
  }
  font_pixcel_height = charHeightByte*8;  
  
  xA=x;
  for(k=0;k<16;k++){
     if(s[k]<0x20)break;
      font_index=(s[k]-StartChar)+0;
      if(fntsz==0){
          font_pixcel_width = godic_24ptDescriptors[font_index].info[0];
          font_pixcel_offset = godic_24ptDescriptors[font_index].info[1];
      }
      else if(fntsz==1){
          font_pixcel_width = godic_24ptDescriptors[font_index].info[0];
          font_pixcel_offset = godic_24ptDescriptors[font_index].info[1];
      }
      //font_index=font_index-1;
      if(font_index<0){
        font_pixcel_width=10;
        font_pixcel_offset=0;
      }  
      
      font_byte_width = (font_pixcel_width%8) ? (font_pixcel_width/8)+1:(font_pixcel_width/8)+0;
      bitmap_offset=font_pixcel_offset;
      
       for(iy=0;iy<drw_ot;iy++){
         ypos=y+iy;
         xpos=xA;
        for(ix=0;ix<font_byte_width;ix++){
          if(iy>=drw_in || (font_index<0)) byte_data=0x00;
          else {
            byte_data=(fntsz==0)? godic_24ptFontInfo.Bitmaps[bitmap_offset++]:godic_24ptFontInfo.Bitmaps[bitmap_offset++];
          }
#if 1           
            for(ixx=0;ixx<8;ixx++){
             if(mode){
                if(byte_data & 0x80) ILI9341_Draw_Pixel(xpos, ypos, fc);//point draw
             }
             else{
               if(byte_data & 0x80)          ILI9341_Draw_Pixel(xpos,ypos, fc);
               else  ILI9341_Draw_Pixel(xpos,ypos, bc);
             }             
             byte_data=(byte_data<<1);
             xpos++;
            }
#endif          
         }
      }
      xA=xA+font_byte_width*8;
      ILI9341_Set_Address(x,y, x + (font_byte_width*8)-1, y + (font_byte_width*8)-1);
  }
  ILI9341_Set_Address(0,0,LCD_WIDTH-1,LCD_HEIGHT-1);

} 

