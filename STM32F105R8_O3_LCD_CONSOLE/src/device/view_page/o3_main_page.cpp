#include "o3_main_page.h"
#include "extern.h"
#include <string.h>
//#include "480x2.h"

O3_MAIN_PAGE *pMain_page;
CONSOLE_VALUE gVal;

O3_MAIN_PAGE::O3_MAIN_PAGE() {
  draw_power_on(1);
  gVal.ctrView_pos=0;
  gVal.sensorView_pos=0;
}

O3_MAIN_PAGE::~O3_MAIN_PAGE() {;}


void O3_MAIN_PAGE::init_console_value(void)
{
  memset(&gVal,0,sizeof(gVal));
  memcpy(&gVal.target,&eeprom, sizeof(eeprom));
  printf("gVal.target.o3[%d]\r\n",gVal.target.o3);
  printf("gVal.target.on_time[%d]\r\n",gVal.target.on_time);
  printf("gVal.target.off_time[%d]\r\n",gVal.target.off_time);
  //printf("gVal.target.fan_onTime[%d]\r\n",gVal.target.fan_onTime);
  //printf("gVal.target.uv_onTime[%d]\r\n",gVal.target.uv_onTime);
  printf("gVal.target.temperature[%d]\r\n",gVal.target.temperature);
}


void O3_MAIN_PAGE::draw_frame()
{
  char ldata[16];
  int x,y,dx,dy;
  x=SN01_Ax;  y=SN01_Ay;
  dx=SN01_Dx; dy=SN01_Dy;
  int o3_x, uv_x, fan_x, led_y;
  o3_x=39;
  uv_x=194;
  fan_x=338;
  led_y=10;
  
  //sprintf(ldata, "\x8A\x88 %d ",value+1); //센서 1
  //GUI_DrawFontTTF(0, x,y,TFT_BLUE,TFT_ORANGE,(uint8_t*)ldata,0);
  GUI_DrawFontTTF(0, o3_x, led_y,TFT_BLUE,TFT_ORANGE,(uint8_t*)"O3 ",0);
  GUI_DrawFontTTF(0, uv_x, led_y,TFT_BLUE,TFT_ORANGE,(uint8_t*)"UV ",0);
  GUI_DrawFontTTF(0, fan_x, led_y,TFT_BLUE,TFT_ORANGE,(uint8_t*)"FAN ",0);
  
  TFT_fillRoundRect(25,50,141,116,10,TFT_WHITE);
  TFT_fillRoundRect(175,50,141,116,10,TFT_WHITE);
  TFT_fillRoundRect(325,50,141,116,10,TFT_WHITE);
  
  //TFT_fillRoundRect(170,250,290,50,10,TFT_WHITE);
  //TFT_fillRoundRect(45,60,100,50,10,TFT_MAGENTA);
  //TFT_fillRoundRect(195,60,100,50,10,TFT_RED);
  //TFT_fillRoundRect(345,60,100,50,10,TFT_GREENYELLOW);
  
  GUI_DrawFontTTF(0, 60,74,TFT_MAGENTA,TFT_WHITE,(uint8_t*)"\x8E\x93",0);//오존
  GUI_DrawFontTTF(0, 210,74,TFT_RED,TFT_WHITE,(uint8_t*)"\x8F\x81",0);//온도
  GUI_DrawFontTTF(0, 365,74,TFT_GREENYELLOW,TFT_WHITE,(uint8_t*)"\x8B\x81",0);//습도
  
  draw_sensorValue(gVal);
  draw_ctrl_view(gVal);
  //GUI_DrawFontTTF(1, 180,SN02_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)"\x82\x84 ",0);//오존
  //GUI_DrawFontTTF(1, 180,SN03_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)"\x83\x80 ",0);//온도
  //GUI_DrawFontTTF(1, 180,SN04_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)"\x81\x80 ",0);//습도
  
  //GUI_DrawFontTTF(0, 420,SN02_Ay,TFT_RED,TFT_ORANGE,(uint8_t*)"ppm",0);
  //GUI_DrawFontTTF(0, 420,SN03_Ay,TFT_RED,TFT_ORANGE,(uint8_t*)"\x7F",0);
  //GUI_DrawFontTTF(0, 420,SN04_Ay,TFT_RED,TFT_ORANGE,(uint8_t*)"%",0);
}

void O3_MAIN_PAGE::initPage(void)
{
  init_console_value();

  ILI9341_Fill_Screen(TFT_ORANGE);
  ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
  draw_power_on(1);
  //ILI9341_Draw_Filled_Rectangle_Coord(WINDOW_CTRL_Ax, WINDOW_CTRL_Ay, WINDOW_CTRL_Bx, WINDOW_CTRL_By, TFT_BLUE);
  //draw_ctrl_view(gVal);
  //draw_sensorValue(gVal);
 // draw_status_title();
 // ILI9341_Draw_Image((const char*)IMG_480x2, SCREEN_HORIZONTAL_2, 0,0, 240, 1);
 // GUI_DrawFontTTF(0, 10,200,TFT_BLUE,TFT_WHITE,(uint8_t*)"1234",0);
 // GUI_DrawFontTTF(1, 10,250,TFT_RED,TFT_WHITE,(uint8_t*)"1234",0);
  //ILI9341_Draw_Hollow_Rectangle_Fill(10,10,100,50,TFT_BLACK);
  draw_frame();

}
  
void O3_MAIN_PAGE::draw_monitor_min(uint16_t ltime, uint16_t total_time)
{
  int value;
  char str_value[8];
  sprintf(str_value, "%03d/%03d ",ltime, total_time);
  ILI9341_Draw_Text(str_value, 20, WINDOW_CTRL_Ay, TFT_BLACK, 3,TFT_ORANGE);
}

void O3_MAIN_PAGE::draw_sesor_pos(uint16_t pos)
{
  int ypos=174;
  int c_size=4;
  ILI9341_Draw_Filled_Circle(35, ypos, c_size, TFT_RED);
  ILI9341_Draw_Filled_Circle(55, ypos, c_size, TFT_RED);
  ILI9341_Draw_Filled_Circle(75, ypos, c_size, TFT_RED);
  switch(pos){
  case 0:
    ILI9341_Draw_Filled_Circle(35, ypos, c_size, TFT_WHITE);
    break;
  case 1:
    ILI9341_Draw_Filled_Circle(55, ypos, c_size, TFT_WHITE);
    break;
  case 2:
   ILI9341_Draw_Filled_Circle(75, ypos, c_size, TFT_WHITE); 
   break;    
  }
}

void O3_MAIN_PAGE::draw_ctrl_set_value(const CONSOLE_VALUE val)
{
  uint8_t pos=val.ctrView_pos;
  int value;
  char str_value[16];
  switch(pos){
        case 0:
          value=val.target.o3;
          sprintf(str_value, "O3 -> %.2f ppm",(float)value/1000L);
          break;
        case 1:
          value=val.target.off_time;
          sprintf(str_value, "OFF -> %03d \x84",value);           
          break;  
        case 2:
          value=val.target.on_time;
          sprintf(str_value, "ON -> %03d \x94",value);       
  }
  TFT_fillRoundRect(170,250,290,50,10,TFT_WHITE);
  GUI_DrawFontTTF(0, 190, 255, TFT_BLACK, TFT_WHITE, (uint8_t*)str_value, 0);
  //GUI_DrawFontTTF(1, 300, WINDOW_CTRL_Ay, TFT_BLACK, TFT_ORANGE, (uint8_t*)str_value, 0);
}

void O3_MAIN_PAGE::draw_ctrl_view(const CONSOLE_VALUE val)
{
        draw_ctrl_set_value(val);
}

void O3_MAIN_PAGE::draw_monitor_title(int value)
{
  char ldata[16];
  int x,y,dx,dy;
  x=SN01_Ax;  y=SN01_Ay;
  dx=SN01_Dx; dy=SN01_Dy;
 
  sprintf(ldata, "\x8A\x88 %d ",value+1); //센서 1
  GUI_DrawFontTTF(0, x,y,TFT_BLUE,TFT_ORANGE,(uint8_t*)ldata,0);

  //GUI_DrawFontTTF(1, 180,SN02_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)"\x82\x84 ",0);//오존
  //GUI_DrawFontTTF(1, 180,SN03_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)"\x83\x80 ",0);//온도
  //GUI_DrawFontTTF(1, 180,SN04_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)"\x81\x80 ",0);//습도
  
  //GUI_DrawFontTTF(0, 420,SN02_Ay,TFT_RED,TFT_ORANGE,(uint8_t*)"ppm",0);
  //GUI_DrawFontTTF(0, 420,SN03_Ay,TFT_RED,TFT_ORANGE,(uint8_t*)"\x7F",0);
  //GUI_DrawFontTTF(0, 420,SN04_Ay,TFT_RED,TFT_ORANGE,(uint8_t*)"%",0);
}

void O3_MAIN_PAGE::draw_status_title()
{
  char ldata[16];
  uint8_t ypos=80;
  uint8_t ycircle_pos=ypos+18;
  
  //GUI_DrawFontTTF(0, 30,ypos,TFT_BLUE,TFT_ORANGE,(uint8_t*)"O3 ",0);
  //GUI_DrawFontTTF(0, 30,ypos+40,TFT_BLUE,TFT_ORANGE,(uint8_t*)"UV ",0);
  //GUI_DrawFontTTF(0, 30,ypos+80,TFT_BLUE,TFT_ORANGE,(uint8_t*)"FAN ",0);
  draw_relay_status_value(pOrange->black_data);
}

void O3_MAIN_PAGE::draw_relay_status_value(BLACK_BOX_DATA bbdata)
{
  uint16_t ypos=21;
  uint16_t o3_xpos=111;
  uint16_t uv_xpos=260;
  uint16_t fan_xpos=417;
  
  if(bbdata.o3_relay.enable)
    ILI9341_Draw_Filled_Circle(o3_xpos, ypos, 14, TFT_GREEN);
  else ILI9341_Draw_Filled_Circle(o3_xpos, ypos, 14, TFT_RED);
 
  if(bbdata.uv_relay.enable)
    ILI9341_Draw_Filled_Circle(uv_xpos, ypos, 14, TFT_GREEN);
  else ILI9341_Draw_Filled_Circle(uv_xpos, ypos, 14, TFT_RED); 
 
  if(bbdata.fan_relay.enable)
    ILI9341_Draw_Filled_Circle(fan_xpos, ypos, 14, TFT_GREEN);
  else ILI9341_Draw_Filled_Circle(fan_xpos, ypos, 14, TFT_RED);  
}


void O3_MAIN_PAGE::draw_monitor_ppm(int value)
{
  char ldata[16];
  sprintf(ldata,"%.2fppm",(float)value/1000L);
  GUI_DrawFontTTF(0, 30,120,TFT_BLACK,TFT_WHITE,(uint8_t*)ldata,0);

}

void O3_MAIN_PAGE::draw_monitor_temperature(int value)
{
  char ldata[16];
  int tmp;
  if(value>0x8000) {
    tmp=(value&0x7FFF)*(-1);
  }
  else tmp=value&0x7FFF;
  sprintf(ldata, "%2.1f\x7F ",(float)tmp/10L);
  printf("=========draw_monitor_temperature======[%x]\r\n",value);
  //GUI_DrawFontTTF(1, 300, SN03_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)ldata,0);
  GUI_DrawFontTTF(0, 205,120,TFT_BLACK,TFT_WHITE,(uint8_t*)ldata,0);
}

void O3_MAIN_PAGE::draw_monitor_humidity(int value)
{
  char ldata[16];
  sprintf(ldata, "%2.1f%%",(float)value/10L);
  //GUI_DrawFontTTF(1, 300,SN04_Ay,TFT_BLACK,TFT_ORANGE,(uint8_t*)ldata,0);
  GUI_DrawFontTTF(0, 350,120,TFT_BLACK,TFT_WHITE,(uint8_t*)ldata,0);
   
}


void O3_MAIN_PAGE::draw_monitor_voltage(int value)
{
  char ldata[16];
  int x,y,dx,dy;
  x=SN02_Ax;  y=SN02_Ay;
  dx=SN02_Dx; dy=SN02_Dy; 
  //value=2010;
  ILI9341_Draw_Filled_Rectangle_Coord(x, y, 480, y+dy, TFT_ORANGE);
  sprintf(ldata, "%2.1f",(float)value/10L);
  ILI9341_Draw_Text(ldata, x, y, TFT_BLACK, 4, TFT_ORANGE);
  ILI9341_Draw_Text("V", x+130, y+10, TFT_RED, 3, TFT_ORANGE);
}

void O3_MAIN_PAGE::draw_monitor_current(int value)
{
  char ldata[16];
  int x,y,dx,dy;
  x=SN03_Ax;  y=SN03_Ay;
  dx=SN03_Dx; dy=SN03_Dy; 
  ILI9341_Draw_Filled_Rectangle_Coord(x, y, 480, y+dx, TFT_ORANGE);
  sprintf(ldata, "%2.1f",(float)value/10L);
  ILI9341_Draw_Text(ldata, x, y, TFT_BLACK, 4, TFT_ORANGE);
  ILI9341_Draw_Text("A", x+130, y+10, TFT_RED, 3, TFT_ORANGE);
}

void O3_MAIN_PAGE::draw_monitor_power(int value)
{
  char ldata[16];
  int x,y,dx,dy;
  x=SN04_Ax;  y=SN04_Ay;
  dx=SN04_Dx; dy=SN04_Dy;
  ILI9341_Draw_Filled_Rectangle_Coord(x, y, 480, y+dy, TFT_ORANGE);
  sprintf(ldata, "%2.1f",(float)value/10L);
  ILI9341_Draw_Text(ldata, x, y, TFT_BLACK, 4, TFT_ORANGE);
  ILI9341_Draw_Text("W", x+130, y+10, TFT_RED, 3, TFT_ORANGE);
}


void O3_MAIN_PAGE::draw_sensorValue(const CONSOLE_VALUE val)
{
  char pos;
  pos=val.sensorView_pos;
  //draw_monitor_title(pos);
  draw_sesor_pos(pos);
  if(pos<3) {
    draw_monitor_ppm(pOrange->sensor_data[pos].o3_ppb);
    draw_monitor_temperature(pOrange->sensor_data[pos].temperature);
    draw_monitor_humidity(pOrange->sensor_data[pos].humidity);
  }
  else{
   // draw_monitor_voltage(pOrange->black_data.ac.volt);
   // draw_monitor_current(pOrange->black_data.ac.current);
   // draw_monitor_power(pOrange->black_data.ac.power);  
  }
}


void O3_MAIN_PAGE::draw_power_on(uint16_t on)
{
  if(on){
    HAL_GPIO_WritePin(BL_ON_GPIO_Port, BL_ON_Pin, GPIO_PIN_SET);
    gVal.bCtrl.back_lite=1;
    gVal.sensorView_pos=0;
    printf("UI_ON\r\n");
  }
  else{
    HAL_GPIO_WritePin(BL_ON_GPIO_Port, BL_ON_Pin, GPIO_PIN_RESET);
    gVal.bCtrl.back_lite=0;
    gVal.sensorView_pos=0;
    printf("UI_OFF\r\n");
  }
}