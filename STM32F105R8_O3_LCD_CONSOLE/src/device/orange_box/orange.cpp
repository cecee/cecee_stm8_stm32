/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "orange.h"
#include "extern.h"
#include <string.h>

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern CONSOLE_VALUE gVal;
extern O3_MAIN_PAGE *pMain_page;

ORANGE_BOX *pOrange;

/*****************************************************************************
*                           Static Variables
******************************************************************************/


ORANGE_BOX::ORANGE_BOX() 
{
  memset(&otime_cnt,0, sizeof(otime_cnt));  
  memset(&black_data,0, sizeof(BLACK_BOX_DATA));
  memset(&blackBoxresp,0, sizeof(blackBoxresp));
  memset(&sensor_data,0, sizeof(sensor_data));
}

ORANGE_BOX::~ORANGE_BOX() {}


void ORANGE_BOX::initOrangeBox(void){
  //memset(&blackBox,0, sizeof(BLACK_BOX_CONTAINER));
  printf("initOrangeBox black_data : %x\r\n", BLACK_BOX_ID);
}
#if 0
void ORANGE_BOX::getBlackBoxRelayCtrl(void){
  if(otime_cnt.perideCnt>gVal.target.periode){
    otime_cnt.perideCnt=0;
    otime_cnt.onTimeCnt=0; 
    if(gVal.target.o3_onTime){
     //printf("##RELAY ON\r\n");      
      relay_on.b.o3=1;
      relay_on.b.uv=1;
      relay_on.b.fan=1;
    }
    else {
      printf("##RELAY OFF\r\n");
      relay_on.u8=0;//All off
    }

  }
  if(otime_cnt.onTimeCnt>gVal.target.o3_onTime){
    //printf("##otime_cnt RELAY OFF\r\n");
    relay_on.u8=0;//All off
  }
}

void ORANGE_BOX::orangeEvent_100ms(void){
  //printf("onTimeCnt[%d] perideCnt[%d]\r\n",otime_cnt.onTimeCnt, otime_cnt.perideCnt);
  //getBlackBoxRelayCtrl();
}
#endif

void ORANGE_BOX::orangeUpdateCheckValue(uint8_t what){
  if(what<3){
    if(sub_bd_access.O3SENSOR_ACCESS[what]>5){
      sub_bd_access.O3SENSOR_ACCESS[what]=10;
      sensor_data[what].o3_ppb=0;
      sensor_data[what].temperature=0;
      sensor_data[what].humidity=0;
    }
  }
  else if(what==3){
  
  }
}

void ORANGE_BOX::orangeUpdateDraw(void){
  //update display
  uint8_t i;
  uint8_t pos= gVal.sensorView_pos;
  pMain_page->draw_relay_status_value(black_data);
  //pMain_page->draw_monitor_min(TICK_MIN);
  for(i=0;i<3;i++)  orangeUpdateCheckValue(i);
 
  switch(pos){
    case SBOX1:
      pMain_page->draw_monitor_ppm(sensor_data[0].o3_ppb);
      pMain_page->draw_monitor_temperature(sensor_data[0].temperature);
      pMain_page->draw_monitor_humidity(sensor_data[0].humidity);
      break;
    case SBOX2:
      pMain_page->draw_monitor_ppm(sensor_data[1].o3_ppb);
      pMain_page->draw_monitor_temperature(sensor_data[1].temperature);
      pMain_page->draw_monitor_humidity(sensor_data[1].humidity);      
      break; 
    case SBOX3:
      pMain_page->draw_monitor_ppm(sensor_data[2].o3_ppb);
      pMain_page->draw_monitor_temperature(sensor_data[2].temperature);
      pMain_page->draw_monitor_humidity(sensor_data[2].humidity);        
      break; 
    case BBOX:
      //pMain_page->draw_monitor_voltage(black_data.ac.volt);
      //pMain_page->draw_monitor_current(black_data.ac.current);
      //pMain_page->draw_monitor_power(black_data.ac.power); 
      //pMain_page->draw_relay_status_value(black_data);
      break;      
  }
}





