#ifndef ORANGE_BOX_H
#define ORANGE_BOX_H
#include "stm32f1xx_hal.h"

#define ORANGE_BOX_ID 0xAAA0
#define BLACK_BOX_ID   0xAAA0
#define SENSOR_BOX_ID0 0xB000
#define SENSOR_BOX_ID1 0xB001
#define SENSOR_BOX_ID2 0xB002


typedef struct 
{
  uint32_t onTimeCnt;
  uint32_t perideCnt;
}TIMER_MINUTE_CNT;

typedef struct _BLACK_BOX_AC
{
  uint16_t volt;
  uint16_t current;
  uint16_t power;
  uint16_t factor;
}AC;

typedef struct
{
    uint8_t  enable: 1;
    uint8_t  state: 1;
    uint8_t  detect: 1;	
    uint8_t  alarm: 1;	
}RELAY_STATUS;


typedef struct {
  AC ac;
  RELAY_STATUS o3_relay;
  RELAY_STATUS uv_relay; 
  RELAY_STATUS fan_relay; 
  RELAY_STATUS power_relay; 
}BLACK_BOX_DATA;

typedef struct {
  uint16_t o3;
  uint16_t temperature;
  uint16_t humidity;//Moisture
}O3_SENSOR;

typedef struct {
  O3_SENSOR sensor;
}SESOR_BOX;


typedef struct   
{
  uint8_t stx; // 1
  uint8_t size;	//1
  uint16_t id; //2
  BLACK_BOX_DATA data;
  uint8_t sum; //1
  uint8_t etx; //1
}BLACKBOX_RS485_RESPONSE;



typedef struct 
{
  uint16_t bd_id;
  uint16_t o3_ppb;
  uint16_t temperature;  
  uint16_t humidity;
  uint8_t error;
}SENSOR_DATA;

typedef struct   
{
  uint8_t stx; // 1
  uint8_t size;	//1
  SENSOR_DATA data;
  uint8_t sum; //1
  uint8_t etx; //1
}SENSOR_BOX_RS485_RESPONSE; 

class ORANGE_BOX
{
  private:
  public:

  TIMER_MINUTE_CNT otime_cnt; 

  BLACK_BOX_DATA black_data;
  SENSOR_DATA sensor_data[3];
  BLACKBOX_RS485_RESPONSE blackBoxresp;
  SENSOR_BOX_RS485_RESPONSE sensorBoxresp1;
  
  ORANGE_BOX();
  virtual ~ORANGE_BOX();
  void initOrangeBox(void);
  void orangeUpdateCheckValue(uint8_t what);
  void orangeUpdateDraw(void);
  //void getBlackBoxRelayCtrl(void);
};

#endif 
