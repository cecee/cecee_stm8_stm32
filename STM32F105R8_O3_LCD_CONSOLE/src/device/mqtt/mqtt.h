#ifndef MQTT_H
#define MQTT_H
#include "stm32f1xx_hal.h"
#include <vector>
#include <string>
using namespace std; 

typedef struct _BOX_BIT   
{
  uint8_t plasma:1;
  uint8_t uv:1;
  uint8_t fan:1;
  uint8_t pump:1;
}BOX_BIT;

typedef struct _MQTT_PUBLISH   
{
  string id;
  string rw;
  BOX_BIT ctrl;
  int sensor_cnt;
  int time_on_period;
  int time_off_period;
  int o3_value;
  int acv,aca,acp;
  int o1,o2,o3;
  int t1,t2,t3;
  int h1,h2,h3;
}MQTT_PUBLISH;

typedef struct _MQTT_SUB   
{
  string id;
  int to;//time on periode
  int tf;//time off periode
  int ct;//contol mode
  int ot;//target o3
}MQTT_SUB;

class MQTT
{
    private:
    public:
      MQTT();
      virtual ~MQTT();
      void mqtt_class_test(void);
      char MqttPub(MQTT_PUBLISH pub);
      char MqttSub(string id);
      char waitMqtt(void);
      uint8_t MqttConnect(void);
};

#endif

#endif 
