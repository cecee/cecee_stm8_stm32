
//#include "keypad_adc.h"
#include "extern.h"
#include "adc.h"

#define MAX_CTRL_STEP 3
#define SENSOR_NUM 3

#define KEY_POWER 0x08
#define KEY_UP    0x01
#define KEY_DN    0x10
#define KEY_SENSOR 0x04
#define KEY_UP_CTRL 0x80
#define KEY_DN_CTRL 0x40

KEYPAD_ADC *pKEY;
uint8_t key_data;

extern void save_eeprom(CONTROL_TARGET eep);
extern CONSOLE_VALUE gVal;
extern O3_MAIN_PAGE *pMain_page;

int pwr_press_cnt=0;

KEYPAD_ADC::KEYPAD_ADC() {
 // memset(&kpad,0,sizeof(KEY_PAD));
 // key_data=0;
}
KEYPAD_ADC::~KEYPAD_ADC() {}

void KEYPAD_ADC::kpad_init(void)
{
  //printf("KEYPAD_ADC::kpad_init sz[%d]\r\n",sizeof(kpad));
  //memset(&kpad,0,sizeof(kpad));
  key_data=0;
  pwr_press_cnt=0;
}

void KEYPAD_ADC::adcRead()
{
  uint16_t lkey=0;
  uint16_t AdcValue[2];
  
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[0]=HAL_ADC_GetValue(&hadc1);
      
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[1]=HAL_ADC_GetValue(&hadc1); 
 
  //printf("adc[%d][%d]\r\n", AdcValue[0], AdcValue[1]);
  //HAL_Delay(10);
  
  if(AdcValue[1]<500) _SetBit(lkey, 0);
  else if(AdcValue[1]<1500) _SetBit(lkey, 1);  
  else if(AdcValue[1]<2500) _SetBit(lkey, 2);
  else if(AdcValue[1]<3500) _SetBit(lkey, 3);

  if(AdcValue[0]<500) _SetBit(lkey, 4);
  else if(AdcValue[0]<1500) _SetBit(lkey, 5);  
  else if(AdcValue[0]<2500) _SetBit(lkey, 6);
  else if(AdcValue[0]<3500) _SetBit(lkey, 7);
  
  //printf("adc[%d][%d]-lkey[%x]\r\n", AdcValue[0], AdcValue[1], lkey);
  if(lkey)keyprocess(lkey);
}

void KEYPAD_ADC::changeCtrlValue(uint8_t pos, uint8_t up){
  switch(pos){
  case 0: //o3
    if(up){gVal.target.o3=gVal.target.o3+10;}
    else {gVal.target.o3=gVal.target.o3-10;}
    if(gVal.target.o3<0)gVal.target.o3=0;
    gVal.target.o3=(gVal.target.o3/10)*10;
    break;
  case 1: //periode-->off_time
    if(up){gVal.target.off_time++;}
    else {gVal.target.off_time--;}
    if(gVal.target.off_time<0)gVal.target.off_time=0;  
    gVal.target.off_time %=10;
    break;    
  case 2: //duty-->on_time
     if(up){gVal.target.on_time=gVal.target.on_time+1;}
    else {gVal.target.on_time=gVal.target.on_time-1;}
    if(gVal.target.on_time<0)gVal.target.on_time=0;
   // else if(gVal.target.duty>=100)gVal.target.duty=100;
    //gVal.target.duty=(gVal.target.duty/10)*10;
    break;
  }
}

void KEYPAD_ADC::keyprocess(uint8_t key){
  //kpad.data.key_data=key;
  key_data=key;
  printf("key[%x]\r\n",key);
  //pwr_press_cnt++;
  //printf("kpad.data.b.power[%d]\r\n", kpad.data.b.power);
  if(key_data==KEY_POWER){
    printf("##########\r\n");
    pwr_press_cnt++;
    if(gVal.bCtrl.back_lite)//ON->OFF조건
    {
       printf("pwr_press_cnt[%d]\r\n",pwr_press_cnt);
      if(pwr_press_cnt>5){
        pwr_press_cnt=0;
        pMain_page->draw_power_on(0);//off
        printf("Power OFF\r\n");
      }
    }
    else //OFF->ON조건
    {
      if(pwr_press_cnt>2){
        pwr_press_cnt=0;
        pMain_page->draw_power_on(1);//on
        printf("Power ON\r\n");
      }
    }
  }
  else {
     pwr_press_cnt=0;
     switch(key_data){
     case KEY_UP_CTRL: //set selector++
        gVal.ctrView_pos++;
        gVal.ctrView_pos%= MAX_CTRL_STEP;
        printf("(1)ctrView_pos[%d]\r\n", gVal.ctrView_pos);
        pMain_page->draw_ctrl_view(gVal);
       break;
     case KEY_DN_CTRL: //set selector--
       if(gVal.ctrView_pos==0)gVal.ctrView_pos=MAX_CTRL_STEP-1;
       else gVal.ctrView_pos--;
       gVal.ctrView_pos%= MAX_CTRL_STEP;
       printf("(1)ctrView_pos[%d]\r\n", gVal.ctrView_pos);
       pMain_page->draw_ctrl_view(gVal);
       break;   
       
     case KEY_UP: //up
       changeCtrlValue(gVal.ctrView_pos, 1);
       save_eeprom(gVal.target);
       pMain_page->draw_ctrl_set_value(gVal);
       break;  
      case KEY_DN: //dn
       changeCtrlValue(gVal.ctrView_pos, 0);
       save_eeprom(gVal.target);
       pMain_page->draw_ctrl_set_value(gVal);
       break;       
     case KEY_SENSOR: //sensor selecter update sensor value draw
       //printf("(1)disp_sensor[%d]\r\n", gVal.disp_pos);
       gVal.sensorView_pos++;
       gVal.sensorView_pos %=SENSOR_NUM;//센서갯수
       printf("(2)disp_sensor[%d]\r\n", gVal.sensorView_pos);
       pMain_page->draw_sensorValue(gVal);
       HAL_Delay(10);
       break;    
    }
   
  }

  //HAL_Delay(1000);
}


