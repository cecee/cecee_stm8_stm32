//#ifndef KEYPAD_ADC_H
//#define KEYPAD_ADC_H

#pragma once
#include "extern.h"


typedef struct _KEY_BIT
{
    uint8_t  disp_mode: 1;
    uint8_t  x1: 1;
    uint8_t  up: 1;	
    uint8_t  dn: 1;
    uint8_t  power: 1;
    uint8_t  set_up: 1;
    uint8_t  confirm: 1;	
    uint8_t  cancel: 1;   
}KEY_BIT;

typedef union _KEY_DATA
{
  KEY_BIT b;
  uint8_t key_data;
}KEY_DATA;

typedef struct _KEY_PAD
{
  KEY_DATA  data;    
}KEY_PAD;

class KEYPAD_ADC
{
  private:

  public:
    KEYPAD_ADC();
    virtual ~KEYPAD_ADC();
    void kpad_init(void);
    void adcRead(void);
    void keyprocess(uint8_t key);
    void changeCtrlValue(uint8_t pos, uint8_t up);
};

//#endif 
