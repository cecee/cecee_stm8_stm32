/**
  ******************************************************************************
  * @file SPI/M25P64_FLASH/spi_flash.h 
  * @author  MCD Application Team
  * @version  V3.0.0
  * @date  04/06/2009
  * @brief  Header for spi_flash.c file.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H
#include "stm32f1xx_hal.h"
//#include "../view_page/o3_main_page.h"
#include "extern.h"    
#include "spi.h"

#define SPI_FLASH_CS_LOW() HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_RESET)
#define SPI_FLASH_CS_HIGH() HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_SET)


//extern FLASH_MEM flash_data;

void SPI_FLASH_Init(void);
void SPI_FLASH_test(void);



uint8_t flash_busy(SPI_HandleTypeDef *SPI);
void flash_write_enable(SPI_HandleTypeDef *SPI);
void flash_write_disable(SPI_HandleTypeDef *SPI);
void flash_read_status(SPI_HandleTypeDef *SPI,uint8_t num,uint8_t* out_buf);
void flash_read_jedec_id(SPI_HandleTypeDef *SPI,uint8_t* out_buf);
void flash_read_unique_id(SPI_HandleTypeDef *SPI,int8_t* out_buf);
void flash_write(SPI_HandleTypeDef *SPI,uint32_t address ,uint8_t* data_buf, uint16_t size);
void flash_read(SPI_HandleTypeDef *SPI,uint32_t address, uint8_t* out_buf, uint16_t size);
void flash_sector_erase(SPI_HandleTypeDef *SPI,uint32_t address);
void flash_erase_chip();
#endif /* SPI_FLASH_H_ */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
