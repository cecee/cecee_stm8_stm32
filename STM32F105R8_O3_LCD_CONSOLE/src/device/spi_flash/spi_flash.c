/**
  ******************************************************************************
  * @file SPI/M25P64_FLASH/spi_flash.c 
  * @author  MCD Application Team
  * @version  V3.0.0
  * @date  04/06/2009
  * @brief  This file provides a set of functions needed to manage the
  *         communication between SPI peripheral and SPI M25P64 FLASH.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 

/* Includes ------------------------------------------------------------------*/
#include "spi_flash.h"
#include "extern.h"
#include<string.h>

//FLASH_MEM flash_data;
CONTROL_TARGET eeprom;
    
extern CONSOLE_VALUE gVal;    
extern SPI_HandleTypeDef hspi1;   

void save_eeprom(CONTROL_TARGET eep);

/** @addtogroup StdPeriph_Examples
  * @{
  */

/** @addtogroup SPI_M25P64_FLASH
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
#define SPI_FLASH_PageSize    0x100

/* Private define ------------------------------------------------------------*/
#define WRITE      0x02  /* Write to Memory instruction */
#define WRSR       0x01  /* Write Status Register instruction */
#define WREN       0x06  /* Write enable instruction */

#define READ       0x03  /* Read from Memory instruction */
#define RDSR       0x05  /* Read Status Register instruction  */
#define RDID       0x9F  /* Read identification */
#define SE         0xD8  /* Sector Erase instruction */
#define BE         0xC7  /* Bulk Erase instruction */

#define WIP_Flag   0x01  /* Write In Progress (WIP) flag */

#define Dummy_Byte 0xA5

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval : None
  */
void SPI_FLASH_Init(void)
{
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
  flash_read(&hspi1, 0, (uint8_t*)&eeprom, sizeof(eeprom));
  printf("[%x][%x]\r\n",eeprom.header, eeprom.nheader);
  if(eeprom.header!=0xAAAA || eeprom.nheader!=0x5555){
    printf("xxxx[%x][%x]\r\n",eeprom.header, eeprom.nheader);
    eeprom.header=0xAAAA;
    eeprom.nheader=0x5555;
    eeprom.on_time=25;
    eeprom.o3=100;
    eeprom.temperature=20;
    eeprom.off_time=10;//10��
    save_eeprom(eeprom);
  }
  
}

void SPI_FLASH_test(void)
{
  uint8_t wdata[4]={0xAA,0xBB,0xCC,0xDD};
  uint8_t rdata[4]={0,};
  //flash_write(SPI_TypeDef *SPI,uint32_t address ,uint8_t* data_buf, uint16_t size)
  flash_write(&hspi1, 0, wdata,4);
  //void flash_read(SPI_TypeDef *SPI,uint32_t address, uint8_t* out_buf, uint16_t size){
  flash_read(&hspi1, 0, rdata, 4);
  printf("[%x][%x][%x][%x]\r\n",rdata[0],rdata[1],rdata[2],rdata[3]);

}

void save_eeprom(CONTROL_TARGET eep)
{
 // CONTROL_TARGET eep;  
  printf("=== save_eeprom ====\r\n");
  memcpy(&eep, &gVal.target, sizeof(eep));
  eep.header=0xAAAA;
  eep.nheader=~eep.header;
  flash_sector_erase(&hspi1,0x0000);
  flash_write(&hspi1, 0, (uint8_t*)&eep, sizeof(eep));
}

uint8_t flash_busy(SPI_HandleTypeDef *SPI){
	uint8_t status_reg;
	flash_read_status(SPI,1,&status_reg);
	return status_reg & 0x01;
}

void flash_write_enable(SPI_HandleTypeDef *SPI){
	uint8_t buf;
	buf=0x06;
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,&buf,1,1);
	SPI_FLASH_CS_HIGH();
}

void flash_write_disable(SPI_HandleTypeDef *SPI){
	uint8_t buf;
	buf=0x04;
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,&buf,1,1);
	SPI_FLASH_CS_HIGH();
}

void flash_read_status(SPI_HandleTypeDef *SPI,uint8_t num,uint8_t* out_buf){
	uint8_t mosi_buf[2];
	uint8_t miso_buf[2];
	if(num==1)
		mosi_buf[0]=0x05;
	else if(num==2)
		mosi_buf[0]=0x35;
	else
		return;

	SPI_FLASH_CS_LOW();
	HAL_SPI_TransmitReceive(SPI,mosi_buf,miso_buf,2,2);
	SPI_FLASH_CS_HIGH();

	out_buf[0]=miso_buf[1];
	//outbuf[1]=miso_buf[2];
}

void flash_read_jedec_id(uint8_t* out_buf){
	uint8_t mosi_buf[4];
	uint8_t miso_buf[4];

	mosi_buf[0]=0x9f;

	SPI_FLASH_CS_LOW();
	HAL_SPI_TransmitReceive(&hspi1,mosi_buf,miso_buf,4,4);
	SPI_FLASH_CS_HIGH();

	for(int i=0;i<3;i++)
		out_buf[i]=miso_buf[i+1];
}

void flash_read_unique_id(SPI_HandleTypeDef *SPI,int8_t* out_buf){
	uint8_t mosi_buf[5];
	uint8_t miso_buf[8];

	mosi_buf[0]=0x4b;

	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,mosi_buf,5,5);
	HAL_SPI_Receive(SPI,miso_buf,8,8);
	SPI_FLASH_CS_HIGH();

	for(int i=0;i<8;i++)
		out_buf[i]=miso_buf[i];
}

void flash_write(SPI_HandleTypeDef *SPI,uint32_t address ,uint8_t* data_buf, uint16_t size){
	uint8_t mosi_buf[4];
	mosi_buf[0]=0x02;
	while(flash_busy(SPI)==0x01);

	for(int i=0;i<3;i++){
		mosi_buf[3-i]=address&0x000000ff;
		address=address>>8;
	}

	flash_write_enable(SPI);
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,mosi_buf,4,4);
	HAL_SPI_Transmit(SPI,data_buf,size,10);
	SPI_FLASH_CS_HIGH();
	//flash_write_disable();
}
void flash_read(SPI_HandleTypeDef *SPI,uint32_t address, uint8_t* out_buf, uint16_t size){
	uint8_t mosi_buf[4];
	mosi_buf[0]=0x03;

	while(flash_busy(SPI)==0x01);

	for(int i=0;i<3;i++){
		mosi_buf[3-i]=address&0x000000ff;
		address=address>>8;
	}

	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,mosi_buf,4,4);
	HAL_SPI_Receive(SPI,out_buf,size,10);
	SPI_FLASH_CS_HIGH();
}
void flash_sector_erase(SPI_HandleTypeDef *SPI,uint32_t address){
	uint8_t mosi_buf[4];
	mosi_buf[0]=0x20;
	for(int i=0;i<3;i++){
            mosi_buf[3-i]=address&0x000000ff;
            address=address>>8;
	}
	flash_write_enable(SPI);
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,mosi_buf,4,4);
	SPI_FLASH_CS_HIGH();
}
void flash_erase_chip()
{
    uint8_t Write_Enable = 0x06;
    uint8_t Erase_Chip = 0xC7;

    SPI_FLASH_CS_LOW();
    HAL_SPI_Transmit(&hspi1,&Write_Enable,1,1); // Write Enable Command
    SPI_FLASH_CS_HIGH();

    SPI_FLASH_CS_LOW();
    HAL_SPI_Transmit(&hspi1,&Erase_Chip,1,1);   // Erase Chip Command
    SPI_FLASH_CS_HIGH();
}


