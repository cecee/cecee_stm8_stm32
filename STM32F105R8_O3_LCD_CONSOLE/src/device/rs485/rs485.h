#ifndef RS485_H
#define RS485_H
#include "stm32f1xx_hal.h"
#define MAX_SIZE 512 // max size of queue

typedef struct 
{
  uint16_t bd_id;
  uint16_t o3_ppb;
  uint16_t temperature;  
  uint16_t humidity;
  uint8_t error;
}SENSOR_INFO;

typedef struct _SENSOR_UPDATA   
{
  uint8_t stx; // 1
  uint8_t size;	//1
  SENSOR_INFO sensor;
  uint8_t sum; //1
  uint8_t etx; //1
}SENSOR_UPDATA;  

typedef struct _queue   
{
  int queue[MAX_SIZE+1];
  int front;
  int rear;
}QUEUE;

typedef struct _SUB_BOARD_ACCESS
{
    uint8_t  RELAY_BOX_ACCESS;
    uint8_t  O3SENSOR_ACCESS[8];
}SUB_BOARD_ACCESS;

class RS485
{
  private:
  public:
  RS485();
  virtual ~RS485();
  void init_qUart1(void);
  int put_qUart1(int k);
  int get_qUart1(void);
  void QueueProcess(void);
  void led_control(uint8_t* data);
  void rs485_req(uint16_t id, uint8_t data);
};

#endif 
