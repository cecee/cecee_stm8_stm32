/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "rs485.h"
#include "extern.h"
#include <stdio.h>


extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

SENSOR_UPDATA o3sensor1;
SENSOR_UPDATA o3sensor2;
SENSOR_UPDATA o3sensor3;

RS485 *pRS485;

SUB_BOARD_ACCESS sub_bd_access; 
//FAN_DATA rep_fan;
    
QUEUE qUart1;
uint8_t qUart1_package=0;
uint8_t qUart1_buffer[512];
uint8_t qUart1_cnt=0;
uint8_t qUart1_sz=0xff;

/*****************************************************************************
*                           Static Variables
******************************************************************************/

uint8_t rx_buffer[256];
uint8_t rx_buffer_idx=0;
uint8_t rxSz=0;
uint8_t package_flag=0;
uint8_t UART_FLAG = 0;


RS485::RS485() {}
RS485::~RS485() {}


void RS485::init_qUart1(void){
  qUart1.front=0;
  qUart1.rear=0;
  memset(&sub_bd_access,0,sizeof(sub_bd_access));
  //HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_RESET);
}


int RS485::put_qUart1(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart1.rear + 1) % MAX_SIZE == qUart1.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart1.rear, qUart1.front);
        return -1;
    }

    qUart1.queue[ qUart1.rear] = k;
    qUart1.rear = (qUart1.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart1.rear);
    return k;
}


int RS485::get_qUart1(void)
{
    int i=0;
     if (qUart1.front == qUart1.rear){
        return -1;
    }
    i = qUart1.queue[qUart1.front];
   qUart1.front = (qUart1.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}



void RS485::QueueProcess(){
  int  d,ex_d;
  int end=0;
    for(;;)
    {
      d =get_qUart1();
      
      if(d==-1 )break;
      //printf("[%x]\r\n",d);
      if(d=='*' && package_flag==0) {
        //printf("find~\r\n");
        package_flag=1;
        rx_buffer_idx=0;
        memset(rx_buffer,0,sizeof(rx_buffer));
      }
      if(package_flag){
        rx_buffer[rx_buffer_idx++]= d;
        if(d==0x0A || d==0x0D){
          rx_buffer_idx=0;
          package_flag=0;
          //pAT->parse((char *)rx_buffer);
          memset(rx_buffer,0,sizeof(rx_buffer));
        } 
        ex_d=d;
      }
      
      if(rx_buffer_idx>120) {
        package_flag=0;
        rx_buffer_idx=0;
        memset(rx_buffer,0,sizeof(rx_buffer));
        break;
      }
      if(end) break;	
    }
}


uint8_t calcu_sum(uint8_t *data, uint8_t len)
{
    uint8_t i, sum=0;
    for(i=0;i<len;i++)sum=sum+data[i];
    return sum;
}

//void RS485::reqLed(uint16_t id, uint8_t timeout)
//{
//  switch(id){
//    case 0xAAA0:
//      led_bar.info[0].tick=timeout;
//      break;
//    case 0xB000:
//      led_bar.info[1].tick=timeout;
//      break; 
//    case 0xB001:
//      led_bar.info[2].tick=timeout;
//      break;  
//    case 0xB002:
//      led_bar.info[3].tick=timeout;
//      break;       
//  }
//}

void RS485::rs485_req(uint16_t id, uint8_t data){
  uint8_t i;
  REQUEST req;
  uint8_t sum=0;
  uint8_t len=sizeof(REQUEST);
  uint8_t buf[128]={0,};  
  uint16_t reqID=id;//&0xFF00;
  uint8_t resLen,resSum;
  
  req.stx=0xFA;
  req.id=id;
  req.size=len;
  req.data=data;
  req.dumy=0;
  req.sum=0;
  req.etx='@';
  
  memcpy(buf, &req, len); 
  sum=calcu_sum(buf, len-1);
  buf[len-2]=sum;
  
  //memset(buf,0xaa,len);//test
  //printf("tx_req stx[%02x] id[%04x] sum[%02x] etx[%02x]--len[%d]\r\n", req.stx, req.id, req.sum, req.etx, len);
  //for(i=0;i<len;i++) printf("tx_req [%02d] [%x] \r\n", i, buf[i]);
  //reqLed(id,50);
  
  HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_SET);
  HAL_UART_Transmit(&huart1, buf, len, 100); 
  HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_RESET);
  memset(buf,0,sizeof(buf));
  HAL_UART_Receive(&huart1, buf, 64,100);
  
  printf("rx_resp [%x][%x][%x][%x] \r\n", buf[0], buf[1], buf[2], buf[3]);
  
  if(buf[0]==0xFA){
    resLen=buf[1];
    resSum=buf[resLen-2];
    sum=calcu_sum(buf, resLen-2);
    if(sum!=resSum) return;
    switch(reqID){
    case BLACK_BOX_ID:
        sub_bd_access.RELAY_BOX_ACCESS=0;
        memcpy(&pOrange->blackBoxresp, buf, resLen);
        memcpy(&pOrange->black_data, &pOrange->blackBoxresp.data, sizeof(BLACK_BOX_DATA));
        printf("RELAY o3_relay[%02X] uv_relay[%02X] fan_relay[%02X] power_relay[%02X]\r\n",
               pOrange->black_data.o3_relay.enable,  
               pOrange->black_data.uv_relay.enable, 
               pOrange->black_data.fan_relay.enable,
               pOrange->black_data.power_relay.enable); 
         printf("------------RELAY V[%03d] A[%03d] P[%03d] F[%03d]\r\n",
               pOrange->black_data.ac.volt,  
               pOrange->black_data.ac.current, 
               pOrange->black_data.ac.power,
               pOrange->black_data.ac.factor);
      break;
    case SENSOR_BOX_ID0:
        sub_bd_access.O3SENSOR_ACCESS[0]=0;
        memcpy(&pOrange->sensorBoxresp1, buf, resLen);
        memcpy(&pOrange->sensor_data[0], &pOrange->sensorBoxresp1.data, sizeof(SENSOR_DATA));
        
        break;
    case SENSOR_BOX_ID1:
        sub_bd_access.O3SENSOR_ACCESS[1]=0;
        memcpy(&pOrange->sensorBoxresp1, buf, resLen);
        memcpy(&pOrange->sensor_data[1], &pOrange->sensorBoxresp1.data, sizeof(SENSOR_DATA));
        break;      
    case SENSOR_BOX_ID2:
        sub_bd_access.O3SENSOR_ACCESS[2]=0;
        memcpy(&pOrange->sensorBoxresp1, buf, resLen);
        memcpy(&pOrange->sensor_data[2], &pOrange->sensorBoxresp1.data, sizeof(SENSOR_DATA));
        break;   
    }
    if(reqID>=0xB000){
      printf("reqID[%04x] o3[%03d] temp[%03d] humidi[%03d] error[%d]\r\n",
             reqID, 
             pOrange->sensorBoxresp1.data.o3_ppb,  
             pOrange->sensorBoxresp1.data.temperature,  
             pOrange->sensorBoxresp1.data.humidity,  
             pOrange->sensorBoxresp1.data.error);  
    }
//    if(reqID==SENSOR_BOX_ID0){
//      sub_bd_access.O3SENSOR_ACCESS[0]=0;
//     //printf("B000\r\n");
//     //for(i=0;i<16;i++)printf("[%02X]",buf[i]); printf("\r\n");  
//      sum=calcu_sum(buf, resLen-2);
//      //printf("sum[%x][%x]\r\n",sum, resSum); 
//      if(sum==resSum){
//        memcpy(&pOrange->sensorBoxresp1, buf, resLen);
//        memcpy(&pOrange->sensor_data[0], &pOrange->sensorBoxresp1.data, sizeof(SENSOR_DATA));
//         //pMain_page->draw_monitor_ppm(pOrange->sensor_data[0].o3_ppb);
//         //pMain_page->draw_monitor_temperature(pOrange->sensor_data[0].temperature);
//         //pMain_page->draw_monitor_humidity(pOrange->sensor_data[0].humidity);
//         
//        //gMQT_Value.o1=o3sensor1.sensor.o3_ppb;
//        //gMQT_Value.t1=o3sensor1.sensor.temperature;
//        //gMQT_Value.h1=o3sensor1.sensor.humidity;
//        //printf("O3SENSOR1 id[%x] o3_ppb[%d] temperature[%d] humidity[%d] error[%d]\r\n",
//        //  o3sensor1.sensor.bd_id,  o3sensor1.sensor.o3_ppb, o3sensor1.sensor.temperature, o3sensor1.sensor.humidity, o3sensor1.sensor.error); 
//
//      }
//    }
    
//    else if(reqID==SENSOR_BOX_ID1){
//      sub_bd_access.O3SENSOR_ACCESS[1]=0;
//      sum=calcu_sum(buf, resLen-2);
//      if(sum==resSum){
//        memcpy(&o3sensor2, buf, resLen);
//        o3sensor2.sensor.bd_id=SWAPBYTE_US(o3sensor2.sensor.bd_id);
//        //gMQT_Value.o2=o3sensor2.sensor.o3_ppb;
//        //gMQT_Value.t2=o3sensor2.sensor.temperature;
//        //gMQT_Value.h2=o3sensor2.sensor.humidity;
//        //printf("O3SENSOR2 id[%x] o3_ppb[%d] temperature[%d] humidity[%d] error[%d]\r\n",
//        //  o3sensor2.sensor.bd_id,  o3sensor2.sensor.o3_ppb, o3sensor2.sensor.temperature, o3sensor2.sensor.humidity, o3sensor2.sensor.error); 
//
//      }
//    }
  }

}



