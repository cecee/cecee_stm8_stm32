#ifndef OZONE_CONTROL_H
#define OZONE_CONTROL_H
#include "stm32f1xx_hal.h"
//#define MAX_SIZE 512 // max size of queue

typedef struct
{
    uint8_t  o3: 1;
    uint8_t  uv: 1;
    uint8_t  fan: 1;	
    uint8_t  rev1: 1;
    uint8_t  rev2: 1;
    uint8_t  state: 2;
}RELAY_CTRL_BIT;

typedef union
{
  RELAY_CTRL_BIT b;
  uint8_t data;
}RELAY_CTRL_DATA;

class OZONE_CONTROL
{
  private:
  public:
  uint16_t ex_ltime;  
  RELAY_CTRL_DATA relay_on;
  OZONE_CONTROL();
  virtual ~OZONE_CONTROL();
  void init(void);
  void loop(uint16_t ltime);
  bool compare_o3(void);
  void RelayCtrl(uint16_t ltime, uint16_t on_time, uint16_t off_time, bool on);
};

#endif 
