/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "ozone_control.h"
#include "extern.h"
#include <stdio.h>


extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern CONSOLE_VALUE gVal;

OZONE_CONTROL *pOzoneCtrl;
extern O3_MAIN_PAGE *pMain_page;

/*****************************************************************************
*                           Static Variables
******************************************************************************/


OZONE_CONTROL::OZONE_CONTROL() {}
OZONE_CONTROL::~OZONE_CONTROL() {}


void OZONE_CONTROL::init(void){
  ex_ltime=0;
}

void OZONE_CONTROL::loop(uint16_t ltime){
  bool result;
  //uint16_t period;
  
  if(ltime ==ex_ltime) return;
  ex_ltime=ltime; //keep old time 
  gVal.target.total_time = 60+ (gVal.target.on_time) +60+(gVal.target.off_time*60);
  //gVal.target.total_time = period;
  
  if(ltime>gVal.target.total_time){
    ELAPSED_SEC=0;
    RelayCtrl(ltime, 0, 0, FALSE);
    return;
  }
  printf("---OZONE_CONTROL ltime[%d] total_time[%d]---\r\n",ltime, gVal.target.total_time);
  pMain_page->draw_monitor_min(ELAPSED_SEC, gVal.target.total_time );
  
  //printf("## OZONE_CONTROL ltime[%d]\r\n",ltime);
  result=compare_o3();
  if(result){
    RelayCtrl(ltime, gVal.target.on_time, gVal.target.off_time, TRUE);
  }
  else{
  //ALL OFF
    RelayCtrl(ltime, 0, 0, FALSE);
  }
  
}

bool OZONE_CONTROL::compare_o3(void){
  bool result=FALSE;
  uint16_t l_o3[3];
  uint16_t o3_sour;
  uint16_t o3_dest;
  l_o3[0]=pOrange->sensor_data[0].o3_ppb;
  l_o3[1]=pOrange->sensor_data[1].o3_ppb;
  l_o3[2]=pOrange->sensor_data[2].o3_ppb;
  o3_sour=l_o3[0];
  o3_dest=gVal.target.o3;
  
  printf("## OZONE_CONTROL o3_sour[%d] o3_dest[%d]\r\n",o3_sour, o3_dest);
  result=(o3_dest>o3_sour) ? TRUE :FALSE;
  return result;
}

void OZONE_CONTROL::RelayCtrl(uint16_t ltime, uint16_t on_time, uint16_t off_time, bool on){
  uint16_t on_period,fan_on_after, fan_on_before, o3_on, o3_ontime, o3_offtime;
  uint8_t state=0;
  
  printf("##(1) OZONE_CONTROL ACTOIN CTRL[%d][%d][%d]-[%d]\r\n", ltime, on_time, off_time, on);
  if(!on){
    relay_on.b.o3=0;
    relay_on.b.uv=0;
    relay_on.b.fan=0;
    return;
  }
  
  on_period= 60+ (gVal.target.on_time) +60;
  fan_on_after=60+ (gVal.target.on_time);
  o3_on=60;//+ (gVal.target.on_time);
  fan_on_before=0;

  if(ltime>on_period)state=3;
  else if(ltime>fan_on_after)state=2;
  else if(ltime>o3_on)state=1;
  else state=0;
 // if(duty>=100)state=2;
  relay_on.b.state=state;
  printf("##(2) OZONE_CONTROL ACTOIN CTRL  ltime[%d] state[%d]\r\n",ltime, relay_on.b.state);
  
  switch(state){
    case 0:
      relay_on.b.o3=0;
      relay_on.b.uv=0;
      relay_on.b.fan=1;      
      break;
    case 1:
      relay_on.b.o3=1;
      relay_on.b.uv=1;
      relay_on.b.fan=1; 
      break;
    case 2:
      relay_on.b.o3=0;
      relay_on.b.uv=0;
      relay_on.b.fan=1; 
      break;
    case 3:    
      relay_on.b.o3=0;
      relay_on.b.uv=0;
      relay_on.b.fan=0; 
      break;
  }
}


