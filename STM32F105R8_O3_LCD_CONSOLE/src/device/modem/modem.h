#ifndef MODEM_H
#define MODEM_H


#include "stm32f1xx_hal.h"
#include <string>
#include <vector>  
  
using namespace std; 


typedef struct _MODEM_N502L   
{
  uint8_t ready;
  string telephon_number;
}MODEM_N502L;

typedef struct _BOX_BIT   
{
  uint8_t plasma:1;
  uint8_t uv:1;
  uint8_t fan:1;
  uint8_t resv1:1;
  uint8_t resv2:2;
}BOX_BIT;

typedef struct _MQTT_PUBLISH   
{
  string id;
  uint8_t rw;
  BOX_BIT ctrl;
  int sensor_cnt;
  int time_on_period;
  int time_off_period;
  int o3_value;
  int acv,aca,acp;
  int o1,o2,o3;
  int t1,t2,t3;
  int h1,h2,h3;
}MQTT_PUBLISH;

class MODEM
{
    private:
    public:
      uint8_t MODEM_READY;
      MODEM_N502L modem_n502l;
      MQTT_PUBLISH mqtt_pub_value;
      
      MODEM();
      virtual ~MODEM();
      char *AT_Req(const char *req);
      void modem_init(void);
      char *AT_CMD(const char *req);
      uint8_t AT_isReady(void);
      string AT_GetPhoneNumber(void);
      bool isMqttConnect(void);
      uint8_t MqttConnect(void);
      uint8_t AT_MqttConnect(void);
      uint8_t AT_MqttDisConnect(void);
      uint8_t AT_isMqttConfig(void);
      uint8_t AT_setMqttConfig(void);
      char MqttSub(string id);
      void MqttPub(uint8_t index);
      
      //vector<string> split2(string s, string divid);
      std::string replaceString(std::string subject, const std::string &search, const std::string &replace);
      void ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace); 
};


#endif 
