/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "modem.h"
#include "extern.h"

#include "iwdg.h"
MODEM *pModem;
extern CONSOLE_VALUE gVal;
extern UART_HandleTypeDef huart2;

vector<string> split2(string s, string divid);

MODEM::MODEM(){
  MODEM_READY=0;
  memcpy(&modem_n502l,0,sizeof(modem_n502l));
  memcpy(&mqtt_pub_value,0,sizeof(mqtt_pub_value));
}

MODEM::~MODEM(){
}


vector<string> split2(string s, string divid) {
	vector<string> v;
	int start = 0;
	int d = s.find(divid);
	while (d != -1){
		v.push_back(s.substr(start, d - start));
		start = d + 1;
		d = s.find(divid, start);
	} 
	v.push_back(s.substr(start, d - start));

	return v;
}


//Immutable Replace 원본 문자열에는 아무 영향을 끼치지 않는다. 변경된 문자열은 함수의 반환값으로 돌아온다.
std::string MODEM::replaceString(std::string subject, const std::string &search, const std::string &replace) { 
  size_t pos = 0; 
  while ((pos = subject.find(search, pos)) != std::string::npos) 
  { 
    subject.replace(pos, search.length(), replace); 
    pos += replace.length(); 
  } 
  return subject; 
}

//Mutable Replace 원본 문자열을 수정한다. 속도가 우선일 경우 사용하자.
void MODEM::ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace) { 
  size_t pos = 0; 
  while ((pos = subject.find(search, pos)) != std::string::npos) { 
    subject.replace(pos, search.length(), replace); pos += replace.length(); 
  } 
}

char *MODEM::AT_CMD(const char *req){
 
  char response[512]={0,};
  int strLen= strlen(req);
  HAL_UART_Transmit(&huart2, (uint8_t*)req, strLen, 100); 
  HAL_UART_Receive(&huart2, (uint8_t*)response, 512,200);
  return response;  
}  


uint8_t MODEM::AT_isReady(void){
  char* res;
  uint8_t ok=0;
  res= AT_CMD("AT\r\n");
  if (strstr(res, "OK") != NULL) ok=1;
  return ok;
}



uint8_t MODEM::AT_isMqttConfig(void){
  char* res;
  uint8_t ok=0;
  res=AT_CMD("AT*WMQTCFG?\r\n");
  printf("%s",res); 
  return ok;
}

uint8_t MODEM::AT_MqttDisConnect(void){
  char* res;
  uint8_t ok=0;
  res=AT_CMD("AT*WMQTDIS\r\n");
  //if (strstr(res, "OK") != NULL) ok=1;
  return ok;
}

string MODEM::AT_GetPhoneNumber(void){

 string res;
 string str="xxx";
 vector<string> v;
 res=(char*)AT_CMD("AT$$CNUM?\r\n");
 v=split2(res, ",");
 for(int i=0;i<v.size();i++)
 for (string s : v) {
      if (s.find("\"+") != string::npos) { 
        str=s;
        ReplaceStringInPlace(str,"\"","");//"""제거
        ReplaceStringInPlace(str,"+","");//"+" 제거
        break;
      }
 }
 return str;
}

uint8_t MODEM::MqttConnect(void){
  char* res;
  uint8_t ok=0;

  AT_CMD("AT*WMQTCFG=endpoint,ssl://a3goy7xojgwxx6-ats.iot.us-west-2.amazonaws.com\r\n");
  AT_CMD("AT*WMQTCFG=port,8883\r\n");
  AT_CMD("AT*WMQTCFG=clientname,pm419_iot\r\n");
  AT_CMD("AT*WMQTCFG=cacerts,/data/cacerts.crt\r\n");
  AT_CMD("AT*WMQTCFG=clientkey,/data/clientkey.key\r\n");
  AT_CMD("AT*WMQTCFG=clientcerts,/data/clientcerts.crt\r\n");  
  
  res=AT_CMD("AT*WMQTCON\r\n");
  if (strstr(res, "CON:2") != NULL) ok=1;
  return ok;
}

bool MODEM::isMqttConnect(void){
  bool connect=false;
  char* res;
  res=AT_CMD("AT*WMQTCON?\r\n");
  //printf("isMqttConnect : %s\r\n",res);
  if (strstr(res, "CON:2") != NULL) connect=true;
  return connect;
}


void MODEM::MqttPub(uint8_t index){

  char* res;
  char i, ok;
  char s_pub[256];
  int t,tmp;
  //MQTT_PUBLISH pub;
  string str;
  //string rw="w";
  //oC: O3 Ctrl value
  //tC: temperature Ctrl value
  //fC: Factor Ctrl value
  //uint8_t act;
  int oC,tC,fC,o1,o2,o3,t1,t2,t3,h1,h2,h3;
  int sensor_cnt, ctrl,acv,aca,acp,acf;
  int on_time, off_time;
  
  on_time=gVal.target.on_time;
  off_time=gVal.target.off_time;
  printf("gVal.target.on_time[%d]\r\n",on_time);
  printf("gVal.target.off_time[%d]\r\n",off_time);
  
  ok=0;
  string id=pModem->modem_n502l.telephon_number;
  ctrl=0x00;
  oC=gVal.target.o3;
  tC=gVal.target.temperature;
  fC=gVal.target.factor;
  acv=pOrange->black_data.ac.volt;
  aca=pOrange->black_data.ac.current; 
  acp=pOrange->black_data.ac.power;
  acf=pOrange->black_data.ac.factor;
  if(pOrange->black_data.o3_relay.enable) _SetBit(ctrl,0);
  if(pOrange->black_data.uv_relay.enable) _SetBit(ctrl,1);
  if(pOrange->black_data.fan_relay.enable) _SetBit(ctrl,2);
  //ctrl=0x07;
  sensor_cnt= 0;
  o1=pOrange->sensor_data[0].o3_ppb; 
  o2=pOrange->sensor_data[1].o3_ppb; 
  o3=pOrange->sensor_data[2].o3_ppb; 
  
  t=pOrange->sensor_data[0].temperature; 
  if(t>0x8000) { tmp=(t&0x7FFF)*(-1); }
  else tmp=t&0x7FFF;
  t1=tmp; 

  t=pOrange->sensor_data[1].temperature; 
  if(t>0x8000) { tmp=(t&0x7FFF)*(-1); }
  else tmp=t&0x7FFF;
  t2=tmp; 
  
  t=pOrange->sensor_data[2].temperature; 
  if(t>0x8000) { tmp=(t&0x7FFF)*(-1); }
  else tmp=t&0x7FFF;
  t3=tmp; 
  //t2=pOrange->sensor_data[1].temperature; 
  //t3=pOrange->sensor_data[2].temperature; 
  h1=pOrange->sensor_data[0].humidity; 
  h2=pOrange->sensor_data[1].humidity; 
  h3=pOrange->sensor_data[2].humidity;   

//  printf("pub=> id : %s sensor[%d]\r\n",id.c_str(), sensor_cnt);
  //printf("AC : [%d][%d][%d]\r\n",acv, aca, acp);
  //printf("o3 : [%d][%d][%d]\r\n",o1, o2, o3);
  //printf("tm : [%d][%d][%d]\r\n",t1, t2, t3);
  //printf("hm : [%d][%d][%d]\r\n",h1, h2, h3);
  
  sprintf(s_pub,"AT*WMQTPUB=/iot/iot_db,\"{\"id\":\"%s\",\"rw\":%d, \
\"oC\":%d, \"tC\":%d, \"fC\":%d, \"o1\":%d, \"o2\":%d, \"o3\":%d, \"t1\":%d, \"t2\":%d, \"t3\":%d, \
\"h1\":%d, \"h2\":%d, \"h3\":%d, \"av\":%d, \"aa\":%d, \"ap\":%d, \"af\":%d,\"rC\":%d, \"sc\":%d , \"on_time\":%d, \"off_time\":%d}\" \r\n", 
id.c_str(), index, oC, tC, fC, o1, o2, o3, t1, t2, t3, h1, h2, h3, acv, aca, acp, acf, ctrl, sensor_cnt, on_time, off_time);

  printf("%s\r\n",s_pub);
  res=AT_CMD(s_pub);
 // printf("RES : %s\r\n",res);
}

char MODEM::MqttSub(string id){
  char ldata[64];
  char* res;
  sprintf(ldata,"AT*WMQTSUB=/iot/%s\r\n", id.c_str());
  printf("[%s]\r\n",ldata);
  
  res=AT_CMD(ldata);
  printf("[%s]\r\n",res);
  return 1;
}

void MODEM::modem_init(void){
  uint8_t time_out_cnt=0;
  modem_n502l.ready=0;
  
  while(1){
    printf("modem_init... time_out_cnt[%d]\r\n",time_out_cnt);
    __HAL_IWDG_RELOAD_COUNTER(&hiwdg);
    
    if(time_out_cnt>30) break;
    //break;
    if(AT_isReady()){
      printf("AT_isReady ok!\r\n");
      modem_n502l.telephon_number=AT_GetPhoneNumber();
      //gMQT_Value.id = modem_n502l.telephon_number;
      printf("Try MQT[%03d] phoneNum[%s]\r\n",time_out_cnt, modem_n502l.telephon_number.c_str());
      //AT_MqttDisConnect();
      //__HAL_IWDG_RELOAD_COUNTER(&hiwdg);
      if(MqttConnect())
      { 
        printf("MQTT CONNECT... OK[%d]\r\n",time_out_cnt);
        //pMqtt->MqttSub(modem_n502l.telephon_number);
        modem_n502l.ready=1;
        break;
      }
    }
    time_out_cnt++; 
    HAL_Delay(500);
  }
  pModem->MqttPub(0);//logon signal push
  printf("modem_init... OKKKKKKKKKKK\r\n");

}

