/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stm32f1xx_hal.h"
#include "stm32f1xx.h"
#include "can.h"
#include "hal_can.h"
//#include "stm32f1xx_hal_can.h"
#include "extern.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/
uint8_t bmsSQ=0;
uint8_t _101Cnt=0;
uint8_t _102Cnt=0;

BMS_101 bms0101;
BMS_102 bms0102;
TX_A0A0 tx_a0a0;
TX_A0A1 tx_a0a1;
   
/*****************************************************************************
*                           Global Variables
******************************************************************************/
extern CAN_HandleTypeDef hcan1;
//extern void Error_Handler(void);

CanTxMsgTypeDef   TxMessage1;
CanRxMsgTypeDef   RxMessage1;

CanTxMsgTypeDef   TxMessage0;
CanRxMsgTypeDef   RxMessage0;

uint8_t TransmitMailbox1 = 0;
uint8_t TransmitMailbox0 = 0;

/*****************************************************************************
*                                Functions
******************************************************************************/

int Hal_CAN1_Init(void)
{
  

 // uint32_t FilterId   =    BMS_ID_100;//
//  uint32_t FilterMask = 0x00000;//0x1FFFFF;
  
  hcan1.pTxMsg = &TxMessage0;
  hcan1.pRxMsg = &RxMessage0; 
  
  CAN_FilterConfTypeDef filtercfg;   
       /* CAN1 필터 적용 */
  
  filtercfg.FilterNumber = 0;
  filtercfg.FilterMode = CAN_FILTERMODE_IDMASK;
  filtercfg.FilterScale = CAN_FILTERSCALE_32BIT;
  filtercfg.FilterIdHigh = 0x0100<<5;//0001 00000000
  filtercfg.FilterIdLow = 0x0000;
  filtercfg.FilterMaskIdHigh = 0x0FFC<<5;//1111-1111-1100
  filtercfg.FilterMaskIdLow = 0x0000;
  filtercfg.FilterFIFOAssignment = 0;
  filtercfg.FilterActivation = ENABLE;
  filtercfg.BankNumber = 0;
  
  if(HAL_CAN_ConfigFilter(&hcan1, &filtercfg) != HAL_OK)
    {
     //   Error_Handler();
	 printf("Error_Handler\r\n");
    }

    hcan1.pTxMsg->StdId = BMS_ID_101;//11bit
    hcan1.pTxMsg->RTR = CAN_RTR_DATA;
    //hcan1.pTxMsg->IDE = CAN_ID_EXT;//CAN_ID_STD
    hcan1.pTxMsg->IDE = CAN_ID_STD;//CAN_ID_STD
    hcan1.pTxMsg->DLC = 8;
    memset(hcan1.pTxMsg->Data,0x00,8);

    //	__HAL_CAN_ENABLE_IT(&hcan1,CAN_IT_FMP0);
    if (HAL_CAN_Receive_IT(&hcan1, CAN_FIFO0) != HAL_OK)
    {
     // Reception Error 
	  printf("Error_Handler\r\n");
      Error_Handler();
    } 

  //  __HAL_CAN_ENABLE_IT(&hcan1,CAN_IT_FMP0);
	return RET_OK;
}



int Hal_CAN2_Init(void)
{
  
	uint32_t FilterId   =    SCT_ID_A0A0;//
	uint32_t FilterMask = 0x1FFFFFFE;

	hcan2.pTxMsg = &TxMessage1;
	hcan2.pRxMsg = &RxMessage1; 
	CAN_FilterConfTypeDef filtercfg;   
	/* CAN1 필터 적용 */
	filtercfg.FilterNumber = 0;
	filtercfg.FilterMode = CAN_FILTERMODE_IDMASK;
	filtercfg.FilterScale = CAN_FILTERSCALE_32BIT;
	filtercfg.FilterIdHigh = (FilterId << 3) >> 16;                   /* 29비트 중 상위 2 바이트 */
	filtercfg.FilterIdLow = (0xFFFF & (FilterId << 3)) | (1 << 2);      /* 29비트 중 하위 2 바이트 + IDE */
	filtercfg.FilterMaskIdHigh = (FilterMask << 3) >> 16;
	filtercfg.FilterMaskIdLow = (0xFFFF & (FilterMask << 3)) | (1 << 2);
	filtercfg.FilterFIFOAssignment = CAN_FIFO0;
	filtercfg.FilterActivation = ENABLE;
	filtercfg.BankNumber = 28;


	if(HAL_CAN_ConfigFilter(&hcan2, &filtercfg) != HAL_OK)
	{
	//   Error_Handler();
	}
    hcan2.pTxMsg->ExtId = SCT_ID_A0A0;
   // hcan2.pTxMsg->StdId = SCT_ID_A0A0;//11bit
    hcan2.pTxMsg->RTR = CAN_RTR_DATA;
    hcan2.pTxMsg->IDE = CAN_ID_EXT;//CAN_ID_STD
    
    hcan2.pTxMsg->DLC = 8;
    memset(hcan2.pTxMsg->Data,0x00,8);

    //	__HAL_CAN_ENABLE_IT(&hcan1,CAN_IT_FMP0);
    if (HAL_CAN_Receive_IT(&hcan2, CAN_FIFO0) != HAL_OK)
    {
     // Reception Error 
      Error_Handler();
    } 

	return RET_OK;
}


//CAN1 2.0A 500k BPS Trasmit
void  BMS_Transmit(uint32_t std_id)
{
  	static uint8_t count=0;
	count++;
	
  	uint8_t data[8];
	
	bms0101.bmsProtocolVersion=0x09;
	bms0101.uBmsActive.active=0x01;
	bms0101.bmsSOC=99;
	bms0101.bmsRequestCurrent=50;//0.5A
	bms0101.bmsTemperature=19;//0x13
	
	bms0102.bmsMaximumBatteryVoltage=820;
	bms0102.bmsMaximumBatteryCurrent=1600;
	bms0102.bmsPresentVoltage=767;
	bms0102.bmsPresentCurrent=-400;
	  
	//data[6]= (std_id == BMS_ID_101) ? 0x01 : 0x02;
	//printf("std_id[%x] data[6]:%x\r\n",std_id, data[6]);
	if(std_id == BMS_ID_101) {
	  bmsSQ++;
	  bms0101.bmsHeaterBeat = bmsSQ;
	  memcpy(data, &bms0101,8);
	}
	else  memcpy(data, &bms0102,8);
	
	hcan1.pTxMsg->StdId = std_id;
 	memcpy(hcan1.pTxMsg->Data, data ,8);
 	do
	{
		TransmitMailbox0 = HAL_CAN_Transmit(&hcan1, 10);
	}
	while( TransmitMailbox0 == CAN_TXSTATUS_NOMAILBOX );
}



//2.0B 250k BPS Trasmit test
void canTransTestScooter(uint32_t id)
{
  	uint8_t data[8]={0x2B,0xFF,0x05,0x00,0xFF,0x01,0x00,0xFE};
	
	if(id != SCT_ID_A0A0) data[6]=0x01;
	hcan2.pTxMsg->ExtId = id;
 	memcpy(hcan2.pTxMsg->Data, data ,8);
	//return;
 	do
	{
		TransmitMailbox1 = HAL_CAN_Transmit(&hcan2, 10);
	}
	while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );
		
}


void canTransScooter(uint32_t id, uint8_t *data)
{
//	uint8_t i=0;
  	hcan2.pTxMsg->ExtId = id;
 	memcpy(hcan2.pTxMsg->Data, data ,8);
	//printf("1TransmitMailbox1[%x]\r\n",TransmitMailbox1);
	//TransmitMailbox1 = HAL_CAN_Transmit(&hcan2, 10);
	//printf("2TransmitMailbox1[%x]\r\n",TransmitMailbox1);
#if 1	
 	do
	{
		TransmitMailbox1 = HAL_CAN_Transmit(&hcan2, 10);

	}
	while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );
#endif
}


int bms_recv(void)
{

  	uint32_t StdId;
  	uint8_t data[8];
	
	uint16_t  packVoltage, packCurrent;
	uint8_t warring=0;
	
  	StdId=hcan1.pRxMsg->StdId;
  	memcpy(data, hcan1.pRxMsg->Data,8);
	BMS_101 m0101;
	BMS_102 m0102;
	//printf("[%x]\r\n",StdId);

	switch(StdId){
	  	 case  BMS_ID_101:
		   _101Cnt++;
		   memcpy(&m0101,data,8);
		   
		   tx_a0a0.version = m0101.bmsProtocolVersion;
		   tx_a0a0.condition.byteCondition=0x00;
		   
		   memset(&tx_a0a1,0,8);
		   
		   if(m0101.uBmsActive.b.bmsWarning || m0101.uBmsActive.b.bmsFault){
		   	tx_a0a1.relayFail.dischargingRelayFail=0x01;
		   }
		   else tx_a0a1.relayFail.dischargingRelayFail=0x00;
			
		   //tx_a0a0.packVoltage=6920;
		   //tx_a0a0.packVoltage = SWAPBYTE_US(tx_a0a0.packVoltage);
		   //tx_a0a0.packCurrent= -40;
		   //tx_a0a0.packCurrent = SWAPBYTE_US(tx_a0a0.packCurrent);
		   
		   tx_a0a0.soc=m0101.bmsSOC;
		   tx_a0a0.temp=m0101.bmsTemperature;
		  break;		  
	  
		case  BMS_ID_102:
		  _102Cnt++;
		   memcpy(&m0102,data,8);
		   packVoltage = m0102.bmsPresentVoltage *10;
		   tx_a0a0.packVoltage = SWAPBYTE_US(packVoltage);
		   packCurrent = m0102.bmsPresentCurrent *10;
		   tx_a0a0.packCurrent = SWAPBYTE_US(packCurrent);
		  break;

	}
	return 1;
}


void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef *hcan)
{

 // printf(" 2.0A RX [%x]\r\n", hcan->pRxMsg->StdId); 
  if (HAL_CAN_Receive_IT(hcan, CAN_FIFO0) != HAL_OK)
  {
    Error_Handler();
  } 
  CAN_RCV_FLAG=1;
  // bms_recv(); 
}




