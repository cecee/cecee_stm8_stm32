#ifndef DRV_CAN_H
#define DRV_CAN_H

#include "stm32f1xx.h"
#include "stm32f1xx_hal_can.h"

#define CAN_MAX_ID 10

typedef struct _SCan
{
	CanRxMsg  CanRxMessage;
	uint16_t  CanID;
	uint8_t   CanFlag;
	void 	  (*m_fp)(void);
	
}SCan;


#define RCC_APB2Periph_GPIO_CAN    RCC_APB2Periph_GPIOA
#define GPIO_CAN                   GPIOA
#define GPIO_Pin_RX                GPIO_Pin_11
#define GPIO_Pin_TX                GPIO_Pin_12

void Can_Init();
void Can_FilterConfig(uint8_t filterMode, uint8_t FiltNum, uint16_t IDHigh, uint16_t IDLow, uint16_t MaskHigh, uint16_t MaskLow);
void Can_SetID(uint8_t index, uint16_t id, void (*func)(void));
void Can_TxMessage(uint16_t id, uint8_t length, uint8_t* data);

extern SCan sCan[CAN_MAX_ID];

#endif