/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "extern.h"
//#include "cpp_extern.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint16_t gAdcValue[4];
extern O3_MAIN_PAGE *pMain_page;
//MODEM *pModem;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t JOB_SEQ=0;
uint8_t exJOB_SEQ=0xFF;
int8_t JOB_State=0;
int8_t exJOB_State=0xff;
int8_t Relay_State=0;
int8_t exRelay_State=0xff;

uint8_t SEQ_CNT=0;
uint8_t MQTT_PUP=0;

int fputc(int ch, FILE *f)
{
	uint8_t temp[1]={ch};
	HAL_UART_Transmit(&huart4, temp, 1, 2);
	return(ch);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_UART4_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_IWDG_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  //__HAL_IWDG_START(&hiwdg);//32-1250 1�� ����
  
  pMain_page= new O3_MAIN_PAGE();
  pKEY= new KEYPAD_ADC();
  pRS485=new RS485();
  pOrange=new ORANGE_BOX();
  pOzoneCtrl=new OZONE_CONTROL();
  pModem = new MODEM();
  //pMqtt = new MQTT();
  //pUtil = new UTIL();

  HAL_ADCEx_Calibration_Start(&hadc1);
  HAL_TIM_Base_Start_IT(&htim1); 
  HAL_TIM_Base_Start_IT(&htim2); 
  
  ILI9341_Init();
  SPI_FLASH_Init();  
  

  pMain_page->initPage();
  pKEY->kpad_init();
  pOrange->initOrangeBox();
  pOzoneCtrl->init();
  HAL_Delay(500);
  pModem->modem_init(); 
  
  //ili9488_test();
  //uint32_t flash_id=SPI_FLASH_ReadID();
  //printf("SPI_FLASH_ReadID[%x]\r\n",flash_id);
  //flash_erase_chip();

  //HAL_Delay(500);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    //printf("test\r\n");
     __HAL_IWDG_RELOAD_COUNTER(&hiwdg);
    pOzoneCtrl->loop(ELAPSED_SEC);
    
    
    
    if(TIM1_KeyEvent){
     // printf("test\r\n");
      pKEY->adcRead();
      TIM1_KeyEvent=0;
    }
    else if(JOB_State != exJOB_State){
      if(JOB_State>3){
        pOrange->orangeUpdateDraw();
        JOB_State=0;
      }
      printf("process JOB_State[%d]\r\n",JOB_State);
      switch(JOB_State){
        case 0:
          sub_bd_access.RELAY_BOX_ACCESS++;
          pRS485->rs485_req(BLACK_BOX_ID, pOzoneCtrl->relay_on.data);
          break;
       case 1:
          sub_bd_access.O3SENSOR_ACCESS[0]++;
          pRS485->rs485_req(SENSOR_BOX_ID0, 0);
          break;         
        case 2:
          sub_bd_access.O3SENSOR_ACCESS[1]++;
          pRS485->rs485_req(SENSOR_BOX_ID1, 0);
          break;
       case 3:
          sub_bd_access.O3SENSOR_ACCESS[2]++;
          pRS485->rs485_req(SENSOR_BOX_ID2, 0);
          break;        
      }
      exJOB_State=JOB_State;
    }
    
    if(pOzoneCtrl->relay_on.data != exRelay_State){
        exRelay_State = pOzoneCtrl->relay_on.data;
      printf("--------CHANGE RELAY-------\r\n");
        sub_bd_access.RELAY_BOX_ACCESS++;
        pRS485->rs485_req(BLACK_BOX_ID, pOzoneCtrl->relay_on.data);
    }
    
    
    if(MQTT_PUP){
      MQTT_PUP=0;
      printf("--------MqttPub-------\r\n");
      printf("--------MqttPub-------\r\n");
      printf("--------MqttPub-------\r\n");
      
      if(!pModem->isMqttConnect()){
        pModem->MqttConnect();
//   printf("----------MqttConnect--TRY AGAIN!!\r\n");
//   for(i=0;i<5;i++){
//    ok=MqttConnect();
//    if(ok) break;
//   }
//   if(!ok) return;
  }
//      else{
//        pModem->MqttConnect();
//        pModem->MqttPub(1);
//      }

      pModem->MqttPub(1);

      HAL_Delay(100);
    }
 
    //pMqtt->waitMqtt();
    
    
    //HAL_Delay(100);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.Prediv1Source = RCC_PREDIV1_SOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  RCC_OscInitStruct.PLL2.PLL2State = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV4;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the Systick interrupt time
  */
  __HAL_RCC_PLLI2S_ENABLE();
}

/* USER CODE BEGIN 4 */
//void HAL_SYSTICK_Callback(void)
//{
//  static uint16_t systick=0;
//  static uint8_t toggle=0;
//  uint8_t i;
//  systick++;
//  if(systick>200)//100ms
//  {  
//    systick=0;
//    TIM2_KeyEvent=1;
//    printf("HAL_SYSTICK_Callback\r\n");
//  }
// 
//}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
