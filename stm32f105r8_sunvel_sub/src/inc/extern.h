#ifndef EXTERN_H_
#define EXTERN_H_


#include <stdio.h>
#include <stdint.h>
#include "string.h"
#include "stm32f1xx_hal.h"
#include "../device/HX711.h"
#include "../device/cat4109.h"
#include "../device/tja1050.h"
//#include "../hal/drv_can.h"

#include "gpio.h"


#define LOGOUT

#define FALSE		0
#define TRUE		1
#define RET_OK		(0)
#define RET_ERR	(-1)
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)
#define ERR		1
#define WARN	     2
#define MSG		3
#define PRT		4
#define GRN		5
#define ATR		6

#define HIGH 1
#define LOW  0

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

         
//extern TIM_HandleTypeDef htim1;
//extern ADC_HandleTypeDef hadc1;
extern uint8_t CAN_LED;
extern uint8_t BLINK_LED;

#endif
