#include "HX711.h"

//HX711::HX711(uint8_t gain) {
//	begin(gain);
//}

HX711::HX711() {
}

HX711::~HX711() {
}

void sclk(uint8_t set){
  //printf("set[%d]\r\n", set);
  if(set) HAL_GPIO_WritePin(pLOAD_CLK_GPIO_Port, pLOAD_CLK_Pin, GPIO_PIN_SET); 
  else HAL_GPIO_WritePin(pLOAD_CLK_GPIO_Port, pLOAD_CLK_Pin, GPIO_PIN_RESET); 
}

bool read_din(){
  if (HAL_GPIO_ReadPin(pLOAD_DIN_GPIO_Port,pLOAD_DIN_Pin)==0) return false;
  else return true;
}


uint8_t HX711::shiftIn(uint8_t bitOrder) {
        uint8_t value = 0;
        uint8_t i;

        for (i = 0; i < 8; ++i) {
                sclk(HIGH);
                if (bitOrder == LSBFIRST)
                        value |= read_din() << i;
                else
                        value |= read_din() << (7 - i);
                sclk(LOW);
        }
        return value;
}


float HX711::get_scale_filter(int32_t input)
{
  float sensitivity=0.1f;
  filterValue=((filterValue*(1-sensitivity))+(input*sensitivity));
  return filterValue;
}

void HX711::test() {
      //printf("scale test...\r\n");
      //begin();
      //printf("scale.read[%d]\r\n",read());
     // printf("read_average[%d]\r\n",read_average(3));
      printf("get_value[%d]\r\n",get_value(3));
	//set_gain(gain);
}

void HX711::begin(uint8_t gain) {
//  printf("begin gain[%d]\r\n",gain);
  set_gain(gain);
  set_offset(181332);

}

bool HX711::is_ready() {
  return read_din() == LOW;
}

void HX711::set_gain(uint8_t gain) {
  switch (gain) {
      case 128:		// channel A, gain factor 128
              GAIN = 1;
              break;
      case 64:		// channel A, gain factor 64
              GAIN = 3;
              break;
      case 32:		// channel B, gain factor 32
              GAIN = 2;
              break;
  }
  sclk(LOW);
  read();
}

long HX711::read() {
	// wait for the chip to become ready
	while (!is_ready()) {
		// Will do nothing on Arduino but prevent resets of ESP8266 (Watchdog Issue)
		//yield();
	}

	unsigned long value = 0;
	uint8_t data[3] = { 0 };
	uint8_t filler = 0x00;

	// pulse the clock pin 24 times to read the data
        
	data[2] = shiftIn(MSBFIRST);
	data[1] = shiftIn(MSBFIRST);
	data[0] = shiftIn(MSBFIRST);
   
	// set the channel and the gain factor for the next reading using the clock pin
	for (unsigned int i = 0; i < GAIN; i++) {
		sclk(HIGH);
		sclk(LOW);
	}

	// Replicate the most significant bit to pad out a 32-bit signed integer
	if (data[2] & 0x80) {
		filler = 0xFF;
	} else {
		filler = 0x00;
	}
        
	// Construct a 32-bit signed integer
	value = ( static_cast<unsigned long>(filler) << 24
			| static_cast<unsigned long>(data[2]) << 16
			| static_cast<unsigned long>(data[1]) << 8
			| static_cast<unsigned long>(data[0]) );
      //printf("value[%d]=>[%d][%d][%d]\r\n",value, data[0],data[1],data[2]);
	return static_cast<long>(value);
}

long HX711::read_average(uint8_t times) {
	long sum = 0;
	for (uint8_t i = 0; i < times; i++) {
		sum += read();
		//HAL_Delay(100);
	}
	return sum / times;
}

long HX711::get_value(uint8_t times) {
	return read_average(times) - OFFSET;
}

float HX711::get_units(uint8_t times) {
	return get_value(times) / SCALE;
}

void HX711::tare(uint8_t times) {
	double sum = read_average(times);
	set_offset(sum);
}

void HX711::set_scale(float scale) {
	SCALE = scale;
}

float HX711::get_scale() {
	return SCALE;
}

void HX711::set_offset(long offset) {
	OFFSET = offset;
}

long HX711::get_offset() {
	return OFFSET;
}

void HX711::power_down() {
	sclk(LOW);
	sclk(HIGH);
}

void HX711::power_up() {
	sclk(LOW);
}
