#include "cat4109.h"

extern HX711 *pScale;
extern TIM_HandleTypeDef htim3;
extern int32_t scale_valueAtOnLED;

CAT4109::CAT4109() {
  rgb.blink=0;
  rgb.enable=1;
  rgb.tick=0;
  rgb.r=0;
  rgb.g=0;
  rgb.b=0;
}

CAT4109::~CAT4109() {}


void CAT4109::init(void)
{
  printf("CAT4109::init\r\n");
  HAL_GPIO_WritePin(pLED_OE_GPIO_Port, pLED_OE_Pin,GPIO_PIN_SET); 
  TIM3->CCR1 = rgb.r;
  TIM3->CCR2 = rgb.g;
  TIM3->CCR3 = rgb.b;
}


void CAT4109::setLED(uint32_t color)
{
  uint8_t r,g,b,blink;
  r= (color>>24)&0xFF;
  g= (color>>16)&0xFF;
  b= (color>>8)&0xFF;
  blink= (color)&0xFF;
 // __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1,r);
 // __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2,g);
 // __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3,b);
  rgb.r=r;
  rgb.g=g;
  rgb.b=b;
  rgb.blink=blink;
  scale_valueAtOnLED = pScale->scale_value;
  onLED(rgb, 1);
}

void CAT4109::onLED(LED_RGB m_rgb, uint8_t on)
{
  if(on){
  __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1,m_rgb.r);
  __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2,m_rgb.g);
  __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3,m_rgb.b);
    
  }
  else {
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1,0);
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2,0);
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3,0); 
  }

}

LED_RGB CAT4109::get_led(){
  return rgb;
}

void CAT4109::run(void){
  //printf("run=>[%d][%d][%d]\r\n",rgb.r,rgb.g,rgb.b);
}


/*
void Drv_RGB_Blink(uint8_t on)
{
  if(led_rgb.blink && on){
    TIM3->CCR1 = 0;
    TIM3->CCR2 = 0;
    TIM3->CCR3 = 0;  
  }
  else{
    TIM3->CCR1 = led_rgb.g;
    TIM3->CCR2 = led_rgb.r;
    TIM3->CCR3 = led_rgb.b;   
  }
}

void Drv_led_tick(void)//10ms loop
{
   uint8_t i;
   
   for(i=0;i<8;i++)
   {
    if(led_mono[i].tick>0 && led_mono[i].tick<0xff) led_mono[i].tick--;
   }

}

//timeout 0:  0xff:forever
void Drv_led_set(uint8_t led,  uint8_t timeout, uint8_t blink)
{
  led=led & 0x07;//led 0~7���� 8ea
  led_mono[led].tick=timeout;
  led_mono[led].blink=blink;
  ui.ledValue = (led_mono[led].tick) ? ui.ledValue|led_seg[led] : ui.ledValue&~led_seg[led];
  writeRegister(DIGIT6, ui.ledValue);
}

void Drv_led_release(void)
{
  uint8_t i;
  uint8_t tmp;
  
  Drv_led_tick();
  if(led_blink.blink){
    tmp=ui.ledValue;
    for(i=0;i<8;i++) if(led_mono[i].blink) tmp = tmp & ~led_seg[i];
    writeRegister(DIGIT6, tmp);
    return;
  }
  
  for(i=0;i<8;i++) if(led_mono[i].tick==0) ui.ledValue = ui.ledValue & ~led_seg[i];
  writeRegister(DIGIT6, ui.ledValue);
  
}

*/

