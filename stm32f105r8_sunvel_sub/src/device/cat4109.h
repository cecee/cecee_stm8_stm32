#ifndef CAT4109_H
#define CAT4109_H
#include "extern.h"

typedef struct _LED_RGB
{
  uint8_t  blink;
  uint8_t  enable;
  uint16_t tick;
  uint8_t r;
  uint8_t g;
  uint8_t b;
}LED_RGB;

class CAT4109
{
	private:
       

	public:
        LED_RGB rgb;
        CAT4109();

        virtual ~CAT4109();

        void init();
        void run();
        void setLED(uint32_t color);
        void onLED(LED_RGB m_rgb, uint8_t on);
        LED_RGB get_led(void);
        //bool is_ready();

};

#endif 
