/**
  ******************************************************************************
  * @file    can.c
  * @brief   This file provides code for the configuration
  *          of the CAN instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"

/* USER CODE BEGIN 0 */
extern CAT4109 *pLed;  
extern HX711 *pScale;


uint32_t sub_id=0xCECEE00;
uint32_t gfilter_id;
uint8_t dip_value;
  
uint8_t can_tx[8];
uint8_t CAN_LED=0;

//CAN_TxHeaderTypeDef txHeader;
//CAN_User_InitTypeDef can_data;

uint32_t TxMailbox;

/* USER CODE END 0 */

CAN_HandleTypeDef hcan1;

/* CAN1 init function */
void MX_CAN1_Init(void)
{

  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 18;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_5TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_CAN_MspInit(CAN_HandleTypeDef* canHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspInit 0 */

  /* USER CODE END CAN1_MspInit 0 */
    /* CAN1 clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* CAN1 interrupt Init */
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
  /* USER CODE BEGIN CAN1_MspInit 1 */

  /* USER CODE END CAN1_MspInit 1 */
  }
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* canHandle)
{

  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspDeInit 0 */

  /* USER CODE END CAN1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_CAN1_CLK_DISABLE();

    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);

    /* CAN1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
  /* USER CODE BEGIN CAN1_MspDeInit 1 */

  /* USER CODE END CAN1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */

uint8_t  get_sub_id(){
  
   uint8_t dip_value=0;
   if( HAL_GPIO_ReadPin(GPIOB, pDIP0_Pin))  dip_value=dip_value|0x01; else dip_value=dip_value&~0x01;
   if( HAL_GPIO_ReadPin(GPIOB, pDIP1_Pin))  dip_value=dip_value|0x02; else dip_value=dip_value&~0x02;
   if( HAL_GPIO_ReadPin(GPIOB, pDIP2_Pin))  dip_value=dip_value|0x04; else dip_value=dip_value&~0x04;
   if( HAL_GPIO_ReadPin(GPIOB, pDIP3_Pin))  dip_value=dip_value|0x08; else dip_value=dip_value&~0x08;
   
   if( HAL_GPIO_ReadPin(GPIOB, pDIP4_Pin))  dip_value=dip_value|0x10; else dip_value=dip_value&~0x10;
   if( HAL_GPIO_ReadPin(GPIOB, pDIP5_Pin))  dip_value=dip_value|0x20; else dip_value=dip_value&~0x20;
   if( HAL_GPIO_ReadPin(GPIOB, pDIP6_Pin))  dip_value=dip_value|0x40; else dip_value=dip_value&~0x40;
   if( HAL_GPIO_ReadPin(GPIOB, pDIP7_Pin))  dip_value=dip_value|0x80; else dip_value=dip_value&~0x80;
   return dip_value;
}

uint32_t  id_change(uint32_t src) {       
  UNION_MASK src_id, dst_id;
  
  src_id.value=src;
  dst_id.b.b11=src_id.b.b00;
  dst_id.b.b12=src_id.b.b01;
  dst_id.b.b13=src_id.b.b02;
  dst_id.b.b14=src_id.b.b03;
  dst_id.b.b15=src_id.b.b04;
  dst_id.b.b16=src_id.b.b05;
  dst_id.b.b17=src_id.b.b06;
  dst_id.b.b18=src_id.b.b07;

  dst_id.b.b19=src_id.b.b08;
  dst_id.b.b20=src_id.b.b09;
  dst_id.b.b21=src_id.b.b10;
  dst_id.b.b22=src_id.b.b11;
  dst_id.b.b23=src_id.b.b12;

  dst_id.b.b27=src_id.b.b13;
  dst_id.b.b28=src_id.b.b14;       
  dst_id.b.b24=src_id.b.b15;
  dst_id.b.b25=src_id.b.b16;
  dst_id.b.b26=src_id.b.b17;   

  dst_id.b.b00=src_id.b.b18;
  dst_id.b.b01=src_id.b.b19;
  dst_id.b.b02=src_id.b.b20;
  dst_id.b.b03=src_id.b.b21;       
  dst_id.b.b04=src_id.b.b22;

  dst_id.b.b05=src_id.b.b23;
  dst_id.b.b06=src_id.b.b24;
  dst_id.b.b07=src_id.b.b25;
  dst_id.b.b08=src_id.b.b26;       
  dst_id.b.b09=src_id.b.b27; 
  dst_id.b.b10=src_id.b.b28; 

  return dst_id.value;
}

void can_start() // 수신필터를 설정하는 함수입니다
{

  dip_value=get_sub_id();
  sub_id = sub_id+ dip_value;
  
  CAN_FilterTypeDef sFilterConfig;
// MASK = 0x00000001 and ID = 0x00000001 will catch all odd IDs.
// MASK = 0x1FFFFFFE and ID = 0x00000002 will catch IDs 2 and 3.
  //gfilter_id=0x0DECEE00+dip_value;
  gfilter_id=0x0DECEE00;
  
  uint32_t filter_id=id_change(gfilter_id);
  uint32_t filter_mask=id_change(0x1FFFFFFF);
  
 // MASK = 0x1FFFFFFE and ID = 0x00000002
    
  //uint32_t STID=(filter_id>>18)&0x07FF;//11bit
  //uint32_t EXID=filter_id&0x03FFFF;//18bit
 
  uint32_t FilterIdHigh=((filter_id << 5)  | (filter_id >> (32 - 5))) & 0xFFFF;
  uint32_t FilterIdLow= (filter_id >> (11 - 3)) & 0xFFF8;
  
  uint32_t FilterMaskIdHigh=((filter_mask << 5)  | (filter_mask >> (32 - 5))) & 0xFFFF;
  uint32_t FilterMaskIdLow= (filter_mask >> (11 - 3)) & 0xFFF8;
  
   
  sFilterConfig.FilterBank = 0;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh =FilterIdHigh; // STID[10:0] & EXTID[17:13]
  sFilterConfig.FilterIdLow = FilterIdLow; // EXID[12:5] & 3 Reserved bits
  sFilterConfig.FilterMaskIdHigh =FilterMaskIdHigh;
  sFilterConfig.FilterMaskIdLow =FilterMaskIdLow;
  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.SlaveStartFilterBank = 14;

   HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig);


   if (HAL_CAN_Start(&hcan1) != HAL_OK)
  {
      Error_Handler();
  }

   //HAL_CAN_RxFifo0MsgPendingCallback();
  if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK){
    // Notification Error 
    Error_Handler();
  }
  
 // if (HAL_CAN_Receive_IT(&hcan2, CAN_FIFO0) != HAL_OK)
 //   {
     // Reception Error 
 //     Error_Handler();
//    } 
  
  printf("can sub_id[%x]\r\n",sub_id);
  
}

void put_canTxd(uint8_t *data) // CAN 송신 함수입니다
{
    uint8_t i;
    CAN_TxHeaderTypeDef TxHeader;
    uint8_t TxData[8];
    memcpy(TxData, data ,8);
    TxHeader.ExtId = sub_id;
    TxHeader.RTR = CAN_RTR_DATA;
    TxHeader.IDE = CAN_ID_EXT;
    TxHeader.DLC = 4;
    TxHeader.TransmitGlobalTime = DISABLE;
    //HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox);

    TxMailbox = HAL_CAN_GetTxMailboxesFreeLevel(&hcan1);
    if ((HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox))!=HAL_OK){
                 Error_Handler();
     }   
    while (HAL_CAN_IsTxMessagePending(&hcan1, TxMailbox));
    

}

//----------------------------------------------------

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan_t) // 수신인터럽트 함수입니다
{
    CAN_RxHeaderTypeDef rxHeader; 
    uint8_t RXData[8];
    uint32_t sv;
    memset(RXData,0,8);
    sv= pScale->scale_value;
    uint8_t myLed;
    uint32_t color_table[16]={
    //RRGGBBAA
    0x10101000,//WHITE-0
    0x00001000,//1st blue //0x00b5e200,//1st
    0x00100000,//2nd green
    0x10100000,//3nd yellow
    0x10000000,//4th red
    0x00100000,//GREEN,-5
    0x00001000,//BLUE,-6
    0x00101000,//CYAN,-7
    0x10001000,//VIOLET,-8
    0x10100000,//YELLOW,-9
    0x00808000,//TEAL,
    0x80008000,//PURPLE,
    0x80800000,//OLIVE,
    0x80000000,//MAROON,
    0x00800000,//GREEN,
    0x00008000//NAVY.
  };
  
    
  if (hcan_t->Instance == CAN1) { // OK?
   
      HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &rxHeader, RXData); // 반드시 -> 읽기를 합니다?
       if(gfilter_id==rxHeader.ExtId) {
        CAN_LED=1;
       // printf("[%08x]-dip[%02x]\n\r", gfilter_id, dip_value);
        myLed=RXData[dip_value/2];
        myLed = (dip_value%2 == 0) ? myLed&0x0F :  (myLed&0xF0)>>4;
       // printf("[%08x]-dip[%02x] LED[%02X]\n\r", gfilter_id, dip_value, myLed);
         //memcpy(rgb, &color_table[myLed],4);
         //pLed->set_led(rgb[3],rgb[2],rgb[1],rgb[0]);//test
         pLed->setLED(color_table[myLed]);
         
        //dip_value
        memcpy(can_tx,&sv,4);  
        can_tx[4]=RXData[0]; can_tx[5]=RXData[1]; can_tx[6]=RXData[2];can_tx[7]=RXData[3];
        put_canTxd(can_tx);
         
      }

   }
}


/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
