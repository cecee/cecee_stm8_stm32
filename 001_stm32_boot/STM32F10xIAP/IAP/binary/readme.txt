/******************** (C) COPYRIGHT 2007 STMicroelectronics ********************
* File Name          : readme.txt
* Author             : MCD Application Team
* Version            : V1.0
* Date               : 10/08/2007
* Description        : Description of the binary directory.
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

Description
===================
This directory contains a set of sources files and pre-configured projects that 
describes how to build an application to be loaded into Flash memory using
In-Application Programming (IAP, through USART).

To build such application, some special configuration has to be peformed: 
1. Set the program load address at 0x08002000, using your toolcahin linker file
2. Relocate the vector table at address 0x08002000, using the "NVIC_SetVectorTable"
   function.
   
   
The SysTick example provided within the STM32F10x Firmware library is used as 
illustration.    
This example configures the SysTick to generate a time base equal to 1 ms.
The system clock is set to 72 MHz, the SysTick is clocked by the AHB clock (HCLK)
divided by 8.
A "Delay" function is implemented based on the SysTick end-of-count event.
Four LEDs connected to the PC.06, PC.07, PC.08 and PC.09 pins are toggled with a
timing defined by the Delay function.


Directory contents
==================
 + template project
     - EWARM: This folder conatins a pre-configured project file that produces a
              binary image of SysTick example to be loaded with IAP.

     - RIDE: This folder conatins a pre-configured project file that produces a
             binary image of SysTick example to be loaded with IAP.

     - RVMDK: This folder conatins a pre-configured project file that produces a
              binary image of SysTick example to be loaded with IAP.              

 + stm32f10x_conf.h  Library Configuration file
 + stm32f10x_it.c    Interrupt handlers
 + stm32f10x_it.h    Header for stm32f10x_it.c
 + main.c            Main program
 + main.h            Header for main.c
                    
   
How to use it
=============
In order to load the SysTick example with the IAP, you must do the following:
 + EWARM:
    - Open the SysTick.eww workspace
    - Rebuild all files: Project->Rebuild all
    - A binary file "SysTick.bin" will be generated under "\BOOT_FLASH\Exe" folder
    - Finaly load this image with IAP application

 + RIDE
    - Open the SysTick.rprj project
    - Rebuild all files: Project->Build Project
    - Run "hextobin.bat"
    - A binary file "SysTick.bin" will be generated under "\Obj" folder
    - Finaly load this image with IAP application
      
 + RVMDK
    - Open the SysTick.Uv2 project
    - Rebuild all files: Project->Rebuild all target files
    - Run "axftobin.bat"
    - A binary file "SysTick.bin" will be generated under "\Obj" folder
    - Finaly load this image with IAP application

******************* (C) COPYRIGHT 2007 STMicroelectronics *****END OF FILE******
