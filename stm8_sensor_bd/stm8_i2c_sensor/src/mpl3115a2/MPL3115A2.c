
#include "MPL3115A2.h"

/*!
    @brief  Instantiates a new MPL3115A2 class
*/

uint8_t mpl3115a2_init() {
  int temp;
  I2c_Init ();
  uint8_t who=read8( MPL3115A2_WHOAMI);
  printf("who[%x]\r\n",who);
      // use to set sea level pressure for current location
    // this is needed for accurate altitude measurement
    // STD SLP = 1013.26 hPa
      setSeaPressure(1013.26);
   //temp=getTemperature();
   getPressure();
   //printf("who[%x] temp[%d]\r\n",who, temp);
}

/*
boolean begin(TwoWire *twoWire) {
  if (i2c_dev)
    delete i2c_dev;
  i2c_dev = new Adafruit_I2CDevice(MPL3115A2_ADDRESS, twoWire);
  if (!i2c_dev->begin())
    return false;

  // sanity check
  uint8_t whoami = read8(MPL3115A2_WHOAMI);
  if (whoami != 0xC4) {
    return false;
  }

  // software reset
  write8(MPL3115A2_CTRL_REG1, MPL3115A2_CTRL_REG1_RST);
  while (read8(MPL3115A2_CTRL_REG1) & MPL3115A2_CTRL_REG1_RST)
    delay(10);

  // set oversampling and altitude mode
  _ctrl_reg1.reg = MPL3115A2_CTRL_REG1_OS128 | MPL3115A2_CTRL_REG1_ALT;
  write8(MPL3115A2_CTRL_REG1, _ctrl_reg1.reg);

  // enable data ready events for pressure/altitude and temperature
  write8(MPL3115A2_PT_DATA_CFG, MPL3115A2_PT_DATA_CFG_TDEFE |
                                    MPL3115A2_PT_DATA_CFG_PDEFE |
                                    MPL3115A2_PT_DATA_CFG_DREM);

  return true;
}
*/

/*!
 *  @brief  Get barometric pressure
 *  @return pressure reading as a floating point value in hPa
 */
int getPressure() {
  // wait for one-shot to clear before proceeding
  while (read8(MPL3115A2_CTRL_REG1) & MPL3115A2_CTRL_REG1_OST) delay(10);

  // configure and initiate measurement
  _ctrl_reg1.bit.ALT = 0; // barometer (pressure) mode
  _ctrl_reg1.bit.OST = 1; // initatiate a one-shot measurement
  write8(MPL3115A2_CTRL_REG1, _ctrl_reg1.reg);

  // poll status to wait for conversion complete
 // while (!(read8(MPL3115A2_REGISTER_STATUS) & MPL3115A2_REGISTER_STATUS_PDR)) delay(10);

  // read data
  u32 pressure=0;
  uint16_t t=0;
  uint8_t buffer[5] = {0,};
  buffer[0]=read8( MPL3115A2_REGISTER_PRESSURE_MSB);
  buffer[1]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+1);
  buffer[2]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+2);
  buffer[3]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+3);
  buffer[4]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+4);
  //i2c_dev->write_then_read(buffer, 1, buffer, 5);
  pressure = (u32)(buffer[0]) << 16 | (u32)(buffer[1]) << 8 | (u32)(buffer[2]);
  t = buffer[3] << 8 | buffer[4];
  printf("pressure [%x][%x][%x][%x][%x] -[%x]\r\n",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4], t);
     
  float temp=(float)(t / 256.0);
  floatToStr((char *)"TMP",temp,2);
 
  float press=(float)(pressure / 6400.0);
  floatToStr((char *)"press",press,2);
  return 0;//(float)pressure/ 6400.0;
}

/*!
 *  @brief  Get altitude
 *  @return altitude reading as a floating-point value in meters
 */
float getAltitude() {
  // wait for one-shot to clear before proceeding
  while (read8(MPL3115A2_CTRL_REG1) & MPL3115A2_CTRL_REG1_OST)
    delay(10);

  // configure and initiate measurement
  _ctrl_reg1.bit.ALT = 1; // altimeter mode
  _ctrl_reg1.bit.OST = 1; // initatiate a one-shot measurement
  write8(MPL3115A2_CTRL_REG1, _ctrl_reg1.reg);

  // poll status to wait for conversion complete
  while (!(read8(MPL3115A2_REGISTER_STATUS) & MPL3115A2_REGISTER_STATUS_PDR))
    delay(10);

  // read data
  u32 alt=0;
  uint8_t buffer[5] = {MPL3115A2_REGISTER_PRESSURE_MSB, 0, 0, 0, 0};
  //i2c_dev->write_then_read(buffer, 1, buffer, 5);
  //alt = u32(buffer[0]) << 24 | u32(buffer[1]) << 16 | u32(buffer[2]) << 8;
  return (float)alt/65536.0f;
}

/*!
 *  @brief  Set the local sea level pressure
 *  @param SLP sea level pressure in hPa
 */
void setSeaPressure(float SLP) {
  // multiply by 100 to convert hPa to Pa
  // divide by 2 to convert to 2 Pa per LSB
  // convert to integer
  uint8_t error;
  uint16_t bar = SLP * 50;
  //printf("setSeaPressure\r\n");
  uint8_t buffer[3];
  buffer[0] = MPL3115A2_BAR_IN_MSB;
  buffer[1] = bar >> 8;
  buffer[2] = bar & 0xFF;
  //error=I2C_WriteBuf(MPL3115A2_ADDRESS, buffer, 3);
  
  //error=I2C_WriteRegister(MPL3115A2_ADDRESS, MPL3115A2_BAR_IN_MSB, buffer[1] );
  error=I2C_WriteBuf(MPL3115A2_ADDRESS, buffer, 3);
  //i2c_dev->write(buffer, 3);
  //printf("setSeaPressure[%x] err[%x]\r\n",MPL3115A2_ADDRESS, error);
}

/*!
 *  @brief  Get temperature
 *  @return temperature reading as a floating-point value in degC
 */
int getTemperature() {
  uint8_t r;
  uint16_t t;
  // wait for one-shot to clear before proceeding
  while (read8(MPL3115A2_CTRL_REG1) & MPL3115A2_CTRL_REG1_OST) delay(10);

  // initatiate a one-shot measurement
  _ctrl_reg1.bit.ALT = 0; // barometer (pressure) mode
  _ctrl_reg1.bit.OST = 1;
  write8(MPL3115A2_CTRL_REG1, _ctrl_reg1.reg);
  
 
  // poll status to wait for conversion complete
//  while(1){
//    r=read8(MPL3115A2_REGISTER_STATUS) ;
//    printf("(2)getTemperature  r[%x]\r\n", r);
//    if(r==MPL3115A2_REGISTER_STATUS_PTDR)break;
//    delay(10);
//  }

 
 // while (!(read8(MPL3115A2_REGISTER_STATUS) & MPL3115A2_REGISTER_STATUS_PTDR)) delay(10);
  //intf("(2)getTemperature\r\n");
  // read data
  u8 buffer[5] = {0, 0, 0, 0, 0};

  //i2c_dev->write_then_read(buffer, 1, buffer, 5);
  buffer[0]=read8( MPL3115A2_REGISTER_PRESSURE_MSB);
  buffer[1]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+1);
  buffer[2]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+2);
  buffer[3]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+3);
  buffer[4]=read8( MPL3115A2_REGISTER_PRESSURE_MSB+4);
  t = buffer[3] << 8 | buffer[4];
  printf("temp [%x][%x][%x][%x][%x] -[%x]\r\n",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4], t);
  // pressure = (u32)(buffer[0]) << 16 | (u32)(buffer[1]) << 8 | (u32)(buffer[2]);
   
  float temp=(float)(t / 256.0);
  floatToStr((char *)"TMP",temp,2);
  return (int)temp*10;
}

/*!
 *  @brief  read 1 byte of data at the specified address
 *  @param  a
 *          the address to read
 *  @return the read data byte
 */
uint8_t read8(uint8_t a) {
   uint8_t  userRegister;
   uint8_t rdata;
   rdata=ReadRegister(0xc0,a);;
  //i2c_dev->write_then_read(buffer, 1, buffer, 1);
  //return buffer[0];
  return rdata;
}

/*!
 *  @brief  write a byte of data to the specified address
 *  @param  a
 *          the address to write to
 *  @param  d
 *          the byte to write
 */
void write8(uint8_t a, uint8_t d) {
  uint8_t buffer[2] = {a, d};
  I2C_WriteBuf(MPL3115A2_ADDRESS, buffer, 2);
}
