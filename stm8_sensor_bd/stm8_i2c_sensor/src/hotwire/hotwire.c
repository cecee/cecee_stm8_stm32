#include "include.h"
#include "hotwire.h"

#define xDBG_WIND

u16 wind_filterValue[2]={0,};
float wind_sensorValue[2]={0,};
u16 WindSpeed;

int __pow(int down, int up)
{
    int round;
    int val = 1;
    for(round = 0; round < up; round++)
        val *= down;
    return val;
}

void floatToStr(char *strMsg, float f, int underPointNum)
{
  char strBuf[32];
  int pointOver, pointUnder;
    
    pointOver = (int)f;
    pointUnder = (f-pointOver)*(__pow(10, underPointNum));
    sprintf(strBuf, "%d.%d", pointOver, pointUnder);
    printf("%s:%s\r\n",strMsg, strBuf);
    return;
}

u16 getWindValue(u16 adc_tmp, u16 adc_rv)
{
  float wind_sensitivity=0.1;
#if 1
  const float zeroWindAdjustment = 0.2; // negative numbers yield smaller wind speeds and vice versa.
  float TMP_Therm_ADunits;  //temp termistor value from wind sensor
  float RV_Wind_ADunits;    //RV output from wind sensor 
  float TMP_Wind_Volts, RV_Wind_Volts;

  float zeroWind_ADunits;
  float zeroWind_volts;
  float fWindSpeed_MPH;
  float Itemp, Rtemp;
  u16 WindSpeed_MPH;
  //float Temp;
  wind_sensorValue[0]=adc_tmp;//adcRead(0);
  wind_sensorValue[1]=adc_rv;//adcRead(1);
  wind_filterValue[0]=(wind_filterValue[0]*(1-wind_sensitivity))+(wind_sensorValue[0]*wind_sensitivity);
  wind_filterValue[1]=(wind_filterValue[1]*(1-wind_sensitivity))+(wind_sensorValue[1]*wind_sensitivity);
  
  TMP_Therm_ADunits = wind_filterValue[0];//analogRead(analogPinForTMP);
  RV_Wind_ADunits   = wind_filterValue[1];//analogRead(analogPinForRV);
  
#ifdef DBG_WIND 
  printf("adc_tmp[%d] adc_rv[%d]\r\n", adc_tmp, adc_rv);
  floatToStr((char *)"TMP_Therm_ADunits",TMP_Therm_ADunits,2);
  floatToStr((char *)"RV_Wind_ADunits", RV_Wind_ADunits,2);
#endif  
  //printf("TMP_Therm_ADunits[%d] RV_Wind_ADunits[%d]\r\n", TMP_Therm_ADunits, RV_Wind_ADunits);
  
 // TMP_Wind_Volts= (TMP_Therm_ADunits * 5)/1024;
  RV_Wind_Volts = (RV_Wind_ADunits * 5)/1024;
 // floatToStr((char *)"TMP_Wind_Volts",TMP_Wind_Volts,2);
 // printf("TMP_Wind_Volts[%d] RV_Wind_Volts[%d]\r\n", TMP_Wind_Volts, RV_Wind_Volts);
  zeroWind_ADunits = -0.0006*((float)TMP_Therm_ADunits * (float)TMP_Therm_ADunits) + 1.0727 * (float)TMP_Therm_ADunits + 47.172;  //  13.0C  553  482.39
 
 // zeroWind_volts = (zeroWind_ADunits * 0.0048828125) - zeroWindAdjustment;  
  zeroWind_volts = ((zeroWind_ADunits*5)/4096) - zeroWindAdjustment;
  
#ifdef DBG_WIND   
  floatToStr((char *)"zeroWind_ADunits",zeroWind_ADunits,3);
  floatToStr((char *)"RV_Wind_Volts", RV_Wind_Volts,3);
  floatToStr((char *)"zeroWind_volts", zeroWind_volts,3);
#endif  
  
  // This from a regression from data in the form of 
  // Vraw = V0 + b * WindSpeed ^ c
  // V0 is zero wind at a particular temperature
  // The constants b and c were determined by some Excel wrangling with the solver.
  float val=(RV_Wind_Volts - zeroWind_volts)/.2300;
  fWindSpeed_MPH=(val)*(val)*(val);
  //WindSpeed_MPH =  pow(((RV_Wind_Volts - zeroWind_volts) /.2300) , 2.7265);  
 // WindSpeed_MPH =  __pow((int)((RV_Wind_Volts - zeroWind_volts) *5) , 3); 
#ifdef DBG_WIND    
  floatToStr((char *)"val", val,3);
  floatToStr((char *)"fWindSpeed_MPH", fWindSpeed_MPH,3);
#endif   
  WindSpeed_MPH=(u16)(fWindSpeed_MPH);
  printf("  WindSpeed_MPH [%d]\r\n",WindSpeed_MPH);
#endif
  return WindSpeed_MPH;
}

u16 get_adc_channel(u8 mChannel)
{
  u16 ResultADC = 0;
  u16 temph = 0;
  u8 templ = 0;

  ADC1->CSR  = 0x00;
  ADC1->CR1  = 0x00;
  ADC1->CR2  = 0x00;
  ADC1->CR3  = 0x00;
  ADC1->CR1 |= 0x40;//2mhz
  ADC1->CR2 |= 0x08;//0x08;//0000_1000 ;right, No scan,

  ADC1->CSR |= (u8)mChannel;
  ADC1->CR1 |= ADC1_CR1_ADON;//ADC on
  ADC1->CR1 |= ADC1_CR1_ADON;

  while(!(ADC1->CSR & 0x80));

  templ = ADC1->DRL;
  temph = ADC1->DRH;
  temph = (u16)(templ | (u16)(temph << 8));

  ResultADC = temph;
  ADC1->CSR &= 0x7F;//EOC Clear

 return ResultADC & 0x3ff;
}

u16 measure_wind(){
  u16 vin, vtemp;
  vtemp=get_adc_channel(3);//temp
  vin=get_adc_channel(4);//vout
  return getWindValue(vtemp, vin);
  //printf("get_adc[%d][%d]\r\n",vtemp, vin);
}

