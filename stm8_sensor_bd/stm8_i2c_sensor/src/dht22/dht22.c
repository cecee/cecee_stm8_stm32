//#include "stm32f1xx_hal.h"
#include "dht22.h"
#include "include.h"

u8 dht_pos=0;
u16 dth_tick=0;
u16 dth_buf[128];
u8 dht_err_cnt=0;

SENSOR_DHT22 dht22;

void dht22_trigger(void)
{
  dht_pos=0;
  dth_tick=0;
  //GPIO_DeInit(GPIOC);
  GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_WriteLow(GPIOC, GPIO_PIN_7);
  delay_us(300);
  GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
  delay_us(30);
  GPIO_WriteLow(GPIOC, GPIO_PIN_7);
  //GPIO_DeInit(GPIOC);
  GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_IN_PU_IT);
}

void dht22_buf_parser(void){
  u8 i;
  u8 offset=0;
  u8 th_bit;
  u32 th_data=0;
  u8 th_sum=0;
  u8 checksum=0;
  u8 data[4]; 
  u16 temp,humi;
  printf("\r\nDHT[%d]\r\n",dht_pos);
  if(dht_pos>30){
    
#if 1 
  //for(i=0;i<10;i++) printf("dth_buf[%d]-[%d][%d][%d][%d]\r\n",i, dth_buf[(i*4)+0], dth_buf[(i*4)+1], dth_buf[(i*4)+2], dth_buf[(i*4)+3]);
#endif    
    for(i=0;i<40;i++){
            if(dth_buf[offset+i]<=7) th_bit=0;//6~10
            else if( dth_buf[offset+i] > 7) th_bit=1;//11~16
            else break;
            if(i<32){
                    th_data= th_data<<1;
                    th_data= th_data+th_bit;
            }
            else {
                    th_sum=th_sum<<1;
                    th_sum= th_sum+th_bit;
            }
                    
    }
    data[0]=(th_data>>24)&0xff ;
    data[1]=(th_data>>16)&0xff ;
    data[2]=(th_data>>8)&0xff ;
    data[3]=th_data&0xff ;
    checksum=data[0]+data[1]+data[2]+data[3];
    //printf("[%x][%x][%x][%x]\r\n",th_sum, checksum, data[0], data[1], data[2], data[3]);
    //printf("th_sum[%x]  chksum[%x]\r\n",th_sum, checksum);
    
    if(checksum==th_sum)
    {
      temp=BUILD_UINT16(data[2],data[3]);//(loByte, hiByte)
      humi=BUILD_UINT16(data[0],data[1]);//(loByte, hiByte)  
      dht22.temperature=(int)temp;
      dht22.humidityRH= (int)humi;
      dht_err_cnt=0;
      //_ClearBit(sensor_bd.error, 0);
      printf("Ondo[%d][%d]\r\n",SWAPBYTE_US(dht22.temperature), SWAPBYTE_US(dht22.humidityRH));
     //printf("Ondo[%d][%d]",(sensor_bd.temperature), (sensor_bd.humidity));
    }
  }
  dht_pos=0;
  
}


void dht22_read(void)
{
  dht22_buf_parser();
  dht22_trigger();
  memset(dth_buf,0,sizeof(dth_buf));
  
}