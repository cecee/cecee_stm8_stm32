#include "include.h"
#include "i2c.h"

void I2C_SDA_SET_OUTPUT(void)
{
  GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_OUT_OD_HIZ_SLOW);
}
void I2C_SCL_SET_OUTPUT(void)
{
 // GPIO_DeInit(GPIOD);
  GPIO_Init(SHT_SCL_GPIO_Port, SHT_SCL_Pin, GPIO_MODE_OUT_OD_HIZ_SLOW);
}

void I2C_SDA_SET_INPUT(void)
{
  GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_IN_FL_NO_IT);
}


void  I2C_SDA_OUT(u8 SDA_status){
  if(SDA_status>0)  GPIO_WriteHigh(SHT_SDA_GPIO_Port, SHT_SDA_Pin);
  else  GPIO_WriteLow(SHT_SDA_GPIO_Port, SHT_SDA_Pin);
}

void  I2C_SCL_OUT(u8 SCL_status){
  if(SCL_status>0)  GPIO_WriteHigh(SHT_SCL_GPIO_Port, SHT_SCL_Pin);
  else  GPIO_WriteLow(SHT_SCL_GPIO_Port, SHT_SCL_Pin);
}

unsigned char I2C_SDA_IN(void)
{
  return (!!GPIO_ReadInputPin(SHT_SDA_GPIO_Port, SHT_SDA_Pin));
}



void  SET_MASTER_SDA(uint8_t SDA_status){
  if(SDA_status==1)  //GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_OUT_PP_HIGH_FAST); //SDA OUTPUT;
  GPIO_WriteHigh(SHT_SDA_GPIO_Port, SHT_SDA_Pin);
  else //GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_OUT_PP_LOW_FAST); //SDA OUTPUT
    GPIO_WriteLow(SHT_SDA_GPIO_Port, SHT_SDA_Pin);
 }
void  SET_MASTER_SCL(uint8_t SCL_status){
  if(SCL_status==1)  //GPIO_Init(SHT_SCL_GPIO_Port, SHT_SCL_Pin, GPIO_MODE_OUT_PP_HIGH_FAST); //SDA OUTPUT;
    GPIO_WriteHigh(SHT_SCL_GPIO_Port, SHT_SCL_Pin);
  else //GPIO_Init(SHT_SCL_GPIO_Port, SHT_SCL_Pin, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_WriteLow(SHT_SCL_GPIO_Port, SHT_SCL_Pin);
}

void RELEASE_MASTER_SDA(uint8_t input){
       // SET_MASTER_SDA(1);//SDA=HIGH;   //release SDA-line
  	if(input){
          GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_IN_FL_NO_IT); //SDA INPUT
	}
	else{
           GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_OUT_PP_HIGH_FAST); //SDA OUTPUT
	}
}
uint8_t GET_MASTER_SDA(void){
	//ret = GPIO_ReadInputPin(SHT_SDA_GPIO_Port, SHT_SDA_Pin);// SHT_SDA_GPIO_Port->IDR;
        return (!!GPIO_ReadInputPin(SHT_SDA_GPIO_Port, SHT_SDA_Pin));
	//return ret;
}


//==============================================================================
void DelayMicroSeconds (u32t nbrOfUs)
{
  for(u32t i=0; i<nbrOfUs; i++)
  {
	//for(int ix=0;ix<20;ix++)
	 // asm("nop");  //nop's may be added for timing adjustment
	//__asm("nop");
	//__asm("nop");
	//__asm("nop"); asm  ("nop");
  }
}


void I2c_Init ()
//==============================================================================
{
        I2C_SDA_SET_OUTPUT();
        SET_MASTER_SDA(LOW);//SDA=LOW;                // Set port as output for configuration
	SET_MASTER_SCL(LOW);//SCL=LOW;                // Set port as output for configuration
	SET_MASTER_SDA(HIGH);//SDA=HIGH;               // I2C-bus idle mode SDA released (input)
	SET_MASTER_SCL(HIGH);//SCL=HIGH;               // I2C-bus idle mode SCL released (input)
}

//==============================================================================
void I2c_StartCondition()
//==============================================================================
{
  I2C_SDA_SET_OUTPUT();	
  I2C_SDA_OUT(1);
  I2C_SCL_OUT(1);
  DelayMicroSeconds(1);  
  I2C_SDA_OUT(0);
  DelayMicroSeconds(10);  // hold time start condition (t_HD;STA)
  I2C_SCL_OUT(0);
  DelayMicroSeconds(10);
}

//==============================================================================
void I2c_StopCondition ()
//==============================================================================
{
  I2C_SDA_SET_OUTPUT();
  I2C_SDA_OUT(0);
  I2C_SCL_OUT(0);
  DelayMicroSeconds(10);//
  I2C_SCL_OUT(1);
  DelayMicroSeconds(10);
  I2C_SDA_OUT(1);
  DelayMicroSeconds(10);
}

uint8_t I2c_WriteByte(uint8_t txByte)
{

  //unsigned char ack=I2C_NACK;   //���?
  unsigned char time_out=200;   //ACK??�?
  u8 error=0;
  I2C_SDA_SET_OUTPUT();

  for(unsigned char i=0x80; i!=0; i>>=1)
  {
    if(txByte&i)        I2C_SDA_OUT(1);
    else                I2C_SDA_OUT(0);
    I2C_SCL_OUT(1);
    DelayMicroSeconds(10);
    I2C_SCL_OUT(0);
    DelayMicroSeconds(10);
  }
  //?�ACK??
  I2C_SDA_SET_INPUT();
  I2C_SCL_OUT(1);
  while(time_out--)
  {
    if(I2C_SDA_IN()==1) error=ACK_ERROR;
    else break;
    DelayMicroSeconds(1);
  }
  I2C_SCL_OUT(0);
  I2C_SDA_SET_OUTPUT();
  I2C_SDA_OUT(1);
  return error;
//if (GET_MASTER_SDA() != LOW) error=ACK_ERROR;
}

void I2C_SEND_ACK(void)
{
  I2C_SDA_SET_OUTPUT();
  I2C_SDA_OUT(0);
  DelayMicroSeconds(10);//
  I2C_SCL_OUT(1);
  DelayMicroSeconds(10);
  I2C_SCL_OUT(0);
  DelayMicroSeconds(10);
}

void I2C_SEND_NACK(void)
{
  I2C_SDA_SET_OUTPUT();
  I2C_SDA_OUT(1);
//  DelayMicroSeconds(10);///
  I2C_SCL_OUT(1);
  DelayMicroSeconds(10);
  I2C_SCL_OUT(0);
  DelayMicroSeconds(10);
}


uint8_t I2c_ReadByte(etI2cAck ack)
{
  uint8_t read_data=0;
  I2C_SDA_SET_INPUT();
  for(unsigned char i=0x80; i!=0; i>>=1)
  {
    I2C_SCL_OUT(1);
    DelayMicroSeconds(10);
    if(I2C_SDA_IN() == 1)
      read_data |= i;
    I2C_SCL_OUT(0);
    DelayMicroSeconds(10);

  }
  I2C_SDA_SET_OUTPUT();
  I2C_SDA_OUT(1);//?????????????????
  if(ack==ACK)      I2C_SEND_ACK();
  if(ack==NO_ACK)   I2C_SEND_NACK();
  return read_data;
}


//===========================================================================
uint8_t I2C_WriteRegister(uint8_t i2c_add_w, uint8_t reg, uint8_t value)
//===========================================================================
{
  uint8_t error=0;   //variable for error code
  I2c_StartCondition();
  error |= I2c_WriteByte (i2c_add_w);
  error |= I2c_WriteByte (reg);
  error |= I2c_WriteByte (value);
  I2c_StopCondition();
 // printf("%x err:%x\r\n",i2c_add_w, error);
  return error;
}


//===========================================================================
uint8_t I2C_WriteBuf(uint8_t i2c_add_w, uint8_t *value, uint8_t len)
//===========================================================================
{
  uint8_t error=0;   //variable for error code
  uint8_t i;
  I2c_StartCondition();
  error |= I2c_WriteByte (i2c_add_w);
  error |= I2c_WriteByte (value[0]);
  for(i=1;i<len;i++){
   error |= I2c_WriteByte (value[i]);
  }
   I2c_StopCondition();
  
  return error;
}

//===========================================================================
uint8_t ReadRegister(uint8_t i2c_adr_w, uint8_t reg)
//===========================================================================
{
  //uint8_t value;   //variable for checksum byte
  uint8_t error=0;    //variable for error code
  uint8_t rdata;
  I2c_StartCondition();
  error |= I2c_WriteByte (i2c_adr_w);
  error |= I2c_WriteByte (reg);
  I2c_StartCondition();
  error |= I2c_WriteByte (i2c_adr_w+1);
  rdata= I2c_ReadByte(NO_ACK);
  //value = I2c_ReadByte(ACK);
 // checksum=I2c_ReadByte(NO_ACK);

  //printf("error[%d] rdata[%x] checksum[%x]\r\n",error, rdata, checksum);
  //error |= SHT2x_CheckCrc (pRegisterValue,1,checksum);
  //error |= SHT2x_CheckCrc (&rdata,1,checksum);
  I2c_StopCondition();
  //printf("error[%d] rdata[%x] \r\n",error, rdata);
  return rdata;
}
