/************************************************************
*	ProjectName:	   LT8522EX
*	FileName:	       lt8522ex.c
*	BuildData:	     2014-03-27
*	Version£º        V1.00
* Company:	       Lontium
************************************************************/

#ifndef		_INCLUDE_H
#define		_INCLUDE_H

#include <stdio.h>
#include "stm8s.h"
#include "main.h"
#include "string.h"
#include "stm8s_uart1.h"
#include "stm8s_spi.h"
#include "stm8s_beep.h"
#include "stm8s_i2c.h"
#include "stm8s_tim1.h"

#include "../util/util.h"
#include "../util/delay.h"
#include "../test/test.h"
#include "../ze27/ze27.h"
#include "../dht22/dht22.h"
#include "../rs485/rs485.h"
#include "../sht20/sht20.h"
#include "../hotwire/hotwire.h"   
#include "../i2c/i2c.h"  
typedef struct 
{
  u16 bd_id;
  u16 o3_ppb;
  u16 temperature;  
  u16 humidity;
  u8 error;
}SENSOR_INFO;

typedef struct _SENSOR_UPDATA   
{
  u8 stx; // 1
  u8 size;	//1
  SENSOR_INFO sensor;
  u8 sum; //1
  u8 etx; //1
}SENSOR_UPDATA; 


extern SENSOR_INFO sensor_bd;
extern SENSOR_SHT20 sht20;
extern SENSOR_DHT22 dht22;

extern u16 RS485_FLAG;
extern u32 ms_tick;
extern u32 ms_cnt;
extern u8 ze27_req_access;
extern u16 WindSpeed;
#endif