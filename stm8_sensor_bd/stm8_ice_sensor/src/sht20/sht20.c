#include "include.h"
#include "sht20.h"
//#include "stdlib.h"

#define xDBG_STH20

SENSOR_SHT20 sht20;

const u16 POLYNOMIAL = 0x131;  //P(x)=x^8+x^5+x^4+1 = 100110001
u8  SerialNumber_SHT2x[8];  //64bit serial number

void SHT20_init()
{
	
   uint8_t error;
   uint8_t  userRegister;        //variable for user register
  
  printf("### --SHT20_init--\r\n");
  I2c_Init ();
  error=SHT2x_SoftReset();
  error |= SHT2x_GetSerialNumber(SerialNumber_SHT2x);
 #if 1        
  // --- Set Resolution e.g. RH 10bit, Temp 13bit ---
  error |= SHT2x_ReadUserRegister(&userRegister);  //get actual user reg
  userRegister = (userRegister & ~SHT2x_RES_MASK) | SHT2x_RES_10_13BIT;
  error |= SHT2x_WriteUserRegister(&userRegister); //write changed user reg
 #endif  
  
#ifdef DBG_STH20 
  printf("### error[%d] userRegister[%d]\r\n",error, userRegister);
#endif
}

SENSOR_SHT20 Get_SHT20(void){
    SENSOR_SHT20 sht20;
    uint8_t error=0;
     float   temperatureC=0;     //variable for temperature[�C] as float
     float   humidityRH=0;       //variable for relative humidity[%RH] as float
//	 char humitityOutStr[21];     //output string for humidity value
     nt16 sT;                     //variable for raw temperature ticks
     nt16 sRH;                    //variable for raw humidity ticks

    error |= SHT2x_MeasurePoll(TEMP, &sT);
    error |= SHT2x_MeasurePoll(HUMIDITY, &sRH);
    //-- calculate humidity and temperature --
    if(error==0){
    temperatureC = SHT2x_CalcTemperatureC(sT._u16);
    humidityRH   = SHT2x_CalcRH(sRH._u16);
    sht20.temperature=(int)(temperatureC*10);
    sht20.humidityRH=(int)(humidityRH*10);
    sht20.exist=1;
    }
    else{
        sht20.temperature=-99;
        sht20.humidityRH=-99;
        sht20.exist=0;
    }
#ifdef DBG_STH20
    printf("SHT20_init error[%d][%d][%d]\r\n",error, (int)temperatureC, (int)humidityRH);
#endif
    return sht20;
}

//===========================================================================
uint8_t SHT2x_SoftReset()
//===========================================================================
{
  uint8_t  error=0;           //error variable
  I2c_StartCondition();
  error |= I2c_WriteByte(I2C_ADR_W); // I2C Adr
  error |= I2c_WriteByte(SOFT_RESET);// Command
  I2c_StopCondition();
  DelayMicroSeconds(15000); // wait till sensor has restarted
  return error;
}


//==============================================================================
uint8_t SHT2x_CheckCrc(uint8_t data[], uint8_t nbrOfBytes, uint8_t checksum)
//==============================================================================
{
  uint8_t crc = 0;
  uint8_t byteCtr;
  //calculates 8-Bit checksum with given polynomial
  for (byteCtr = 0; byteCtr < nbrOfBytes; ++byteCtr)
  { crc ^= (data[byteCtr]);
    for (uint8_t bit = 8; bit > 0; --bit)
    { if (crc & 0x80) crc = (crc << 1) ^ POLYNOMIAL;
      else crc = (crc << 1);
    }
  }
  if (crc != checksum) return CHECKSUM_ERROR;
  else return 0;
}

//===========================================================================
uint8_t SHT2x_ReadUserRegister(uint8_t *pRegisterValue)
//===========================================================================
{
  uint8_t checksum;   //variable for checksum byte
  uint8_t error=0;    //variable for error code
//  uint8_t rdata;
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);
  error |= I2c_WriteByte (USER_REG_R);
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);
 // rdata= I2c_ReadByte(ACK);
  *pRegisterValue = I2c_ReadByte(ACK);
  checksum=I2c_ReadByte(NO_ACK);

  //printf("error[%d] rdata[%x] checksum[%x]\r\n",error, rdata, checksum);
  error |= SHT2x_CheckCrc (pRegisterValue,1,checksum);
  //error |= SHT2x_CheckCrc (&rdata,1,checksum);
  I2c_StopCondition();
  return error;
}



//===========================================================================
uint8_t SHT2x_WriteUserRegister(uint8_t *pRegisterValue)
//===========================================================================
{
	uint8_t error=0;   //variable for error code

  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);
  error |= I2c_WriteByte (USER_REG_W);
  error |= I2c_WriteByte (*pRegisterValue);
  I2c_StopCondition();
  return error;
}

//===========================================================================
uint8_t SHT2x_MeasureHM(etSHT2xMeasureType eSHT2xMeasureType, nt16 *pMeasurand)
//===========================================================================
{
	uint8_t  checksum;   //checksum
	uint8_t  data[2];    //data array for checksum verification
	uint8_t  error=0;    //error variable
	uint16_t i;          //counting variable

  //-- write I2C sensor address and command --
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W); // I2C Adr
  switch(eSHT2xMeasureType)
  { case HUMIDITY: error |= I2c_WriteByte (TRIG_RH_MEASUREMENT_HM); break;
    case TEMP    : error |= I2c_WriteByte (TRIG_T_MEASUREMENT_HM);  break;
    //default: assert(0);
  }
  //-- wait until hold master is released --
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);
  SET_MASTER_SCL(HIGH);//SCL=HIGH;                     // set SCL I/O port as input
  for(i=0; i<1000; i++)         // wait until master hold is released or
  { DelayMicroSeconds(1000);    // a timeout (~1s) is reached
    //if (SCL_CONF==1) break;
    if (GET_MASTER_SDA() != LOW) break;
  }
  //-- check for timeout --
  //if(SCL_CONF==0) error |= TIME_OUT_ERROR;
  if (GET_MASTER_SDA() == LOW) error=ACK_ERROR;
  //-- read two data bytes and one checksum byte --
  //pMeasurand->s16.u8H =
		  data[0] = I2c_ReadByte(ACK);
 // pMeasurand->s16.u8L =
		  data[1] = I2c_ReadByte(ACK);
  checksum=I2c_ReadByte(NO_ACK);


  //-- verify checksum --
  error |= SHT2x_CheckCrc (data,2,checksum);
  I2c_StopCondition();
  return error;
}

//===========================================================================
uint8_t SHT2x_MeasurePoll(etSHT2xMeasureType eSHT2xMeasureType, nt16 *pMeasurand)
//===========================================================================
{
	uint8_t  checksum;   //checksum
	uint8_t  data[2];    //data array for checksum verification
	uint8_t  error=0;    //error variable
	uint16_t i=0;        //counting variable

  //-- write I2C sensor address and command --
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W); // I2C Adr
  switch(eSHT2xMeasureType)
  { case HUMIDITY: error |= I2c_WriteByte (TRIG_RH_MEASUREMENT_POLL); break;
    case TEMP    : error |= I2c_WriteByte (TRIG_T_MEASUREMENT_POLL);  break;
    //default: assert(0);
  }
  //-- poll every 10ms for measurement ready. Timeout after 20 retries (200ms)--
  do
  { I2c_StartCondition();
    DelayMicroSeconds(10000);  //delay 10ms
    //HAL_Delay(10);
    if(i++ >= 20) break;
  } while(I2c_WriteByte (I2C_ADR_R) == ACK_ERROR);
  if (i>=20) error |= TIME_OUT_ERROR;

   //-- read two data bytes and one checksum byte --
  pMeasurand->s16.u8H = data[0] = I2c_ReadByte(ACK);
  pMeasurand->s16.u8L = data[1] = I2c_ReadByte(ACK);
  checksum=I2c_ReadByte(NO_ACK);
   //-- verify checksum --
  error |= SHT2x_CheckCrc (data,2,checksum);
  I2c_StopCondition();
  //printf("---- SHT2x_MeasurePoll error[%d] data[%x][%x]  checksum[%x]\r\n",error, data[0], data[1], checksum);
  return error;
}


//==============================================================================
float SHT2x_CalcRH(uint16_t u16sRH)
//==============================================================================
{
	float humidityRH;              // variable for result

  u16sRH &= ~0x0003;          // clear bits [1..0] (status bits)
  //-- calculate relative humidity [%RH] --

  humidityRH = -6.0 + 125.0/65536 * (float)u16sRH; // RH= -6 + 125 * SRH/2^16
  return humidityRH;
}

//==============================================================================
float SHT2x_CalcTemperatureC(uint16_t u16sT)
//==============================================================================
{
	float temperatureC;            // variable for result

  u16sT &= ~0x0003;           // clear bits [1..0] (status bits)

  //-- calculate temperature [�C] --
  temperatureC= -46.85 + 175.72/65536 *(float)u16sT; //T= -46.85 + 175.72 * ST/2^16
  return temperatureC;
}

//==============================================================================
uint8_t SHT2x_GetSerialNumber(uint8_t u8SerialNumber[])
//==============================================================================
{
	uint8_t  error=0;                          //error variable

  //Read from memory location 1
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);    //I2C address
  error |= I2c_WriteByte (0xFA);         //Command for readout on-chip memory

  error |= I2c_WriteByte (0x0F);         //on-chip memory address
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);    //I2C address

  u8SerialNumber[5] = I2c_ReadByte(ACK); //Read SNB_3
  I2c_ReadByte(ACK);                     //Read CRC SNB_3 (CRC is not analyzed)
  u8SerialNumber[4] = I2c_ReadByte(ACK); //Read SNB_2
  I2c_ReadByte(ACK);                     //Read CRC SNB_2 (CRC is not analyzed)
  u8SerialNumber[3] = I2c_ReadByte(ACK); //Read SNB_1
  I2c_ReadByte(ACK);                     //Read CRC SNB_1 (CRC is not analyzed)
  u8SerialNumber[2] = I2c_ReadByte(ACK); //Read SNB_0
  I2c_ReadByte(NO_ACK);                  //Read CRC SNB_0 (CRC is not analyzed)
  I2c_StopCondition();

  //Read from memory location 2
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);    //I2C address
  error |= I2c_WriteByte (0xFC);         //Command for readout on-chip memory
  error |= I2c_WriteByte (0xC9);         //on-chip memory address
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);    //I2C address
  u8SerialNumber[1] = I2c_ReadByte(ACK); //Read SNC_1
  u8SerialNumber[0] = I2c_ReadByte(ACK); //Read SNC_0
  I2c_ReadByte(ACK);                     //Read CRC SNC0/1 (CRC is not analyzed)
  u8SerialNumber[7] = I2c_ReadByte(ACK); //Read SNA_1
  u8SerialNumber[6] = I2c_ReadByte(ACK); //Read SNA_0
  I2c_ReadByte(NO_ACK);                  //Read CRC SNA0/1 (CRC is not analyzed)
  I2c_StopCondition();
  //printf("###SHT2x_GetSerialNumber error[%d]  u8SerialNumber[%x]\r\n", error, u8SerialNumber[0]);
  return error;
}

