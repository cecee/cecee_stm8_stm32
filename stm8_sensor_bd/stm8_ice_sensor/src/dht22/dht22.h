/*
dht22.h
*/
#ifndef _DHT22_H_
#define _DHT22_H_

typedef struct
{
  unsigned char exist;
  int temperature;
  int humidityRH;
}SENSOR_DHT22;

void dht22_trigger(void);
void dht22_read(void);
void dht22_buf_parser(void);
#endif