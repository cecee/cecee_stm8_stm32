#ifndef _I2C_H
#define _I2C_H	
void I2C_SDA_SET_OUTPUT(void);
  void I2C_SCL_SET_OUTPUT(void);
  void I2C_SDA_OUT(u8 SDA_status);
  void I2C_SCL_OUT(u8 SCL_status);
  unsigned char I2C_SDA_IN(void);
  
  void RELEASE_MASTER_SDA(uint8_t input);
  void SET_MASTER_SDA(uint8_t SDA_status);
  void SET_MASTER_SCL(uint8_t SCL_status);
  uint8_t GET_MASTER_SDA(void);
  void I2c_Init ();
  void DelayMicroSeconds (u32t nbrOfUs);
  void I2c_StartCondition();
  void I2c_StopCondition();
  uint8_t I2c_WriteByte (uint8_t txByte);
  uint8_t I2c_ReadByte (etI2cAck ack);
  void I2C_SEND_ACK(void);
  void I2C_SEND_NACK(void);
  //uint8_t _I2C_WriteRegister(uint8_t i2c_add_w, uint8_t reg, uint8_t value);
  uint8_t I2C_WriteRegister(uint8_t i2c_add_w, uint8_t reg, uint8_t value);
  uint8_t I2C_WriteBuf(uint8_t i2c_add_w, uint8_t *value, uint8_t len);
  uint8_t ReadRegister(uint8_t i2c_adr_w, uint8_t reg);
#endif