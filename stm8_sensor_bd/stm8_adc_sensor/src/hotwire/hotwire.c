#include "include.h"
#include "hotwire.h"
#include "math.h"

#define xDBG_WIND

u16 wind_filterValue[2]={0,};
float wind_sensorValue[2]={0,};

int __pow(int down, int up)
{
    int round;
    int val = 1;
    for(round = 0; round < up; round++)
        val *= down;
    return val;
}

void floatToStr(char *strMsg, float f, int underPointNum)
{
  char strBuf[32];
  int pointOver, pointUnder;
    
    pointOver = (int)f;
    pointUnder = (f-pointOver)*(__pow(10, underPointNum));
    sprintf(strBuf, "%d.%d", pointOver, pointUnder);
    printf("%s:%s\r\n",strMsg, strBuf);
    return;
}

void getWindValue(u16 adc_tmp, u16 adc_rv)
{
  float wind_sensitivity=0.1;
  //const float zeroWindAdjustment = 0.2; // negative numbers yield smaller wind speeds and vice versa.
  int adc_tmp_filter;  //temp termistor value from wind sensor
  int adc_rv_filter;    //RV output from wind sensor 
  //float TMP_Wind_Volts, RV_Wind_Volts;

  //float zeroWind_ADunits;
  //float zeroWind_volts;
  //float fWindSpeed_MPH;
  //float Itemp, Rtemp;
  
  //u16 WindSpeed_MPH;
  //float Temp;
  wind_sensorValue[0]=adc_tmp;//adcRead(0);
  wind_sensorValue[1]=adc_rv;//adcRead(1);
  wind_filterValue[0]=(int)((wind_filterValue[0]*(1-wind_sensitivity))+(wind_sensorValue[0]*wind_sensitivity));
  wind_filterValue[1]=(int)((wind_filterValue[1]*(1-wind_sensitivity))+(wind_sensorValue[1]*wind_sensitivity));
  
  adc_tmp_filter = wind_filterValue[0];//analogRead(analogPinForTMP);
  adc_rv_filter  = wind_filterValue[1];//analogRead(analogPinForRV);
  int t1=get_temperature((u16)adc_tmp_filter) ;
  int t2=get_temperature((u16)adc_rv_filter) ;
#ifdef DBG_WIND 
  printf("adc_tmp[%d] adc_rv[%d] t1[%d] t2[%d]\r\n", adc_tmp, adc_rv, t1, t2);
  //floatToStr((char *)"adc_tmp_filter",adc_tmp_filter,2);
  //floatToStr((char *)"adc_rv_filter", adc_rv_filter,2);
#endif  
  //printf("TMP_Therm_ADunits[%d] RV_Wind_ADunits[%d]\r\n", TMP_Therm_ADunits, RV_Wind_ADunits);
  
 // TMP_Wind_Volts= (TMP_Therm_ADunits * 5)/1024;
 // RV_Wind_Volts = (RV_Wind_ADunits * 5)/1024;
 // floatToStr((char *)"TMP_Wind_Volts",TMP_Wind_Volts,2);
 // printf("TMP_Wind_Volts[%d] RV_Wind_Volts[%d]\r\n", TMP_Wind_Volts, RV_Wind_Volts);
 // zeroWind_ADunits = -0.0006*((float)TMP_Therm_ADunits * (float)TMP_Therm_ADunits) + 1.0727 * (float)TMP_Therm_ADunits + 47.172;  //  13.0C  553  482.39
 
 // zeroWind_volts = (zeroWind_ADunits * 0.0048828125) - zeroWindAdjustment;  
 // zeroWind_volts = ((zeroWind_ADunits*5)/4096) - zeroWindAdjustment;
  
#ifdef DBG_WIND   
//  floatToStr((char *)"zeroWind_ADunits",zeroWind_ADunits,3);
//  floatToStr((char *)"RV_Wind_Volts", RV_Wind_Volts,3);
//  floatToStr((char *)"zeroWind_volts", zeroWind_volts,3);
#endif  
  
  // This from a regression from data in the form of 
  // Vraw = V0 + b * WindSpeed ^ c
  // V0 is zero wind at a particular temperature
  // The constants b and c were determined by some Excel wrangling with the solver.
  //float val=(RV_Wind_Volts - zeroWind_volts)/.2300;
  //fWindSpeed_MPH=(val)*(val)*(val);
  //WindSpeed_MPH =  pow(((RV_Wind_Volts - zeroWind_volts) /.2300) , 2.7265);  
 // WindSpeed_MPH =  __pow((int)((RV_Wind_Volts - zeroWind_volts) *5) , 3); 
   // WindSpeed_MPH=(int)(fWindSpeed_MPH);
    sensor_bd.sensor0=SWAPBYTE_US(t1);
    sensor_bd.sensor1=SWAPBYTE_US(t2);
#ifdef DBG_WIND    
  //floatToStr((char *)"val", val,3);
  //floatToStr((char *)"fWindSpeed_MPH", fWindSpeed_MPH,3);
  //printf("  WindSpeed_MPH [%d]\r\n",WindSpeed_MPH);
#endif   
  
}

u16 get_adc_channel(u8 mChannel)
{
  u16 ResultADC = 0;
  u16 temph = 0;
  u8 templ = 0;

  ADC1->CSR  = 0x00;
  ADC1->CR1  = 0x00;
  ADC1->CR2  = 0x00;
  ADC1->CR3  = 0x00;
  ADC1->CR1 |= 0x40;//2mhz
  ADC1->CR2 |= 0x08;//0x08;//0000_1000 ;right, No scan,

  ADC1->CSR |= (u8)mChannel;
  ADC1->CR1 |= ADC1_CR1_ADON;//ADC on
  ADC1->CR1 |= ADC1_CR1_ADON;

  while(!(ADC1->CSR & 0x80));

  templ = ADC1->DRL;
  temph = ADC1->DRH;
  temph = (u16)(templ | (u16)(temph << 8));

  ResultADC = temph;
  ADC1->CSR &= 0x7F;//EOC Clear

 return ResultADC & 0x3ff;
}

void get_adc(){
  u16 vin, vtemp;
  vtemp=get_adc_channel(3);//temp
  vin=get_adc_channel(4);//vout
  getWindValue(vtemp, vin);
  //printf("get_adc[%d][%d]\r\n",vtemp, vin);
}

int get_temperature(u16 adc_value)
{
  u16 adc_volt;
  float fV,fR ,tempC;
  float ParmB=3950.0f;
  long ohm;
 //adc_temp=100000;//x100times
  //fV=(float)((adc_temp * 5.0f)/1024.0f)/100.0f;
#ifdef DBG_WIND  
  printf("\r\n----\r\nadc_value[%d]\r\n", adc_value);
#endif
  fV=(float)(adc_value /2.08f);
  //floatToStr((char *)"fV",fV,3);
  //Rt=((Vs*R)/Vo)-R; Vs:5V R:50000 Vo: fV
  //fR=((497*51)/fV)-51.0f;//kohm
  //floatToStr((char *)"fR",fR,3);
  fR=(51.0f*(497.0f-fV))/fV;
  ohm=(long)(fR*1000);//floatx1000(fR);
  //adc_volt=(u16)(fV);
  //ohm=74000;
#ifdef DBG_WIND  
  _printf("ohm[%d]\r\n", ohm);
#endif
  //printf("adc_volt[%d]\r\n", adc_volt);

  //tempC =(ParmB/(log(ohm/112197.33f)+(ParmB/(273.15f+25.0f)))) -273.15f;
  tempC =(ParmB/(log(ohm/50000.0f)+(ParmB/(273.15f+25.0f)))) -273.15f;
  //floatToStr((char *)"fV",fV,3);
  //floatToStr((char *)"fI",fI,4);
  //floatToStr((char *)"fR",fR,2);
#ifdef DBG_WIND  
  floatToStr((char *)"tempC",tempC,2);
  printf("tempC[%d]\r\n", (int)tempC);
#endif  
  //gCUR.current_temp=(int)(tempC*10);
  //printf("ohm[%d] current_temp[%d]\r\n", ohm, gCUR.current_temp);
  return (int)(tempC*10);
}

