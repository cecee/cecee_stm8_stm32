#include "include.h"
#include "ze27.h"
#include "stdlib.h"

u8 kQuestionModePayload[9] = {0xFF, 0x01, 0x78, 0x41, 0x00, 0x00, 0x00, 0x00, 0x46}; // message which is send to sensor to set it to question/answer mode
u8 kActiveModePayload[9] = {0xFF, 0x01, 0x78, 0x40, 0x00, 0x00, 0x00, 0x00, 0x47};   // message which is send to sensor to set it to active upload mode
u8 kQuestionPayload[9] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};     // message which is send to sensor to request the reading in QA mode
u8 ze27_req_access=0;
void ZE27_Init(void){
  ZE_Req(kQuestionModePayload, 9);
  delay_us(1000);
}

void ZE_Req(u8 *data, u8 len){
  u8 i;
  ze27_req_access++;
  if(ze27_req_access>10) ze27_req_access=100;
#if 0  
  printf("ZE_writer_command\r\n");  
  for(i=0;i<len;i++)printf("[%d][%x]\r\n", i, data[i]);
#endif 
  for(i=0;i<len;i++){
    CUartTxChar(data[i]);
  }

}
void ZE_Read(){
  ZE_Req(kQuestionPayload, 9);
}

