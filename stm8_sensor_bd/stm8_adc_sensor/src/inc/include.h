/************************************************************
*	ProjectName:	   LT8522EX
*	FileName:	       lt8522ex.c
*	BuildData:	     2014-03-27
*	Version£º        V1.00
* Company:	       Lontium
************************************************************/

#ifndef		_INCLUDE_H
#define		_INCLUDE_H

#include <stdio.h>
#include "stm8s.h"
#include "main.h"
#include "string.h"
#include "stm8s_uart1.h"
#include "stm8s_spi.h"
#include "stm8s_beep.h"
#include "stm8s_i2c.h"

#include "../util/util.h"
#include "../util/delay.h"
#include "../test/test.h"
#include "../rs485/rs485.h"
#include "../hotwire/hotwire.h"   

typedef struct 
{
  u16 bd_id;
  u16 sensor0;
  u16 sensor1;  
  u16 sensor2;
  u8 error;
}SENSOR_INFO;

typedef struct _SENSOR_UPDATA   
{
  u8 stx; // 1
  u8 size;	//1
  SENSOR_INFO sensor;
  u8 sum; //1
  u8 etx; //1
}SENSOR_UPDATA; 


extern SENSOR_INFO sensor_bd;
extern u16 RS485_FLAG;
extern u32 ms_tick;
extern u32 ms_cnt;
//extern u8 ze27_req_access;

#endif