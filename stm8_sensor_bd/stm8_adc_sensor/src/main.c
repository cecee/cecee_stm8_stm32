#include "include.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BD_WIND

//extern u8 dht_pos;
//extern u16 dth_tick;
//extern u16 dth_buf[64];

SENSOR_INFO sensor_bd;

u32 ms_tick=0;
u32 ms_cnt=0;
u16 adc_val;
//u16 dh_tick=0;

u16 Beep_Time=0;
u16 sec_cnt=0;
u8 Beep_flag=0;
u8 beep_status=0;
u8 SET_BEEP=1;
//u8 SEQ_CNT=0;
//u8 SEQ_ZE27=0;

int fputc(int ch, FILE *f)
{
	u8 temp[1]={ch};
        CUartTxChar(temp[0]); //udelay(1);
	return(ch);
}

void BEEP_ON()
{
  Beep_Time=0; 
  Beep_flag=1;///
}


void CLK_Configuration(void)
{
  // Fmaster = 16MHz 
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  //CLK_PeripheralClockConfig(CLK_PERIPHERAL_I2C, ENABLE);
}



void GPIO_Initial(void)
{
  GPIO_DeInit(GPIOA);
  GPIO_Init(GPIOA,GPIO_PIN_1,GPIO_MODE_OUT_PP_LOW_FAST);

  GPIO_DeInit(GPIOB);
  //GPIO_Init(SHT_SCL_GPIO_Port, SHT_SCL_Pin, GPIO_MODE_OUT_PP_HIGH_FAST); //SCL
  //GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_OUT_PP_HIGH_FAST); //SDA
  GPIO_Init(GPIOB, GPIO_PIN_4, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_FAST);
  
  GPIO_DeInit(GPIOC);
  GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_FAST);
        
  GPIO_DeInit(GPIOD);
  GPIO_Init(SHT_SCL_GPIO_Port, SHT_SCL_Pin, GPIO_MODE_IN_FL_NO_IT); //ADC
  GPIO_Init(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_MODE_IN_FL_NO_IT); //ADC
}


void Timer_Init(void)
{

  //1/2/4/8/16/32/64/128
  TIM4->SR1 = 0;       // clear overflow flag
  TIM4->PSCR = 1;     // Prescaler to divide Fcpu by 64: 4 us clock.// Max CPU freq = 16 MHz
  TIM4->ARR = 100;    // 25*4 = 100 us. 10KHZ
  TIM4->IER = 0x01;   // Enable interrupt
  TIM4->CR1 = 0x01;   // Start timer
  
  TIM1->PSCRH = 0x00;  // 8M  f=fck/(PSCR+1)
  TIM1->PSCRL = 0x07; // PSCR=0x1F3F，f=8M/(0x1F3F+1)=1000Hz，Period : 1ms
  TIM1->ARRH = 0x4E;  // 0x4e20=20000us=20ms
  TIM1->ARRL = 0x20;  // 
  TIM1->IER = 0x01;   // Enable interrupt
  TIM1->CR1 = 0x01;   // Start timer
}


u16 get_board_id(void)
{
  u16 id;
  u8 dip=0;
  id=0xB000;
  //ready RS485 RX
  GPIO_WriteLow(GPIOA, GPIO_PIN_1);//TXEN LOW
  //if(GPIO_ReadInputPin(GPIOB,GPIO_PIN_5)!=0) _SetBit(dip, 0); 
  //if(GPIO_ReadInputPin(GPIOB,GPIO_PIN_4)!=0) _SetBit(dip, 1);
  if(GPIO_ReadInputPin(GPIOC,GPIO_PIN_3)!=0) _SetBit(dip, 0);  
  if(GPIO_ReadInputPin(GPIOC,GPIO_PIN_4)!=0) _SetBit(dip, 1);
  if(GPIO_ReadInputPin(GPIOC,GPIO_PIN_5)!=0) _SetBit(dip, 2);  
  if(GPIO_ReadInputPin(GPIOC,GPIO_PIN_6)!=0) _SetBit(dip, 3);
  id=id+dip;  
  return id;
}


main()
{
  CLK_Configuration();
  GPIO_Initial();
  //I2C_setup();
  UART1_DeInit(); // Initialize serial
  //UART1_Init(115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_Init(9600, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_ITConfig(UART1_IT_RXNE,ENABLE); //test

  init_qUart1();
  sensor_bd.bd_id = get_board_id();
  
#ifndef BD_WIND
  ZE27_Init();   
#endif
  
  Timer_Init();  
  BEEP_DeInit();
  BEEP_Init(BEEP_FREQUENCY_1KHZ);//1KHZ
  BEEP_Cmd(DISABLE);
  
  EXTI_DeInit();
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOC, EXTI_SENSITIVITY_FALL_ONLY);

#ifndef BD_WIND
  SHT20_init();
#endif
  enableInterrupts(); 
  printf("sensor_bd.bd_id [%x]\r\n", sensor_bd.bd_id );
  while (1)
  {
    processSerialBuf();
  }
}


#pragma vector=7
__interrupt void EXTI_PORTC_IRQHandler(void)
{
   //printf("EXTI_PORTC_IRQHandler\n\r");
   //dth_buf[dht_pos++]=dth_tick;
   //dth_tick=0;
}


#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  u8 data;
  data = UART1_ReceiveData8();
  put_qUart1(data);
}


#pragma vector = 25
__interrupt void _100us_ISR(void)
{
  static u32 t4=0;
  TIM4->SR1 = 0;      // clear overflow flag
  //dth_tick++;
  t4++;
 // GPIO_WriteReverse(GPIOC, GPIO_PIN_6);
  if(t4>100){
    t4=0;
    ms_tick++;
    ms_cnt++;
  }
}
/////////////////////////
//10ms Timer
/////////////////////////
#pragma vector=0xD
__interrupt void _10ms_ISR(void)
{
  static u32 t1=0;
  static u32 wind_tick=0;
  Beep_Time++;
  t1++;  
  //wind_tick++;
  TIM1->SR1 = 0;
   if(sensor_bd.bd_id==0xB003){//HotWire
     wind_tick++;
     if(wind_tick>10){
        wind_tick=0;
        get_adc();
     }
  }
  #ifndef BD_WIND
  if(t1>200){
 // printf("timer\r\n");
    t1=0;
   // SEQ_CNT=SEQ_CNT%3;
    if(sensor_bd.bd_id==0xB000){
     // dht22_read();
      ZE_Read();//오존센서에게 요청하면 비동기적으로 값을 Serial로 보내온다. 여기는 요청만한다.
    }
    else if(sensor_bd.bd_id==0xB001) {
      sht20=Get_SHT20();
      //printf("sht20[%d][%d]\r\n", sht20.temperature, sht20.humidityRH);
    }
    
#if 1    
    if(sensor_bd.error) { 
      if(Beep_flag==0){
          BEEP_Cmd(ENABLE);
          Beep_flag=1;
          Beep_Time=0;
      }
    }
#endif
  } 
#endif
  
  if(Beep_flag && Beep_Time>5){
    Beep_flag=0;
    BEEP_Cmd(DISABLE);
  }
  
 

}
