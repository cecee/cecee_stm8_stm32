
#ifndef _RS485_H
#define _RS485_H		

#define MAX_SIZE 128
typedef struct _queue   
{
  int queue[MAX_SIZE+1];
  int front;
  int rear;
}QUEUE;

//BOARD ID
typedef enum{
  BD_ZE27_AND_DHT20  = 0xB000,
  BD_SHT20           = 0xB001,
  BD_DTH20           = 0xB002,
  BD_WIND            = 0xB003,
  BD_ZE27            = 0xB004
}bdID;

void init_qUart1(void);
int put_qUart1(int k);
int get_qUart1(void);
void processSerialBuf(void);
void process_request(u8 *data);
void RS485_transmit(bdID id);
				  		 
#endif  
	 
	 



