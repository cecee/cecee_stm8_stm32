#include "include.h"
#include "rs485.h"
#include "stdlib.h"

#define xDBG_RS485

u16 RS485_FLAG=0;


//extern UART_HandleTypeDef huart1;
SENSOR_UPDATA o3sensor;

//SUB_BOARD_ACCESS sub_bd_access; 
   
QUEUE qUart1;

u8 qUart1_package=0;
u8 qUart1_buffer[16];
u8 qUart1_cnt=0;
u8 qUart1_sz=0xff;

u8 idx=0;
u8 rxSz=0;
u8 package_flag=0;
u8 UART_FLAG = 0;
u8 RS485_REQ;
u8 RS485_data_size;

int measured_o3=0;
/////////////////////////////////////////////
///Queue
////////////////////////////////////////////

void init_qUart1(void){
  qUart1.front=0;
  qUart1.rear=0;
//  memset(&sub_bd_access,0,sizeof(sub_bd_access));
  //HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_RESET);
}

int put_qUart1(int k){
    // 큐가 꽉차있는지 확인
    if ((qUart1.rear + 1) % MAX_SIZE == qUart1.front){
        printf("\r\n Queue overflow..rear[%d] front[%d]\r\n",qUart1.rear, qUart1.front);
        return -1;
    }
    qUart1.queue[ qUart1.rear] = k;
    qUart1.rear = (qUart1.rear+1) % MAX_SIZE;
    return k;
}

int get_qUart1(void)
{
    int i=0;
     if (qUart1.front == qUart1.rear){
        return -1;
    }
    i = qUart1.queue[qUart1.front];
   qUart1.front = (qUart1.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}




void process_ze27(u8 *data){
    u8 i, sum;
     //printf("process_ze27!! [%x][%x]\r\n",data[0], data[1]);
    sum=0;
    for(i=0;i<8;i++) sum=sum+data[i];
    sum = (~sum)&0xFF;
    if(data[8]==sum){
      measured_o3=BUILD_UINT16(data[2],data[3] );
    }
}

void processSerialBuf(void)
{
//    u8 i;
    int d;    
	for(;;)
	{
          d =get_qUart1();
          if(d==-1 )break;
          d=d&0xff;
         // printf("[%d][%x]\r\n",qUart1_package, d);
          if((d==0xFF || d==0xFA || d==0xFB) && qUart1_package==0){  
             qUart1_package=1;
             qUart1_cnt=0;
             qUart1_sz=0xff;
             memset(qUart1_buffer,0,sizeof(qUart1_buffer));
          }
          if(qUart1_package){
            qUart1_buffer[qUart1_cnt++]=d;
            if(d==0x40){
              qUart1_package=0;
              if(qUart1_buffer[0]==0xFA){
                //printf("qUart1_cnt[%d] [%x]\r\n",qUart1_cnt, d);
                process_request(qUart1_buffer);
              }
             }
            else if(qUart1_buffer[0]==0xFF && qUart1_cnt==9)//ZE27               
            {
                //비동기적으로 들어오는 오존센서(ZE27)로 부터 들어오는 값 처리 프로세서
                qUart1_package=0;
                process_ze27(qUart1_buffer);
            }  
            else if(qUart1_cnt>14){
              qUart1_package=0;
            }
        }
    }

}

void process_request(u8 *data){
    bdID my_id, req_id;
    my_id=(bdID)sensor_bd.bd_id;
    req_id =(bdID) BUILD_UINT16(data[2], data[3]);
    
#ifdef DBG_RS485
    printf("my_id[%x] req_id[%x]\r\n",my_id, req_id);
#endif 
    
    if(my_id==req_id){
      RS485_transmit(my_id);     
     }
}

void RS485_transmit(bdID id){
  u8 buf[64]={0,};
  u8 i, size, sum;
  SENSOR_UPDATA sensor_up;
  
  size=sizeof(sensor_up);
  sensor_up.stx=0xFB;//repose stx
  sensor_up.size=size;
  sensor_up.sensor=sensor_bd;
  sensor_up.sum=0;
  sensor_up.etx='@';
  memcpy(buf,&sensor_up,size);
  sum=0;
  for(i=0;i<size-1;i++)sum=sum+buf[i];
  buf[size-2]=sum;
#ifdef DBG_RS485
  printf("bd_id[%X] size[%x] error[%X]  sensor0[%d] sensor1[%d] sensor2[%d]\r\n",
  sensor_bd.bd_id, size, sensor_bd.error, sensor_bd.sensor0, sensor_bd.sensor1, sensor_bd.sensor2);
#endif   
  //printf("SENSOR_UPDATA SIZE[%d]\r\n",sizeof(sensor_up));

  GPIO_WriteHigh(GPIOA, GPIO_PIN_1);  //txen HIGH
  for(i=0;i<size;i++) CUartTxChar(buf[i]);
  GPIO_WriteLow(GPIOA, GPIO_PIN_1); //TXEN LOW
  
}
