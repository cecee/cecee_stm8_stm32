/************************************************************
*	ProjectName:	   LT86104EX
*	FileName:	       main.h
*	BuildData:	     2013-01-05
*	Version£º        V3.20
* Company:	       Lontium
************************************************************/

#ifndef		_MAIN_H
#define		_MAIN_H

extern  void  GPIO_Initial(void);
extern  void  CLK_Configuration(void);

#define SHT_SDA_Pin GPIO_PIN_2
#define SHT_SDA_GPIO_Port GPIOD
#define SHT_SCL_Pin GPIO_PIN_3
#define SHT_SCL_GPIO_Port GPIOD
#endif