/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "mqtt.h"
#include "extern.h"
#include <stdio.h>
#include <string.h>
#include "iwdg.h"

//uint8_t MODEM_READY;
//MODEM_N502L modem;

extern UART_HandleTypeDef huart1;

MQTT_PUBLISH gMQT_Value;

MQTT::MQTT(){
  memcpy(&gMQT_Value,0,sizeof(gMQT_Value));
}

MQTT::~MQTT(){
}
  
void MQTT::mqtt_class_test(void){
  string str1 = "MQTT";
  printf("mqtt_class_test C++ string test [%s]\r\n",str1.c_str());
}

uint8_t MQTT::MqttConnect(void){
  char* res;
  uint8_t ok=0;

  pAT->AT_CMD("AT*WMQTCFG=endpoint,ssl://a3goy7xojgwxx6-ats.iot.ap-northeast-2.amazonaws.com\r\n");
  pAT->AT_CMD("AT*WMQTCFG=port,8883\r\n");
  pAT->AT_CMD("AT*WMQTCFG=clientname,pm419_iot\r\n");
  pAT->AT_CMD("AT*WMQTCFG=cacerts,/data/cacerts.crt\r\n");
  pAT->AT_CMD("AT*WMQTCFG=clientkey,/data/clientkey.key\r\n");
  pAT->AT_CMD("AT*WMQTCFG=clientcerts,/data/clientcerts.crt\r\n");  
  
  res=pAT->AT_CMD("AT*WMQTCON\r\n");
  if (strstr(res, "CON:2") != NULL) ok=1;
  return ok;
}


char MQTT::MqttPub(MQTT_PUBLISH pub){
  
  char* res;
  char ok=0;
  char s_pub[256];
  string str;
  string rw="w";
  
  int ot,o1,o2,o3,t1,t2,t3,h1,h2,h3;
  int sensor_cnt, ctrl,acv,aca,acp;
  string id=pub.id;
  ot=100;
  acv=pub.acv;
  aca=pub.aca; 
  acp=pub.acp;
  ctrl=0x07;
  sensor_cnt= pub.sensor_cnt;
  o1=pub.o1; o2=pub.o2; o3=pub.o3;
  t1=pub.t1; t2=pub.t2; t3=pub.t3;
  h1=pub.h1; h2=pub.h2; h3=pub.h3;
  printf("pub=> id : %s sensor[%d]\r\n",id.c_str(), sensor_cnt);
  //printf("AC : [%d][%d][%d]\r\n",acv, aca, acp);
  //printf("o3 : [%d][%d][%d]\r\n",o1, o2, o3);
  //printf("tm : [%d][%d][%d]\r\n",t1, t2, t3);
  //printf("hm : [%d][%d][%d]\r\n",h1, h2, h3);
  
  sprintf(s_pub,"AT*WMQTPUB=/iot,\"{\"id\":\"%s\",\"rw\":\"w\", \
\"ot\":%d, \"o1\":%d, \"o2\":%d, \"o3\":%d, \"t1\":%d, \"t2\":%d, \"t3\":%d, \
\"h1\":%d, \"h2\":%d, \"h3\":%d, \"acv\":%d, \"aca\":%d, \"acp\":%d, \"ct\":%d, \"sc\":%d}\" \r\n", 
id.c_str(), ot, o1, o2, o3, t1, t2, t3, h1, h2, h3, acv, aca, acp, ctrl, sensor_cnt);

#if 1
  //printf("%s",s_pub);
  res=pAT->AT_CMD(s_pub);
  //printf("%s",res);
#endif  
  return RET_OK;
}

char MQTT::MqttSub(string id){
  char ldata[64];
  char* res;
  sprintf(ldata,"AT*WMQTSUB=/iot/%s\r\n", id.c_str());
  //printf("[%s]\r\n",ldata);
  res=pAT->AT_CMD(ldata);
  printf("[%s]\r\n",res);
  return RET_OK;
}


int pick_integer(char* str) { /* 문자열에서 숫자만 추출하는 함수 */
        int res=0; //숫자를 추출해 저장
        int i; 
        for(i=0; str[i]!='\0';i++){
            if(str[i] >= 48 && str[i] <=57){
                res = res*10 + (str[i]-48);
            }
        }
        return res;
}

char MQTT::waitMqtt(){
  string str;
  int i,val;
  char ldata[256]={0,};
  char temp[64];
  MQTT_SUB mqttSb;       
  vector<string> v;
  mqttSb.to=-1;
  mqttSb.tf=-1;
  mqttSb.ct=-1;
  mqttSb.ot=-1;
  
  HAL_UART_Receive(&huart1, (uint8_t*)ldata, 128, 1000);
  printf("waitMqtt~~~[%x][%x][%x][%x]\r\n",ldata[0], ldata[1], ldata[2], ldata[3]);
  if(ldata[0] =='*'){
    str=ldata;
    if (str.find("*WMQTRCV") != string::npos) { 
      v=pUtil->split2(str, ",");
      //printf("[%s]\r\n",str.c_str());
      for (string s : v) {
         sprintf(temp,"%s",s.c_str());      
         val=pick_integer(temp);
         //printf("[%s][%d]\r\n",s.c_str(),val);
          if (s.find("to\":") != string::npos) { 
           sprintf(temp,"%s",s.c_str()); 
           mqttSb.to=pick_integer(temp);
        }
         else if(s.find("tf\":") != string::npos) { 
           sprintf(temp,"%s",s.c_str()); 
           mqttSb.tf=pick_integer(temp);
        }
         else if(s.find("ct\":") != string::npos) { 
           sprintf(temp,"%s",s.c_str()); 
           mqttSb.ct=pick_integer(temp);
        }
        else if(s.find("ot\":") != string::npos) { 
           sprintf(temp,"%s",s.c_str()); 
           mqttSb.ot=pick_integer(temp);
        }
      }
      printf("to[%d] tf[%d] ct[%d] ot[%d]\r\n",mqttSb.to, mqttSb.tf, mqttSb.ct, mqttSb.ot);
      //HAL_Delay(20);
      //HAL_UART_Receive(&huart1, (uint8_t*)ldata, 128, 100);//fulsh
      return RET_OK;
    }
    else {  
      //HAL_Delay(20);
      //HAL_UART_Receive(&huart1, (uint8_t*)ldata, 128, 100);//fulsh
      return RET_ERR;}
  }
  else {
      //HAL_Delay(20);
      //HAL_UART_Receive(&huart1, (uint8_t*)ldata, 128, 100);//fulsh
      return RET_ERR;
  }
}
