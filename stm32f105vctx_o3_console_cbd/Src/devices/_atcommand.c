/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "atcommand.h"
#include "extern.h"
#include <stdio.h>
#include <string.h>
#include "iwdg.h"

uint8_t MODEM_READY;
MODEM_N502L modem;

extern UART_HandleTypeDef huart1;
//extern UART_HandleTypeDef huart2;

void AT_Req(const char *req){
  char response[512]={0,};
  int strLen= strlen(req);
  rx_buffer_idx=0;
  HAL_UART_Transmit(&huart1, (uint8_t*)req, strLen, 100); 
  //HAL_UART_Receive(&huart1, (uint8_t*)response, 512,200);
  //printf("%s->[%02X][%02X][%02X][%02X][%02X][%02X][%02X][%02X]\r\n",response, 
  //       response[0], response[1], response[2], response[3], response[4], response[5], response[6], response[7]);
  //return response;  
}

void AT_isReady(void){
  char* res;
  uint8_t ok=0;
  AT_Req("AT\r\n");
}

uint8_t AT_setMqttConfig(void){
  char* res;
  uint8_t ok=0;
  res=AT_Req("AT*WMQTCFG=endpoint,13.209.169.85\r\n");
  res=AT_Req("AT*WMQTCFG=clientname,cecee_o3iot\r\n");
  //printf("%s\r\n",res); 
  if (strstr(res, "CON:2") != NULL) ok=1;
  return ok;
}

uint8_t AT_isMqttConfig(void){
  char* res;
  uint8_t ok=0;
  res=AT_Req("AT*WMQTCFG?\r\n");
  printf("%s",res); 
  return ok;
}




uint8_t AT_MqttConnect(void){
  char* res;
  uint8_t ok=0;
  res=AT_Req("AT*WMQTCON\r\n");
  //printf("AT_MqttConnect->%s\r\n",res);
  if (strstr(res, "CON:2") != NULL) ok=1;
  return ok;
}

uint8_t AT_MqttDisConnect(void){
  char* res;
  uint8_t ok=0;
  res=AT_Req("AT*WMQTDIS\r\n");
  if (strstr(res, "OK") != NULL) ok=1;
  return ok;
}

string AT_GetPhoneNumber(void){
 string res;
 string str="xxx";
 vector<string> v;
 res=(char*)AT_Req("AT$$CNUM?\r\n");
 v=pUtil->split2(res, ",");
 for (string s : v) {
      if (s.find("\"+") != string::npos) { 
        str=s;
        pUtil->ReplaceStringInPlace(str,"\"","");//"""����
        pUtil->ReplaceStringInPlace(str,"+","");//"+" ����
        break;
      }
 }
  return str;
}

void modem_init(void){
  uint8_t time_out_cnt=0;
  //string phoneNum;
  modem.ready=0;
  while(1){
    //printf("modem_init... time_out_cnt[%d]\r\n",time_out_cnt);
    __HAL_IWDG_RELOAD_COUNTER(&hiwdg);
    time_out_cnt++; 
    if(time_out_cnt>20) break;
    //break;
    if(AT_isReady()){
      modem.telephon_number=AT_GetPhoneNumber();
      gMQT_Value.id = modem.telephon_number;
      
      printf("phoneNum[%s]\r\n",modem.telephon_number.c_str());
      //AT_MqttDisConnect();
      AT_setMqttConfig();
      //AT_isMqttConfig();
      //__HAL_IWDG_RELOAD_COUNTER(&hiwdg);
      if(AT_MqttConnect())
      { 
        printf("MQTT CONNECT... OK[%d]\r\n",time_out_cnt);
        pMqtt->MqttSub(modem.telephon_number);
        modem.ready=1;
        break;
      }
    }
    HAL_Delay(1000);
  }

}

