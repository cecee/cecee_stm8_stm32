/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "modem.h"
#include "extern.h"
#include <stdio.h>
#include <string.h>
#include "iwdg.h"

uint8_t MODEM_READY;
MODEM_N502L modem_n502l;

extern UART_HandleTypeDef huart1;
//extern UART_HandleTypeDef huart2;

MODEM::MODEM(){
  memcpy(&modem_n502l,0,sizeof(modem_n502l));
}

MODEM::~MODEM(){
}


uint8_t MODEM::AT_isReady(void){
  char* res;
  uint8_t ok=0;
  res=pAT->AT_CMD("AT\r\n");
  if (strstr(res, "OK") != NULL) ok=1;
  return ok;
}



uint8_t MODEM::AT_isMqttConfig(void){
  char* res;
  uint8_t ok=0;
  res=pAT->AT_CMD("AT*WMQTCFG?\r\n");
  printf("%s",res); 
  return ok;
}

uint8_t MODEM::AT_MqttDisConnect(void){
  char* res;
  uint8_t ok=0;
  res=pAT->AT_CMD("AT*WMQTDIS\r\n");
  if (strstr(res, "OK") != NULL) ok=1;
  return ok;
}

string MODEM::AT_GetPhoneNumber(void){
 string res;
 string str="xxx";
 vector<string> v;
 res=(char*)pAT->AT_CMD("AT$$CNUM?\r\n");
 v=pUtil->split2(res, ",");
 for (string s : v) {
      if (s.find("\"+") != string::npos) { 
        str=s;
        pUtil->ReplaceStringInPlace(str,"\"","");//"""����
        pUtil->ReplaceStringInPlace(str,"+","");//"+" ����
        break;
      }
 }
  return str;
}

void MODEM::modem_init(void){
  uint8_t time_out_cnt=0;
  //string phoneNum;
  modem_n502l.ready=0;
  
  
  while(1){
    //printf("modem_init... time_out_cnt[%d]\r\n",time_out_cnt);
    __HAL_IWDG_RELOAD_COUNTER(&hiwdg);
    
    if(time_out_cnt>20) break;
    //break;
    if(AT_isReady()){
      modem_n502l.telephon_number=AT_GetPhoneNumber();
      gMQT_Value.id = modem_n502l.telephon_number;
      printf("Try MQT[%03d] phoneNum[%s]\r\n",time_out_cnt, modem_n502l.telephon_number.c_str());
      //AT_MqttDisConnect();
      //__HAL_IWDG_RELOAD_COUNTER(&hiwdg);
      if(pMqtt->MqttConnect())
      { 
        printf("MQTT CONNECT... OK[%d]\r\n",time_out_cnt);
        pMqtt->MqttSub(modem_n502l.telephon_number);
        modem_n502l.ready=1;
        break;
      }
    }
    time_out_cnt++; 
    HAL_Delay(1000);
  }

}

