#ifndef ATCOMMAND_H
#define ATCOMMAND_H
#include "stm32f1xx_hal.h"
#include <string>

using namespace std; 

typedef struct _MODEM_N502L   
{
  uint8_t ready;
  string telephon_number;
}MODEM_N502L;



void AT_Req(const char *req);
void modem_init(void);
uint8_t AT_isReady(void);

string AT_GetPhoneNumber(void);

uint8_t AT_MqttConnect(void);
uint8_t AT_MqttDisConnect(void);

uint8_t AT_isMqttConfig(void);
uint8_t AT_setMqttConfig(void);
#endif 
