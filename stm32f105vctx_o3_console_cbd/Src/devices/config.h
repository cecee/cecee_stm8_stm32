#ifndef CONFIG_H
#define CONFIG_H

#include "flash_if.h"
//eeprom data
typedef struct _SETUP_DATA
{
    uint8_t   sum;
    uint8_t   mode;
    uint16_t  dest_O3;
    uint16_t  dest_ON_Period;
    uint16_t  dest_OFF_Period;
}SETUP_DATA;

void config_reset();
uint8_t config_sum(SETUP_DATA sd);
void writer_config(SETUP_DATA sd);
SETUP_DATA read_config(void);
#endif 
