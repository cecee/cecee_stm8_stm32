/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "queue.h"
#include "extern.h"
#include <stdio.h>


extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

SENSOR_UPDATA o3sensor1;
SENSOR_UPDATA o3sensor2;
SENSOR_UPDATA o3sensor3;

SUB_BOARD_ACCESS sub_bd_access; 
FAN_DATA rep_fan;
    
QUEUE qUart1;
uint8_t qUart1_package=0;
uint8_t qUart1_buffer[512];
uint8_t qUart1_cnt=0;
uint8_t qUart1_sz=0xff;

/*****************************************************************************
*                           Static Variables
******************************************************************************/

uint8_t rx_buffer[256];
uint8_t rx_buffer_idx=0;
uint8_t rxSz=0;
uint8_t package_flag=0;
uint8_t UART_FLAG = 0;

/////////////////////////////////////////////
///Queue
////////////////////////////////////////////

void init_qUart1(void){
  qUart1.front=0;
  qUart1.rear=0;
  memset(&sub_bd_access,0,sizeof(sub_bd_access));
  //HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(TXEN2_GPIO_Port, TXEN2_Pin, GPIO_PIN_RESET);
}


int put_qUart1(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart1.rear + 1) % MAX_SIZE == qUart1.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart1.rear, qUart1.front);
        return -1;
    }

    qUart1.queue[ qUart1.rear] = k;
    qUart1.rear = (qUart1.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart1.rear);
    return k;
}


int get_qUart1(void)
{
    int i=0;
     if (qUart1.front == qUart1.rear){
        return -1;
    }
    i = qUart1.queue[qUart1.front];
   qUart1.front = (qUart1.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}



void QueueProcess(){
  int  d,ex_d;
  int end=0;
    for(;;)
    {
      d =get_qUart1();
      
      if(d==-1 )break;
      //printf("[%x]\r\n",d);
      if(d=='*' && package_flag==0) {
        //printf("find~\r\n");
        package_flag=1;
        rx_buffer_idx=0;
        memset(rx_buffer,0,sizeof(rx_buffer));
      }
      if(package_flag){
        rx_buffer[rx_buffer_idx++]= d;
        if(d==0x0A || d==0x0D){
          rx_buffer_idx=0;
          package_flag=0;
          pAT->parse((char *)rx_buffer);
          memset(rx_buffer,0,sizeof(rx_buffer));
        } 
        ex_d=d;
      }
      
      if(rx_buffer_idx>120) {
        package_flag=0;
        rx_buffer_idx=0;
        memset(rx_buffer,0,sizeof(rx_buffer));
        break;
      }
      if(end) break;	
    }
}


uint8_t calcu_sum(uint8_t *data, uint8_t len)
{
    uint8_t i, sum=0;
    for(i=0;i<len;i++)sum=sum+data[i];
    return sum;
}

void reqLed(uint16_t id, uint8_t timeout)
{
  switch(id){
    case 0xAAA0:
      led_bar.info[0].tick=timeout;
      break;
    case 0xB000:
      led_bar.info[1].tick=timeout;
      break; 
    case 0xB001:
      led_bar.info[2].tick=timeout;
      break;  
    case 0xB002:
      led_bar.info[3].tick=timeout;
      break;       
  }
}

void tx_req(uint16_t id, uint8_t data){
  uint8_t i;
  REQUEST req;
  uint8_t sum=0;
  uint8_t len=sizeof(REQUEST);
  uint8_t buf[128]={0,};  
  uint16_t reqID=id;//&0xFF00;
  uint8_t resLen,resSum;
  
  req.stx=0xFA;
  req.id=id;
  req.size=len;
  req.data=data;
  req.dumy=0;
  req.sum=0;
  req.etx='@';
  
  memcpy(buf, &req, len); 
  sum=calcu_sum(buf, len-1);
  buf[len-2]=sum;
  //printf("tx_req stx[%02x] id[%04x] sum[%02x] etx[%02x]--len[%d]\r\n", req.stx, req.id, req.sum, req.etx, len);
  //for(i=0;i<len;i++) printf("tx_req [%02d] [%x] \r\n", i, buf[i]);
  reqLed(id,50);
  
  HAL_GPIO_WritePin(TXEN2_GPIO_Port, TXEN2_Pin, GPIO_PIN_SET);
  HAL_UART_Transmit(&huart2, buf, len, 100); 
  HAL_GPIO_WritePin(TXEN2_GPIO_Port, TXEN2_Pin, GPIO_PIN_RESET);
  memset(buf,0,sizeof(buf));
  HAL_UART_Receive(&huart2, buf, 64,100);
  if(buf[0]==0xFA){
    resLen=buf[1];
    resSum=buf[resLen-2];
    
    if(reqID==0xAAA0){
      sub_bd_access.CONTROL_BOX_ACCESS=0;
      sum=calcu_sum(buf, resLen-2);
      if(sum==resSum){
        memcpy(&rep_fan, buf, resLen);
        gMQT_Value.acv=rep_fan.power.v;
        gMQT_Value.aca=rep_fan.power.a;
        gMQT_Value.acp=rep_fan.power.p;
       // printf("CONTROL id[%x] sum[%x][%x] [%x]\r\n",rep_fan.id,  rep_fan.sum, sum, reqID); 
       // printf("v[%d] a[%d] p[%d]\r\n",rep_fan.power.v,  rep_fan.power.a, rep_fan.power.p); 
      }
    }

    else if(reqID==0xB000){
      sub_bd_access.O3SENSOR_ACCESS[0]=0;
     //printf("B000\r\n");
     //for(i=0;i<16;i++)printf("[%02X]",buf[i]); printf("\r\n");  
      sum=calcu_sum(buf, resLen-2);
      //printf("sum[%x][%x]\r\n",sum, resSum); 
      if(sum==resSum){
        memcpy(&o3sensor1, buf, resLen);
        o3sensor1.sensor.bd_id=SWAPBYTE_US(o3sensor1.sensor.bd_id);
        gMQT_Value.o1=o3sensor1.sensor.o3_ppb;
        gMQT_Value.t1=o3sensor1.sensor.temperature;
        gMQT_Value.h1=o3sensor1.sensor.humidity;
        //printf("O3SENSOR1 id[%x] o3_ppb[%d] temperature[%d] humidity[%d] error[%d]\r\n",
        //  o3sensor1.sensor.bd_id,  o3sensor1.sensor.o3_ppb, o3sensor1.sensor.temperature, o3sensor1.sensor.humidity, o3sensor1.sensor.error); 

      }
    }
    
    else if(reqID==0xB001){
      sub_bd_access.O3SENSOR_ACCESS[1]=0;
      sum=calcu_sum(buf, resLen-2);
      if(sum==resSum){
        memcpy(&o3sensor2, buf, resLen);
        o3sensor2.sensor.bd_id=SWAPBYTE_US(o3sensor2.sensor.bd_id);
        gMQT_Value.o2=o3sensor2.sensor.o3_ppb;
        gMQT_Value.t2=o3sensor2.sensor.temperature;
        gMQT_Value.h2=o3sensor2.sensor.humidity;
        //printf("O3SENSOR2 id[%x] o3_ppb[%d] temperature[%d] humidity[%d] error[%d]\r\n",
        //  o3sensor2.sensor.bd_id,  o3sensor2.sensor.o3_ppb, o3sensor2.sensor.temperature, o3sensor2.sensor.humidity, o3sensor2.sensor.error); 

      }
    }
  }

}



