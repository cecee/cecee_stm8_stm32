#ifndef QUEUE_H
#define QUEUE_H
#include "stm32f1xx_hal.h"

#define MAX_SIZE 512 // max size of queue

typedef struct 
{
  uint16_t bd_id;
  uint16_t o3_ppb;
  uint16_t temperature;  
  uint16_t humidity;
  uint8_t error;
}SENSOR_INFO;

typedef struct _SENSOR_UPDATA   
{
  uint8_t stx; // 1
  uint8_t size;	//1
  SENSOR_INFO sensor;
  uint8_t sum; //1
  uint8_t etx; //1
}SENSOR_UPDATA;  

typedef struct _RELAY_BIT
{
    uint8_t  enable: 1;
    uint8_t  state: 1;
    uint8_t  detect: 1;	
    uint8_t  alarm: 1;	
}RELAY_BIT;

typedef struct _POWER
{
    uint16_t  v;
    uint16_t  a;
    uint16_t  p;	
}POWER;  

typedef struct _FAN_DATA   
{
  uint8_t stx; // 1
  uint8_t size;	//1
  uint16_t id; //2
  POWER power;// 6byte
  RELAY_BIT relay[4]; //4
  uint8_t sum; //1
  uint8_t etx; //1
}FAN_DATA;


typedef struct _queue   
{
	int queue[MAX_SIZE+1];
	int front;
	int rear;
}QUEUE;

void init_qUart1(void);
int put_qUart1(int k);
int get_qUart1(void);
void QueueProcess(void);
void led_control(uint8_t* data);
void tx_req(uint16_t id, uint8_t data);

#endif 
