#include "config.h"
#include "extern.h"

SETUP_DATA setup_data;

void config_reset(void)
{
  printf("config reset\r\n");
 // HAL_GPIO_WritePin(GPIOB, PWR_EN_SYS_Pin,GPIO_PIN_SET); 
  HAL_Delay(10);
 // HAL_GPIO_WritePin(GPIOB, RES_RKCPU_Pin,GPIO_PIN_RESET); 
  HAL_Delay(2);
 // HAL_GPIO_WritePin(GPIOB, RES_RKCPU_Pin,GPIO_PIN_SET); 
}

uint8_t config_sum(SETUP_DATA sd)
{
  uint8_t buf[8];
  uint8_t i,sum=0;
  sd.sum=0;
  memcpy(buf,&sd,8);
  for(i=0;i<8;i++) sum=sum+buf[i];
  printf("config_sum[%x]\r\n", sum);
  return sum;
}


void writer_config(SETUP_DATA s)
{
  u32 Data[2];
  memcpy(&Data,&s,8);
  printf("writer_config sum[%x] mode[%x] dest_O3[%d] ON_Period[%d] OFF_Period[%d]\r\n",s.sum, s.mode, s.dest_O3, s.dest_ON_Period, s.dest_OFF_Period);
  FLASH_WriterWord(APPLICATION_ADDRESS, Data, 2);
}

SETUP_DATA read_config(void){
  SETUP_DATA s;
  uint8_t buf[32];
  uint32_t rdata;

  rdata=FLASH_ReadWord(0);
  memcpy(buf,&rdata,4);
  rdata=FLASH_ReadWord(1);
  memcpy(buf+4,&rdata,4);
  memcpy(&s,buf,8);
  return s;
}

