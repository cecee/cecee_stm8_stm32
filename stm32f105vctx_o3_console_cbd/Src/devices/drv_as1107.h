/*
  drv_as1107.h
*/
#ifndef DRV_AS1107_H_
#define DRV_AS1107_H_

/****************************************************************************
*                               Includes
*****************************************************************************/
#include <stdint.h>
#include "queue.h"
#include <string>
using namespace std; 

#define TIME_BLINK 200


typedef struct _LED_BLINK
{
  uint8_t blink;
  uint16_t tick;
}LED_BLINK;

typedef struct _KEY_MAP
{
  uint8_t key;
  uint16_t timeout;
}KEY_MAP;

typedef struct _LED_BAR
{
  uint8_t led;
  LED_BLINK info[5];
}LED_BAR;

typedef struct _LED_STATE
{
  //0: mode 1:control  PLASMA(RED) FAN(GREEN) FAN/PLASMA(YELLOW) 
  uint8_t led;//xxxx-0000// b0:#1 /red b1:#1/ green b2:#2 /red b3:#2 green
  LED_BLINK info[4];
}LED_STATE;

typedef struct tag_ui
{
  uint8_t  LED_BAR;   
  uint16_t DISP1;
  uint16_t DISP2;
}UI_FND;


/****************************************************************************
*                               function
*****************************************************************************/
void Drv_as1107_init(void);
void writeRegister(uint8_t addr, uint8_t value);
void writeRegisters(uint16_t value);
void writer_buf2regi(uint8_t *reg);
void show_fanbox(FAN_DATA fbox);
void show_sensor(SENSOR_UPDATA sensor);
void show_targetO3(uint16_t value);
void show_telephonNumber(string num, uint8_t seq);
void show_TemperAndHumidity(uint16_t temper, uint16_t humidity);
void LED_PressKeyShowOn(uint8_t sensor);
void LED_PressKeyShowOff(void);
void Drv_display_err(void);

void Drv_led_set(uint8_t led,  uint8_t timeout, uint8_t blink);
void Drv_led_release(void);
void Drv_led_tick(void);
void LED_ModeOn(uint8_t mode, uint8_t ontime);

#endif