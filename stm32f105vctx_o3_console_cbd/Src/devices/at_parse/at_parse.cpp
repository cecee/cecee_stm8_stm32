#include "at_parse.h"
#include "extern.h"

extern UART_HandleTypeDef huart1;

AT_PARSE::AT_PARSE(){
}

AT_PARSE::~AT_PARSE(){
}
  
char *AT_PARSE::AT_CMD(const char *req){
  char response[512]={0,};
  int strLen= strlen(req);
  rx_buffer_idx=0;
  HAL_UART_Transmit(&huart1, (uint8_t*)req, strLen, 100); 
  HAL_UART_Receive(&huart1, (uint8_t*)response, 512,200);
  return response;  
}  


void AT_PARSE::at_parse_init(void){
  string str1 = "AT_PARSE";
  printf("C++ string test [%s]\r\n",str1.c_str());
}

void AT_PARSE::parse(char *data){
  string str;
  str=data;
  //printf("->%s<-\r\n",str.c_str());
printf("--------\r\n");
printf("%s",data);
printf("\r\n--------\r\n");
}
