#ifndef AT_PARSE_H
#define AT_PARSE_H
#include <vector>
#include <string>
using namespace std; 


class AT_PARSE
{
    private:
    public:
      string str;

      AT_PARSE();
      virtual ~AT_PARSE();
      
      void at_parse_init(void);
      void parse(char *data);
      char *AT_CMD(const char *req);

      //char* my_strtok(char* str, const char* delimiters);
      //vector<string> split2(string s, string divid);
      //std::string replaceString(std::string subject, const std::string &search, const std::string &replace);
      //void ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace);
};

#endif 
