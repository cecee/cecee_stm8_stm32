#ifndef UTIL_H
#define UTIL_H
#include <vector>
#include <string>
using namespace std; 

class UTIL
{
    private:
    public:
      string str;

      UTIL();
      virtual ~UTIL();
      
      void util_init(void);
      char* my_strtok(char* str, const char* delimiters);
      vector<string> split2(string s, string divid);
      std::string replaceString(std::string subject, const std::string &search, const std::string &replace);
      void ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace);
};

#endif 
