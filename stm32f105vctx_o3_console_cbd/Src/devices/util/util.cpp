#include "util.h"
#include "extern.h"
 

UTIL::UTIL(){
}

UTIL::~UTIL(){
}
  
void UTIL::util_init(void){
  string str1 = "BlockDMask";
  //HAL_GPIO_WritePin(U1TXEN_GPIO_Port, U1TXEN_Pin, GPIO_PIN_RESET); 
  //qUart1.front=0;
  //qUart1.rear=0;
  //memset(&updata,0,sizeof(updata));
  printf("C++ string test [%s]\r\n",str1.c_str());
}

//Immutable Replace 원본 문자열에는 아무 영향을 끼치지 않는다. 변경된 문자열은 함수의 반환값으로 돌아온다.
std::string UTIL::replaceString(std::string subject, const std::string &search, const std::string &replace) { 
  size_t pos = 0; 
  while ((pos = subject.find(search, pos)) != std::string::npos) 
  { 
    subject.replace(pos, search.length(), replace); 
    pos += replace.length(); 
  } 
  return subject; 
}

//Mutable Replace 원본 문자열을 수정한다. 속도가 우선일 경우 사용하자.
void UTIL::ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace) { 
  size_t pos = 0; 
  while ((pos = subject.find(search, pos)) != std::string::npos) { 
    subject.replace(pos, search.length(), replace); pos += replace.length(); 
  } 
}

vector<string> UTIL::split2(string s, string divid) {
	vector<string> v;
	int start = 0;
	int d = s.find(divid);
	while (d != -1){
		v.push_back(s.substr(start, d - start));
		start = d + 1;
		d = s.find(divid, start);
	} 
	v.push_back(s.substr(start, d - start));

	return v;
}


char* UTIL::my_strtok(char* str, const char* delimiters){
   static char* pCurrent;
   char* pDelimit;

   if ( str != NULL )pCurrent = str;
   else str = pCurrent;

   if(*pCurrent == NULL) return NULL;
   //문자열 점검
   while (*pCurrent)
   {
       pDelimit = (char*)delimiters ;
       
       while (*pDelimit){
         if(*pCurrent == *pDelimit){
               *pCurrent = NULL;
               ++pCurrent;
               return str;
            }
            ++pDelimit;
       }
       ++pCurrent;
   }
    // 더이상 자를 수 없다면 NULL반환
    return str;
}