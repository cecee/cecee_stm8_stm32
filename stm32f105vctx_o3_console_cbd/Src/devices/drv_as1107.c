#include "main.h"
#include "stm32f1xx_hal.h"
#include "extern.h"
#include "drv_as1107.h"
#include "spi.h"

/****************************************************************************
 *                                Defines
 *****************************************************************************/
#define AS_TRACE(...)    DBG(__VA_ARGS__)

#define NO_OP 0
#define DIGIT0 1
#define DIGIT1 2
#define DIGIT2 3
#define DIGIT3 4
#define DIGIT4 5
#define DIGIT5 6
#define DIGIT6 7
#define DIGIT7 8
#define DEC_MODE 9
#define INTENSITY  10
#define SCAN_LIMIT 11
#define SHUTDOWN 12
#define N_A    13
#define FEATURE  14
#define DISP_TEST  15

//SHUTDOWN
//Shutdown mode
#define SD_DEFAULT   0x00
#define SD_UNCHANGED 0x80
//Normal mode
#define DEFAULT   0x01
#define UNCHANGED  0x81

//DEC_MODE
#define NO_DEC_ALL  0x00
#define NO_DEC_17  0x01
#define NO_DEC_47  0x0F
#define DEC_ALL      0xFF

//DISP_TEST
#define NORMAL  0x00
#define TEST    0x01

//INTENSITY
#define DUTY_1    0x00
#define DUTY_3    0x01
#define DUTY_5    0x02
#define DUTY_7    0x03
#define DUTY_9    0x04
#define DUTY_11    0x05
#define DUTY_13    0x06
#define DUTY_15    0x07
#define DUTY_17    0x08
#define DUTY_19    0x09
#define DUTY_21    0x0A
#define DUTY_23    0x0B
#define DUTY_25    0x0C
#define DUTY_27    0x0D
#define DUTY_29    0x0E
#define DUTY_31    0x0F

//SCAN_LIMIT
#define DISP_00    0x00
#define DISP_01    0x01
#define DISP_02    0x02
#define DISP_03    0x03
#define DISP_04    0x04
#define DISP_05    0x05
#define DISP_06    0x06
#define DISP_07    0x07

//FEATURE
#define CLK_EN    0x00
#define REG_RES   0x01
#define DEC_SEL   0x02
#define SPI_EN    0x03
#define BLK_EN    0x04
#define BLK_FR    0x05
#define SYNC     0x06
#define BLK_START  0x07

//7Segment
#define SEG_DP  0x80
#define SEG_A   0x40
#define SEG_B   0x20
#define SEG_C   0x10
#define SEG_D   0x08
#define SEG_E   0x04
#define SEG_F   0x02
#define SEG_G   0x01
//////////////////////////
//   |-A-|
//   F   B
//   |-G-|
//   E   C
//   |_D_|  DP
//////////////////////////
uint8_t seg[16]={0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b, 0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47};
uint8_t led_seg[8]={SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_G, SEG_DP};
LED_BAR led_bar;
LED_STATE led_state;
KEY_MAP key_map;

void Drv_as1107_init(void)
{
    memset(&led_bar, 0, sizeof(led_bar));
    memset(&led_state, 0, sizeof(led_state));  
    memset(&key_map, 0, sizeof(key_map));  
    //memset(&fnd, 0, sizeof(UI_FND));
    writeRegister(SCAN_LIMIT, DISP_07);//set digit 0~7 8개
    writeRegister(INTENSITY, DUTY_31);
    writeRegister(DEC_MODE, NO_DEC_ALL);
    writeRegister(SHUTDOWN,DEFAULT);
      
}

void LED_ModeOn(uint8_t mode, uint8_t ontime)
{
    mode%=2;
    led_state.info[mode].tick=ontime;
}

void stateLedOn(uint8_t pos, uint8_t on)
{
    uint8_t led_port=0; 
      
    if(on)  _SetBit(led_state.led, pos);  
    else    _ClearBit(led_state.led, pos);
    if(_CheckBit(led_state.led, 0)!=0) led_port=led_port|SEG_A;
    if(_CheckBit(led_state.led, 1)!=0) led_port=led_port|SEG_B;  
    if(_CheckBit(led_state.led, 2)!=0) led_port=led_port|SEG_C;  
    if(_CheckBit(led_state.led, 3)!=0) led_port=led_port|SEG_D;

    writeRegister(DIGIT7,led_port); 
}

void led_onoff(uint8_t pos, uint8_t on)//10ms loop
{
    uint8_t led_port=0; 
    if(on)  _SetBit(led_bar.led, pos);  
    else    _ClearBit(led_bar.led, pos);

    if(_CheckBit(led_bar.led, 0)!=0) led_port=led_port|SEG_A;
    if(_CheckBit(led_bar.led, 1)!=0) led_port=led_port|SEG_B;  
    if(_CheckBit(led_bar.led, 2)!=0) led_port=led_port|SEG_C;  
    if(_CheckBit(led_bar.led, 3)!=0) led_port=led_port|SEG_D;
    if(_CheckBit(led_bar.led, 4)!=0) led_port=led_port|SEG_E;  
    if(_CheckBit(led_bar.led, 5)!=0) led_port=led_port|SEG_F;   
    if(_CheckBit(led_bar.led, 6)!=0) led_port=led_port|SEG_G; 
    if(_CheckBit(led_bar.led, 7)!=0) led_port=led_port|SEG_DP; 
    writeRegister(DIGIT6,led_port); 
}



void led_timeout(void)//10ms loop
{
   uint8_t i;
   
   for(i=0;i<5;i++)
   {
    //printf("led.info[%d].tick[%d]\r\n",i,led.info[i].tick);
      if(led_bar.info[i].tick>0 && led_bar.info[i].tick<0xff) led_bar.info[i].tick--;
   }
   for(i=0;i<4;i++)
   {
    //printf("led.info[%d].tick[%d]\r\n",i,led.info[i].tick);
      if(led_state.info[i].tick>0 && led_state.info[i].tick<0xff) led_state.info[i].tick--;
   }   
}

void LED_PressKeyShowOff()
{
  led_bar.info[0].tick=0;
  led_bar.info[1].tick=0;
  led_bar.info[2].tick=0;
  led_bar.info[3].tick=0; 
  led_bar.info[4].tick=0;   
}

void LED_PressKeyShowOn(uint8_t kind)
{
  LED_PressKeyShowOff();
  led_bar.info[kind].tick=0xff; 
}

//////////////////////////////
// 1ms sub routin
// recover show display 
// led blink
/////////////////////////////
void Drv_led_release(void)
{
  uint8_t i;
  led_timeout();
  for(i=0;i<5;i++)
  {
    if(led_bar.info[i].tick==0) led_onoff(i,0);
    else led_onoff(i,1);
  }
    
  for(i=0;i<4;i++)
  {
    if(led_state.info[i].tick==0)stateLedOn(i,0);
    else stateLedOn(i,1);
  } 
    
  if(key_map.timeout++ > RECOVER_DISPLAY_TIME){
      key_map.key=0;
      key_map.timeout=0;
      LED_PressKeyShowOff();
      show_sensor(o3sensor1);
  };
        
}

void writer_buf2regi(uint8_t *reg){

    writeRegister(DIGIT0,reg[0]);
    writeRegister(DIGIT1,reg[1]);
    writeRegister(DIGIT2,reg[2]);
    writeRegister(DIGIT3,reg[3]);
    writeRegister(DIGIT4,reg[4]);
    writeRegister(DIGIT5,reg[5]);
}

//============================================================
// data1[0]|data1[1]|data1[2]|---data2[0]|data2[1]|data2[2]|
// fan__[5]|fan__[4]|fan__[3]|---fan__[2]|fan__[1]|fan__[0]|
//============================================================

void show_fanbox(FAN_DATA fbox){
    uint8_t data1[3];
    uint8_t data2[3]; 
    uint8_t fData[6];
    uint16_t disp1=fbox.power.v/10;
    uint16_t disp2=fbox.power.a/10;
      
    data1[0]=(disp1/100)%10; 
    data1[1]=(disp1/10)%10; 
    data1[2]=disp1%10;
    data2[0]=(disp2/100)%10;  
    data2[1]=(disp2/10)%10; 
    data2[2]=disp2%10; 
      
    fData[5]=seg[data1[0]];
    fData[4]=seg[data1[1]];    
    fData[3]=seg[data1[2]];  
    fData[2]=seg[data2[0]];
    fData[1]=seg[data2[1]];    
    fData[0]=seg[data2[2]]; 
    writer_buf2regi(fData);  
}

void show_targetO3(uint16_t value){
    uint8_t data1[3];
    uint8_t data2[3]; 
    uint8_t fData[6];
    uint16_t disp2=value/10;
    data1[1]=3; 
    
    data2[0]=(disp2/100)%10;  
    data2[1]=(disp2/10)%10; 
    data2[2]=disp2%10; 
      
    fData[5]=SEG_C|SEG_D|SEG_E|SEG_G;
    fData[4]=seg[data1[1]];    
    fData[3]=SEG_G;
      
    fData[2]=seg[data2[0]]|SEG_DP;
    fData[1]=seg[data2[1]];    
    fData[0]=seg[data2[2]]; 
    writer_buf2regi(fData);  
}

void show_TemperAndHumidity(uint16_t temper, uint16_t humidity){
    uint8_t data1[3];
    uint8_t data2[3]; 
    uint8_t fData[6];
    uint16_t disp1=temper;//sensor.sensor.o3_ppb/10;
    uint16_t disp2=humidity;//sensor.sensor.temperature;
      
    data1[0]=(disp1/100)%10; 
    data1[1]=(disp1/10)%10; 
    data1[2]=disp1%10;
    data2[0]=(disp2/100)%10;  
    data2[1]=(disp2/10)%10; 
    data2[2]=disp2%10; 
      
    fData[5]=seg[data1[0]];
    fData[4]=seg[data1[1]]|SEG_DP;    
    fData[3]=seg[data1[2]];  
    fData[2]=seg[data2[0]];
    fData[1]=seg[data2[1]]|SEG_DP;    
    fData[0]=seg[data2[2]]; 
    writer_buf2regi(fData);  
}

 void show_telephonNumber(string num, uint8_t seq){
     
    uint8_t data1[3];
    uint8_t data2[3]; 
    uint8_t fData[6];
    uint16_t disp1; 
    uint16_t disp2;
    data1[1]=3;

    string n0=  num.substr(0, 4); 
    string n1=  num.substr(4, 4);  
    string n2=  num.substr(8);
    n0=n0.substr(2); 

    int i0= stoi(n0);
    int i1= stoi(n1);
    int i2= stoi(n2);
    
    //printf("show_telephonNumber [%s][%s][%s]\r\n",n0.c_str(), n1.c_str(),n2.c_str());
    printf("show_telephonNumber [%d][%d][%d]\r\n",i0, i1, i2);  
    switch(seq){
      case 0:
        disp2=i0;
        data2[0]=(disp2/100)%10;  
        data2[1]=(disp2/10)%10; 
        data2[2]=disp2%10; 

        fData[5]=seg[0];
        fData[4]=SEG_G;    
        fData[3]=SEG_G; 
        fData[2]=seg[data2[0]];
        fData[1]=seg[data2[1]];    
        fData[0]=seg[data2[2]]; 
        break;
      case 1:
        disp1=(i1/1000)%10;
        disp2=i1;

        data2[0]=(disp2/100)%10;  
        data2[1]=(disp2/10)%10; 
        data2[2]=disp2%10; 

        fData[5]=seg[1];
        fData[4]=SEG_G;    
        fData[3]=seg[disp1];
        fData[2]=seg[data2[0]];
        fData[1]=seg[data2[1]];    
        fData[0]=seg[data2[2]]; 
        break;
      case 2:
        disp1=(i2/1000)%10;
        disp2=i2;

        data2[0]=(disp2/100)%10;  
        data2[1]=(disp2/10)%10; 
        data2[2]=disp2%10; 

        fData[5]=seg[2];
        fData[4]=SEG_G;    
        fData[3]=seg[disp1];
        fData[2]=seg[data2[0]];
        fData[1]=seg[data2[1]];    
        fData[0]=seg[data2[2]]; 
        break;
    }
      
//    uint8_t data1[3];
//    uint8_t data2[3]; 
//    uint8_t fData[6];
//    uint16_t disp2=value/10;
//    data1[1]=3; 
//    
//    data2[0]=(disp2/100)%10;  
//    data2[1]=(disp2/10)%10; 
//    data2[2]=disp2%10; 
//      
//    fData[5]=SEG_C|SEG_D|SEG_E|SEG_G;
//    fData[4]=seg[data1[1]];    
//    fData[3]=SEG_G;
//      
//    fData[2]=seg[data2[0]]|SEG_DP;
//    fData[1]=seg[data2[1]];    
//    fData[0]=seg[data2[2]]; 
      writer_buf2regi(fData);  
}     

void show_sensor(SENSOR_UPDATA sensor){
    uint8_t data1[3];
    uint8_t data2[3]; 
    uint8_t fData[6];
    uint16_t disp1=sensor.sensor.o3_ppb/10;
    uint16_t disp2=sensor.sensor.temperature;
      
    data1[0]=(disp1/100)%10; 
    data1[1]=(disp1/10)%10; 
    data1[2]=disp1%10;
    data2[0]=(disp2/100)%10;  
    data2[1]=(disp2/10)%10; 
    data2[2]=disp2%10; 
      
    fData[5]=seg[data1[0]]|SEG_DP;
    fData[4]=seg[data1[1]];    
    fData[3]=seg[data1[2]];  
    fData[2]=seg[data2[0]];
    fData[1]=seg[data2[1]]|SEG_DP;    
    fData[0]=seg[data2[2]]; 
    writer_buf2regi(fData); 
}


void Drv_display_err(void)
{
    uint8_t _seg=0x01;
    writeRegister(DIGIT0,_seg);
    writeRegister(DIGIT1,_seg);
    writeRegister(DIGIT2,_seg);
    writeRegister(DIGIT3,_seg);
    writeRegister(DIGIT4,_seg);
    writeRegister(DIGIT5,_seg);
    //writeRegister(DIGIT6,0x00);
}


void displayTest(void)
{
  writeRegister(DISP_TEST, TEST);
}

void noDisplayTest(void)
{
  writeRegister(DISP_TEST, NORMAL);
}

void writeRegister(uint8_t addr, uint8_t value) {
  uint16_t data;
  uint8_t hiByte;
  uint8_t loByte;
  hiByte=addr&0x0f;
  loByte=value;
  data=BUILD_UINT16(loByte, hiByte);
  writeRegisters(data);
}

void writeRegisters(uint16_t value) {
  Hal_Spi2_Write(value);
}


