/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "extern.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
    
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
const char* VERSION="DKF0001D00100001";            
const volatile uint8_t *userConfig = (const volatile uint8_t *)0x08000000;
//#define STM32F0XX_UDID  ((uint32_t *)0x1FFFF7E8)     // <- UID 시작 주소
//#define STM32F0XX_UDID  ((uint32_t *) 0xE0042000)     // <- UID 시작 주소
#define STM32F0XX_RESET ((uint32_t *) 0x08000000)  
#define STM32F0XX_API  ((uint32_t *) APPLICATION_ADDRESS) 

#define MAX_RX_BUF 	16

#define ARRAY_LEN(x)	(sizeof(x) / sizeof((x)[0]))

#define get_dma_data_length() huart1.hdmarx->Instance->CNDTR
#define get_dma_total_size()  huart1.RxXferSize

//#define DMA_WRITE_PTR ( (CIRC_BUF_SZ - &huart1->hdmarx->Instance->CNDTR) & (CIRC_BUF_SZ - 1) )
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

uint8_t JOB_SEQ=0;
uint8_t SEQ_CNT=0;
uint8_t MQTT_PUP=0;
 
UTIL *pUtil=NULL;
MQTT *pMqtt=NULL;    
MODEM *pModem=NULL;   
AT_PARSE *pAT=NULL;    

uint8_t rx_data;

extern uint8_t rxd;
extern uint8_t UART_FLAG;
extern uint8_t rx_buffer[128];
extern uint8_t rxSz;

void uart_rx_dma_handler();

uint8_t sub_board_check(void);

uint32_t buf[32];
char  exJob=-1;

//uint8_t message[MAX_RX_BUF];
//uint8_t dma_buf[MAX_RX_BUF + 16];

uint8_t rx_dma_circ_buf[CIRC_BUF_SZ];

uint8_t RxBuffer[MAX_RX_BUF + 16];
uint16_t wrPtr = 0;
uint16_t rdPtr = 0;
uint16_t rcvdLen = 0;
uint8_t rcvFlag = 0;
char RxBuf1[MAX_RX_BUF + 16];

uint32_t totalSentBytes = 0;
uint32_t totalRcvdBytes = 0;

uint16_t sentbytes = 0;

uint16_t _HT_Count = 0;
uint16_t _TC_Count = 0;
uint16_t _IDLE_Count = 0;

/*
#ifdef __cplusplus
extern "C" int _write(int32_t file, uint8_t *ptr, int32_t len) {
#else
int _write(int32_t file, uint8_t *ptr, int32_t len) {
#endif
    if( HAL_UART_Transmit(&huart4, ptr, len, len) == HAL_OK ) return len;
    else return 0;
}

*/

int fputc(int ch, FILE *f)
{
	uint8_t temp[1]={ch};
	HAL_UART_Transmit(&huart4, temp, 1, 2);
	return(ch);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
    uint8_t sum;
  //uint8_t buf[128];
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM3_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_IWDG_Init();
  MX_USART1_UART_Init();
  MX_SPI2_Init();
  MX_USART2_UART_Init();
  MX_UART4_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */


   FLASH_If_Init();
   
  pUtil = new UTIL();
  pAT = new AT_PARSE();    
  pMqtt= new MQTT();
  pModem = new MODEM();
  
  pUtil->util_init();
  pMqtt->mqtt_class_test();
  
  pAT->at_parse_init();
  show_sensor(o3sensor1);
  
  printf("O3 Console main!~~\r\n");  

  modem_n502l.ready=0;
  setup_data =read_config();
  sum=config_sum(setup_data);
 
//  if(sum != setup_data.sum){  
//    printf("###(1) Flash writing!!! --> sum[%02X][%02X]\r\n",sum, setup_data.sum);
//    HAL_FLASH_Unlock();
//    FLASH_If_Erase(APPLICATION_ADDRESS);
//    setup_data.sum=sum;
//    setup_data.mode=0;
//    setup_data.dest_O3=1000;
//    setup_data.dest_ON_Period=1200;
//    setup_data.dest_OFF_Period=2230;
//    writer_config(setup_data);
//    HAL_FLASH_Lock();
//  } 
  //check MEM
  for(int i=0;i<2;i++)   printf("MEM_APPLICATION[%08X][%08X]\r\n",&(*MEM_APPLICATION)+(i), MEM_APPLICATION[i]);  
  printf("###read_config -->sum[%x] mode[%d] dest_O3[%d] ON[%d] OFF[%d]\r\n",
   setup_data.sum, setup_data.mode, setup_data.dest_O3, setup_data.dest_ON_Period, setup_data.dest_OFF_Period);
   
  HAL_Delay(1000);
  Drv_as1107_init();
  init_qUart1();
  
 // __HAL_UART_DISABLE_IT(&huart1,UART_IT_RXNE); 
  pModem->modem_init();
  
  LED_ModeOn(setup_data.mode, 0xff);
  led_state.info[1].tick=0xff;//fan led_bar 50ms on
  
  //Start Stand by modem  
  //package_flag=0;
  rx_buffer_idx=0;
 
  HAL_TIM_Base_Start_IT(&htim1);
  HAL_TIM_Base_Start_IT(&htim2);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);//PWM 
  
 __HAL_IWDG_START(&hiwdg);//32-2500으로 2초 설정  
 
 // HAL_UART_Receive_IT(&huart1, &rxd, 1);//modem 대기
 
 __HAL_UART_ENABLE_IT(&huart1,UART_IT_RXNE); //From MODEM
 
  printf("START~~\r\n");
 
// __HAL_UART_ENABLE_IT(&huart1,UART_IT_IDLE);
//HAL_UART_Receive_DMA(&huart1,rx_dma_circ_buf,CIRC_BUF_SZ);

 // show_sensor(o3sensor1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
   // printf("sunvel~~\r\n");
    __HAL_IWDG_RELOAD_COUNTER(&hiwdg);
    QueueProcess();

   // HAL_UART_Receive(&huart5, (uint8_t*)rx_dma_circ_buf, 16,100);
   // printf("%s\r\n",rx_dma_circ_buf);
    
    
//    if(rcvFlag){
//      rcvFlag=0;
//        int i;
// // printf("DMA1_Channel5_IRQHandler\r\n");
//  for(i=0;i<sizeof(rx_dma_circ_buf);i++)printf("[%02d][%02X]\r\n",i,rx_dma_circ_buf[i]);
//  // __HAL_DMA_ENABLE(&hdma_usart1_rx);
// // return;
//      
//    }
    
    //uart_rx_dma_handler();
    if(JOB_SEQ)
    {
       led_state.info[1].tick=50;//fan led_bar 50ms on
 // uint8_t ok=0;
    
 // if (strstr(res, "OK") != NULL) ok=1;
     switch(JOB_SEQ)
     {
        case 1:
          sub_bd_access.CONTROL_BOX_ACCESS++;
          tx_req(0xAAA0, 0xC3);
          break;
        case 2:
        sub_bd_access.O3SENSOR_ACCESS[0]++;
        tx_req(0xB000, 0x00);          
          break;
        case 3:
        sub_bd_access.O3SENSOR_ACCESS[1]++;
        tx_req(0xB001, 0x00); 
          break; 
        case 4:
          sub_bd_access.O3SENSOR_ACCESS[2]++;
          //tx_req(0xB002, 0x00); 
           tx_req(0xAAA0, 0x00);
          break;           
      }
      gMQT_Value.sensor_cnt=sub_board_check();
      JOB_SEQ=0;
    }
    
    if(MQTT_PUP){
      MQTT_PUP=0;
      
      //pMqtt->MqttPub(gMQT_Value);
      HAL_Delay(100);
    }
  
    //pMqtt->waitMqtt();
  //  HAL_Delay(20);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
   //__HAL_UART_ENABLE_IT(&huart1,UART_IT_RXNE); 
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.Prediv1Source = RCC_PREDIV1_SOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  RCC_OscInitStruct.PLL2.PLL2State = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the Systick interrupt time
  */
  __HAL_RCC_PLLI2S_ENABLE();
}

/* USER CODE BEGIN 4 */


void UART_IDLECallback(UART_HandleTypeDef *huart)
{
	_IDLE_Count++;
	rcvFlag = 1;
}


uint8_t  sub_board_check(){
    uint8_t sensor_cnt=3;
    if(sub_bd_access.CONTROL_BOX_ACCESS >5){
      sub_bd_access.CONTROL_BOX_ACCESS=20;
    }
    if(sub_bd_access.O3SENSOR_ACCESS[0] >5){
      sensor_cnt--;
      sub_bd_access.O3SENSOR_ACCESS[0]=20;
    }
    if(sub_bd_access.O3SENSOR_ACCESS[1] >5){
       sensor_cnt--;
      sub_bd_access.O3SENSOR_ACCESS[1]=20;
    }
     if(sub_bd_access.O3SENSOR_ACCESS[2] >5){
        sensor_cnt--;
      sub_bd_access.O3SENSOR_ACCESS[2]=20;
    }
    return sensor_cnt;
}


//void HAL_SYSTICK_Callback(void)
//{
//  // HAL_GPIO_TogglePin(GPIOB, pBAT_ENABLE_Pin);
//  static uint16_t systick=0;
//  static uint8_t SEQ_CNT=0;
//  static uint8_t mqtt_sec_cnt=0;
//  systick++;
//  if(systick%10==0)  Drv_led_release();
//  if(systick>1000)//1000ms
//  {  
//    systick=0;
//    mqtt_sec_cnt++;
//    switch(SEQ_CNT){
//      case 0: JOB_SEQ=1; SEQ_CNT++; break;
//      case 1: JOB_SEQ=2; SEQ_CNT++; break;
//      case 2: JOB_SEQ=3; SEQ_CNT++; break;
//      case 3: JOB_SEQ=4; SEQ_CNT++; break;  
//      case 4: SEQ_CNT=0; break;
//    }
//  }
//  if(mqtt_sec_cnt>60*10){
//    mqtt_sec_cnt=0;
//    if(modem_n502l.ready) MQTT_PUP=1;
//  }
//
//}

//////////////////////////////////////
//  KEY PRESS with Anti-chattering
///////////////////////////////////////
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
  static uint32_t temp;
  int chattering_delay=200;
  
  if(GPIO_Pin == GPIO_PIN_0) {
    if((HAL_GetTick()-temp)>chattering_delay){
      key_map.key++;
      key_map.key=key_map.key%9;
      key_map.timeout=0;
      switch(key_map.key){
        case 0:  show_sensor(o3sensor1); break;
        case 1:  show_targetO3(123); break;  //show_fanbox(rep_fan);
        case 2:  show_telephonNumber(modem_n502l.telephon_number,0); break;    //show_sensor(o3sensor1);
        case 3:  show_telephonNumber(modem_n502l.telephon_number,1); break;   //show_sensor(o3sensor1);
        case 4:  show_telephonNumber(modem_n502l.telephon_number,2); break;   //show_sensor(o3sensor1);
        case 5:  
          LED_PressKeyShowOn(1);
          show_TemperAndHumidity(gMQT_Value.t1,  gMQT_Value.h1);
          break;
        case 6:  
          LED_PressKeyShowOn(2);
          show_TemperAndHumidity(gMQT_Value.t2,  gMQT_Value.h2);
          break;
        case 7:  
          LED_PressKeyShowOn(3);
          show_TemperAndHumidity(gMQT_Value.t3,  gMQT_Value.h3);
          break;
        case 8:  
          LED_PressKeyShowOn(0);
          show_fanbox(rep_fan);  
          break;      
      }
       //printf("EXTI0_IRQHandler\r\n");
    }
    //HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);  // 100ms Chattering
  }
  while(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)==GPIO_PIN_RESET);
  temp = HAL_GetTick();
}

#if 0

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
   
      if (huart->Instance == UART5){
   // if(rxd==0x0d) printf("rxd[%x]\r\n", rxd);
        printf("(5) HAL_UART_ErrorCallback \r\n");
  }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

  if (huart->Instance == USART1)
  {
    put_qUart1(rxd);
    HAL_UART_Receive_IT(&huart1, &rxd, 1);
  }
  //else if (huart->Instance == UART5){
   // if(rxd==0x0d) printf("rxd[%x]\r\n", rxd);
     // put_qUart1(rxd);
     // HAL_UART_Receive_IT(&huart5, &rxd, 1);
 // }
  
}
#endif

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
