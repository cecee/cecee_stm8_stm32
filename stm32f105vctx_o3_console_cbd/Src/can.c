/**
  ******************************************************************************
  * @file    can.c
  * @brief   This file provides code for the configuration
  *          of the CAN instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"

/* USER CODE BEGIN 0 */
#include "extern.h"
extern SCALE_VALUE scale_value; 
//extern POST_CAN pCan;

uint32_t main_id=0xDECEE00;
uint32_t gfilter_id=0x0CECEE00;

uint32_t TxMailbox;
/* USER CODE END 0 */

CAN_HandleTypeDef hcan1;

/* CAN1 init function */
void MX_CAN1_Init(void)
{

  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 18;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_5TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_CAN_MspInit(CAN_HandleTypeDef* canHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspInit 0 */

  /* USER CODE END CAN1_MspInit 0 */
    /* CAN1 clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* CAN1 interrupt Init */
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
  /* USER CODE BEGIN CAN1_MspInit 1 */

  /* USER CODE END CAN1_MspInit 1 */
  }
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* canHandle)
{

  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspDeInit 0 */

  /* USER CODE END CAN1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_CAN1_CLK_DISABLE();

    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);

    /* CAN1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
  /* USER CODE BEGIN CAN1_MspDeInit 1 */

  /* USER CODE END CAN1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */

uint32_t  id_change(uint32_t src) {       
  UNION_MASK src_id, dst_id;
  
  src_id.value=src;
  dst_id.b.b11=src_id.b.b00;
  dst_id.b.b12=src_id.b.b01;
  dst_id.b.b13=src_id.b.b02;
  dst_id.b.b14=src_id.b.b03;
  dst_id.b.b15=src_id.b.b04;
  dst_id.b.b16=src_id.b.b05;
  dst_id.b.b17=src_id.b.b06;
  dst_id.b.b18=src_id.b.b07;

  dst_id.b.b19=src_id.b.b08;
  dst_id.b.b20=src_id.b.b09;
  dst_id.b.b21=src_id.b.b10;
  dst_id.b.b22=src_id.b.b11;
  dst_id.b.b23=src_id.b.b12;

  dst_id.b.b27=src_id.b.b13;
  dst_id.b.b28=src_id.b.b14;       
  dst_id.b.b24=src_id.b.b15;
  dst_id.b.b25=src_id.b.b16;
  dst_id.b.b26=src_id.b.b17;   

  dst_id.b.b00=src_id.b.b18;
  dst_id.b.b01=src_id.b.b19;
  dst_id.b.b02=src_id.b.b20;
  dst_id.b.b03=src_id.b.b21;       
  dst_id.b.b04=src_id.b.b22;

  dst_id.b.b05=src_id.b.b23;
  dst_id.b.b06=src_id.b.b24;
  dst_id.b.b07=src_id.b.b25;
  dst_id.b.b08=src_id.b.b26;       
  dst_id.b.b09=src_id.b.b27; 
  dst_id.b.b10=src_id.b.b28; 

  return dst_id.value;
}

void can_start() // 수신필터를 설정하는 함수입니다
{

  CAN_FilterTypeDef sFilterConfig;
// MASK = 0x00000001 and ID = 0x00000001 will catch all odd IDs.
// MASK = 0x1FFFFFFE and ID = 0x00000002 will catch IDs 2 and 3.
  //gfilter_id=main_id;
 // gfilter_id=0x0CECEE00;
  uint32_t filter_id=id_change(gfilter_id);
  uint32_t filter_mask=id_change(0x1FFFFF00);
  
 // MASK = 0x1FFFFFFE and ID = 0x00000002
    
  //uint32_t STID=(filter_id>>18)&0x07FF;//11bit
  //uint32_t EXID=filter_id&0x03FFFF;//18bit
 
  uint32_t FilterIdHigh=((filter_id << 5)  | (filter_id >> (32 - 5))) & 0xFFFF;
  uint32_t FilterIdLow= (filter_id >> (11 - 3)) & 0xFFF8;
  
  uint32_t FilterMaskIdHigh=((filter_mask << 5)  | (filter_mask >> (32 - 5))) & 0xFFFF;
  uint32_t FilterMaskIdLow= (filter_mask >> (11 - 3)) & 0xFFF8;
  
   
  sFilterConfig.FilterBank = 0;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh =FilterIdHigh; // STID[10:0] & EXTID[17:13]
  sFilterConfig.FilterIdLow = FilterIdLow; // EXID[12:5] & 3 Reserved bits
  sFilterConfig.FilterMaskIdHigh =FilterMaskIdHigh;
  sFilterConfig.FilterMaskIdLow =FilterMaskIdLow;
  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.SlaveStartFilterBank = 14;

   HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig);


   if (HAL_CAN_Start(&hcan1) != HAL_OK)
  {
      Error_Handler();
  }

   //HAL_CAN_RxFifo0MsgPendingCallback();
  if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK){
    // Notification Error 
    Error_Handler();
  }
  
 // if (HAL_CAN_Receive_IT(&hcan2, CAN_FIFO0) != HAL_OK)
 //   {
     // Reception Error 
 //     Error_Handler();
//    } 
  
  printf("can main_id[%x]\r\n",main_id);
  
}

void put_canTxd(uint8_t id, uint8_t *data) // CAN 송신 함수입니다
{

    CAN_TxHeaderTypeDef TxHeader;
   // uint32_t txMailBox;
    
     uint8_t TxData[8];
    memcpy(TxData, data ,8);
    TxHeader.ExtId = main_id+id;
    TxHeader.RTR = CAN_RTR_DATA;
    TxHeader.IDE = CAN_ID_EXT;
    TxHeader.DLC = 8;
    TxHeader.TransmitGlobalTime = DISABLE;
    
    // printf("can tx\r\n");
    
    TxMailbox = HAL_CAN_GetTxMailboxesFreeLevel(&hcan1);
    if ((HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox))!=HAL_OK){
                  printf("can error\r\n");
                 Error_Handler();
     }   
    while (HAL_CAN_IsTxMessagePending(&hcan1, TxMailbox));
    
    
    //if (HAL_CAN_AddTxMessage(&hcan, &TxHeader, Data_1, &TxMailbox) != HAL_OK){
    //            Error_Handler();
    //        }
    // __HAL_UNLOCK(&hcan1);
    //printf("can [%02x][%02x][%02x][%02x]\r\n",TxData[0],TxData[1],TxData[2],TxData[3]);
//HAL_CAN_Transmit()
}

/*
void Hal_CAN1_writer(uint32_t can_id, uint8_t *data)
{
  printf("@@@@@CAN1_Tx(%x)(0x%02X)\r\n",can_id, data[0]);
  hcan1.pTxMsg->ExtId = can_id;//0x0cecee00;
  hcan1.pTxMsg->RTR = CAN_RTR_DATA;
  hcan1.pTxMsg->IDE = CAN_ID_EXT;
  hcan1.pTxMsg->DLC = 8;
  memset(hcan1.pTxMsg->Data,0x00,8);
  memcpy(hcan1.pTxMsg->Data, data ,8);
  do
  {
    TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
  }
  while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );
	// __HAL_UNLOCK(&hcan1);	
}
*/

//----------------------------------------------------

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan_t) // 수신인터럽트 함수입니다
{
    CAN_RxHeaderTypeDef rxHeader; 
    uint8_t RXData[8];
    memset(RXData,0,8);
    uint8_t recv_id;
    int32_t recv_raw;
    // int32_t t;
    
  if (hcan_t->Instance == CAN1) { // OK?
      //rxHeader.ExtId = 0; // clear?
      HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &rxHeader, RXData); // 반드시 -> 읽기를 합니다?
       if(gfilter_id==(rxHeader.ExtId&0xFFFFFF00)){
          //printf("gfilter_id[%08x] rxHeader.ExtId=>%08x [%02X][%02X][%02X][%02X]\n\r",gfilter_id, rxHeader.ExtId, RXData[0], RXData[1], RXData[2], RXData[3]);
          // printf("RX [%08x]\n\r", rxHeader.ExtId);
         
           recv_id = rxHeader.ExtId&0xFF;
           recv_raw=(RXData[3]<<24)+(RXData[2]<<16)+(RXData[1]<<8)+RXData[0];
           scale_value.raw_value[recv_id]=recv_raw;
          // printf("RX [%02x][%08x][%08x]\n\r",recv_id, recv_raw, t);
           
       }
   }
}


/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
