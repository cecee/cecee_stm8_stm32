#ifndef EXTERN_H_
#define EXTERN_H_


#include <stdio.h>
#include <stdint.h>
#include "string.h"
#include "stm32f1xx_hal.h"
#include "../Src/devices/drv_as1107.h"
#include "../Src/devices/queue.h"
#include "../Src/devices/config.h"
#include "../Src/devices/flash_if.h"
//#include "../Src/devices/atcommand.h"
#include "../Src/devices/util/util.h"
#include "../Src/devices/mqtt/mqtt.h"
#include "../Src/devices/at_parse/at_parse.h"
#include "../Src/devices/modem/modem.h"
#include "gpio.h"

#include <string>

#define FAN_ID    0xAA00 //AAxx
#define SENSOR_ID 0xB000 //B0xx

#define FALSE		0
#define TRUE		1
#define RET_OK		(1)
#define RET_ERR	        (0)
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)
#define ERR		1
#define WARN	     2
#define MSG		3
#define PRT		4
#define GRN		5
#define ATR		6

#define HIGH 1
#define LOW  0

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"  

#define RECOVER_DISPLAY_TIME 500
#define CIRC_BUF_SZ 	32


#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사
 
using namespace std;            
            
typedef struct _SUB_BOARD_ACCESS
{
    uint8_t  CONTROL_BOX_ACCESS;
    uint8_t  O3SENSOR_ACCESS[8];
}SUB_BOARD_ACCESS;

           
typedef struct _REQUEST   
{
  uint8_t stx;
  uint8_t size;
  uint16_t id;
  uint8_t data;
  uint8_t dumy;  
  uint8_t sum;
  uint8_t etx;
}REQUEST;

extern UTIL *pUtil;
extern MQTT *pMqtt;  
extern AT_PARSE *pAT;  
extern MODEM *pModem; 

extern SUB_BOARD_ACCESS sub_bd_access;            
extern SENSOR_UPDATA o3sensor1; 
extern SENSOR_UPDATA o3sensor2; 
extern SENSOR_UPDATA o3sensor3; 

extern uint8_t qUart1_package;
//extern uint8_t qUart1_buffer[128];

//extern UI_FND fnd;
extern FAN_DATA rep_fan;
extern SETUP_DATA setup_data;
extern KEY_MAP key_map;

extern LED_BAR led_bar;
extern LED_STATE led_state;
extern MODEM_N502L modem_n502l;
extern MQTT_PUBLISH gMQT_Value;
extern uint8_t rx_dma_circ_buf[CIRC_BUF_SZ];
//extern uint8_t rx_buffer[256];
extern uint8_t rx_buffer_idx;
extern uint8_t package_flag;
#endif
