#include  "stm8s.h"
#include <stdint.h>

#ifndef	_I2C_MASTER_H
#define	_I2C_MASTER_H


extern  bool   FLAG_I2C_ERROR;
//extern  u8   I2CADR;
//extern  GPIO_TypeDef      *I2C_master_Port;
//extern  GPIO_Pin_TypeDef  I2C_master_SDA;
//extern  GPIO_Pin_TypeDef  I2C_master_SCL;
//extern  Pin_Status  GET_GPIO_InputPin(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin);

//extern  void Delay_ms(u16 mscount);

u8  MPU6050_ReadI2C_Byte(u8 RegAddr);
extern bool MPU6050_ReadI2C_ByteN(u8 RegAddr,u8 *p_data,u8 N);
extern bool MPU6050_WriteI2C_Byte(u8 RegAddr, u8 d);
extern bool MPU6050_WriteI2C_ByteN(u8 RegAddr, u8 *d,u8 N);
int8_t 	I2Cdev_writeWord(uint8_t dev_addr, uint8_t reg_addr, uint16_t data);
int8_t I2Cdev_readBit(uint8_t dev_addr, uint8_t reg_addr, uint8_t bitn, uint8_t *data);
int8_t I2Cdev_writeBit(uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t data);
int8_t I2Cdev_readBytes(uint8_t dev_addr, uint8_t reg_addr, uint8_t len, uint8_t *data);
int8_t I2Cdev_readByte(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data);
int8_t I2Cdev_writeByte(uint8_t dev_addr, uint8_t reg_addr, uint8_t data);
int8_t I2Cdev_writeBits(uint8_t dev_addr, uint8_t reg_addr, uint8_t start_bit,
		uint8_t len, uint8_t data);
int8_t I2Cdev_readBits(uint8_t dev_addr, uint8_t reg_addr, uint8_t start_bit, 
		uint8_t len, uint8_t *data);
#endif
