#ifndef KN_MOTOR_H
#define KN_MOTOR_H

#include "stm8s_adc1.h"
#include "util.h"

//port A
#define pLED_FF_port    (GPIOA) 
#define pLED_RR         (GPIO_PIN_2) 
#define pLED_RR_port    (GPIOA) 
#define pLED_FF         (GPIO_PIN_1) 

//port B
#define pREMOCON_ADC_port    (GPIOB) 
#define pREMOCON_ADC (GPIO_PIN_2) 
#define pINT_port    (GPIOB) 
#define pINT         (GPIO_PIN_3) 
#define pMP_SCL_port (GPIOB) 
#define pMP_SCL      (GPIO_PIN_4)
#define pMP_SDA_port (GPIOB) 
#define pMP_SDA      (GPIO_PIN_5)
#define pDIRSW_FF_port (GPIOB) 
#define pDIRSW_FF      (GPIO_PIN_6)
#define pDIRSW_BB_port (GPIOB) 
#define pDIRSW_BB      (GPIO_PIN_7)

//port C
#define pPWM1_port    (GPIOC) 
#define pPWM1    (GPIO_PIN_1) 
#define pPWM2_port    (GPIOC) 
#define pPWM2    (GPIO_PIN_2) 
#define pRTN_port    (GPIOC) 
#define pRTN    (GPIO_PIN_3) 
#define pBK_port    (GPIOC) 
#define pBK   (GPIO_PIN_4)
#define pDIR_EN_port    (GPIOC) 
#define pDIR_EN   (GPIO_PIN_5)
#define pDIR_EN2_port    (GPIOC) 
#define pDIR_EN2   (GPIO_PIN_6)
#define pDIR_port    (GPIOC) 
#define pDIR   (GPIO_PIN_7)

//port D
#define pDIR2_port    (GPIOD) 
#define pDIR2    (GPIO_PIN_0) 
#define pR_SW_port    (GPIOD) 
#define pR_SW    (GPIO_PIN_2) 
#define pL_SW_port    (GPIOD) 
#define pL_SW    (GPIO_PIN_3) 
#define pBEEP_port    (GPIOD) 
#define pBEEP    (GPIO_PIN_4) 
#define pTEST_port    (GPIOD) 
#define pTEST    (GPIO_PIN_7) 

//port E
#define pRELAY_port  (GPIOE) 
#define pRELAY    (GPIO_PIN_5) 

//port F
#define pBISANG_port  (GPIOF) 
#define pBISANG    (GPIO_PIN_4) 


#define TROTTLE_OFFSET  180
//#define TROTTLE_MAX  900-TROTTLE_OFFSET
//#define TROTTLE_MIN  180-TROTTLE_OFFSET
//#define TROTTLE_GAP  TROTTLE_MAX-TROTTLE_MIN

//#define PWM_MAX  820 //1024 80%
#define ROTATION_PWM  250
#define DPDT_100  114 // (820/720)*100 


typedef struct 
{
  u8 on;
  u16 buz_on_cnt;
}BUZZER;

extern BUZZER buz;

void CF_CAR_init(void);
//void CF_CAR_Driver_direction(u8 lr, u8 fr, u8 rotate);
void CF_CAR_Driver_rotation(u8 left);
void CF_CAR_Driver_direction(u8 jenjin);

void sub_10ms(void);
//filter+
u16 moving_average_filter(u16 val, u8 idx);
u16 quantized(u16 val, u16 offset);
u16 quantized_percent(u16 val);

void buzzer_on(u8 on);
void bk_release();
//filter-

//void set_relay(u8 on);
u16 get_adc(u8 mChannel);
void get_TLP280(void);
//void get_FBdirection(void);
//void get_LRdirection(void);

u8 get_bisang(void);

//void set_direction();
void M24V_relay(u8 on);
void set_led(u8 f, u8 r);
void set_diretion_led(u8 dir);
void set_diretion_ctrl(u8 dir);

u8 get_brake(void);
void set_brake(u8 brake);
void motor_stop(int value);
void motor_stopA(int value);
void motor_stopB(int value);
//void get_handle_break(void);

#endif