#ifndef _INCLUDE_H
#define _INCLUDE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm8s.h"
#include "stm8s_type.h"
#include "stm8s_i2c.h"
#include "stm8s_gpio.h"

#include "main.h"
#include "string.h"
#include "stm8s_uart1.h"
#include "kn_motor_cf.h"
#include "kn_pwm_cf.h"
#include "util.h"
#include "MPU6050.h"
#include "I2C_Master.h"

#endif