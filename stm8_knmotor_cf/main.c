
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include.h"

#define MOTOR_CF_SPIN
#define AFTER_MSEC  300 //pwm0 이후 Stop Delay millisec  ?? 300->400ms delay
#define POWER_ON_STANDBY_MSEC 300 //power on delay


void Beep_On(void);
void CLK_Configuration(void);
void GPIO_Configuration(void);

//////////////////////

void Timer1_Init(void);
void Timer4_Init(void);
void Timer2_Init(void);
u8 i;

void delay_delay(unsigned int mul);

u16 timer4_cnt=0;
u32 timer_1ms=0;
u8  time_10ms_cnt=0;

//u16 Wait_tmp=0;
u16 Beep_Time=0;
u16  sec_cnt=0;
u8 Beep_flag=0;
u8 SET_BEEP=1;
u8 Flag_1ms=0;
u8 Flag_10ms=0;

//CURRENT_STATUS gValue;

int fputc(int ch, FILE *f)
{
  u8 temp[1]={ch};
  //HAL_UART_Transmit(&huart4, temp, 1, 2);
  CUartTxChar(temp[0]); 
  return(ch);
}

u8 get_state(){
  static u8 ex_dir=-1;
  u8 state;
  //printf("#input(%X)\r\n", gValue.input.value);
  if(gValue.output.b.standy_ok==0) return 0xA0;//state of standy-by
  
  if(gValue.input.b.trot_zero || gValue.input.b.fr==2) {
    state=0; 
  }
  else if(gValue.ex_ctrlValue>10 && gValue.input.b.fr!=2) state=1;

 if(ex_dir != gValue.input.b.fr){
    printf("change DIR [%d]\r\n",gValue.input.b.fr);
    ex_dir=gValue.input.b.fr;
    if(gValue.input.b.fr==2) {
      gValue.b4_break=gValue.ex_pwm;
      set_diretion_led(gValue.input.b.fr); 
      state=0;
    }
    else state=3; 
  }
 //rotation state
 if(gValue.stop && (gValue.input.b.Lsw || gValue.input.b.Rsw)){
#ifdef MOTOR_CF_SPIN   
  state=4;
#else
  ;
#endif  
   
 }
//-----------------------
//  if(gValue.state==0xA0) state=0xA0;
  return state;
}

void change_subroutine(u8 state){
  u16 wtime;
  printf("#state(%X)\r\n", state);
  switch(state){
    case 0://stop
      printf("#stop(%d)\r\n", gValue.b4_break);
      motor_stop(gValue.b4_break);
      set_pwm(0); 
      delay_ms(100);
      wtime=gValue.b4_break*2;
      if(wtime>AFTER_MSEC) delay_ms(AFTER_MSEC);
      else delay_ms(wtime);
      set_brake(1); //브레이크 ON
      gValue.break_state=2; 
      break;
     case 1: //run
      printf("#run(%d)(%d)\r\n", state, gValue.input.b.fr); 
      //gValue.dir=get_direction();
      get_TLP280();
      set_diretion_led(gValue.input.b.fr);
      CF_CAR_Driver_direction(gValue.input.b.fr);
      gValue.break_state=0;
      set_brake(0);//break release(해재)
      break;
      
    case 2: //fast dn
       break;
    case 3://change direction
      printf("#1 change direction[%d]\r\n",gValue.input.b.fr);
      motor_stop(gValue.ex_pwm);
      delay_ms(AFTER_MSEC);
      set_diretion_led(gValue.input.b.fr); 
      CF_CAR_Driver_direction(gValue.input.b.fr);
     // printf("#2 change direction\r\n");
      break; 
    case 4://rotation
      printf("#1 event rotation state [%d] L[%d] R[%d]\r\n", gValue.input.b.fr, gValue.input.b.Lsw, gValue.input.b.Rsw);
      set_brake(0);//break release(해재)
      gValue.stop=1;//브레이크 해재는 하였지만 정지상태로 표시함
      delay_ms(20);//브레이크 해재후 10ms후 동작 .. 바로종작하면 충돌 염려
      if(gValue.input.b.fr==0){
        if(gValue.input.b.Lsw)  CF_CAR_Driver_rotation(1);
        else if(gValue.input.b.Rsw) CF_CAR_Driver_rotation(0);
      }
      else if(gValue.input.b.fr==1){
        if(gValue.input.b.Lsw)  CF_CAR_Driver_rotation(0);
        else if(gValue.input.b.Rsw) CF_CAR_Driver_rotation(1);
      }      
      break;  
  }
 // gValue.ex_pwm=0;
}

void done_subroutine(u8 state){
    static uint16_t wait_cnt=0;
    static uint8_t toggle=0;
 // printf("done_subroutine[%x][%03d] [%03d]\r\n", state, break_cnt, gValue.wait_msec);
  switch(state){
    case 0://stop
      break;
    case 1: //run
      break;
    case 2: //fast dn
      break;
    case 3://change direction
      break;
    case 0xA0:
      if(gValue.wait_msec>POWER_ON_STANDBY_MSEC) gValue.output.b.standy_ok=1;//gValue.state=0xA1;
      break;
  }

}


void main(void)
{
  u8 state, ex_state;
  CLK_Configuration(); /* Configures clocks */
  GPIO_Configuration();/* Configures GPIOs */
 
  UART1_DeInit(); // Initialize serial
  UART1_Init(115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_ITConfig(UART1_IT_RXNE,ENABLE); //test
  M24V_relay(0);
  
  printf("(1)Start!!\r\n"); 
  Udelay(100000);//1sec
  printf("(2)car_init!!\r\n"); 
  
  M24V_relay(1);
  //mpu6050_init();
  MPU6050(0xD0);
  MPU6050_initialize();
  
  Timer2_Init();  //10ms
  Timer4_Init();  //1ms
  PWM_init(); //Timer1 PWM  
  CF_CAR_init();
  enableInterrupts();   
  
  ex_state=0xA0;
  while (1)
  {
   //printf("(2)car_init!!\r\n"); 
    gValue.state=get_state();
    if(ex_state != gValue.state){
      change_subroutine(gValue.state);
    }
    done_subroutine(gValue.state);
    ex_state=gValue.state;
    //Udelay(100);
  }
   
}

#if 1
//===================================================
//             TIMER2 1millisec Timer
//===================================================
void Timer2_Init() {
  TIM2->SR1 = 0;     // clear overflow flag
  TIM2->PSCR = 7;    // Prescaler to divide Fcpu by 128: 8 us clock.// Max CPU freq = 16 MHz
  TIM2->ARRH = 0x04; // per10msec, 16bit
  TIM2->ARRL = 0xE2;
  TIM2->IER |= 0x1;  // Update interrupt enabled
  TIM2->CR1 |= 0x1;  // Counter enabled
}

#pragma vector = 0x0F
__interrupt void Timer2_10ms(void)
{
  static u8 mpu_time=0;
  static uint16_t A0_wait=0;
  static uint8_t toggle=0;
  TIM2->SR1 = 0;
  
 // A0_wait++;
 // printf("Timer2_10ms!!\r\n"); 
  mpu_time++;
  if(gValue.state==0xA0 || gValue.output.b.bisang){
    
    if( gValue.ctrlValue>5) {
      ;//gValue.wait_msec=0;
    }
    else A0_wait++; 
    
    
    if(A0_wait>50){
      A0_wait=0;
      toggle=~toggle;
      if(toggle){
        if(gValue.output.b.bisang) set_led(1, 1);
        else set_led(1, 0);
      }
      else{
            set_led(0, 0);
          }
    }      
  }
  
 // GPIO_WriteReverse(GPIOC, pBK);
 
  if(mpu_time>200){
    mpu_time=0;
    //getMPU6050_update();
  }
  sub_10ms();
}

#endif

//==================================
//       TIMER4 1mSec Timer
//==================================
void Timer4_Init(void)
{
  TIM4->SR1 = 0;       // clear overflow flag
  TIM4->PSCR = 7;     // Prescaler to divide Fcpu by 128: 4 us clock.// Max CPU freq = 16 MHz
  TIM4->ARR = 125;    // 125*4*2 = 1000 us.
  TIM4->IER = 0x01;   // Enable interrupt
  TIM4->CR1 = 0x01;   // Start timer
}

#pragma vector = 25
__interrupt void Timer4_1ms(void)
{
 // static u8 beep_ms=0;
 if(gValue.state>=0xA0) gValue.wait_msec++; 
 if(gValue.break_state==2) break_cnt++;
 timer4_cnt++;
 PWM_1ms();
  //GPIO_WriteReverse(GPIOC, pBK);
  TIM4->SR1 = 0;      // clear overflow flag  
#if 1 
  if(timer4_cnt>9){
    timer4_cnt=0;    
    if(gValue.input.b.fr==1 && gValue.state==1){ 
      buz.buz_on_cnt++;
      if(buz.buz_on_cnt>100) buz.buz_on_cnt=0;
    }  
    else buz.buz_on_cnt=0;
    
    if(buz.buz_on_cnt>80) {
      if(!buz.on && gValue.state==1)buzzer_on(1);
    }
    else {
      if(buz.on)buzzer_on(0);
    }
  }
#endif
  //Wait_tmp++;
  //Beep_Time++;

}

#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  //u8 data;
  //data = UART1_ReceiveData8();

}

#if 0
#pragma vector=6 //5A 6B 7D 
__interrupt void EXTI_PORTB_IRQHandler(void)
{
   // disableInterrupts();
   // Udelay(5000);//20msec
   PWR_IRQ=1;

}

#pragma vector=8 //5A 6B 7D 
__interrupt void EXTI_PORTD_IRQHandler(void)
{
  //if(GPIO_ReadInputPin(GPIOD,EMER_IN)==0) EMER_CNT++;
  //else if(GPIO_ReadInputPin(GPIOD,COIN_IN)==0) {
  //	COIN_CNT++;
  //}
}
#endif

void CLK_Configuration(void)
{
  /* Fmaster = 16MHz */
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  //CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER4, DISABLE);
  //CLK_PeripheralClockConfig(CLK_Peripheral_TypeDef CLK_Peripheral, FunctionalState NewState);
  //CLK-> CKDIVR | = 0x00;
}

void GPIO_Configuration(void)
{

  GPIO_DeInit(GPIOA); 
  GPIO_Init(pLED_FF_port, pLED_FF, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(pLED_RR_port, pLED_RR, GPIO_MODE_OUT_PP_HIGH_FAST);
   
  GPIO_DeInit(GPIOB);/* GPIOB reset */
  //PB0 ->ADC0 Trottle ADC
  //PB1 ->ADC1 Analog Volume Voltage
  //PB2 ->ADC2 Analog pREMOCON_ADC
  GPIO_Init(pREMOCON_ADC_port, pREMOCON_ADC, GPIO_MODE_IN_FL_NO_IT); 
  GPIO_Init(pINT_port, pINT, GPIO_MODE_IN_PU_NO_IT);  
  GPIO_Init(pMP_SCL_port, pMP_SCL, GPIO_MODE_OUT_OD_HIZ_FAST); 
  GPIO_Init(pMP_SDA_port, pMP_SDA, GPIO_MODE_OUT_OD_HIZ_FAST); 
  
  GPIO_Init(pDIRSW_FF_port, pDIRSW_FF, GPIO_MODE_IN_PU_NO_IT); 
  GPIO_Init(pDIRSW_BB_port, pDIRSW_BB, GPIO_MODE_IN_PU_NO_IT); 


  GPIO_DeInit(GPIOC); // C-Port 
  GPIO_Init(pRTN_port, pRTN, GPIO_MODE_IN_PU_NO_IT); 
  GPIO_Init(pBK_port, pBK, GPIO_MODE_OUT_PP_LOW_FAST);   
  GPIO_Init(pDIR_EN_port, pDIR_EN, GPIO_MODE_OUT_PP_LOW_FAST); 
  GPIO_Init(pDIR_EN2_port, pDIR_EN2, GPIO_MODE_OUT_PP_LOW_FAST);  
  GPIO_Init(pDIR_port, pDIR, GPIO_MODE_OUT_PP_HIGH_FAST);

  //----------------------------------------------------------------------
  GPIO_DeInit(GPIOD); 
  GPIO_Init(pDIR2_port, pDIR2, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(pR_SW_port, pR_SW, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(pL_SW_port, pL_SW, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(pBEEP_port, pBEEP, GPIO_MODE_OUT_PP_LOW_FAST);//buz off
  GPIO_Init(pTEST_port, pTEST, GPIO_MODE_OUT_PP_HIGH_FAST);
  
  
  //---------------------------------------------------------------------
 
  GPIO_DeInit(GPIOE); 
  GPIO_Init(pRELAY_port, pRELAY, GPIO_MODE_OUT_PP_LOW_FAST);
  
  
  GPIO_DeInit(GPIOF); 
  GPIO_Init(pBISANG_port, pBISANG, GPIO_MODE_IN_PU_NO_IT);
  
#if 0
  EXTI_DeInit();    
 // EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOB, EXTI_SENSITIVITY_FALL_LOW);
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_FALL_ONLY);
  EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  
 // EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  
#endif
  
}


void delay_delay(unsigned int mul)
{
   for(; mul>0; mul--);
}









