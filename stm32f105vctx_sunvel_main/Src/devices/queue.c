/*
 * cbuffer.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "queue.h"
#include "extern.h"
#include <stdio.h>

extern SCALE_VALUE scale_value;  
//extern POST_CAN *pCan;
extern UART_HandleTypeDef huart5;

QUEUE qUart5;
uint8_t qUart5_package=0;
uint8_t qUart5_buffer[256];
uint8_t qUart5_cnt=0;
uint8_t qUart5_sz=0xff;

/*****************************************************************************
*                           Static Variables
******************************************************************************/

uint8_t rx_buffer[128];
uint8_t idx=0;
uint8_t rxSz=0;
uint8_t package_flag=0;
uint8_t UART_FLAG = 0;

/////////////////////////////////////////////
///Queue
////////////////////////////////////////////

void recvRX(uint8_t  data){
  uint8_t pack_sz=0;
  if(package_flag==0){
      if( data=='>'){
            package_flag=1;
            rxSz=0;
            idx=0;
       }
  }     
  if(package_flag){
          rx_buffer[idx++]=data;
           if(data=='@' && idx>=21 )  
           {
              package_flag=0;
              rxSz=idx;
              UART_FLAG=1;       
          }         
  }
  if(idx>26) package_flag=0;
  
 }


void init_qUart5(void){
  qUart5.front=0;
  qUart5.rear=0;
}

int put_qUart5(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart5.rear + 1) % MAX_SIZE == qUart5.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart5.rear, qUart5.front);
        return -1;
    }

    qUart5.queue[ qUart5.rear] = k;
    qUart5.rear = (qUart5.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart5.rear);
    return k;
}





int get_qUart5(void)
{
    int i=0;
     if (qUart5.front == qUart5.rear){
        return -1;
    }
    i = qUart5.queue[qUart5.front];
   qUart5.front = (qUart5.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}


int GetSerialBuf_rk(void)
{
	int  d;
	for(;;)
	{
		d =get_qUart5();
		if(d==-1 )break;
          d=d&0xff;
          if((qUart5_package==0) && (d=='>')) 
          {
             qUart5_package=1;
             qUart5_cnt=0;
             qUart5_sz=0xff;
             memset(qUart5_buffer,0,sizeof(qUart5_buffer));
          }
 	  if(qUart5_package){
               qUart5_buffer[qUart5_cnt++]=d;
              // if(qUart5_cnt==5) qUart5_sz=qUart5_buffer[4];
              // if((d=='@') && (qUart5_sz==qUart5_cnt))
               if(d=='@')               
              {
                  qUart5_package=0;
                  switch(qUart5_buffer[1]){
                      case '?':
                       // printf("SCL[%02x][%02x][%02x][%02x] cnt[%02d]\r\n",qUart5_buffer[0],qUart5_buffer[1],qUart5_buffer[2],qUart5_buffer[3], qUart5_cnt);
                         scaleValue_txToRK(scale_value);
                        break;

                      case '!':
                        // printf("LED[%02x][%02x][%02x][%02x] cnt[%02d]\r\n",qUart5_buffer[0],qUart5_buffer[1],qUart5_buffer[2],qUart5_buffer[3], qUart5_cnt);
                        // led_control(qUart5_buffer);
                          //pCan->can_tx_broadcast(qUart5_buffer);
                          can_tx_broadcast(qUart5_buffer);
                      break;
                  }
                  //txToScale();
              }
          }
    }         
    return 1;
}





void led_control(uint8_t* data)
{
  uint8_t i, tmp;
  uint8_t rgb[4];
  uint8_t led_data[32];
  uint32_t color_table[16]={
    //RRGGBBAA
    0x00000000,//WHITE-0
    0x0000FF00,//1st//0x00b5e200,//1st
    0x00FF0000,//2nd
    0xFFFF0000,//3nd
    0xFF000000,//4th
    0x00FF0000,//GREEN,-5
    0x0000FF00,//BLUE,-6
    0x00FFFF00,//CYAN,-7
    0xFF00FF00,//VIOLET,-8
    0xFFFF0000,//YELLOW,-9
    0x00808000,//TEAL,
    0x80008000,//PURPLE,
    0x80800000,//OLIVE,
    0x80000000,//MAROON,
    0x00800000,//GREEN,
    0x00008000//NAVY.
  };
  
  memcpy(led_data, data+3,20);
 // for(i=0;i<20;i++)  printf("led_data[%d][%x]\r\n",i, led_data[i]);
 // printf("tmp[%d][%x]\r\n",i, led_data[0]);
#if 1  
  for(i=0;i<16;i++){
    tmp=led_data[i]&0x0F;
    memcpy(rgb,&color_table[tmp],4);
    can_tx_led(i, rgb[3],rgb[2],rgb[1],rgb[0]);
    //pCan->can_tx_led(i, rgb[3],rgb[2],rgb[1],rgb[0]);
    HAL_Delay(10);
  }
#endif 

}
