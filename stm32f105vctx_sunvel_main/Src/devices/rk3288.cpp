#include "rk3288.h"




void RK_reset(void)
{
  printf("RK3288::reset\r\n");
  HAL_GPIO_WritePin(GPIOB, PWR_EN_SYS_Pin,GPIO_PIN_SET); 
  HAL_Delay(10);
  HAL_GPIO_WritePin(GPIOB, RES_RKCPU_Pin,GPIO_PIN_RESET); 
  HAL_Delay(2);
  HAL_GPIO_WritePin(GPIOB, RES_RKCPU_Pin,GPIO_PIN_SET); 
}



/*
void Drv_RGB_Blink(uint8_t on)
{
  if(led_rgb.blink && on){
    TIM3->CCR1 = 0;
    TIM3->CCR2 = 0;
    TIM3->CCR3 = 0;  
  }
  else{
    TIM3->CCR1 = led_rgb.g;
    TIM3->CCR2 = led_rgb.r;
    TIM3->CCR3 = led_rgb.b;   
  }
}

void Drv_led_tick(void)//10ms loop
{
   uint8_t i;
   
   for(i=0;i<8;i++)
   {
    if(led_mono[i].tick>0 && led_mono[i].tick<0xff) led_mono[i].tick--;
   }

}

//timeout 0:  0xff:forever
void Drv_led_set(uint8_t led,  uint8_t timeout, uint8_t blink)
{
  led=led & 0x07;//led 0~7���� 8ea
  led_mono[led].tick=timeout;
  led_mono[led].blink=blink;
  ui.ledValue = (led_mono[led].tick) ? ui.ledValue|led_seg[led] : ui.ledValue&~led_seg[led];
  writeRegister(DIGIT6, ui.ledValue);
}

void Drv_led_release(void)
{
  uint8_t i;
  uint8_t tmp;
  
  Drv_led_tick();
  if(led_blink.blink){
    tmp=ui.ledValue;
    for(i=0;i<8;i++) if(led_mono[i].blink) tmp = tmp & ~led_seg[i];
    writeRegister(DIGIT6, tmp);
    return;
  }
  
  for(i=0;i<8;i++) if(led_mono[i].tick==0) ui.ledValue = ui.ledValue & ~led_seg[i];
  writeRegister(DIGIT6, ui.ledValue);
  
}

*/

