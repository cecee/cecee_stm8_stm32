#include "post_can.h"

extern UART_HandleTypeDef huart5;

void can_tx_led(uint8_t id, uint8_t r, uint8_t g, uint8_t b, uint8_t blink)
{
  uint8_t can_tx[8];
  memset(can_tx,0,8);
  //한꺼번에 다 writer .. 8byte 각각 4bit color로 
  can_tx[0]=r;
  can_tx[1]=g;
  can_tx[2]=b;
  can_tx[3]=blink;
  put_canTxd(id, can_tx);
}

int can_tx_broadcast(const uint8_t* data)
{
  int i;
  uint8_t led_data[32];
  uint8_t tx_buf[8];
  uint8_t hi,lo;
  memcpy(led_data, data+3,20);
  if(led_data[16]!='@') return RET_ERR;
 
  for(i=0;i<8;i++){
    hi=((led_data[(i*2)+1]&0x0F)<<4);
    lo=led_data[(i*2)+0]&0x0F;
    tx_buf[i]=  hi + lo;
    //printf("tx_buf[%d][%02x]-h[%02x] l[%02x]\r\n",i,tx_buf[i],hi,lo);
  }
  put_canTxd(0, tx_buf);
  return RET_OK;
}

void scaleValue_txToRK(const SCALE_VALUE s_value )
{
  uint8_t i;
  uint8_t t_buf[80]={0,};
  uint8_t checksum;
  memcpy(t_buf+1,&s_value,64);//16개 load cell 각 4byte =64 byte
  
 
  t_buf[0]='<';  t_buf[65]='@';
  checksum=0;
  for(i=0;i<65;i++){
    checksum=checksum + t_buf[i]&0xFF;
  }
  t_buf[66]=checksum&0XFF;
  //printf("checksum[%02x]\r\n",checksum);
  HAL_UART_Transmit(&huart5, t_buf, 68, 50); 
  //HAL_Delay(10);
}


