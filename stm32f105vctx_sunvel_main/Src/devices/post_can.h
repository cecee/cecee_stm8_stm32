#ifndef POST_CAN_H
#define POST_CAN_H
#include "extern.h"

typedef struct _LED_RGB
{
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t blink;  
}LED_RGB;   

typedef struct _SCALE_VALUE
{
   int32_t raw_value[16]; 
   LED_RGB led[16];
}SCALE_VALUE;


void can_tx_led(uint8_t id, uint8_t r, uint8_t g, uint8_t b, uint8_t blink);
int can_tx_broadcast(const uint8_t* data);
void scaleValue_txToRK(const SCALE_VALUE s_value);


#endif 
