#ifndef QUEUE_H
#define QUEUE_H
#include "extern.h"

#define MAX_SIZE 512 // max size of queue
typedef struct _queue   
{
	int queue[MAX_SIZE+1];
	int front;
	int rear;
}QUEUE;

void init_qUart5(void);
int put_qUart5(int k);
int get_qUart5(void);
int GetSerialBuf_rk(void);
void led_control(uint8_t* data);
void recvRX(uint8_t data);

#endif 
