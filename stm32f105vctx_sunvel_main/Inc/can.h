/**
  ******************************************************************************
  * @file    can.h
  * @brief   This file contains all the function prototypes for
  *          the can.c file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_H__
#define __CAN_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern CAN_HandleTypeDef hcan1;

/* USER CODE BEGIN Private defines */
typedef struct tag_cctrlBit
{
	uint32_t b00: 1;
	uint32_t b01: 1;
	uint32_t b02: 1;
	uint32_t b03: 1;
	uint32_t b04: 1;
	uint32_t b05: 1;
	uint32_t b06: 1;
	uint32_t b07: 1;
	uint32_t b08: 1;	
	uint32_t b09: 1;	
	uint32_t b10: 1;	
	uint32_t b11: 1;	
	uint32_t b12: 1;	
	uint32_t b13: 1;	
	uint32_t b14: 1;			
	uint32_t b15: 1;
	uint32_t b16: 1;
	uint32_t b17: 1;
	uint32_t b18: 1;
	uint32_t b19: 1;
	uint32_t b20: 1;
	uint32_t b21: 1;
	uint32_t b22: 1;
	uint32_t b23: 1;
	uint32_t b24: 1;	
	uint32_t b25: 1;	
	uint32_t b26: 1;	
	uint32_t b27: 1;	
	uint32_t b28: 1;	
	uint32_t b29: 1;	
	uint32_t b30: 1;			
	uint32_t b31: 1;        
}BIT_MADK;

typedef union _UNION_MASK
{
	uint32_t value;
	BIT_MADK b;
}UNION_MASK;


typedef struct {
   uint8_t rxData[8]; // 수신버퍼
   uint8_t txData[8]; // 송신버퍼
} CAN_User_InitTypeDef;


/* USER CODE END Private defines */

void MX_CAN1_Init(void);

/* USER CODE BEGIN Prototypes */
void can_start();
void put_canTxd(uint8_t id, uint8_t *data);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan_t);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __CAN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
