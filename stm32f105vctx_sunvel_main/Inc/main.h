/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define nRST_NFC_Pin GPIO_PIN_0
#define nRST_NFC_GPIO_Port GPIOC
#define nRST_IOT_Pin GPIO_PIN_1
#define nRST_IOT_GPIO_Port GPIOC
#define nRST_MOD_Pin GPIO_PIN_2
#define nRST_MOD_GPIO_Port GPIOC
#define POWER_LED_Pin GPIO_PIN_0
#define POWER_LED_GPIO_Port GPIOB
#define WORK_LED_Pin GPIO_PIN_1
#define WORK_LED_GPIO_Port GPIOB
#define pM_PWR_Pin GPIO_PIN_2
#define pM_PWR_GPIO_Port GPIOB
#define pIOT_PWR_Pin GPIO_PIN_7
#define pIOT_PWR_GPIO_Port GPIOE
#define PMU_GPIO_Pin GPIO_PIN_12
#define PMU_GPIO_GPIO_Port GPIOB
#define RES_RKCPU_Pin GPIO_PIN_13
#define RES_RKCPU_GPIO_Port GPIOB
#define PWR_EN_SYS_Pin GPIO_PIN_14
#define PWR_EN_SYS_GPIO_Port GPIOB
#define PWR_EN_Pin GPIO_PIN_15
#define PWR_EN_GPIO_Port GPIOB
#define pDIMMER_Pin GPIO_PIN_6
#define pDIMMER_GPIO_Port GPIOC
#define GPIO1_Pin GPIO_PIN_7
#define GPIO1_GPIO_Port GPIOC
#define GPIO2_Pin GPIO_PIN_8
#define GPIO2_GPIO_Port GPIOC
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
