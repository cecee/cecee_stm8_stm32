#ifndef EXTERN_H_
#define EXTERN_H_


#include <stdio.h>
#include <stdint.h>
#include "string.h"
#include "stm32f1xx_hal.h"
#include "../Src/devices/rk3288.h"
#include "../Src/devices/post_can.h"
#include "../Src/devices/queue.h"


#include "gpio.h"
#include "can.h"

#define LOGOUT
#define FALSE		0
#define TRUE		1
#define RET_OK		(0)
#define RET_ERR	(-1)
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)
#define ERR		1
#define WARN	     2
#define MSG		3
#define PRT		4
#define GRN		5
#define ATR		6

#define HIGH 1
#define LOW  0

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))


//extern TIM_HandleTypeDef htim1;
//extern ADC_HandleTypeDef hadc1;

#endif
