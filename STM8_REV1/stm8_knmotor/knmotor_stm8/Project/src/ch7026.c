#include "stm8s.h"
#include "ch7026.h"
#include "stm8s_gpio.h"

extern void _printf(u8 *pFmt, u32 wVal);


u8 I2CWrite(u8 dev, u8 reg, u8 val)
{

	u8 Temp;
	u16 i;
	u8 dev_id;
	
	if(dev==0) dev_id = CH7026A_ADDR;
	else dev_id = CH7026B_ADDR;
		   
	I2C_START();
	I2C_9BIT_WR(dev_id & 0xfe );
	I2C_9BIT_WR(reg);
	I2C_9BIT_WR(val);
	I2C_STOP();
	return 0;
}

u8 i2c_write_byte(u8 Addr, u8 Reg, u8 Data)
{
 //   I2C_ITConfig((I2C_IT_TypeDef)(I2C_IT_EVT) , ENABLE); 
   u8 Temp;
   u16 i;
   _printf("i2c_write_byte OK\r\n",0);		
 	I2C->CR2 |= 0x01;        //I2C Start Condition
//		while((I2C->SR1 & 0x01) == 0);  //Start condition generated
 		for(i=0;i<10000;i++) if((I2C->SR1 & 0x01) != 0) break;
 		if(i>9000) return 0;


  	I2C->DR = Addr;         //Device address(write)
 //		while((I2C->SR1 & 0x02) == 0); //Address sent  //Ack Check;
 		for(i=0;i<10000;i++)
 		{
// 		 _printf("I2C->SR1[%x]\r\n",I2C->SR1);

 		 if((I2C->SR1 & 0x02) != 0) break;
 		}
 		if(i>9000) return 1;
 		Temp = I2C->SR3;        //Status register

		 I2C->DR = Reg;         //Device register
//	while((I2C->SR1 & 0x80) == 0); //Data register empty (transmitters)
 		for(i=0;i<10000;i++)
 		{
 		 if((I2C->SR1 & 0x80) != 0) break;
 		}
		if(i>9000) return 2;

 	
		 I2C->DR = Data;         //Device data
//	 while((I2C->SR1 & 0x80) == 0); //Data register empty (transmitters)
 		for(i=0;i<10000;i++)
 		{
 		 if((I2C->SR1 & 0x80) != 0) break;
 		}
		if(i>9000) return 3;
		
		 I2C->CR2 |= 0x02;        //I2C Stop Condition
 		 i = 0xFFFF;
		 while(i--);
// _printf("i2c_write_byte OK\r\n",0);		
		return 4;
}

u8 I2C_ReadByte(u8 dev, u8 Reg)
{
//	unsigned char Temp;//, Byte_Read;
	u8 tmp;
	u8 Byte_Read;
	u8 dev_id;
	
	if(dev==0) dev_id=CH7026A_ADDR;
	else dev_id=CH7026B_ADDR;
		
	I2C_START();
  I2C_9BIT_WR(dev_id&0xfe);//read dev add
  I2C_9BIT_WR(Reg);//read reg add
	I2C_RESTART();
  I2C_9BIT_WR(dev_id|0x01);//read dev add
	Byte_Read=I2C_9BIT_RD();
	I2C_STOP();
	return Byte_Read;
}

void init_ch7026()
{
	u8 i;
	u8 sZ;
	//YPbPr
	u8 init_YPbPr[][2]=
	{
		{ 0x02, 0x01 },
		{ 0x02, 0x03 }, //reset
		{ 0x03, 0x00 },
		{ 0x04, 0x39 },//power
		{ 0x09, 0x80 },
		{ 0x0A, 0x10 },
		{ 0x0D, 0x49 },	//Output Video Format	
		{ 0x0E, 0xE0 },	
		{ 0x0F, 0x1B },
		{ 0x10, 0x20 },
		{ 0x11, 0x84 },
		{ 0x12, 0xC0 },
		{ 0x13, 0x28 },
		{ 0x14, 0x28 },
		{ 0x15, 0x11 },
		{ 0x16, 0xE0 },
		{ 0x17, 0x0D },
		{ 0x18, 0x00 },
		{ 0x19, 0x0D },
		{ 0x1A, 0x0C },
		{ 0x1B, 0x35 },
		{ 0x1C, 0x00 },
		{ 0x1D, 0x72 },
		
		{ 0x20, 0x10 },		
		{ 0x21, 0x12 },
		{ 0x22, 0xD0 },
		{ 0x23, 0xEE },
		{ 0x24, 0x00 },		
		{ 0x25, 0x0A },
		{ 0x26, 0x03 },
		{ 0x27, 0x08 },
		{ 0x28, 0x01 },
		{ 0x29, 0x40 },
////////////////////////		
		{ 0x33, 0x07 },
		{ 0x34, 0xB7 },
		{ 0x35, 0x07 },
		{ 0x36, 0x4c },
////////////////////////										
		{ 0x4D, 0x03 },
		{ 0x4E, 0x18 },
		{ 0x4F, 0x00 },
		{ 0x50, 0x00 },
		
		{ 0x51, 0x4d },
		{ 0x52, 0x1b },
		{ 0x53, 0x0A },
		{ 0x55, 0xE5 },
		{ 0x56, 0x80 },
				
		{ 0x5C, 0x20 },
		{ 0x5D, 0x40 },		
		{ 0x5E, 0x80 },
		{ 0x69, 0x64 },
		{ 0x75, 0xA3 },		//clk_inv
		{ 0x7D, 0x62 },
		
		{ 0x04, 0x38 },
		{ 0x06, 0x71 },	
		
		{ 0x03, 0x00 },
		{ 0x03, 0x00 },
		{ 0x03, 0x00 },
		{ 0x03, 0x00 },
		{ 0x03, 0x00 },
		
		{ 0x06, 0x70 },
		{ 0x02, 0x02 },
		{ 0x02, 0x03 },
		{ 0x04, 0x00 } //ALL Power ON

//		{ 0x0A, 0x12 }, //DAC0 DAC1 DAC2
//		{ 0x0A, 0x10 }
						
	//	{0x03, 0x01}, //test pattern
	//	{0x04, 0x34}
		
	};
	
	u8 init_CVBS[][2]=
	{
		{0x02, 0x01},
		{0x02, 0x03},
		{0x03, 0x00},
		{0x04, 0x39},
		{0x0A, 0x11},//DAC CS DACSP
		{0x0F, 0x1B},
		{0x10, 0x20},
		{0x11, 0x84},
		{0x12, 0x40},
		{0x13, 0x2E},
		
		{0x14, 0x28},
		{0x15, 0x11},
		{0x16, 0xE0},	
		{0x17, 0x0D},
		{0x18, 0x00},
		{0x19, 0x0D},
		{0x1A, 0x0C},	

		{0x1B, 0x35},
		{0x1C, 0x8c},	
		{0x1D, 0xB4},	


		{0x1f, 0x40},	
		{0x20, 0x10},	
		{0x21, 0x11},	
		{0x22, 0xE0},
		{0x23, 0x0d},	
		{0x24, 0x00},	
		{0x25, 0x20},	
		{0x26, 0x03},		

		{0x27, 0x08},
		{0x28, 0x01},	
		{0x29, 0x40},	
		{0x2A, 0x08},	
		{0x2B, 0x01},	
		{0x2C, 0x61},	

		{0x2E, 0x34},		//hue
		{0x2F, 0x3D},		//saturation
		{0x30, 0x4c},		//contrast
		{0x31, 0x76},		//brightness		
		
												
//////////////////////// <->	& ����	
		{ 0x33, 0x07 },//����
		{ 0x34, 0xEF },
		{ 0x35, 0x07 },//<->
		{ 0x36, 0xA5 },
////////////////////////			
	
//		{ 0x37, 0x1d },//AX+B
//		{ 0x38, 0x00 },//AX+B default:00
//		{ 0x39, 0x20 },//AX+B default:20
//		{ 0x3A, 0x00 },//AX+B default:00
	
		
		{0x4D, 0x04},
		{0x4E, 0x80},
		{0x4F, 0x00},
	
		{0x50, 0x00},	
		{0x51, 0x4d},
		{0x52, 0x1b},
		{0x53, 0x1a},
		{0x55, 0xE5},	
		{0x5E, 0x80},
		{0x69, 0x64},
		{ 0x75, 0xA3 },		//clk_inv
		
		{0x7D, 0x62},
		{0x06, 0x70},		
		{0x03, 0x00},
	
		{0x03, 0x00},
		{0x03, 0x00},
		{0x03, 0x00},		
		{0x03, 0x00},	
		{0x06, 0x70},
		{0x02, 0x02},
		{0x02, 0x03},		
		{0x04, 0x00}
		//,
		//		{0x03, 0x01}, //test pattern
		//{0x04, 0x34}
	};

	sZ= sizeof(init_YPbPr);
	 _printf("i2c_ init_YPbPr size[%d]\r\n", sZ);	
	for(i=0;i<sZ/2;i++){
		I2CWrite( 0, init_YPbPr[i][0], init_YPbPr[i][1] );
	}

	sZ= sizeof(init_CVBS);	
	for(i=0;i<(sZ/2);i++){
		I2CWrite( 1, init_CVBS[i][0], init_CVBS[i][1] );
	}
// I2CWrite( 0x03, 0x01 );
// I2CWrite( 0x04, 0x34 );
};

void udelay(u32 dd)
{
  u32 i,j;
  for(i=0;i<dd;i++){for(j=0;j<3;j++){}
	}
}

void I2C_CLOCK()
{
	GPIO_WriteHigh(GPIOB, SCL);	udelay(1);
	GPIO_WriteLow(GPIOB, SCL);
}

void I2C_START()
{
	//GPIO_WriteReverse(GPIOB, SCL);
	//GPIO_WriteReverse(GPIOB, SDA);
	GPIO_WriteHigh(GPIOB, SCL);	
 	GPIO_Init(GPIOB, SDA, GPIO_MODE_IN_PU_NO_IT);   //SDA_high
	//GPIO_WriteHigh(GPIOB, SDA);	
	udelay(4);
	GPIO_Init(GPIOB, SDA, GPIO_MODE_OUT_PP_LOW_FAST);   //SDA_low
//	GPIO_WriteLow(GPIOB, SDA);	udelay(2);
	GPIO_WriteLow(GPIOB, SCL);udelay(2);
 //	GPIO_Init(GPIOB, SDA, GPIO_MODE_IN_PU_NO_IT);   //SDA_high
	
	
}

void I2C_STOP()
{
	//GPIO_WriteReverse(GPIOB, SCL);
	//GPIO_WriteReverse(GPIOB, SDA);
//	GPIO_WriteLow(GPIOB, SDA);	
	GPIO_Init(GPIOB, SDA, GPIO_MODE_OUT_PP_LOW_SLOW);   //SDA_low

	udelay(1);	
	GPIO_WriteHigh(GPIOB, SCL);	
	udelay(1);	
	//GPIO_WriteHigh(GPIOB, SDA);	
	GPIO_Init(GPIOB, SDA, GPIO_MODE_IN_PU_NO_IT);   //SDA_high

//	GPIO_WriteHigh(GPIOB, SCL);udelay(2);
}
void I2C_RESTART()
{
	//GPIO_WriteReverse(GPIOB, SCL);
	//GPIO_WriteReverse(GPIOB, SDA);
	GPIO_WriteHigh(GPIOB, SDA);	
	GPIO_WriteHigh(GPIOB, SCL);	
	udelay(1);
	GPIO_WriteLow(GPIOB, SDA);	udelay(1);
	GPIO_WriteLow(GPIOB, SCL);udelay(1);
}



void I2C_9BIT_WR(u8 dev_add)
{
 u8 i;
 u8 SR=dev_add;
 //	_printf("i2c_ dev_add[%x]\r\n", dev_add);	
  	udelay(1);
 for(i=0;i<8;i++){
 	// GPIO_Init(GPIOB, SDA, GPIO_MODE_IN_PU_NO_IT);   //SDA
	//	GPIO_Init(GPIOB, SDA, GPIO_MODE_OUT_PP_LOW_SLOW);   //SDA

	if((SR&0x80)==0) {
		GPIO_Init(GPIOB, SDA, GPIO_MODE_OUT_PP_LOW_SLOW);   //SDA
	}
 	else 	{
 		GPIO_Init(GPIOB, SDA, GPIO_MODE_IN_PU_NO_IT);   //SDA
 	}
 	SR=SR<<1;	
 	I2C_CLOCK(); 	
 	udelay(1);
	
 }
 
// udelay(1);
 //GPIO_WriteHigh(GPIOB, SDA);
 //GPIO_Init(GPIOB, SDA, GPIO_MODE_IN_PU_NO_IT);   //SDA
 udelay(2);
 I2C_CLOCK();	//for ack
 //GPIO_Init(GPIOB, SDA, GPIO_MODE_OUT_PP_HIGH_SLOW);   //SDA
 //GPIO_WriteHigh(GPIOB, SDA);
 
}

u8 I2C_9BIT_RD()
{
 u8 i;
 u8 SR=0;
 //	_printf("i2c_ dev_add[%x]\r\n", dev_add);	
 GPIO_Init(GPIOB, SDA, GPIO_MODE_IN_PU_NO_IT);   //SDA
 for(i=0;i<8;i++){
  SR=SR<<1;	
	if(GPIO_ReadInputPin(GPIOB,SDA)) SR=SR|0x01;
 	I2C_CLOCK(); 	
 	udelay(1);
 }
 udelay(2);
 I2C_CLOCK();	//for ack
 GPIO_Init(GPIOB, SDA, GPIO_MODE_OUT_PP_HIGH_FAST);   //SDA
  return SR;
}
