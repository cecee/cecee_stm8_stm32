#include "kn_pwm.h"

u16 TIM1_Period=1023;//10000;//1023;

void PWM_init(void)
{
          
  u16 pwm_init=0;
  
  TIM1->CR1 &= ~ BIT(0); // Close TIM1 
  TIM1->PSCRH = 0; 
  TIM1->PSCRL = 0; // divide-

  TIM1->ARRH = TIM1_Period>>8; 
  TIM1->ARRL = TIM1_Period&0x00ff;

  TIM1->CCR1H = pwm_init>>8;
  TIM1->CCR1L = pwm_init&0x00ff; //
  TIM1->CCR2H = pwm_init>>8;
  TIM1->CCR2L = pwm_init&0x00ff; // 
  
  
  TIM1->CCMR1 = 0x60; //  CH1 PWM mode 1
  TIM1->CCMR2 = 0x60; //  CH2 PWM mode 1
  
  TIM1->CCER1 |= BIT(0); //  Enable OC1(CH1)
  TIM1->CCER1 |= BIT(4); //  Enable OC2(CH2)

  TIM1->CR1 |= BIT(0); //  enable TIM1 
  TIM1->BKR |= BIT(7); //  prohibit brake 
  
 // ---------------------------------
}

#if 0
void PWM_4ch_init(void)
{
	
  TIM1->CR1 &= ~ BIT(0); // Close TIM1 
  TIM1->PSCRH = 0; 
 	TIM1->PSCRL = 0; // divide-

	TIM1->ARRH = TIM1_Period>>8; 
	TIM1->ARRL = TIM1_Period&0x00ff;
 
	TIM1->CCR1H = CCR_VAL.iccr[0]>>8;
	TIM1->CCR1L = CCR_VAL.iccr[0]&0x00ff; //
	TIM1->CCMR1 = 0x60; //  PWM mode 1
	TIM1->CCER1 |= BIT(0); //  Enable OC1
 
	TIM1->CCR2H = CCR_VAL.iccr[1]>>8;
	TIM1->CCR2L = CCR_VAL.iccr[1]&0x00ff; //
	TIM1->CCMR2 = 0x60; //  PWM mode 1
	TIM1->CCER1 |= BIT(4); //  Enable OC2
 
//	TIM1->CCR3H = CCR_VAL.ccr[2]>>8;
//	TIM1->CCR3L = CCR_VAL.ccr[2]&0x00ff; //
//	TIM1->CCMR3 = 0x60; //  PWM mode 1
//	TIM1->CCER2 |= BIT(0); //  enable OC3
        
//  TIM1->CCR4H = CCR_VAL.ccr[3]>>8;
//	TIM1->CCR4L = CCR_VAL.ccr[3]&0x00ff; //
//	TIM1->CCMR4 = 0x60; //  PWM mode 1
//	TIM1->CCER2 |= BIT(4); //  enable OC4

	
	TIM1->CR1 |= BIT(0); //  enable TIM1 
	TIM1->BKR |= BIT(7); //  prohibit brake 
}
#endif

void _PWM_init(void)
{
          
  u16 pwm_init=0;
  
  TIM1->CR1 &= ~ BIT(0); // Close TIM1 
  TIM1->PSCRH = 0; 
  TIM1->PSCRL = 0; // divide-

  TIM1->ARRH = TIM1_Period>>8; 
  TIM1->ARRL = TIM1_Period&0x00ff;

  TIM1->CCR1H = pwm_init>>8;
  TIM1->CCR1L = pwm_init&0x00ff; //
  TIM1->CCMR1 = 0x60; //  PWM mode 1
  TIM1->CCER1 |= BIT(0); //  Enable OC1

  TIM1->CR1 |= BIT(0); //  enable TIM1 
  TIM1->BKR |= BIT(7); //  prohibit brake 
}



void set_pwm(u16 pwm)
{
  
  u16 pwm1, pwm2;
  if (pwm==0) pwm=1;
  
  if(gValue.dir) {
    pwm1=1;
    pwm1=pwm;
    pwm2=pwm;
  }
  else{
    pwm1=pwm;
    pwm2=1;
    pwm2=pwm;
  }
  
  TIM1->CCR1H = pwm1>>8;
  TIM1->CCR1L = pwm1&0x00ff; //
  
  TIM1->CCR2H = pwm2>>8;
  TIM1->CCR2L = pwm2&0x00ff; //
}


