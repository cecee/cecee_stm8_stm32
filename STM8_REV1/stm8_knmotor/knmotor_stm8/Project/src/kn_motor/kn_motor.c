#include "stm8s.h"
#include "stm8s_gpio.h"
#include "kn_motor.h"
#include "../kn_pwm/kn_pwm.h"
#include <stdio.h>
#include <string.h>
 
#define RR_PERCENT 70  //후진 퍼센트
#define A_NORRMAL 1     //normal :기울기 y=ax+b 의 a
#define A_STOPING  2    //normal :기울기 y=ax+b 의 a
#define B_OFFSET   180L //offset :y=ax+b 의 b 180L=>0
#define AFTER_MSEC   200 //pwm0 이후 Stop Delay millisec  ?? 50->100ms delay
#define SECOND_POLL 60 //Stop시 기울기가 변화는 Point  value
#define FET_MAX  1000L //
#define TROTTLE_MAX  870L //


CURRENT_STATUS gValue;
u16 bank[4][6];

u8 BREAK_FLAG=0;
int break_cnt=0;

u8 DIRECTION_FLAG=0;
u16 direction_delay_cnt=0;
u16 direction_delay_ref=0;

void car_init(void)
{
  gValue.seqStatus=0;
  gValue.dir=2;
  gValue.stop=1;
  gValue.pwm=0;
  gValue.msec=0;
  //gValue.r_percent=70;
  GPIO_WriteHigh(GPIOC, pDIS);
  
}

void sub_1ms(void)
{
  gValue.msec++;
  
  if(gValue.seqStatus==0 && gValue.msec>500 ) {
    gValue.seqStatus=1;
  }
  
  if(BREAK_FLAG){
      break_cnt++;
      if(break_cnt>AFTER_MSEC){
        BREAK_FLAG=0;
        break_cnt=0;
        set_brake(1); //브레이크 ON
      }
  }
  set_PWM();
}

void sub_10ms(void)
{
 
  u16 raw0,raw1;//,raw2,raw3;
  u16 lpercent;//0~100
  raw0 = get_adc(0);//trottle
  raw1 = get_adc(1);//ff percent
 // raw3 = get_adc(3);//release
    get_direction();
    
    if(DIRECTION_FLAG==1){
 //     direction_delay_cnt++;
      raw0=0;
      //printf("direction_delay_ref[%d] cnt [%d] [%04d][%04d]\r\n",direction_delay_ref, direction_delay_cnt, gValue.f_control, gValue.ex_pwm);
     
      if( gValue.ex_pwm==0){
        direction_delay_cnt++;
        if(direction_delay_cnt>(direction_delay_ref/10)+0){
            DIRECTION_FLAG=0;
            direction_delay_cnt=0;
            direction_delay_ref=0;
            set_direction();       
        }
       
      }
    }
  
    gValue.f_raw=moving_average_filter(raw0,0);
    lpercent =moving_average_filter(raw1,1);
    gValue.f_percent =quantized_percent(lpercent);
    gValue.f_control=quantized(gValue.f_raw, B_OFFSET);  
#ifdef DEBUG
 // printf("msec[%04d] f_control[%04d] [%03d] [%04d]\r\n",gValue.msec, gValue.f_control, gValue.f_percent, gValue.ex_pwm);
 
  //  printf("[%d]  gValue.f_raw[%04d] f_control[%04d] [%03d] [%04d]\r\n",gValue.seqStatus, gValue.f_raw,  gValue.f_control, gValue.f_percent, gValue.ex_pwm);

#endif  
  //BREAK_REF = quantized_percent(gValue.r_percent);
  bk_release();
  
  //if(gValue.seqStatus==0
  
    if(gValue.seqStatus) set_relay(1);
    else set_relay(0);
  
     if(gValue.seqStatus==1 &&  gValue.f_control==0) 
    {
       gValue.seqStatus=2;
       gValue.ex_pwm=0;
       GPIO_WriteLow(GPIOC, pDIS);
    }
     if(gValue.seqStatus<2) 
    {
       gValue.ex_pwm=0;
    }
 // if(gValue.seqStatus==0 && gValue.msec>1000  ) {
   // gValue.seqStatus=1;
  //  GPIO_WriteLow(GPIOC, pDIS);
//  }

}

void  get_direction(void)
{
  u8 dir=0;
  u8 ff=0, rr=0;
  ff = GPIO_ReadInputPin(GPIOB, DIR_FF);
  rr = GPIO_ReadInputPin(GPIOB, DIR_RR);
  
   if(ff ==0) dir=1;//rr
   else dir =0;
   /*
  
  if(ff && rr==0) dir=1;//rr
  else if (ff==0 && rr) dir =0;//ff
  else dir =2;
  */
  if(dir==2) dir=1;
  if(dir != gValue.dir && DIRECTION_FLAG==0 )  {
    DIRECTION_FLAG=1;
    direction_delay_ref=gValue.ex_pwm;
    gValue.dir=dir;
  }
}

void set_direction()
{
  diretion_LED(gValue.dir);
  diretion_MOTOR(gValue.dir);
}






u16 moving_average_filter(u16 val, u8 idx)
{
  u16 mbank[6]={0,};
//  u16 lbank[4]={0,};
  u32 sum;
  u16 avr;
  u8 i;
 
 // memcpy(&lbank,&bank[idx],sizeof(mbank));
 // memcpy(mbank,lbank+2,6);
    
  mbank[0]=bank[idx][1];
  mbank[1]=bank[idx][2];
  mbank[2]=bank[idx][3];
  mbank[3]=bank[idx][4];
  mbank[4]=bank[idx][5];
  
  mbank[5]=val;
  sum=mbank[0]+mbank[1]+mbank[2]+mbank[3]+mbank[4]+mbank[5];
  avr=sum/6L;
  
  memcpy(bank[idx],mbank,sizeof(mbank));
  
//  for(i=0;i<4;i++) bank[idx][i]=mbank[i];
  //memcpy(bank,mbank,4);
  if(avr>1024L)avr=1024L;
  return avr;
}

u16 quantized(u16 val, u16 offset)
{
  u16 result;
  u16 lvaluet;
  u16 delta;
  if(val<=offset) return 0;
  lvaluet=val-offset;
  delta= TROTTLE_MAX- offset;
  
  result=(lvaluet*1024L)/(delta);
  
  //result=(val*1024L*10)/(delta*10);
  result=(result>1020L) ? 1024L: result;
  return result;
}

u16 quantized_percent(u16 val)
{
  u16 result;
  result=(val*1000L)/10240L;
  result=(result>97L) ? 100L: result;
  return result;
}

void bk_release()
{
  if(BREAK_FLAG==0 && (gValue.f_control>10 || gValue.ex_pwm>0) ) {
    set_brake(0);//break release(해재)
    return;
  }
}


void set_relay(u8 on)
{
  if(on) GPIO_WriteHigh(GPIOD, pRELAY);
  else  GPIO_WriteLow(GPIOD, pRELAY);  
}



void set_brake(u8 brake)
{
  //_printf("set_brake[%d]",brake);
  if(brake) GPIO_WriteLow(GPIOC, pBK);
  else GPIO_WriteHigh(GPIOC, pBK); 
  gValue.stop=brake;
}


u8 get_brake(void)
{
  u8 bk;
  bk = GPIO_ReadInputPin(GPIOC, pRTN);
  if(gValue.pwm==0) bk=1;
  return bk;
}

void diretion_LED(u8 dir)
{
  switch(dir){
    case 0:
      GPIO_WriteLow(GPIOD, pLED_FF);
      GPIO_WriteHigh(GPIOD, pLED_RR);
      break;
    case 1:
      GPIO_WriteHigh(GPIOD, pLED_FF);
      GPIO_WriteLow(GPIOD, pLED_RR);
      break;  
    case 2:
      GPIO_WriteHigh(GPIOD, pLED_FF);
      GPIO_WriteHigh(GPIOD, pLED_RR);
      break;   
  }
}


void diretion_MOTOR(u8 dir)
{
  switch(dir){
    case 0://ff
      GPIO_WriteLow(GPIOC, pDIR_FF);
      GPIO_WriteHigh(GPIOC, pDIR_RR);
      break;
    case 1://rr
      GPIO_WriteHigh(GPIOC, pDIR_FF);
      GPIO_WriteLow(GPIOC, pDIR_RR);
      break;  
    case 2://stop
      GPIO_WriteHigh(GPIOC, pDIR_FF);
      GPIO_WriteHigh(GPIOC, pDIR_RR);
      break;   
  }
}

void set_PWM(void)
{
  long ltemp;
  long mPWM;
  int mControlPWM;
  u16 percent;
  u8 sign;
 
  if(gValue.stop) mControlPWM=0;
  else {
    percent=(gValue.dir==0) ? gValue.f_percent: RR_PERCENT;
    
    ltemp= ((gValue.f_control/100L) * percent) + ((gValue.f_control%100L) * percent)/100L;
    
    //printf("ltemp[%d] \r\n",ltemp);  
    
    mPWM= ltemp;
    

    //mPWM= gValue.f_control;
    sign=(mPWM >= gValue.ex_pwm) ? 0:1;//0 +  1:-
    if(sign==0) {
      mControlPWM = gValue.ex_pwm + A_NORRMAL;
      mControlPWM = (mControlPWM>=FET_MAX) ? FET_MAX :mControlPWM;
      if(mPWM <= mControlPWM) mControlPWM=mPWM;
    }
    else 
    {
        if(gValue.ex_pwm<SECOND_POLL)  mControlPWM=gValue.ex_pwm - A_STOPING;
        else mControlPWM = gValue.ex_pwm - A_NORRMAL;
        
        mControlPWM = (mControlPWM<=0) ? 0 :mControlPWM;
        if(mPWM >= mControlPWM) mControlPWM=mPWM;
    }
     
  }
  //check edage of falling to Zero
  if( gValue.ex_pwm>0 && mControlPWM==0){
    if(BREAK_FLAG==0) BREAK_FLAG=1;
  }
  
  if(gValue.seqStatus==2){
      set_pwm(mControlPWM);
      gValue.ex_pwm=mControlPWM;
  }
}


u16 get_adc(u8 mChannel)
{
  u16 ResultADC = 0;
  u16 temph = 0;
  u8 templ = 0;

  ADC1->CSR  = 0x00;
  ADC1->CR1  = 0x00;
  ADC1->CR2  = 0x00;
  ADC1->CR3  = 0x00;
  ADC1->CR1 |= 0x40;//2mhz
  ADC1->CR2 |= 0x08;//0x08;//0000_1000 ;right, No scan,

  ADC1->CSR |= (u8)mChannel;
  ADC1->CR1 |= ADC1_CR1_ADON;//ADC on
  ADC1->CR1 |= ADC1_CR1_ADON;

  while(!(ADC1->CSR & 0x80));

  templ = ADC1->DRL;
  temph = ADC1->DRH;
  temph = (u16)(templ | (u16)(temph << 8));

  ResultADC = temph;
  ADC1->CSR &= 0x7F;//EOC Clear

 return ResultADC & 0x3ff;
}
