#include "include.h"
#define MAX_VOLTAGE 54
#define VLTPERVAL 527L // 54/1024
#define VLT_YPOS 24
#define VLT_XPOS 50


typedef struct 
{
  u16 voltage;
  u16 temperature;  
  u16 ex_voltage;
  u16 ex_temperature;
}DISPLAY_INFO; 

u32 ms_tick=0;
u32 ms_cnt=0;
u16 adc_val;

DISPLAY_INFO disp =
{
  0, //  voltage
  0, //  temperature
  -1,
  -1
};

void lcd_show(DISPLAY_INFO ldisp);

void CLK_Configuration(void)
{
  // Fmaster = 16MHz 
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  //CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
}

void GPIO_Initial(void)
{
  GPIO_DeInit(GPIOA);
  //GPIO_Init(GPIOA,GPIO_PIN_3,GPIO_MODE_IN_PU_IT);

  GPIO_DeInit(GPIOB);
  GPIO_Init(GPIOB,LCD_CS,GPIO_MODE_OUT_PP_HIGH_FAST);  
  GPIO_Init(GPIOB,LCD_RS,GPIO_MODE_OUT_PP_HIGH_FAST);    
  
  //GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_IN_PU_NO_IT);  
  //GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_IN_PU_NO_IT); 
  //GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_IN_PU_NO_IT); 
  //GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_IN_PU_NO_IT); 

  GPIO_DeInit(GPIOC);
  GPIO_Init(GPIOC,LCD_RST,GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOC,LCD_BLK,GPIO_MODE_OUT_PP_HIGH_FAST);
}

void Timer_Init(void)
{
  TIM4->SR1 = 0;       // clear overflow flag
  TIM4->PSCR = 6;     // Prescaler to divide Fcpu by 64: 4 us clock.// Max CPU freq = 16 MHz
  TIM4->ARR = 24;    // 25*4 = 100 us. 10KHZ
  TIM4->IER = 0x01;   // Enable interrupt
  TIM1->PSCRH = 0x00;  // 8M  f=fck/(PSCR+1)
  TIM1->PSCRL = 0x07; // PSCR=0x1F3F��f=8M/(0x1F3F+1)=1000Hz��Period : 1ms
  TIM4->CR1 = 0x01;   // Start timer
  
  TIM1->ARRH = 0x4E;  // 0x4e20=20000us=20ms
  TIM1->ARRL = 0x20;  // 
  TIM1->IER = 0x01;   // Enable interrupt
  TIM1->CR1 = 0x01;   // Start timer
}


u16 get_adc(u8 mChannel)
{
  u16 ResultADC = 0;
  u16 temph = 0;
  u8 templ = 0;

  ADC1->CSR  = 0x00;
  ADC1->CR1  = 0x00;
  ADC1->CR2  = 0x00;
  ADC1->CR3  = 0x00;
  ADC1->CR1 |= 0x40;//2mhz
  ADC1->CR2 |= 0x08;//0x08;//0000_1000 ;right, No scan,

  ADC1->CSR |= (u8)mChannel;
  ADC1->CR1 |= ADC1_CR1_ADON;//ADC on
  ADC1->CR1 |= ADC1_CR1_ADON;

  while(!(ADC1->CSR & 0x80));

  templ = ADC1->DRL;
  temph = ADC1->DRH;
  temph = (u16)(templ | (u16)(temph << 8));

  ResultADC = temph;
  ADC1->CSR &= 0x7F;//EOC Clear
 return ResultADC & 0x3ff;
}


void lcd_show_frame(){
  LCD_Fill(0,0,160,80,BLACK);
  GUI_DrawFont32(120, VLT_YPOS, WHITE, BLACK, "V",1);
  
}
void lcd_show(DISPLAY_INFO ldisp){

  u8 v_buf[16]={0,};
  sprintf(v_buf,"%d.%d",ldisp.voltage/10,ldisp.voltage%10);
  //sprintf(t_buf,"%d.%d C",ldisp.temperature/10, ldisp.temperature%10);
 // sprintf(t_buf,"%d",ldisp.temperature);
 //  LCD_Fill(VLT_XPOS,VLT_YPOS,160-VLT_XPOS,60,BLACK);
   GUI_DrawFont32(VLT_XPOS, VLT_YPOS, WHITE, BLACK, v_buf,0);

}

main()
{
	CLK_Configuration();
	GPIO_Initial();
        
        UART1_DeInit(); // Initialize serial
        UART1_Init(115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
        UART1_ITConfig(UART1_IT_RXNE,ENABLE); //test
        Timer_Init();
        SPI0_Init(); 
        enableInterrupts();
        
        LCD_BLK_SET;
        LCD_Init();
        LCD_Clear(BLACK);
        LCD_direction(1);
        
        LCD_Clear(BLACK);
        delay_ms(100);
        lcd_show_frame();
        lcd_show(disp);  
        
 	while (1)
	{
          //test_gui();
          //delay_ms(4000);
          _printf("loop\r\n",0);
          delay_ms(1);
 	}
}

#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  u8 data;
  data = UART1_ReceiveData8();
  _printf("Rcv [%x]\r\n",data);
}


#pragma vector = 25
__interrupt void _100us_ISR(void)
{
  static u32 t4=0;
  TIM4->SR1 = 0;      // clear overflow flag
  t4++;
  if(t4>10){
    t4=0;
    ms_tick++;
    ms_cnt++;
  }
}
/////////////////////////
//10ms Timer
/////////////////////////
#pragma vector=0xD
__interrupt void _10ms_ISR(void)
{
  static u32 t1=0;
  u16 v_value;
  u16 t_value;
  u16 value;
  //u32 vol_x100;
  t1++;  
  TIM1->SR1 = 0;

  if(t1>50){
    _printf("ms_tick[%d]\r\n",ms_tick);
    t1=0;
    v_value = get_adc(4);
    //t_value = get_adc(5);
    t_value = v_value;
    disp.voltage =v_value*VLTPERVAL/1000L; 
    
    //disp.temperature =(t_value * 50L )/1024L; 
    disp.temperature =t_value;
    if(disp.voltage != disp.ex_voltage) 
    {
      lcd_show(disp);
      disp.ex_voltage=disp.voltage;
      //disp.ex_temperature=disp.temperature;
    }
    
    
  } 
}
