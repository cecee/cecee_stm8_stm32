//-----------------------------------
//	ILI9488 Driver library for STM32
//-----------------------------------
#ifndef ILI9488_DRV_H
#define ILI9488_DRV_H

#include "stm32f1xx_hal.h"
#include "extern.h"
#include "typedef.h"

//#define ILI9341_SCREEN_HEIGHT 240 
//#define ILI9341_SCREEN_WIDTH 	320

#define ILI9488_SCREEN_HEIGHT 320 
#define ILI9488_SCREEN_WIDTH 480
//SPI INSTANCE
#define HSPI_INSTANCE	&hspi2

//CHIP SELECT PIN AND PORT, STANDARD GPIO
#define LCD_CS_PORT	SPI2_CS_GPIO_Port
#define LCD_CS_PIN	SPI2_CS_Pin

//DATA COMMAND PIN AND PORT, STANDARD GPIO
#define LCD_DC_PORT	LCD_DC_GPIO_Port
#define LCD_DC_PIN	LCD_DC_Pin

//RESET PIN AND PORT, STANDARD GPIO
#define	LCD_RST_PORT	pLCD_RESET_GPIO_Port
#define	LCD_RST_PIN	pLCD_RESET_Pin

//500
#define HORIZONTAL_IMAGE 0
#define VERTICAL_IMAGE	1

#define BURST_MAX_SIZE 	600 
#define SCREEN_VERTICAL_1		0
#define SCREEN_HORIZONTAL_1		1
#define SCREEN_VERTICAL_2		2
#define SCREEN_HORIZONTAL_2		3
//#define tft_color(color) ( (uint16_t)((color >> 8) | (color << 8)) )
#define swap(a, b) { int16_t t = a; a = b; b = t; }

#define	LCD_CS_SET  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET)
#define	LCD_CS_CLR  HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET)
#define	LCD_DC_SET  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET)
#define	LCD_DC_CLR  HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET)


typedef struct
{
  uint16_t info[2];
}FONT_CHAR_INFO;

typedef struct
{
  uint16_t CharHeight;
  uint16_t StartChar;
  uint16_t EndChar;
  uint16_t WidthPixcel;
  const FONT_CHAR_INFO *Descriptors;
  const uint8_t *Bitmaps;
}FONT_INFO;


extern const color_t TFT_BLACK;
extern const color_t TFT_NAVY;
extern const color_t TFT_DARKGREEN;
extern const color_t TFT_DARKCYAN;
extern const color_t TFT_MAROON;
extern const color_t TFT_PURPLE ;
extern const color_t TFT_OLIVE;
extern const color_t TFT_LIGHTGREY;
extern const color_t TFT_DARKGREY;
extern const color_t TFT_GREY;
extern const color_t TFT_BLUE;
extern const color_t TFT_GREEN;
extern const color_t TFT_CYAN;
extern const color_t TFT_RED;
extern const color_t TFT_MAGENTA;
extern const color_t TFT_YELLOW;
extern const color_t TFT_WHITE ;
extern const color_t TFT_ORANGE ;
extern const color_t TFT_GREENYELLOW;
extern const color_t TFT_PINK;
extern uint16_t LCD_HEIGHT;
extern uint16_t LCD_WIDTH;

class ILI9488_DRV
{
    private:
    public:
      ILI9488_DRV();
      virtual ~ILI9488_DRV();
      //void ili9488_test(void);
      void ILI9341_SPI_Init(void);
      void ILI9341_SPI_Send(unsigned char SPI_Data);
      void ILI9341_Write_Command(uint8_t Command);
      void ILI9341_Write_Data(uint8_t Data);
      void ILI9341_WriteData_16Bit(uint16_t Data);

      void ILI9341_Set_Address(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2);
      void ILI9341_Reset(void);
      void ILI9341_Set_Rotation(uint8_t Rotation);
      void ILI9341_Enable(void);
      void ILI9341_Init(void);

      void ILI9341_Fill_Screen(color_t color);
      void ILI9341_Draw_Colour(color_t color);
      void ILI9341_Draw_Pixel(uint16_t X,uint16_t Y, color_t color);
      void ILI9341_Draw_Colour_Burst(color_t color, uint32_t Size);

      void ILI9341_Draw_Rectangle(uint16_t X, uint16_t Y, uint16_t Width, uint16_t Height, color_t color);
      void ILI9341_Draw_Horizontal_Line(uint16_t X, uint16_t Y, uint16_t Width, color_t color);
      void ILI9341_Draw_Vertical_Line(uint16_t X, uint16_t Y, uint16_t Height, color_t color);
      ///
      void commandList(const uint8_t *addr);
      void LCD_Test(void);
      void DrawLine( int x0, int y0, int x1, int y1, color_t color);
      void Draw_Hollow_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color);
      void Draw_Filled_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color);
      void Draw_Hollow_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color);
      void _drawFastVLine(int16_t x, int16_t y, int16_t h, color_t color);
      void _drawFastHLine(int16_t x, int16_t y, int16_t w, color_t color);
      void _fillRect(int16_t x, int16_t y, int16_t w, int16_t h, color_t color);
      void ILI9341_Draw_Char(char Character, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour);
      void ILI9341_Draw_Text(const char* Text, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour);
      void ILI9341_Draw_LedBar(uint16_t x, uint16_t y, uint16_t value);
      void ILI9341_Draw_Filled_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color);
      void ILI9341_Draw_RGB565(const char* Image_Array, uint16_t X, uint16_t Y, uint16_t width, uint16_t line);
      void ILI9341_Draw_Image(const char* Image_Array, uint8_t Orientation, uint16_t posX, uint16_t posY, uint16_t imgX, uint16_t imgY);
      void DrawFontTTF24(uint16_t x, uint16_t y, color_t fc, color_t bc, uint8_t *s,uint8_t mode);
      void DrawFontTTF16(uint16_t x, uint16_t y, color_t fc, color_t bc, uint8_t *s, uint8_t mode);
      //void DrawFontTTF36(uint16_t x, uint16_t y, color_t fc, color_t bc, uint8_t *s,uint8_t mode);
      void TFT_fillRoundRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t r, color_t color);
      void fillCircleHelper(int16_t x0, int16_t y0, int16_t r,	uint8_t cornername, int16_t delta, color_t color);
      void ILI9341_Draw_Filled_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color);
      void ILI9341_Draw_VLineW(uint16_t x, uint16_t y, uint16_t length, uint16_t w, color_t color);
      void ILI9341_Draw_HLineW(uint16_t x, uint16_t y, uint16_t length, uint16_t w, color_t color);
      void DrawFontTTF48(uint16_t x, uint16_t y, color_t fc, color_t bc, uint8_t *s, uint8_t mode);

};

#endif

