//-----------------------------------
//	ILI9488 Driver library for STM32
//-----------------------------------
#include "ili9488_drv.h"
#include "extern.h"
#include "spi.h"
#include "gpio.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "5x5_font.h"
//#include "godic_36pt.h"
#include "godic_24pt.h"
#include "godic_16pt23h.h"
#include <cstring>

#define DELAY 0x80
#define ILI9341_SWRESET 0x01
#define ILI9341_PIXFMT  0x3A
#define TFT_MADCTL	   0x36
#define MADCTL_MY  0x80
#define MADCTL_MX  0x40
#define MADCTL_MV  0x20
#define MADCTL_ML  0x10
#define MADCTL_RGB 0x00
#define MADCTL_BGR 0x08
#define MADCTL_MH  0x04
/* Global Variables ------------------------------------------------------------------*/
extern TIM_HandleTypeDef htim1;

uint16_t LCD_HEIGHT = ILI9488_SCREEN_HEIGHT;
uint16_t LCD_WIDTH = ILI9488_SCREEN_WIDTH;

// Color definitions constants
const color_t TFT_BLACK = { 0, 0, 0 };
const color_t TFT_NAVY = { 0, 0, 128 };
const color_t TFT_DARKGREEN = { 0, 128, 0 };
const color_t TFT_DARKCYAN = { 0, 128, 128 };
const color_t TFT_MAROON = { 128, 0, 0 };
const color_t TFT_PURPLE = { 128, 0, 128 };
const color_t TFT_OLIVE = { 128, 128, 0 };
const color_t TFT_LIGHTGREY = { 192, 192, 192 };
const color_t TFT_DARKGREY = { 128, 128, 128 };
const color_t TFT_GREY = { 93, 88, 83 };
const color_t TFT_BLUE = { 0, 0, 255 };
const color_t TFT_GREEN = { 0, 255, 0 };
const color_t TFT_CYAN = { 0, 255, 255 };
const color_t TFT_RED = { 255, 0, 0 };
const color_t TFT_MAGENTA = { 255, 0, 255 };
const color_t TFT_YELLOW = { 255, 255, 0 };
const color_t TFT_WHITE = { 255, 255, 255 };
const color_t TFT_ORANGE = { 255, 165, 0 };
const color_t TFT_GREENYELLOW = { 173, 255, 47 };
const color_t TFT_PINK = { 255, 192, 203 };

const uint8_t ILI9488_init[] = {
		18,                   	// 18 commands in list
		ILI9341_SWRESET,
		DELAY,   	// 1: Software reset, no args, w/delay
		200,				// 200 ms delay
		0xE0, 15, 0x00, 0x03, 0x09, 0x08, 0x16, 0x0A, 0x3F, 0x78, 0x4C, 0x09,
		0x0A, 0x08, 0x16, 0x1A, 0x0F, 0xE1, 15, 0x00, 0x16, 0x19, 0x03, 0x0F,
		0x05, 0x32, 0x45, 0x46, 0x04, 0x0E, 0x0D, 0x35, 0x37, 0x0F, 0xC0, 2, //Power Control 1
		0x17,    //Vreg1out
		0x15,    //Verg2out
		0xC1, 1,   //Power Control 2
		0x41,    //VGH,VGL
		0xC5, 3,   //Power Control 3
		0x00, 0x12,    //Vcom
		0x80,
		TFT_MADCTL, 1, // Memory Access Control (orientation)
		(MADCTL_MV | MADCTL_BGR),
		// *** INTERFACE PIXEL FORMAT: 0x66 -> 18 bit; 0x55 -> 16 bit
		ILI9341_PIXFMT, 1, 0x66, 0xB0, 1, // Interface Mode Control
		0x00,    // 0x80: SDO NOT USE; 0x00 USE SDO
		0xB1, 1, //Frame rate
		0xA0,    //60Hz
		0xB4, 1, //Display Inversion Control
		0x02,    //2-dot
		0xB6, 2, //Display Function Control  RGB/MCU Interface Control
		0x02,    //MCU
		0x02,    //Source,Gate scan direction
		0xE9, 1, // Set Image Function
		0x00,    // Disable 24 bit data
		0x53, 1, // Write CTRL Display Value
		0x28,    // BCTRL && DD on
		0x51, 1, // Write Display Brightness Value
		0x80,    //
		0xF7, 4, // Adjust Control
		0xA9, 0x51, 0x2C, 0x02,    // D7 stream, loose
		0x11, DELAY,  //Exit Sleep
		120, 0x29, 0,      //Display on
		};

ILI9488_DRV::ILI9488_DRV() {
	//memcpy(&modem_n502l,0,sizeof(modem_n502l));
}

ILI9488_DRV::~ILI9488_DRV() {
}

/* Initialize SPI */
void ILI9488_DRV::ILI9341_SPI_Init(void) {
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);	//CS OFF
}

/*Send data (char) to LCD*/
void ILI9488_DRV::ILI9341_SPI_Send(unsigned char SPI_Data) {
	HAL_SPI_Transmit(HSPI_INSTANCE, &SPI_Data, 1, 1);
}

/* Send command (char) to LCD */
void ILI9488_DRV::ILI9341_Write_Command(uint8_t Command) {
	LCD_CS_CLR;
	LCD_DC_CLR;
	ILI9341_SPI_Send(Command);
	LCD_CS_SET;
}

/* Send Data (char) to LCD */
void ILI9488_DRV::ILI9341_Write_Data(uint8_t Data) {
	LCD_CS_CLR;
	LCD_DC_SET;
	ILI9341_SPI_Send(Data);
	LCD_CS_SET;
}

void ILI9488_DRV::ILI9341_WriteData_16Bit(uint16_t Data) {
	ILI9341_Write_Data(Data >> 8);
	ILI9341_Write_Data(Data);
}

/* Set Address - Location block - to draw into */
void ILI9488_DRV::ILI9341_Set_Address(uint16_t X1, uint16_t Y1, uint16_t X2,
		uint16_t Y2) {
	//printf("ILI9341_Set_Address [%d][%d]-[%d][%d]\r\n",X1,Y1,X2,Y2);
	ILI9341_Write_Command(0x2A);
	ILI9341_Write_Data(X1 >> 8);
	ILI9341_Write_Data(X1);
	ILI9341_Write_Data(X2 >> 8);
	ILI9341_Write_Data(X2);
	ILI9341_Write_Command(0x2B);
	ILI9341_Write_Data(Y1 >> 8);
	ILI9341_Write_Data(Y1);
	ILI9341_Write_Data(Y2 >> 8);
	ILI9341_Write_Data(Y2);
	ILI9341_Write_Command(0x2C);
}

/*HARDWARE RESET*/
void ILI9488_DRV::ILI9341_Reset(void) {
	HAL_GPIO_WritePin(LCD_RST_PORT, LCD_RST_PIN, GPIO_PIN_SET);
	HAL_Delay(20);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	HAL_Delay(20);
	HAL_GPIO_WritePin(LCD_RST_PORT, LCD_RST_PIN, GPIO_PIN_SET);
}

/*Ser rotation of the screen - changes x0 and y0*/
void ILI9488_DRV::ILI9341_Set_Rotation(uint8_t Rotation) {

	uint8_t screen_rotation = Rotation;

	ILI9341_Write_Command(0x36);
	HAL_Delay(1);
	switch (screen_rotation) {
	case SCREEN_VERTICAL_1:
		ILI9341_Write_Data(0x40 | 0x08);
		LCD_WIDTH = 320;	//240;
		LCD_HEIGHT = 480;	//320;
		break;
	case SCREEN_HORIZONTAL_1:
		ILI9341_Write_Data(0x20 | 0x08);
		LCD_WIDTH = 480;	//320;
		LCD_HEIGHT = 320;	//240;
		break;
	case SCREEN_VERTICAL_2:
		ILI9341_Write_Data(0x80 | 0x08);
		LCD_WIDTH = 320;	//240;
		LCD_HEIGHT = 480;	//320;
		break;
	case SCREEN_HORIZONTAL_2:
		ILI9341_Write_Data(0x40 | 0x80 | 0x20 | 0x08);
		LCD_WIDTH = 480;	//320;
		LCD_HEIGHT = 320;	//240;
		break;
	default:
		//EXIT IF SCREEN ROTATION NOT VALID!
		break;
	}
}

/*Enable LCD display*/
void ILI9488_DRV::ILI9341_Enable(void) {
	HAL_GPIO_WritePin(BL_ON_GPIO_Port, BL_ON_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_RST_PORT, LCD_RST_PIN, GPIO_PIN_SET);
}

/*Initialize LCD display*/
void ILI9488_DRV::ILI9341_Init(void) {
	printf("ILI9341_Init\r\n");
	ILI9341_Enable();
	ILI9341_SPI_Init();
	ILI9341_Reset();
	commandList(ILI9488_init);
}

void ILI9488_DRV::commandList(const uint8_t *addr) {
	uint8_t i, numCommands, numArgs, cmd;
	uint16_t ms;
	uint8_t *value;
	numCommands = *addr++;         // Number of commands to follow
	// printf("numCommands:[%d]\r\n",numCommands);
	while (numCommands--) {         // For each command...
		cmd = *addr++;               // save command
		numArgs = *addr++;          //   Number of args to follow
		ms = numArgs & DELAY;  //   If high bit set, delay follows args
		numArgs &= ~DELAY;           //   Mask out delay bit
		//printf("cmd:[%02x] numArgs:[%d]-",cmd, numArgs);
		ILI9341_Write_Command(cmd);
		for (i = 0; i < numArgs; i++) {
			value = (uint8_t*) addr + i;
			ILI9341_Write_Data(value[0]);
			//printf("[%02x]",value[0]%0xff);
		}
		// printf("\r\n");
		//disp_spi_transfer_cmd_data(cmd, (uint8_t *)addr, numArgs);
		addr += numArgs;
		if (ms) {
			ms = *addr++;              // Read post-command delay time (ms)
			if (ms == 255)
				ms = 500;    // If 255, delay for 500 ms
			HAL_Delay(ms);
			//printf("DELAY->[%d]ms\r\n",ms);
			//vTaskDelay(ms / portTICK_RATE_MS);
		}
	}
}

//INTERNAL FUNCTION OF LIBRARY, USAGE NOT RECOMENDED, USE Draw_Pixel INSTEAD
/*Sends single pixel colour information to LCD*/
void ILI9488_DRV::ILI9341_Draw_Colour(color_t color) {
//SENDS COLOUR
	unsigned char TempBuffer[3];	 // = {color>>8, color};
	TempBuffer[0] = color.r;
	TempBuffer[1] = color.g;
	TempBuffer[2] = color.b;

	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
//HAL_SPI_Transmit(HSPI_INSTANCE, TempBuffer, 2, 1);
	HAL_SPI_Transmit(HSPI_INSTANCE, TempBuffer, 3, 1);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);
}

//INTERNAL FUNCTION OF LIBRARY
/*Sends block colour information to LCD*/
void ILI9488_DRV::ILI9341_Draw_Colour_Burst(color_t color, uint32_t Size) {

	unsigned char burst_buffer[BURST_MAX_SIZE];
	uint32_t Buffer_Size = 0;
	if ((Size * 3) < BURST_MAX_SIZE)
		Buffer_Size = Size;
	else
		Buffer_Size = BURST_MAX_SIZE;
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	for (uint32_t j = 0; j < Buffer_Size; j += 3) {
		burst_buffer[j + 0] = 255 - color.r;	 //chifted;
		burst_buffer[j + 1] = 255 - color.g;	 //Colour;
		burst_buffer[j + 2] = 255 - color.b;	 //Colour;
	}
	//uint32_t Sending_Size = Size*2;
	uint32_t Sending_Size = Size * 3;
	uint32_t Sending_in_Block = Sending_Size / Buffer_Size;
	uint32_t Remainder_from_block = Sending_Size % Buffer_Size;
	//printf("Sending_Size[%d] Sending_in_Block[%d] Remainder_from_block[%d]\r\n",Sending_Size,Sending_in_Block, Remainder_from_block);
	if (Sending_in_Block != 0) {
		for (uint32_t j = 0; j < (Sending_in_Block); j++) {
			HAL_SPI_Transmit(HSPI_INSTANCE, (unsigned char*) burst_buffer,
					Buffer_Size, 10);
		}
	}
	//REMAINDER!
	HAL_SPI_Transmit(HSPI_INSTANCE, (unsigned char*) burst_buffer,
			Remainder_from_block, 10);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);
	//DBG(WARN,"ILI9341_Draw_Colour_Burst..!!\n");
}

//FILL THE ENTIRE SCREEN WITH SELECTED COLOUR (either #define-d ones or custom 16bit)
/*Sets address (entire screen) and Sends Height*Width ammount of colour information to LCD*/

//void ILI9341_Draw_Colour_Burst(color_t color, uint32_t Size)
void ILI9488_DRV::ILI9341_Fill_Screen(color_t color) {
	// DBG(ATR,"ILI9341_Fill_Screen..!!\n");
	ILI9341_Set_Address(0, 0, LCD_WIDTH, LCD_HEIGHT);
	ILI9341_Draw_Colour_Burst(color, LCD_WIDTH * LCD_HEIGHT);
}

//DRAW PIXEL AT XY POSITION WITH SELECTED COLOUR
//
//Location is dependant on screen orientation. x0 and y0 locations change with orientations.
//Using pixels to draw big simple structures is not recommended as it is really slow
//Try using either rectangles or lines if possible
//
void ILI9488_DRV::ILI9341_Draw_Pixel(uint16_t X, uint16_t Y, color_t color) {
	if ((X >= LCD_WIDTH) || (Y >= LCD_HEIGHT))
		return;	//OUT OF BOUNDS!

	//ADDRESS
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	ILI9341_SPI_Send(0x2A);
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

	//XDATA
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	unsigned char Temp_Buffer[4] = { X >> 8, X, (X + 1) >> 8, (X + 1) };
	HAL_SPI_Transmit(HSPI_INSTANCE, Temp_Buffer, 4, 1);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

	//ADDRESS
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	ILI9341_SPI_Send(0x2B);
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

	//YDATA
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	unsigned char Temp_Buffer1[4] = { Y >> 8, Y, (Y + 1) >> 8, (Y + 1) };
	HAL_SPI_Transmit(HSPI_INSTANCE, Temp_Buffer1, 4, 1);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

	//ADDRESS
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	ILI9341_SPI_Send(0x2C);
	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

	//COLOUR
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	unsigned char Temp_Buffer2[3];	//= {Colour>>8, Colour};
	Temp_Buffer2[0] = 255 - color.r;
	Temp_Buffer2[1] = 255 - color.g;
	Temp_Buffer2[2] = 255 - color.b;

	HAL_SPI_Transmit(HSPI_INSTANCE, Temp_Buffer2, 3, 1);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);

}

//DRAW RECTANGLE OF SET SIZE AND HEIGTH AT X and Y POSITION WITH CUSTOM COLOUR
//
//Rectangle is hollow. X and Y positions mark the upper left corner of rectangle
//As with all other draw calls x0 and y0 locations dependant on screen orientation
//

void ILI9488_DRV::ILI9341_Draw_Rectangle(uint16_t X, uint16_t Y, uint16_t Width,
		uint16_t Height, color_t color) {
	//printf("LCD_WIDTH:%d X:%d LCD_HEIGHT:%d Y:%d\r\n",LCD_WIDTH, X, LCD_HEIGHT, Y);
	if ((X >= LCD_WIDTH) || (Y >= LCD_HEIGHT))
		return;
	if ((X + Width - 1) >= LCD_WIDTH)
		Width = LCD_WIDTH - X;
	if ((Y + Height - 1) >= LCD_HEIGHT)
		Height = LCD_HEIGHT - Y;
	ILI9341_Set_Address(X, Y, X + Width - 1, Y + Height - 1);
	ILI9341_Draw_Colour_Burst(color, Height * Width);
}

//DRAW LINE FROM X,Y LOCATION to X+Width,Y LOCATION
void ILI9488_DRV::ILI9341_Draw_Horizontal_Line(uint16_t X, uint16_t Y,
		uint16_t Width, color_t color) {
	if ((X >= LCD_WIDTH) || (Y >= LCD_HEIGHT))
		return;
	if ((X + Width - 1) >= LCD_WIDTH)
		Width = LCD_WIDTH - X;
	ILI9341_Set_Address(X, Y, X + Width - 1, Y);
	ILI9341_Draw_Colour_Burst(color, Width);
}

//DRAW LINE FROM X,Y LOCATION to X,Y+Height LOCATION
void ILI9488_DRV::ILI9341_Draw_Vertical_Line(uint16_t X, uint16_t Y,
		uint16_t Height, color_t color) {
	if ((X >= LCD_WIDTH) || (Y >= LCD_HEIGHT))
		return;
	if ((Y + Height - 1) >= LCD_HEIGHT) {
		Height = LCD_HEIGHT - Y;
	}
	ILI9341_Set_Address(X, Y, X, Y + Height - 1);
	ILI9341_Draw_Colour_Burst(color, Height);
}

void ILI9488_DRV::LCD_Test(void) {
	printf("ili9488_test..!!\n");
	//ILI9341_Fill_Screen(TFT_GREY);
	//ILI9341_Fill_Screen(TFT_BLUE);
	//ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
	//ILI9341_Draw_LedBar(20,100,50);
	//return;
}

/*Draw hollow circle at X,Y location with specified radius and colour. X and Y represent circles center */
void ILI9488_DRV::Draw_Hollow_Circle(uint16_t X, uint16_t Y, uint16_t Radius,
		color_t color) {
	int x = Radius - 1;
	int y = 0;
	int dx = 1;
	int dy = 1;
	int err = dx - (Radius << 1);

	while (x >= y) {
		ILI9341_Draw_Pixel(X + x, Y + y, color);
		ILI9341_Draw_Pixel(X + y, Y + x, color);
		ILI9341_Draw_Pixel(X - y, Y + x, color);
		ILI9341_Draw_Pixel(X - x, Y + y, color);
		ILI9341_Draw_Pixel(X - x, Y - y, color);
		ILI9341_Draw_Pixel(X - y, Y - x, color);
		ILI9341_Draw_Pixel(X + y, Y - x, color);
		ILI9341_Draw_Pixel(X + x, Y - y, color);

		if (err <= 0) {
			y++;
			err += dy;
			dy += 2;
		}
		if (err > 0) {
			x--;
			dx += 2;
			err += (-Radius << 1) + dx;
		}
	}
}

/*Draw filled circle at X,Y location with specified radius and colour. X and Y represent circles center */
void ILI9488_DRV::Draw_Filled_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color) {

	int x = Radius;
	int y = 0;
	int xChange = 1 - (Radius << 1);
	int yChange = 0;
	int radiusError = 0;

	while (x >= y) {
		for (int i = X - x; i <= X + x; i++) {
			ILI9341_Draw_Pixel(i, Y + y, color);
			ILI9341_Draw_Pixel(i, Y - y, color);
		}
		for (int i = X - y; i <= X + y; i++) {
			ILI9341_Draw_Pixel(i, Y + x, color);
			ILI9341_Draw_Pixel(i, Y - x, color);
		}

		y++;
		radiusError += yChange;
		yChange += 2;
		if (((radiusError << 1) + xChange) > 0) {
			x--;
			radiusError += xChange;
			xChange += 2;
		}
	}
	//Really slow implementation, will require future overhaul
	//TODO:	https://stackoverflow.com/questions/1201200/fast-algorithm-for-drawing-filled-circles
}

/*Draw a hollow rectangle between positions X0,Y0 and X1,Y1 with specified colour*/
void ILI9488_DRV::Draw_Hollow_Rectangle_Coord(uint16_t X0, uint16_t Y0,
		uint16_t X1, uint16_t Y1, color_t color) {
	uint16_t X_length = 0;
	uint16_t Y_length = 0;
	uint8_t Negative_X = 0;
	uint8_t Negative_Y = 0;
	float Calc_Negative = 0;

	Calc_Negative = X1 - X0;
	if (Calc_Negative < 0)
		Negative_X = 1;
	Calc_Negative = 0;

	Calc_Negative = Y1 - Y0;
	if (Calc_Negative < 0)
		Negative_Y = 1;

	//DRAW HORIZONTAL!
	if (!Negative_X) {
		X_length = X1 - X0;
	} else {
		X_length = X0 - X1;
	}
	ILI9341_Draw_Horizontal_Line(X0, Y0, X_length, color);
	ILI9341_Draw_Horizontal_Line(X0, Y1, X_length, color);

	//DRAW VERTICAL!
	if (!Negative_Y) {
		Y_length = Y1 - Y0;
	} else {
		Y_length = Y0 - Y1;
	}
	ILI9341_Draw_Vertical_Line(X0, Y0, Y_length, color);
	ILI9341_Draw_Vertical_Line(X1, Y0, Y_length, color);

	if ((X_length > 0) || (Y_length > 0)) {
		ILI9341_Draw_Pixel(X1, Y1, color);
	}

}

/*Draw a filled rectangle between positions X0,Y0 and X1,Y1 with specified colour*/
void ILI9488_DRV::ILI9341_Draw_Filled_Rectangle_Coord(uint16_t X0, uint16_t Y0,
		uint16_t X1, uint16_t Y1, color_t color) {
	uint16_t X_length = 0;
	uint16_t Y_length = 0;
	uint8_t Negative_X = 0;
	uint8_t Negative_Y = 0;
	int32_t Calc_Negative = 0;

	uint16_t X0_true = 0;
	uint16_t Y0_true = 0;

	Calc_Negative = X1 - X0;
	if (Calc_Negative < 0)
		Negative_X = 1;
	Calc_Negative = 0;

	Calc_Negative = Y1 - Y0;
	if (Calc_Negative < 0)
		Negative_Y = 1;

	//DRAW HORIZONTAL!
	if (!Negative_X) {
		X_length = X1 - X0;
		X0_true = X0;
	} else {
		X_length = X0 - X1;
		X0_true = X1;
	}

	//DRAW VERTICAL!
	if (!Negative_Y) {
		Y_length = Y1 - Y0;
		Y0_true = Y0;
	} else {
		Y_length = Y0 - Y1;
		Y0_true = Y1;
	}

	ILI9341_Draw_Rectangle(X0_true, Y0_true, X_length, Y_length, color);
}

void ILI9488_DRV::_drawFastVLine(int16_t x, int16_t y, int16_t h,
		color_t color) {
	ILI9341_Draw_Vertical_Line(x, y, h, color);
}

void ILI9488_DRV::_drawFastHLine(int16_t x, int16_t y, int16_t w,
		color_t color) {
	ILI9341_Draw_Horizontal_Line(x, y, w, color);
}

void ILI9488_DRV::_fillRect(int16_t x, int16_t y, int16_t w, int16_t h,
		color_t color) {
	// clipping
	ILI9341_Draw_Filled_Rectangle_Coord(x, y, x + w, y + h, color);
}

/*Draws a character (fonts imported from fonts.h) at X,Y location with specified font colour, size and Background colour*/
/*See fonts.h implementation of font on what is required for changing to a different font when switching fonts libraries*/
//5x5 font
void ILI9488_DRV::ILI9341_Draw_Char(char Character, uint16_t X, uint16_t Y,
		color_t color, uint16_t Size, color_t Background_Colour) {
	uint8_t function_char;
	uint8_t i, j;

	function_char = Character;
	//if(Size==3){
	//  printf("1-X:%d Y:%d [%c]\r\n",X,Y,function_char);
	//}

	if (function_char < ' ') {
		Character = 0;
	} else {
		function_char -= 32;
	}

	char temp[CHAR_WIDTH];
	for (uint8_t k = 0; k < CHAR_WIDTH; k++) {
		temp[k] = font[function_char][k];
	}
	// Draw pixels
	ILI9341_Draw_Rectangle(X, Y, CHAR_WIDTH * Size, CHAR_HEIGHT * Size,
			Background_Colour);
	for (j = 0; j < CHAR_WIDTH; j++) {
		for (i = 0; i < CHAR_HEIGHT; i++) {
			if (temp[j] & (1 << i)) {
				if (Size == 1) {
					ILI9341_Draw_Pixel(X + j, Y + i, color);
				} else {
					ILI9341_Draw_Rectangle(X + (j * Size), Y + (i * Size), Size,
							Size, color);
				}
			}
		}
	}
}

/*Draws an array of characters (fonts imported from fonts.h) at X,Y location with specified font colour, size and Background colour*/
/*See fonts.h implementation of font on what is required for changing to a different font when switching fonts libraries*/
void ILI9488_DRV::ILI9341_Draw_Text(const char *Text, uint16_t X, uint16_t Y,
		color_t color, uint16_t Size, color_t Background_Colour) {
	while (*Text) {
		ILI9341_Draw_Char(*Text++, X, Y, color, Size, Background_Colour);
		X += CHAR_WIDTH * Size;
		//printf("ILI9341_Draw_Text X:%d\r\n",X);
	}
}

void ILI9488_DRV::ILI9341_Draw_LedBar(uint16_t x, uint16_t y, uint16_t value) {
	color_t color;
	uint8_t x0, ix, w = 30;
	uint8_t i;
	color.r = 0xff;
	color.g = 0x0;
	color.b = 0x0;
	x0 = x;
	for (i = 0; i < 8; i++) {
		ix = x0 + (w * i);
		ILI9341_Draw_Filled_Rectangle_Coord(ix, y, ix + 20, y + 80, color);
	}
}

void ILI9488_DRV::DrawLine( int x0, int y0, int x1, int y1, color_t color)
{

    int x = x1-x0;
    int y = y1-y0;
    int dx = abs(x), sx = x0<x1 ? 1 : -1;
    int dy = -abs(y), sy = y0<y1 ? 1 : -1;
    int err = dx+dy, e2;                                                /* error value e_xy             */
    for (;;){                                                           /* loop                         */
        //setPixel(x0,y0,color);
        ILI9341_Draw_Pixel(x0,y0,color);
        e2 = 2*err;
        if (e2 >= dy) {                                                 /* e_xy+e_x > 0                 */
            if (x0 == x1) break;
            err += dy; x0 += sx;
        }
        if (e2 <= dx) {                                                 /* e_xy+e_y < 0                 */
            if (y0 == y1) break;
            err += dx; y0 += sy;
        }
    }

}



void ILI9488_DRV::ILI9341_Draw_VLineW(uint16_t x, uint16_t y, uint16_t length, uint16_t w, color_t color) {
	ILI9341_Draw_Filled_Rectangle_Coord(x, y, x+w, y+length, color);
}
void ILI9488_DRV::ILI9341_Draw_HLineW(uint16_t x, uint16_t y, uint16_t length, uint16_t w, color_t color) {
	ILI9341_Draw_Filled_Rectangle_Coord(x, y, x+length, y+w, color);
}

void ILI9488_DRV::ILI9341_Draw_RGB565(const char *Image_Array, uint16_t X,
		uint16_t Y, uint16_t width, uint16_t line) {
	uint16_t oneLineDataSz, oneLinePushSz;
	//uint16_t block, push_sz;
	uint16_t i, ix, iy, rgb565;
	uint32_t k;
	color_t color;
	uint8_t tmp[4];
	//uint8_t* push_buffer;//[(width*3)+10];
	//uint8_t* buf;//[(width*3)+10];
	uint8_t push_buffer[(240 * 3) + 10];

	//uint8_t push_bufferx[480];
	//memset(push_buffer,0,sizeof(push_buffer));
	ILI9341_Set_Address(X, Y, X + width - 1, Y + line);	////
	oneLineDataSz = width * 2;	//byte/pixel 16bit 565RGB
	oneLinePushSz = width * 3;	//byte/pixel 24bit 888RGB

	HAL_GPIO_WritePin(LCD_DC_PORT, LCD_DC_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	k = 0;
	for (i = 0; i < line; i++) {
		k = oneLineDataSz * i;
		//push_buffer = malloc(oneLinePushSz+1);
		//push_buffer = malloc(1000);
		//printf("buf[%x]\r\n",push_buffer);
		//memset(buf,0,sizeof(buf));
		//printf("Draw oneline[%d] k[%d]\r\n",i, k);
		for (ix = 0, iy = 0; ix < oneLineDataSz; ix += 2) {

			tmp[0] = Image_Array[k + ix];
			tmp[1] = Image_Array[k + ix + 1];
			rgb565 = BUILD_UINT16(tmp[1], tmp[0]);
			//rgb565=0xf800;
			color.r = (rgb565 >> 11) << 3;
			color.g = ((rgb565 >> 5) & 0x3f) << 2;
			color.b = (rgb565 & 0x1f) << 1;
			push_buffer[iy + 0] = color.r;
			push_buffer[iy + 1] = color.g;
			push_buffer[iy + 2] = color.b;
			iy += 3;
			//if(i==0 && ix==0) printf("Draw oneline[%x] push_buffer[%x] \r\n",rgb565, push_buffer[0]);
			//printf("Draw oneline[%d]\r\n",k);
		}
		//printf("Draw oneline oneLinePushSz[%d] iy[%d]\r\n",oneLinePushSz, iy);
		HAL_SPI_Transmit(&hspi2, push_buffer, oneLinePushSz, 20);
		//free(push_buffer);
	}
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_SET);
}

/*Draws a full screen picture from flash. Image converted from RGB .jpeg/other to C array using online converter*/
//USING CONVERTER: http://www.digole.com/tools/PicturetoC_Hex_converter.php
//65K colour (2Bytes / Pixel)
void ILI9488_DRV::ILI9341_Draw_Image(const char *Image_Array,
		uint8_t Orientation, uint16_t posX, uint16_t posY, uint16_t imgX,
		uint16_t imgY) {
	printf("ILI9341_Draw_Image Orientation[%d]\r\n", Orientation);
#if 1
	if (Orientation == SCREEN_HORIZONTAL_1) {
		ILI9341_Set_Rotation(SCREEN_HORIZONTAL_1);
		ILI9341_Draw_RGB565(Image_Array, posX, posY, imgX, imgY);
	} else if (Orientation == SCREEN_HORIZONTAL_2) {
		ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
		ILI9341_Draw_RGB565(Image_Array, posX, posY, imgX, imgY);

	} else if (Orientation == SCREEN_VERTICAL_2) {
		ILI9341_Set_Rotation(SCREEN_VERTICAL_2);
		ILI9341_Draw_RGB565(Image_Array, posX, posY, imgX, imgY);
	} else if (Orientation == SCREEN_VERTICAL_1) {
		ILI9341_Set_Rotation(SCREEN_VERTICAL_1);
		ILI9341_Draw_RGB565(Image_Array, posX, posY, imgX, imgY);
	}
#endif
}

/*****************************************************************************
 * @name       :void GUI_DrawFontTTF(u8 fntsz, u16 x, u16 y, u16 fc, u16 bc, u8 *s,u8 mode)
 * @function   :Display  TTF Bitmap Fnt 24pt/36pt
 * @parameters :
 fntsz 36pt
 x:the bebinning x coordinate of the Chinese character
 y:the bebinning y coordinate of the Chinese character
 fc:the color value of Chinese character
 bc:the background color of Chinese character
 s:the start address of the Chinese character
 mode:0-no overlying,1-overlying
 * @retvalue   :None
 ******************************************************************************/
void ILI9488_DRV::DrawFontTTF24(uint16_t x, uint16_t y, color_t fc, color_t bc,
		uint8_t *s, uint8_t mode) {
	uint16_t ix, iy, ixx, j;
	uint16_t k;
	//uint16_t HZnum;
	//uint16_t x0 = x;
	uint16_t xA, xpos, ypos;

	uint16_t charHeightByte, charSpaceByte;
//	uint16_t font_pixcel_height;
	uint8_t drw_ot, drw_in;

	int font_index;
	uint16_t font_pixcel_width;
	uint16_t font_pixcel_offset;
	uint16_t font_byte_width;
//	uint16_t font_byte_height;
	uint16_t bitmap_offset;

	uint8_t byte_data;
	uint8_t StartChar;

	FONT_INFO font_info;

	memcpy(&font_info, &godic_24ptFontInfo, sizeof(FONT_INFO));
	drw_ot = 40;
	drw_in = 38;
	charHeightByte = font_info.CharHeight;
	charSpaceByte = font_info.WidthPixcel;
	StartChar = font_info.StartChar;
	//font_pixcel_height = charHeightByte * 8;
	xA = x;
	for (k = 0; k < 32; k++) {
		if (s[k] < 0x20)
			break;
		font_index = (s[k] - StartChar) + 0;
		font_pixcel_width = godic_24ptDescriptors[font_index].info[0];
		font_pixcel_offset = godic_24ptDescriptors[font_index].info[1];
		if (font_index < 0) {
			font_pixcel_width = 10;
			font_pixcel_offset = 0;
		}
		font_byte_width =
				(font_pixcel_width % 8) ?
						(font_pixcel_width / 8) + 1 :
						(font_pixcel_width / 8) + 0;
		bitmap_offset = font_pixcel_offset;

		for (iy = 0; iy < drw_ot; iy++) {
			ypos = y + iy;
			xpos = xA;
			for (ix = 0; ix < font_byte_width; ix++) {
				byte_data = 0x00;
				if (iy >= drw_in || (font_index < 0))
					byte_data = 0x00;
				else {
					byte_data = font_info.Bitmaps[bitmap_offset++];
				}
				for (ixx = 0; ixx < 8; ixx++) {
					if (mode) {
						if (byte_data & 0x80)
							ILI9341_Draw_Pixel(xpos, ypos, fc);     //point draw
					} else {
						if (byte_data & 0x80)
							ILI9341_Draw_Pixel(xpos, ypos, fc);
						else
							ILI9341_Draw_Pixel(xpos, ypos, bc);
					}
					byte_data = (byte_data << 1);
					xpos++;
				}
			}
		}
		xA = xA + font_byte_width * 8;
		ILI9341_Set_Address(x, y, x + (font_byte_width * 8) - 1,
				y + (font_byte_width * 8) - 1);
	}
	ILI9341_Set_Address(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
}

void ILI9488_DRV::DrawFontTTF16(uint16_t x, uint16_t y, color_t fc, color_t bc,
	uint8_t *s, uint8_t mode) {
	uint16_t i, ix, iy, ixx, j;
	uint16_t k;
	uint16_t HZnum;
	uint16_t x0 = x;
	uint16_t xA, xpos, ypos;

	uint16_t charHeightByte, charSpaceByte;
	uint16_t font_pixcel_height;
	uint8_t drw_ot, drw_in;

	int font_index;
	uint16_t font_pixcel_width;
	uint16_t font_pixcel_offset;
	uint16_t font_byte_width;
	uint16_t font_byte_height;
	uint16_t bitmap_offset;

	uint8_t byte_data;
	uint8_t StartChar;

	FONT_INFO font_info;

	memcpy(&font_info, &godic_16ptFontInfo, sizeof(FONT_INFO));
	drw_ot = 32;//8*4 폰트바이트
	drw_in = 23;//폰트높이
	charHeightByte = font_info.CharHeight;
	charSpaceByte = font_info.WidthPixcel;
	StartChar = font_info.StartChar;
	font_pixcel_height = charHeightByte * 8;

#if 1
	xA = x;
	for (k = 0; k < 128; k++) {
		if (s[k] < 0x20) break;
		font_index = (s[k] - StartChar) + 0;
		font_pixcel_width = godic_16ptDescriptors[font_index].info[0];
		font_pixcel_offset = godic_16ptDescriptors[font_index].info[1];
		//font_index=font_index-1;
		if (font_index < 0) {
			font_pixcel_width = 5;
			font_pixcel_offset = 0;
		}
		font_byte_width = (font_pixcel_width % 8) ? (font_pixcel_width / 8) + 1 :	(font_pixcel_width / 8) + 0;
		bitmap_offset = font_pixcel_offset;
		for (iy = 0; iy < drw_ot; iy++) {
			ypos = y + iy;
			xpos = xA;
			for (ix = 0; ix < font_byte_width; ix++) {
				byte_data = 0x00;
				if (iy >= drw_in || (font_index < 0)) byte_data = 0x00;
				else {
					byte_data = font_info.Bitmaps[bitmap_offset++];
				}
				for (ixx = 0; ixx < 8; ixx++) {
					if (mode) {
						if (byte_data & 0x80)
							ILI9341_Draw_Pixel(xpos, ypos, fc);     //point draw
					} else {
						if (byte_data & 0x80)
							ILI9341_Draw_Pixel(xpos, ypos, fc);
						else
							ILI9341_Draw_Pixel(xpos, ypos, bc);
					}
					byte_data = (byte_data << 1);
					xpos++;
				}

			}
		}

		xA = xA + font_byte_width * 4;
		ILI9341_Set_Address(x, y, x + (font_byte_width * 8) - 1,
				y + (font_byte_width * 8) - 1);
	}
	ILI9341_Set_Address(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
#endif
}



#if 0
void ILI9488_DRV::DrawFontTTF36(uint16_t x, uint16_t y, color_t fc, color_t bc,
		uint8_t *s, uint8_t mode) {
	uint16_t i, ix, iy, ixx, j;
	uint16_t k;
	uint16_t HZnum;
	uint16_t x0 = x;
	uint16_t xA, xpos, ypos;

	uint16_t charHeightByte, charSpaceByte;
	uint16_t font_pixcel_height;
	uint8_t drw_ot, drw_in;

	int font_index;
	uint16_t font_pixcel_width;
	uint16_t font_pixcel_offset;
	uint16_t font_byte_width;
	uint16_t font_byte_height;
	uint16_t bitmap_offset;

	uint8_t byte_data;
	uint8_t StartChar;

	FONT_INFO font_info;

	memcpy(&font_info, &godic_36ptFontInfo, sizeof(FONT_INFO));
	drw_ot = 56;//8*7 폰트바이트
	drw_in = 54;//폰트높이
	charHeightByte = font_info.CharHeight;
	charSpaceByte = font_info.WidthPixcel;
	StartChar = font_info.StartChar;
	font_pixcel_height = charHeightByte * 8;

#if 1
	xA = x;
	for (k = 0; k < 16; k++) {
		if (s[k] < 0x20)
			break;
		font_index = (s[k] - StartChar) + 0;
		font_pixcel_width = godic_36ptDescriptors[font_index].info[0];
		font_pixcel_offset = godic_36ptDescriptors[font_index].info[1];
		//font_index=font_index-1;
		if (font_index < 0) {
			font_pixcel_width = 10;
			font_pixcel_offset = 0;
		}
		font_byte_width =
				(font_pixcel_width % 8) ?
						(font_pixcel_width / 8) + 1 :
						(font_pixcel_width / 8) + 0;
		bitmap_offset = font_pixcel_offset;
#if 1
		for (iy = 0; iy < drw_ot; iy++) {
			ypos = y + iy;
			xpos = xA;
			for (ix = 0; ix < font_byte_width; ix++) {
				byte_data = 0x00;
				if (iy >= drw_in || (font_index < 0))
					byte_data = 0x00;
				else {
					byte_data = font_info.Bitmaps[bitmap_offset++];
				}
				for (ixx = 0; ixx < 8; ixx++) {
					if (mode) {
						if (byte_data & 0x80)
							ILI9341_Draw_Pixel(xpos, ypos, fc);     //point draw
					} else {
						if (byte_data & 0x80)
							ILI9341_Draw_Pixel(xpos, ypos, fc);
						else
							ILI9341_Draw_Pixel(xpos, ypos, bc);
					}
					byte_data = (byte_data << 1);
					xpos++;
				}

			}
		}
#endif
		xA = xA + font_byte_width * 8;
		ILI9341_Set_Address(x, y, x + (font_byte_width * 8) - 1,
				y + (font_byte_width * 8) - 1);
	}
	ILI9341_Set_Address(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
#endif
}

#endif
void ILI9488_DRV::TFT_fillRoundRect(int16_t x, int16_t y, uint16_t w,
		uint16_t h, uint16_t r, color_t color) {
	//x += dispWin.x1;
	//y += dispWin.y1;
	// smarter version
	_fillRect(x + r, y, w - 2 * r, h, color);
	// draw four corners
	fillCircleHelper(x + w - r - 1, y + r, r, 1, h - 2 * r - 1, color);
	fillCircleHelper(x + r, y + r, r, 2, h - 2 * r - 1, color);
}

void ILI9488_DRV::fillCircleHelper(int16_t x0, int16_t y0, int16_t r,
		uint8_t cornername, int16_t delta, color_t color) {
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;
	int16_t ylm = x0 - r;

	while (x < y) {
		if (f >= 0) {
			if (cornername & 0x1)
				_drawFastVLine(x0 + y, y0 - x, 2 * x + 1 + delta, color);
			if (cornername & 0x2)
				_drawFastVLine(x0 - y, y0 - x, 2 * x + 1 + delta, color);
			ylm = x0 - y;
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		if ((x0 - x) > ylm) {
			if (cornername & 0x1)
				_drawFastVLine(x0 + x, y0 - y, 2 * y + 1 + delta, color);
			if (cornername & 0x2)
				_drawFastVLine(x0 - x, y0 - y, 2 * y + 1 + delta, color);
		}
	}
}

/*Draw filled circle at X,Y location with specified radius and colour. X and Y represent circles center */
void ILI9488_DRV::ILI9341_Draw_Filled_Circle(uint16_t X, uint16_t Y,
		uint16_t Radius, color_t color) {

	int x = Radius;
	int y = 0;
	int xChange = 1 - (Radius << 1);
	int yChange = 0;
	int radiusError = 0;

	while (x >= y) {
		for (int i = X - x; i <= X + x; i++) {
			ILI9341_Draw_Pixel(i, Y + y, color);
			ILI9341_Draw_Pixel(i, Y - y, color);
		}
		for (int i = X - y; i <= X + y; i++) {
			ILI9341_Draw_Pixel(i, Y + x, color);
			ILI9341_Draw_Pixel(i, Y - x, color);
		}

		y++;
		radiusError += yChange;
		yChange += 2;
		if (((radiusError << 1) + xChange) > 0) {
			x--;
			radiusError += xChange;
			xChange += 2;
		}
	}
	//Really slow implementation, will require future overhaul
	//TODO:	https://stackoverflow.com/questions/1201200/fast-algorithm-for-drawing-filled-circles
}

